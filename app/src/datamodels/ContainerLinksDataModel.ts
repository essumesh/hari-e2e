import { BaseDataModel } from './BaseDataModel';
import { LinkMediaDataModel } from './LinkMediaDataModel';

export class ContainerLinksDataModel extends BaseDataModel {
    assign?: LinkMediaDataModel;

    unassign?: LinkMediaDataModel;

    relocate?: LinkMediaDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {

            let assignJSON = _data['assign'];
            if (assignJSON != null && assignJSON.constructor === {}.constructor) {
                this.assign = new LinkMediaDataModel();
                this.assign.parse(assignJSON);
            }

            let unassignJSON = _data['unassign'];
            if (unassignJSON != null && unassignJSON.constructor === {}.constructor) {
                this.unassign = new LinkMediaDataModel();
                this.unassign.parse(unassignJSON);
            }

            let relocateJSON = _data['relocate'];
            if (relocateJSON != null && relocateJSON.constructor === {}.constructor) {
                this.relocate = new LinkMediaDataModel();
                this.relocate.parse(relocateJSON);
            }
        }

    }

    toJSON(): any {
        let jsonData = super.toJSON();

        if (this.assign != null)
            jsonData['assign'] = this.assign.toJSON();
        if (this.unassign != null)
            jsonData['unassign'] = this.unassign.toJSON();
        if (this.relocate != null)
            jsonData['relocate'] = this.relocate.toJSON();

        return jsonData;
    }
}
