import { DateTime } from 'ionic-angular';
import { PickupDriverInfoDataModel } from './PickupDriverInfoDataModel';
import { PickUpAirwaybillDataModel } from './PickUpAirwaybillDataModel';
import { BaseDataModel } from './BaseDataModel';
export class PickUpDeliveryDataModel extends BaseDataModel {
    airwaybillArray: Array<PickUpAirwaybillDataModel> = [];
    pickupDriverInfo: PickupDriverInfoDataModel;
    pickupType: string;
    receiptOrTransferNumber: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
           this.pickupDriverInfo = _data['pickupDriverInfo'];
           this.pickupType = _data['pickupType'];
           this.receiptOrTransferNumber = _data['receiptOrTransferNumber'];
           let deliveryArray = _data['airwaybill'];
           if (deliveryArray != null && deliveryArray instanceof Array) {
                for (let arrayItem of deliveryArray) {
                    let airwaybillDataModel = new PickUpAirwaybillDataModel();
                    airwaybillDataModel.parse(arrayItem);
                    this.airwaybillArray.push(airwaybillDataModel);
                  }
            }
        }
    }
}
