import { BaseDataModel } from './BaseDataModel';

export class SpecialHandlingCodesDataModel extends BaseDataModel {
    id?: string;
    code?: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['code'];
            this.code = _data['code'];
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;
        jsonData['code'] = this.code;

        return jsonData;
    }
}
