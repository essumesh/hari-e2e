import { BaseDataModel } from './BaseDataModel';

export class ServerErrorResponseGlobalErrorsDataModel extends BaseDataModel {
    userMessage?: string;
    fields?: Array<string>;

    toJSON(): any {
        let jsonData = super.toJSON();
        if (this.userMessage != null)
            jsonData['userMessage'] = this.userMessage;
        if (this.fields != null)
            jsonData['fields'] = JSON.parse(JSON.stringify(this.fields));
        return jsonData;
    }

    fromJSON(json: any) {
        if (json.userMessage != null)
            this.userMessage = json.userMessage;
        if (json.fields != null)
            this.fields = json.fields;
    }
}
