import { BaseDataModel } from './BaseDataModel';
import { PickUpDeliveryDataModel } from './PickUpDeliveryDataModel';

export class PickUpDeliveryReceiptDataModel extends BaseDataModel {
    deliveryReceipt: Array<PickUpDeliveryDataModel> = [];

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let _embeddedJSON = _data['_embedded'];
            if (_embeddedJSON != null && _embeddedJSON.constructor === {}.constructor) {
                let deliveryReciptArray = _embeddedJSON['delivery'];
                // console.log(deliveryReciptArray);
                if (deliveryReciptArray != null && deliveryReciptArray instanceof Array) {
                    for (let arrayItem of deliveryReciptArray) {
                        let pickUpDeliveryDataModel = new PickUpDeliveryDataModel();
                        pickUpDeliveryDataModel.parse(arrayItem);
                        this.deliveryReceipt.push(pickUpDeliveryDataModel);
                      }
                }
            }
        }
    }
}
