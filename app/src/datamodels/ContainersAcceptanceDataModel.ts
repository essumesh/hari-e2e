import { BaseDataModel } from './BaseDataModel';
import { TransportMeansDataModel } from './TransportMeansDataModel';
import { LocationsDataModel } from './LocationsDataModel';

export class ContainersAcceptanceDataModel extends BaseDataModel {
    transportMeans?: TransportMeansDataModel;

    plannedDestination?: LocationsDataModel;

    shipperLoaded?: boolean;

    /**
     * unique id for this system-users
     */
    performedById?: string;

    /**
     * unique id for this parts
     */
    partId?: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {

            let transportMeansJSON = _data['transportMeans'];
            if (transportMeansJSON != null && transportMeansJSON.constructor === {}.constructor) {
                this.transportMeans = new TransportMeansDataModel();
                this.transportMeans.parse(transportMeansJSON);
            }

            let plannedDestinationJSON = _data['plannedDestination'];
            if (plannedDestinationJSON != null && plannedDestinationJSON.constructor === {}.constructor) {
                this.plannedDestination = new LocationsDataModel();
                this.plannedDestination.parse(plannedDestinationJSON);
            }

            this.shipperLoaded = _data['shipperLoaded'];
            this.performedById = _data['performedById'];
            this.partId = _data['partId'];
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        if (this.transportMeans != null)
            jsonData['transportMeans'] = this.transportMeans.toJSON();

        if (this.plannedDestination != null)
            jsonData['plannedDestination'] = this.plannedDestination.toJSON();

        jsonData['shipperLoaded'] = this.shipperLoaded;
        jsonData['performedById'] = this.performedById;
        jsonData['partId'] = this.partId;

        return jsonData;
    }
}
