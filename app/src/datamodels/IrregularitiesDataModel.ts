
import { BaseDataModel } from './BaseDataModel';
import { AmountUnitDataModel } from './AmountUnitDataModel';
import { LinksDataModel } from './LinksDataModel';
import { LookupDataModel } from './LookupDataModel';
import { AirwaybillsDataModel } from './AirwaybillsDataModel';
import { HouseAirwaybillsDataModel } from './HouseAirwaybillsDataModel';
import { CarriersDataModel } from './CarriersDataModel';
import { LocationsDataModel } from './LocationsDataModel';
import { SystemUsersDataModel } from './SystemUsersDataModel';
import { DateParserHelper } from '../helpers/DateParserHelper';

export class IrregularitiesDataModel extends BaseDataModel {
    /**
     * unique id for this irregularities
     */
    id?: string;
    pieces?: number;
    weight?: AmountUnitDataModel;
    airwaybill?: AirwaybillsDataModel;
    houseAirwaybill?: HouseAirwaybillsDataModel;
    irregularityDetails?: CarriersDataModel;
    reportingLocation?: LocationsDataModel;
    station?: string;
    createdBy?: SystemUsersDataModel;
    createdOn?: Date;
    modifiedOn?: Date;
    modifiedBy?: SystemUsersDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];
            this.pieces = _data['pieces'];
            this.station = _data['station'];
            let weightJSON = _data['weight'];
            if (weightJSON != null && weightJSON.constructor === {}.constructor) {
                this.weight = new AmountUnitDataModel();
                this.weight.parse(weightJSON);
            }
            let irregularityDetailsJSON = _data['irregularityDetails'];
            if (irregularityDetailsJSON != null && irregularityDetailsJSON.constructor === {}.constructor) {
                this.irregularityDetails = new CarriersDataModel();
                this.irregularityDetails.parse(irregularityDetailsJSON);
            }
            let airwaybillJSON = _data['airwaybill'];
            if (airwaybillJSON != null && airwaybillJSON.constructor === {}.constructor) {
                this.airwaybill = new AirwaybillsDataModel();
                this.airwaybill.parse(airwaybillJSON);
            }

            let houseAirwaybillJSON = _data['houseAirwaybill'];
            if (houseAirwaybillJSON != null && houseAirwaybillJSON.constructor === {}.constructor) {
                this.houseAirwaybill = new HouseAirwaybillsDataModel();
                this.houseAirwaybill.parse(houseAirwaybillJSON);
            }
            let reportingLocationJSON = _data['reportingLocation'];
            if (reportingLocationJSON != null && reportingLocationJSON.constructor === {}.constructor) {
                this.reportingLocation = new LocationsDataModel();
                this.reportingLocation.parse(reportingLocationJSON);
            }
            this.createdOn = DateParserHelper.parseStringToDate(_data['createdOn']);
            this.modifiedOn = DateParserHelper.parseStringToDate(_data['modifiedOn']);
            let createdByText = _data['createdBy'];
            if (createdByText != null && createdByText.constructor === ''.constructor) {
                this.createdBy = new SystemUsersDataModel();
                this.createdBy.fullName = createdByText;
            }

            this.modifiedOn = DateParserHelper.parseStringToDate(_data['modifiedOn']);

            let modifiedByJSON = _data['modifiedBy'];
            if (modifiedByJSON != null && modifiedByJSON.constructor === {}.constructor) {
                this.modifiedBy = new SystemUsersDataModel();
                this.modifiedBy.parse(modifiedByJSON);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;
        jsonData['pieces'] = this.pieces;
        jsonData['station'] = this.station;
        if (this.weight != null)
        jsonData['weight'] = this.weight.toJSON();
        if (this.irregularityDetails != null)
        jsonData['irregularityDetails'] = this.irregularityDetails.toJSON();
        if (this.airwaybill != null)
        jsonData['airwaybill'] = this.airwaybill.toJSON();
        if (this.houseAirwaybill != null)
        jsonData['houseAirwaybill'] = this.houseAirwaybill.toJSON();
        if (this.reportingLocation != null)
        jsonData['reportingLocation'] = this.reportingLocation.toJSON();
        if (this.createdBy != null)
        jsonData['createdBy'] = this.createdBy.toJSON();
        if (this.modifiedOn != null)
        jsonData['modifiedOn'] = DateParserHelper.parseDateToString(this.modifiedOn);
        if (this.createdOn != null)
        jsonData['createdOn'] = DateParserHelper.parseDateToString(this.createdOn);
        if (this.modifiedBy != null)
        jsonData['modifiedBy'] = this.modifiedBy.toJSON();
        return jsonData;
    }

}
