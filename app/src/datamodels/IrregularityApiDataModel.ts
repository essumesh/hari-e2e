import { BaseDataModel } from './BaseDataModel';
import { AmountUnitDataModel } from './AmountUnitDataModel';
import { LocationsDataModel } from './LocationsDataModel';

export class IrregularityApiDataModel extends BaseDataModel {
     pieces: number;
     airwaybillId: string;
     irregularityCode: string;
     weight: AmountUnitDataModel;
     reportingLocation: LocationsDataModel;

     parse(_data: any) {
        this.pieces = _data['pieces'];
        this.airwaybillId = _data['airwaybillId'];
        this.irregularityCode = _data['irregularityCode'];
        let reportingLocationJSON = _data['reportingLocation'];
        if (reportingLocationJSON != null && reportingLocationJSON.constructor === {}.constructor) {
            this.reportingLocation = new LocationsDataModel();
            this.reportingLocation.parse(reportingLocationJSON);
        }
        let weightJSON = _data['weight'];
        if (weightJSON != null && weightJSON.constructor === {}.constructor) {
            this.weight = new AmountUnitDataModel();
            this.weight.parse(weightJSON);
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        delete jsonData['uniqueObjectId'];

        jsonData['pieces'] = this.pieces;
        jsonData['airwaybillId'] = this.airwaybillId;
        jsonData['irregularityCode'] = this.irregularityCode;
        if (this.reportingLocation != null)
            jsonData['reportingLocation'] = this.reportingLocation.toJSON();
        if (this.weight != null)
            jsonData['weight'] = this.weight.toJSON();

        return jsonData;
    }
}
