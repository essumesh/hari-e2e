import { BaseDataModel } from './BaseDataModel';
import { AmountUnitDataModel } from './AmountUnitDataModel';
import { LogisticsPackagesDataModel } from './LogisticsPackagesDataModel';
import { SpecialHandlingCodesDataModel } from './SpecialHandlingCodesDataModel';
import { HandlingInformationDataModel } from './HandlingInformationDataModel';
import { WarehouseLocationDataModel } from './WarehouseLocationDataModel';
import { SecurityScreeningsDataModel } from './SecurityScreeningsDataModel';

export class PartsAcceptanceDataModel extends BaseDataModel {
    acceptedPieces?: number;

    acceptedWeight?: AmountUnitDataModel;

    acceptedVolume?: AmountUnitDataModel;

    logisticsPackages?: Array<LogisticsPackagesDataModel>;

    specialHandling?: Array<SpecialHandlingCodesDataModel>;

    xRay?: boolean;

    euPalletCount?: number;

    handlingInformation?: Array<HandlingInformationDataModel>;

    storageLocation?: WarehouseLocationDataModel;

    securityStatus?: boolean;

    securityScreenings?: Array<SecurityScreeningsDataModel>;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {

            this.acceptedPieces = _data['acceptedPieces'];

            let acceptedWeightJSON = _data['acceptedWeight'];
            if (acceptedWeightJSON != null && acceptedWeightJSON.constructor === {}.constructor) {
                this.acceptedWeight = new AmountUnitDataModel();
                this.acceptedWeight.parse(acceptedWeightJSON);
            }

            let acceptedVolumeJSON = _data['acceptedVolume'];
            if (acceptedVolumeJSON != null && acceptedVolumeJSON.constructor === {}.constructor) {
                this.acceptedVolume = new AmountUnitDataModel();
                this.acceptedVolume.parse(acceptedVolumeJSON);
            }

            let logisticsPackagesArray = _data['logisticsPackages'];
            if (logisticsPackagesArray != null && logisticsPackagesArray instanceof Array) {
                for (let arrayItem of logisticsPackagesArray) {
                    let logisticsPackagesDataModel = new LogisticsPackagesDataModel();
                    logisticsPackagesDataModel.parse(arrayItem);
                    this.logisticsPackages.push(logisticsPackagesDataModel);
                  }
            }

            let specialHandlingArray = _data['specialHandling'];
            if (specialHandlingArray != null && specialHandlingArray instanceof Array) {
                for (let arrayItem of specialHandlingArray) {
                    let specialHandlingCodesDataModel = new SpecialHandlingCodesDataModel();
                    specialHandlingCodesDataModel.parse(arrayItem);
                    this.specialHandling.push(specialHandlingCodesDataModel);
                  }
            }

            this.xRay = _data['xRay'];
            this.euPalletCount = _data['euPalletCount'];

            let handlingInformationArray = _data['handlingInformation'];
            if (handlingInformationArray != null && handlingInformationArray instanceof Array) {
                for (let arrayItem of handlingInformationArray) {
                    let handlingInformationDataModel = new HandlingInformationDataModel();
                    handlingInformationDataModel.parse(arrayItem);
                    this.handlingInformation.push(handlingInformationDataModel);
                  }
            }

            let storageLocationJSON = _data['storageLocation'];
            if (storageLocationJSON != null && storageLocationJSON.constructor === {}.constructor) {
                this.storageLocation = new WarehouseLocationDataModel();
                this.storageLocation.parse(storageLocationJSON);
            }

            this.securityStatus = _data['securityStatus'];

            let securityScreeningsArray = _data['securityScreenings'];
            if (securityScreeningsArray != null && securityScreeningsArray instanceof Array) {
                for (let arrayItem of securityScreeningsArray) {
                    let securityScreeningsDataModel = new SecurityScreeningsDataModel();
                    securityScreeningsDataModel.parse(arrayItem);
                    this.securityScreenings.push(securityScreeningsDataModel);
                  }
            }

        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['acceptedPieces'] = this.acceptedPieces;

        if (this.acceptedWeight != null)
            jsonData['acceptedWeight'] = this.acceptedWeight.toJSON();

        if (this.acceptedVolume != null)
            jsonData['acceptedVolume'] = this.acceptedVolume.toJSON();

        if (this.logisticsPackages != null)
            jsonData['logisticsPackages'] = JSON.parse(JSON.stringify(this.logisticsPackages));

        if (this.specialHandling != null)
            jsonData['specialHandling'] = JSON.parse(JSON.stringify(this.specialHandling));

        jsonData['xRay'] = this.xRay;
        jsonData['euPalletCount'] = this.euPalletCount;

        if (this.handlingInformation != null)
            jsonData['handlingInformation'] = JSON.parse(JSON.stringify(this.handlingInformation));

        if (this.storageLocation != null)
            jsonData['storageLocation'] = this.storageLocation.toJSON();

        jsonData['securityStatus'] = this.securityStatus;

        if (this.securityScreenings != null)
            jsonData['securityScreenings'] = JSON.parse(JSON.stringify(this.securityScreenings));

        return jsonData;
    }
}
