import { BaseDataModel } from './BaseDataModel';
import { LookupDataModel } from './LookupDataModel';
import { SystemUsersDataModel } from './SystemUsersDataModel';

export class SecurityScreeningsDataModel extends BaseDataModel {
    /**
     * unique id for this security-screenings
     */
    id?: string;

    type?: LookupDataModel;

    screenedOn?: string;

    createdOn?: string;

    createdBy?: SystemUsersDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];

            let typeJSON = _data['type'];
            if (typeJSON != null && typeJSON.constructor === {}.constructor) {
                this.type = new LookupDataModel();
                this.type.parse(typeJSON);
            }

            this.screenedOn = _data['screenedOn'];
            this.createdOn = _data['createdOn'];

            let createdByJSON = _data['createdBy'];
            if (createdByJSON != null && createdByJSON.constructor === {}.constructor) {
                this.createdBy = new SystemUsersDataModel();
                this.createdBy.parse(createdByJSON);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;

        if (this.type != null)
            jsonData['type'] = this.type.toJSON();

        jsonData['screenedOn'] = this.screenedOn;
        jsonData['createdOn'] = this.createdOn;

        if (this.createdBy != null)
            jsonData['createdBy'] = this.createdBy.toJSON();

        return jsonData;
    }
}
