import { BaseDataModel } from './BaseDataModel';

export class WarehouseLocation {
    id: string;
    location: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];
            this.location = _data['location'];

        }
    }
}

export class PieceLocation {
    deliveryPieces: string;
    warehouseLocation: WarehouseLocation ;

    parse(_data: any) {
      if (_data != null && _data.constructor === {}.constructor) {
          this.deliveryPieces = _data['deliveryPieces'];
          this.warehouseLocation = _data['warehouseLocation'];
      }
    }
}

export class PickUPHouseWayBillDataModel extends BaseDataModel {
    houseAirwaybillNumber: string;
    pieceLocations: Array<PieceLocation>;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.houseAirwaybillNumber = _data['houseAirwaybillNumber'];
            let pieceLocationsArray = _data['pieceLocations'];
            if (pieceLocationsArray != null && pieceLocationsArray instanceof Array) {
                for (let arrayItem of pieceLocationsArray) {
                    let pieceLocationDataModel = new PieceLocation();
                    pieceLocationDataModel.parse(arrayItem);
                    this.pieceLocations.push(pieceLocationDataModel);
                }
            }
        }
    }
}
