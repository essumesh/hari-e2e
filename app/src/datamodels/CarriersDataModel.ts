import { BaseDataModel } from './BaseDataModel';

export class CarriersDataModel extends BaseDataModel {
    code: string;
    name: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.code = _data['code'];
            this.name = _data['name'];
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['code'] = this.code;
        jsonData['name'] = this.name;

        return jsonData;
    }
}
