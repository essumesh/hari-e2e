import { BaseDataModel } from './BaseDataModel';
import { LocationsDataModel } from './LocationsDataModel';

export class WarehousesDataModel extends BaseDataModel {
    /**
     * unique id for this warehouses
     */
    id?: string;

    /**
     * name of the warehouse
     */
    warehouse?: string;

    station?: LocationsDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];
            this.warehouse = _data['warehouse'];

            let stationJSON = _data['station'];
            if (stationJSON != null && stationJSON.constructor === {}.constructor) {
                this.station = new LocationsDataModel();
                this.station.parse(stationJSON);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;
        jsonData['warehouse'] = this.warehouse;

        if (this.station != null)
        jsonData['station'] = this.station.toJSON();

        return jsonData;
    }
}
