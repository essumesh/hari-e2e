import { BaseDataModel } from './BaseDataModel';
import { DateTime } from 'ionic-angular';
import { FoundCargoStorageLocationDataModel } from './FoundCargoStorageLocationDataModel';
import { FoundCargoPreferedLocationDataModel } from './FoundCargoPreferedLocationDataModel';
import { AirwaybillsDataModel } from './AirwaybillsDataModel';
import { HouseAirwaybillsDataModel } from './HouseAirwaybillsDataModel';
import { TransportMeansDataModel } from './TransportMeansDataModel';
import { AmountUnitDataModel } from './AmountUnitDataModel';
import { FetchOriginItemDataModel } from './FetchOriginItemDataModel';

export class FoundCargoDataModel extends BaseDataModel {
    storageLocation?: Array<FoundCargoStorageLocationDataModel> = [];
    preferedLocation?: string;
    origin?: FetchOriginItemDataModel;
    destination?: string;
    reportingShipmentType?: string;
    airwaybill?: AirwaybillsDataModel ;
    houseAirwaybill?: HouseAirwaybillsDataModel ;
    transportMeans?: TransportMeansDataModel;
    uld?: string ;
    manifestedPieces?: string;
    manifestedWeight?: AmountUnitDataModel ;
    foundPieces?: string;
    foundWeight?: AmountUnitDataModel ;
    natureOfGoods?: string ;
    shc?: Array<string> = [] ;
    reportedOn?: string ;
    reportingLocation?: FetchOriginItemDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let originJSON = _data['origin'];
            if (originJSON != null && originJSON.constructor === ''.constructor) {
                this.origin = new FetchOriginItemDataModel();
                this.origin.parse(originJSON);
            }

            let reportingLocationJSON = _data['reportingLocation'];
            if (reportingLocationJSON != null && reportingLocationJSON.constructor === ''.constructor) {
                this.reportingLocation = new FetchOriginItemDataModel();
                this.reportingLocation.parse(reportingLocationJSON);
            }
            this.destination = _data['destination'];
            this.reportingShipmentType = _data['reportingShipmentType'];
            this.uld = _data['uld'];
            this.manifestedPieces = _data['manifestedPieces'];
            this.foundPieces = _data['foundPieces'];
            this.natureOfGoods = _data['natureOfGoods'];
            this.reportedOn = _data['reportedOn'];
            this.preferedLocation = _data['preferedLocation'];

            let storageLocationArray = _data['storageLocation'];
            if (storageLocationArray != null && storageLocationArray instanceof Array) {
                for (let arrayItem of storageLocationArray) {
                    let foundCargoStorageLocationDataModel = new FoundCargoStorageLocationDataModel();
                    foundCargoStorageLocationDataModel.parse(arrayItem);
                    this.storageLocation.push(foundCargoStorageLocationDataModel);
                  }
            }
            let shcArray = _data['shc'];
            if (shcArray != null && shcArray instanceof Array) {
                for (let arrayItem of shcArray) {
                    this.shc.push(arrayItem);
                  }
            }

            let airwaybillText = _data['airwaybill'];
            if (airwaybillText != null && airwaybillText.constructor === ''.constructor) {
                this.airwaybill = new AirwaybillsDataModel();
                this.airwaybill = airwaybillText;
            }
            let houseAirwaybillText = _data['houseAirwaybill'];
            if (houseAirwaybillText != null && houseAirwaybillText.constructor === ''.constructor) {
                this.houseAirwaybill = new HouseAirwaybillsDataModel();
                this.houseAirwaybill = houseAirwaybillText;
            }
            let transportMeansText = _data['transportMeans'];
            if (transportMeansText != null && transportMeansText.constructor === ''.constructor) {
                this.transportMeans = new TransportMeansDataModel();
                this.transportMeans = transportMeansText;
            }
            let manifestedWeightText = _data['manifestedWeight'];
            if (manifestedWeightText != null && manifestedWeightText.constructor === ''.constructor) {
                this.manifestedWeight = new AmountUnitDataModel();
                this.manifestedWeight = manifestedWeightText;
            }
            let foundWeightText = _data['foundWeight'];
            if (foundWeightText != null && foundWeightText.constructor === ''.constructor) {
                this.foundWeight = new AmountUnitDataModel();
                this.foundWeight = foundWeightText;
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        if (this.origin != null)
        jsonData['origin'] = this.origin.toJSON();
        jsonData['reportingLocation'] = this.reportingLocation.toJSON();
        jsonData['destination'] = this.destination;
        jsonData['reportingShipmentType'] = this.reportingShipmentType;
        jsonData['uld'] = this.uld;
        jsonData['manifestedPieces'] = this.manifestedPieces;
        jsonData['foundPieces'] = this.foundPieces;
        jsonData['natureOfGoods'] = this.natureOfGoods;
        jsonData['reportedOn'] = this.reportedOn;
        jsonData['preferedLocation'] = this.preferedLocation;

        if (this.storageLocation != null)
        jsonData['storageLocation'] = JSON.parse(JSON.stringify(this.storageLocation));
        if (this.airwaybill != null)
        jsonData['airwaybill'] = this.airwaybill.toJSON();
        if (this.houseAirwaybill != null)
        jsonData['houseAirwaybill'] = this.houseAirwaybill.toJSON();
        if (this.transportMeans != null)
        jsonData['transportMeans'] = this.transportMeans.toJSON();
        if (this.manifestedWeight != null)
        jsonData['manifestedWeight'] = this.manifestedWeight.toJSON();
        if (this.foundWeight != null)
        jsonData['foundWeight'] = this.foundWeight.toJSON();
        if (this.shc != null)
        jsonData['shc'] = JSON.parse(JSON.stringify(this.shc));
        return jsonData;
    }
}
