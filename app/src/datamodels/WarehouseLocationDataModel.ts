import { BaseDataModel } from './BaseDataModel';
import { WarehousesDataModel } from './WarehousesDataModel';

export class WarehouseLocationDataModel extends BaseDataModel {
    /**
     * unique id for this warehouses
     */
    id?: string;

    /**
     * name of the warehouse
     */
    warehouse?: WarehousesDataModel;

    location?: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];
            this.location = _data['location'];

            let warehouseJSON = _data['warehouse'];
            if (warehouseJSON != null && warehouseJSON.constructor === {}.constructor) {
                this.warehouse = new WarehousesDataModel();
                this.warehouse.parse(warehouseJSON);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;
        jsonData['location'] = this.location;

        if (this.warehouse != null)
            jsonData['warehouse'] = this.warehouse.toJSON();

        return jsonData;
    }
}
