
import { BaseDataModel } from './BaseDataModel';
import { CountriesDataModel } from './CountriesDataModel';

export class FetchOriginItemDataModel extends BaseDataModel {
     code?: string;
     name?: string;
     type?: string;
     // cityId?: string;
     country?: CountriesDataModel;

     parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.code = _data['code'];
            this.name = _data['name'];
            this.type = _data['type'];
           // this.cityId = _data['cityId'];
            let countryText = _data['country'];
            if (countryText != null && countryText.constructor === {}.constructor) {
                this.country = new CountriesDataModel();
                this.country.parse(countryText);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['code'] = this.code;
        jsonData['name'] = this.name;
        jsonData['type'] = this.type;
        // jsonData['cityId'] = this.cityId;
        if (this.country != null)
        jsonData['country'] = this.country.toJSON();
        return jsonData;
    }
}
