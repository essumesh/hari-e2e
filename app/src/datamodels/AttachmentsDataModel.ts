import { BaseDataModel } from './BaseDataModel';
import { LinkMediaDataModel } from './LinkMediaDataModel';

export class AttachmentsDataModel extends BaseDataModel {
    imageURLs?: Array<LinkMediaDataModel>;

    toJSON(): any {
        let jsonData = super.toJSON();

        if (this.imageURLs != null)
            jsonData['imageURLs'] = JSON.parse(JSON.stringify(this.imageURLs));

        return jsonData;
    }
}
