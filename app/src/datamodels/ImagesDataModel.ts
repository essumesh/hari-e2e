import { BaseDataModel } from './BaseDataModel';
import { LinkMediaDataModel } from './LinkMediaDataModel';

export class ImagesDataModel extends BaseDataModel {
    linkMediaDataModels: Array<LinkMediaDataModel> = [];

    doWeHaveAtLeastOneMediaGeneratedFromDevice(): boolean {
        for (let i = 0; i < this.linkMediaDataModels.length; i++) {
            if (!this.linkMediaDataModels[i].haveWeGotMediaDataFromServer())
                return true;
        }
        return false;
    }

    parse(_data: any) {
        if (_data != null && _data instanceof Array) {
            for (let arrayItem of _data) {
                let linkMediaDataModel = new LinkMediaDataModel();
                linkMediaDataModel.parse(arrayItem);
                this.linkMediaDataModels.push(linkMediaDataModel);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        if (this.linkMediaDataModels != null)
            jsonData = JSON.parse(JSON.stringify(this.linkMediaDataModels));

        return jsonData;
    }
}
