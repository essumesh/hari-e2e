
import { BaseDataModel } from './BaseDataModel';
import { PartsDataModel } from './PartsDataModel';

export class AirwaybillsEmbeddedDataModel extends BaseDataModel {

    parts?: Array<PartsDataModel>;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let partsArray = _data['parts'];
            if (partsArray != null && partsArray instanceof Array) {
                for (let arrayItem of partsArray) {
                    let partsModel = new PartsDataModel();
                    partsModel.parse(arrayItem);
                    this.parts.push(partsModel);
                  }
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        if (this.parts != null)
        jsonData['parts'] = JSON.parse(JSON.stringify(this.parts));

        return jsonData;
    }
}
