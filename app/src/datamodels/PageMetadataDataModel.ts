import { BaseDataModel } from './BaseDataModel';

export class PageMetadataDataModel extends BaseDataModel {
    number?: number;
    size?: number;
    totalElements?: number;
    totalPages?: number;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.number = _data['number'];
            this.size = _data['size'];
            this.totalElements = _data['totalElements'];
            this.totalPages = _data['totalPages'];
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['number'] = this.number;
        jsonData['size'] = this.size;
        jsonData['totalElements'] = this.totalElements;
        jsonData['totalPages'] = this.totalPages;

        return jsonData;
    }
}
