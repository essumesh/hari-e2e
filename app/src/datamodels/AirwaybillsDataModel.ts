import { BaseDataModel } from './BaseDataModel';

export class AirwaybillsDataModel extends BaseDataModel {
    /**
     * unique id for this airwaybills
     */
    id?: string;

    /**
     * AWB prefix
     */
    airwaybillPrefix?: string;

    /**
     * AWB number
     */
    airwaybillSerial?: string;
    pieces: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];
            this.airwaybillPrefix = _data['airwaybillPrefix'];
            this.airwaybillSerial = _data['airwaybillSerial'];
            this.pieces = _data['pieces'];
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;
        jsonData['airwaybillPrefix'] = this.airwaybillPrefix;
        jsonData['airwaybillSerial'] = this.airwaybillSerial;
        jsonData['pieces'] = this.pieces;

        return jsonData;
    }
}
