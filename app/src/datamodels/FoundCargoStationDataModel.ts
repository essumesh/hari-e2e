import { BaseDataModel } from './BaseDataModel';
import { FetchOriginItemDataModel } from './FetchOriginItemDataModel';

export class FoundCargoStationDataModel extends BaseDataModel {
    locations: Array<FetchOriginItemDataModel> = [];

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let locationArray = _data['locations'];
            if (locationArray != null && locationArray instanceof Array) {
                for (let arrayItem of locationArray) {
                    let fetchOriginItemDataModel = new FetchOriginItemDataModel();
                    fetchOriginItemDataModel.parse(arrayItem);
                    this.locations.push(fetchOriginItemDataModel);
                  }
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        if (this.locations != null)
        jsonData['locations'] = JSON.parse(JSON.stringify(this.locations));
    }
}
