import { BaseDataModel } from './BaseDataModel';
import { CarriersDataModel } from './CarriersDataModel';

export class IrregularityQualifierDataModel extends BaseDataModel {
    irregularityDetailList: Array<CarriersDataModel> = [];

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let _embeddedJSON = _data['_embedded'];
            if (_embeddedJSON != null && _embeddedJSON.constructor === {}.constructor) {
                let irregularityListArray = _embeddedJSON['irregularityDetailList'];
                // console.log(deliveryReciptArray);
                if (irregularityListArray != null && irregularityListArray instanceof Array) {
                    for (let arrayItem of irregularityListArray) {
                        let carriersDataModel = new CarriersDataModel();
                        carriersDataModel.parse(arrayItem);
                        this.irregularityDetailList.push(carriersDataModel);
                      }
                }
            }
        }
    }
}
