import { BaseDataModel } from './BaseDataModel';
import { LinksDataModel } from './LinksDataModel';

export class SystemUsersDataModel extends BaseDataModel {
    /**
     * unique id for this system-users
     */
    id?: string;

    /**
     * login username for this user
     */
    username?: string;

    /**
     * full name of this user calculated from first and last
     */
    fullName?: string;

    linksDataModel?: LinksDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];
            this.username = _data['username'];
            this.fullName = _data['fullName'];

            let linksJSON = _data['_links'];
            if (linksJSON != null && linksJSON.constructor === {}.constructor) {
                this.linksDataModel = new LinksDataModel();
                this.linksDataModel.parse(linksJSON);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;
        jsonData['username'] = this.username;
        jsonData['fullName'] = this.fullName;
        if (this.linksDataModel != null)
            jsonData['_links'] = JSON.stringify(this.linksDataModel);

        return jsonData;
    }
}
