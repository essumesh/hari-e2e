import { BaseDataModel } from './BaseDataModel';
import { PickUpPiecesLocationDataModel } from './PickUpPiecesLocationDataModel';
import { PickUPHouseWayBillDataModel } from './PickUPHouseWayBillDataModel';

export class PickUpAirwaybillDataModel extends BaseDataModel {
    id: string ;
    airwaybillPrefix: string;
    airwaybillSerial: string;
    pieces: string;
    previouslyCollectedPieces: string;
    pieceLocations: Array <PickUpPiecesLocationDataModel>= [];
    houseAirwaybill: PickUPHouseWayBillDataModel;
    isChecked: boolean;

    parse(_data: any) {
       if (_data != null && _data.constructor === {}.constructor) {
           this.id = _data['id'];
           this.airwaybillPrefix = _data['airwaybillPrefix'];
           this.airwaybillSerial = _data['airwaybillSerial'];
           this.pieces = _data['pieces'];
           this.previouslyCollectedPieces = _data['previouslyCollectedPieces'];
           this.houseAirwaybill = _data['houseAirwaybill'];
           this.isChecked = _data['isChecked'];

           let pieceLocationsArray = _data['pieceLocations'];
           if (pieceLocationsArray != null && pieceLocationsArray instanceof Array) {
               for (let arrayItem of pieceLocationsArray) {
                   let pieceLocationDataModel = new PickUpPiecesLocationDataModel();
                   pieceLocationDataModel.parse(arrayItem);
                   this.pieceLocations.push(pieceLocationDataModel);
                 }
           }
       }
   }
}
