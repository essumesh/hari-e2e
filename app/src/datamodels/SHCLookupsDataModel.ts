import { BaseDataModel } from './BaseDataModel';
import { SHCLookupDataModel } from './SHCLookupDataModel';

export class SHCLookupsDataModel extends BaseDataModel {
    shcType: Array<SHCLookupDataModel> = [];

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let shcTypeArray = _data['shcType'];
            if (shcTypeArray != null && shcTypeArray instanceof Array) {
                for (let arrayItem of shcTypeArray) {
                    let shcLookupDataModel = new SHCLookupDataModel();
                    shcLookupDataModel.parse(arrayItem);
                    this.shcType.push(shcLookupDataModel);
                  }
            }
        }
    }
    toJSON(): any {
        let jsonData = super.toJSON();

        if (this.shcType != null)
            jsonData['shcType'] = JSON.parse(JSON.stringify(this.shcType));

        return jsonData;
    }

    contains(_shcLookupDataModel: SHCLookupDataModel): boolean {
        let doesItContain: boolean = false;
        for (let _shcLookupDataModelTemp of this.shcType) {
            if (_shcLookupDataModelTemp.code === _shcLookupDataModel.code) {
                doesItContain = true;
                break;
            }
        }
        return doesItContain;
    }

    addIfNotPresent(_shcLookupDataModel: SHCLookupDataModel) {
        let toInclude: boolean = true;
        for (let _shcLookupDataModelTemp of this.shcType) {
            if (_shcLookupDataModelTemp.code === _shcLookupDataModel.code) {
                toInclude = false;
                break;
            }
        }
        if (toInclude) {
            this.shcType.push(_shcLookupDataModel);
        }
    }

    removeIfPresent(_shcLookupDataModel: SHCLookupDataModel) {
        let toRemove: boolean = false;
        for (let _shcLookupDataModelTemp of this.shcType) {
            if (_shcLookupDataModelTemp.code === _shcLookupDataModel.code) {
                toRemove = true;
                break;
            }
        }
        if (toRemove) {
            this.shcType.splice(this.shcType.indexOf(_shcLookupDataModel), 1);
        }
    }
}
