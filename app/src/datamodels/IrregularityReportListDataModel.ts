import { BaseDataModel } from './BaseDataModel';
import { IrregularitiesDataModel } from './IrregularitiesDataModel';

export class IrregularityReportListDataModel extends BaseDataModel {
    irregularityList: Array<IrregularitiesDataModel> = [];

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let _embeddedJSON = _data['_embedded'];
            if (_embeddedJSON != null && _embeddedJSON.constructor === {}.constructor) {
                let irregularityGetListArray = _embeddedJSON['irregularityGetList'];
                // console.log(deliveryReciptArray);
                if (irregularityGetListArray != null && irregularityGetListArray instanceof Array) {
                    for (let arrayItem of irregularityGetListArray) {
                        let irregularitiesDataModel = new IrregularitiesDataModel();
                        irregularitiesDataModel.parse(arrayItem);
                        this.irregularityList.push(irregularitiesDataModel);
                      }
                }
            }
        }
    }
}
