import { BaseDataModel } from './BaseDataModel';
import { SystemUsersDataModel } from './SystemUsersDataModel';
import { AmountUnitDataModel } from './AmountUnitDataModel';

export class LogisticsPackagesDataModel extends BaseDataModel {
    /**
     * unique id for this logistics-packages
     */
    id?: string;

    pieces?: number;

    height?: number;

    width?: number;

    length?: number;

    unit?: string;

    volume?: AmountUnitDataModel;

    createdOn?: string;

    createdBy?: SystemUsersDataModel;

    modifiedOn?: string;

    modifiedBy?: SystemUsersDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];
            this.pieces = _data['pieces'];
            this.height = _data['height'];
            this.width = _data['width'];
            this.length = _data['length'];
            this.unit = _data['unit'];

            let volumeJSON = _data['volume'];
            if (volumeJSON != null && volumeJSON.constructor === {}.constructor) {
                this.volume = new AmountUnitDataModel();
                this.volume.parse(volumeJSON);
            }

            this.createdOn = _data['createdOn'];

            let createdByJSON = _data['createdBy'];
            if (createdByJSON != null && createdByJSON.constructor === {}.constructor) {
                this.createdBy = new SystemUsersDataModel();
                this.createdBy.parse(createdByJSON);
            }

            this.modifiedOn = _data['modifiedOn'];

            let modifiedByJSON = _data['modifiedBy'];
            if (modifiedByJSON != null && modifiedByJSON.constructor === {}.constructor) {
                this.modifiedBy = new SystemUsersDataModel();
                this.modifiedBy.parse(modifiedByJSON);
            }

        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;
        jsonData['pieces'] = this.pieces;
        jsonData['height'] = this.height;
        jsonData['width'] = this.width;
        jsonData['length'] = this.length;
        jsonData['unit'] = this.unit;

        if (this.volume != null)
            jsonData['volume'] = this.volume.toJSON();

        jsonData['createdOn'] = this.createdOn;

        if (this.createdBy != null)
            jsonData['createdBy'] = this.createdBy.toJSON();

        jsonData['modifiedOn'] = this.modifiedOn;

        if (this.modifiedBy != null)
            jsonData['modifiedBy'] = this.modifiedBy.toJSON();

        return jsonData;
    }
}
