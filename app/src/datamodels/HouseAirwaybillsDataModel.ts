import { BaseDataModel } from './BaseDataModel';
import { AirwaybillsDataModel } from './AirwaybillsDataModel';
import { AmountUnitDataModel } from './AmountUnitDataModel';
import { HandlingInformationDataModel } from './HandlingInformationDataModel';
import { AirwaybillsEmbeddedDataModel } from './AirwaybillsEmbeddedDataModel';

export class HouseAirwaybillsDataModel extends BaseDataModel {
    /**
     * unique id for this house-airwaybills
     */
    id?: string;

    houseAirwaybillNumber?: string;

    masterAirwaybill?: AirwaybillsDataModel;
    pieces: string;

    bookedPieces?: number;

    bookedWeight?: AmountUnitDataModel;

    handlingInformation?: Array<HandlingInformationDataModel>;

    embedded?: AirwaybillsEmbeddedDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];
            this.houseAirwaybillNumber = _data['houseAirwaybillNumber'];

            this.masterAirwaybill = new AirwaybillsDataModel();
            this.masterAirwaybill.parse(_data['masterAirwaybill']);
            this.pieces = _data['pieces'];

            this.bookedPieces = _data['bookedPieces'];

            let bookedWeightJSON = _data['bookedWeight'];
            if (bookedWeightJSON != null && bookedWeightJSON.constructor === {}.constructor) {
                this.bookedWeight = new AmountUnitDataModel();
                this.bookedWeight.parse(bookedWeightJSON);
            }

            let handlingInformationArray = _data['handlingInformation'];
            if (handlingInformationArray != null && handlingInformationArray instanceof Array) {
                for (let arrayItem of handlingInformationArray) {
                    let handlingInformationDataModel = new HandlingInformationDataModel();
                    handlingInformationDataModel.parse(arrayItem);
                    this.handlingInformation.push(handlingInformationDataModel);
                  }
            }

            let embeddedJSON = _data['embedded'];
            if (embeddedJSON != null && embeddedJSON.constructor === {}.constructor) {
                this.embedded = new AirwaybillsEmbeddedDataModel();
                this.embedded.parse(embeddedJSON);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;
        jsonData['houseAirwaybillNumber'] = this.houseAirwaybillNumber;
        if (this.masterAirwaybill != null)
            jsonData['masterAirwaybill'] = this.masterAirwaybill.toJSON();
        jsonData['pieces'] = this.pieces;
        jsonData['bookedPieces'] = this.bookedPieces;
        if (this.bookedWeight != null)
        jsonData['bookedWeight'] = this.bookedWeight.toJSON();
        if (this.handlingInformation != null)
        jsonData['handlingInformation'] = JSON.parse(JSON.stringify(this.handlingInformation));
        if (this.embedded != null)
        jsonData['embedded'] = this.embedded.toJSON();

        return jsonData;
    }
}
