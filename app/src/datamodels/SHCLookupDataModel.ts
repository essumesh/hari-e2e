import { BaseDataModel } from './BaseDataModel';

export class SHCLookupDataModel extends BaseDataModel {
    id: string;
    code: string;
    name: string;
    carrierId: string;
    specialHandlingGroup: string;
    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];
            this.code = _data['code'];
            this.name = _data['name'];
            this.carrierId = _data['carrierId'];
            this.specialHandlingGroup = _data['specialHandlingGroup'];
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;
        jsonData['code'] = this.code;
        jsonData['name'] = this.name;
        jsonData['carrierId'] = this.carrierId;
        jsonData['specialHandlingGroup'] = this.specialHandlingGroup;
        return jsonData;
    }
}
