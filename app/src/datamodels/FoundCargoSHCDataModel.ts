import { BaseDataModel } from './BaseDataModel';
import { SHCLookupsDataModel } from './SHCLookupsDataModel';
import { SHCLookupDataModel } from './SHCLookupDataModel';

export class FoundCargoSHCDataModel extends BaseDataModel {
    specialHandling: SHCLookupsDataModel = new SHCLookupsDataModel();

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let specialHandlingArray = _data['specialHandling'];
            if (specialHandlingArray != null && specialHandlingArray instanceof Array) {
                for (let arrayItem of specialHandlingArray) {
                    let shcLookupDataModel = new SHCLookupDataModel();
                    shcLookupDataModel.parse(arrayItem);
                    this.specialHandling.addIfNotPresent(shcLookupDataModel);
                }
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        if (this.specialHandling != null)
        jsonData['specialHandling'] = JSON.parse(JSON.stringify(this.specialHandling.shcType));
        return jsonData;
    }
}
