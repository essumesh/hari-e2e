import { BaseDataModel } from './BaseDataModel';
import { LocationsDataModel } from './LocationsDataModel';
import { SystemUsersDataModel } from './SystemUsersDataModel';
import { ContainerLinksDataModel } from './ContainerLinksDataModel';
import { ContainersEmbeddedDataModel } from './ContainersEmbeddedDataModel';

export class ContainersDataModel extends BaseDataModel {
    /**
     * unique id for this containers
     */
    id?: string;

    type?: string;

    shipperLoadedContainer?: boolean;

    containerNumber?: string;

    containerDestination?: LocationsDataModel;

    status?: string;

    createdOn?: string;

    createdBy?: SystemUsersDataModel;

    modifiedOn?: string;

    modifiedBy?: SystemUsersDataModel;

    links?: ContainerLinksDataModel;

    embedded?: ContainersEmbeddedDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];
            this.type = _data['type'];
            this.shipperLoadedContainer = _data['shipperLoadedContainer'];
            this.containerNumber = _data['containerNumber'];

            let containerDestinationJSON = _data['containerDestination'];
            if (containerDestinationJSON != null && containerDestinationJSON.constructor === {}.constructor) {
                this.containerDestination = new LocationsDataModel();
                this.containerDestination.parse(containerDestinationJSON);
            }

            this.status = _data['status'];
            this.createdOn = _data['createdOn'];

            let createdByJSON = _data['createdBy'];
            if (createdByJSON != null && createdByJSON.constructor === {}.constructor) {
                this.createdBy = new SystemUsersDataModel();
                this.createdBy.parse(createdByJSON);
            }

            this.modifiedOn = _data['modifiedOn'];

            let modifiedByJSON = _data['modifiedBy'];
            if (modifiedByJSON != null && modifiedByJSON.constructor === {}.constructor) {
                this.modifiedBy = new SystemUsersDataModel();
                this.modifiedBy.parse(modifiedByJSON);
            }

            let linksJSON = _data['links'];
            if (linksJSON != null && linksJSON.constructor === {}.constructor) {
                this.links = new ContainerLinksDataModel();
                this.links.parse(linksJSON);
            }

            let embeddedJSON = _data['embedded'];
            if (embeddedJSON != null && embeddedJSON.constructor === {}.constructor) {
                this.embedded = new ContainersEmbeddedDataModel();
                this.embedded.parse(embeddedJSON);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;
        jsonData['type'] = this.type;
        jsonData['shipperLoadedContainer'] = this.shipperLoadedContainer;
        jsonData['containerNumber'] = this.containerNumber;

        if (this.containerDestination != null)
            jsonData['containerDestination'] = this.containerDestination.toJSON();

        jsonData['status'] = this.status;
        jsonData['createdOn'] = this.createdOn;

        if (this.createdBy != null)
            jsonData['createdBy'] = this.createdBy.toJSON();

        jsonData['modifiedOn'] = this.modifiedOn;

        if (this.modifiedBy != null)
            jsonData['modifiedBy'] = this.modifiedBy.toJSON();

        if (this.links != null)
            jsonData['links'] = this.links.toJSON();

        if (this.embedded != null)
            jsonData['embedded'] = this.embedded.toJSON();

        return jsonData;
    }
}
