import { BaseDataModel } from './BaseDataModel';
import { FileTransferCategory }
from '../network/requestobjects/FileTransferRequestObject';

export class LinkMediaDataModel extends BaseDataModel {
    href?: string;
    rel?: string;
    templated?: boolean;
    thumbnailBase64Data?: string;
    fileTransferCategory: FileTransferCategory = FileTransferCategory.IMAGE;
    fromUIhref?: string;

    haveWeGotMediaDataFromServer(): boolean {
        return this.thumbnailBase64Data != null;
    }

    fileNameWithExtension(): string {
        if (this.href != null) {
            const _index = this.href.lastIndexOf('/');
            if (_index !== -1) {
                return this.href.substring(_index + 1);
            }
        }
        return null;
    }

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.href = _data['href'];
            this.rel = _data['rel'];
            this.templated = _data['templated'];

            let thumbnailJSON = _data['thumbnail'];
            if (thumbnailJSON != null && thumbnailJSON.constructor === {}.constructor) {
                this.thumbnailBase64Data = thumbnailJSON['base64Content'];
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['href'] = this.href;
        jsonData['rel'] = this.rel;
        jsonData['templated'] = this.templated;
        jsonData['thumbnailBase64Data'] = this.thumbnailBase64Data;
        jsonData['fileTransferCategory'] = this.fileTransferCategory;
        jsonData['fromUIhref'] = this.fromUIhref;

        return jsonData;
    }
}
