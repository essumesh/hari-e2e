import { BaseDataModel } from './BaseDataModel';
import { LocationsDataModel } from './LocationsDataModel';
import { TransportMeansDataModel } from './TransportMeansDataModel';
import { SystemUsersDataModel } from './SystemUsersDataModel';

export class ItinerariesDataModel extends BaseDataModel {
    /**
     * unique id for this itineraries
     */
    id?: string;

    /**
     * segment number for this itinerary
     */
    segment?: number;

    origin?: LocationsDataModel;

    destination?: LocationsDataModel;

    transportMeans?: TransportMeansDataModel;

    createdOn?: string;

    createdBy?: SystemUsersDataModel;

    modifiedOn?: string;

    modifiedBy?: SystemUsersDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];
            this.segment = _data['segment'];

            let originJSON = _data['origin'];
            if (originJSON != null && originJSON.constructor === {}.constructor) {
                this.origin = new LocationsDataModel();
                this.origin.parse(originJSON);
            }

            let destinationJSON = _data['destination'];
            if (destinationJSON != null && destinationJSON.constructor === {}.constructor) {
                this.destination = new LocationsDataModel();
                this.destination.parse(destinationJSON);
            }

            let transportMeansJSON = _data['transportMeans'];
            if (transportMeansJSON != null && transportMeansJSON.constructor === {}.constructor) {
                this.transportMeans = new TransportMeansDataModel();
                this.transportMeans.parse(transportMeansJSON);
            }

            this.createdOn = _data['createdOn'];

            let createdByJSON = _data['createdBy'];
            if (createdByJSON != null && createdByJSON.constructor === {}.constructor) {
                this.createdBy = new SystemUsersDataModel();
                this.createdBy.parse(createdByJSON);
            }

            this.modifiedOn = _data['modifiedOn'];

            let modifiedByJSON = _data['modifiedBy'];
            if (modifiedByJSON != null && modifiedByJSON.constructor === {}.constructor) {
                this.modifiedBy = new SystemUsersDataModel();
                this.modifiedBy.parse(modifiedByJSON);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;
        jsonData['segment'] = this.segment;

        if (this.origin != null)
            jsonData['origin'] = this.origin.toJSON();

        if (this.destination != null)
            jsonData['destination'] = this.destination.toJSON();

        if (this.transportMeans != null)
            jsonData['transportMeans'] = this.transportMeans.toJSON();

        jsonData['createdOn'] = this.createdOn;

        if (this.createdBy != null)
            jsonData['createdBy'] = this.createdBy.toJSON();

        jsonData['modifiedOn'] = this.modifiedOn;

        if (this.modifiedBy != null)
            jsonData['modifiedBy'] = this.modifiedBy.toJSON();

        return jsonData;
    }
}
