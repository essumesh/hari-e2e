import { Component } from '@angular/core';
import { AppUIHelper } from '../helpers/AppUIHelper';

export class BaseDataModel {
    private uniqueObjectId: string = this.getUniqueObjectId();

    constructor() {
    }

    getUniqueObjectId(): string {
        if (this.uniqueObjectId == null) {
            this.uniqueObjectId = this.constructor.name + '_' + AppUIHelper.guid();
        }
        return this.uniqueObjectId;
    }

    parse(_data: any) {
    }

    toJSON(): any {
        let jsonData = {
        uniqueObjectId: this.uniqueObjectId,
        };
        return jsonData;
    }

    fromJSON(json: any) {
        this.uniqueObjectId = json.uniqueObjectId;
    }
}
