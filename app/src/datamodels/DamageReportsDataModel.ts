import { BaseDataModel } from './BaseDataModel';
import { LocationsDataModel } from './LocationsDataModel';
import { AirwaybillsDataModel } from './AirwaybillsDataModel';
import { HouseAirwaybillsDataModel } from './HouseAirwaybillsDataModel';
import { TransportMeansDataModel } from './TransportMeansDataModel';
import { AmountUnitDataModel } from './AmountUnitDataModel';
import { LookupsDataModel } from './LookupsDataModel';
import { ImagesDataModel } from './ImagesDataModel';
import { SystemUsersDataModel } from './SystemUsersDataModel';
import { DateParserHelper } from '../helpers/DateParserHelper';
import { AppUIHelper } from '../helpers/AppUIHelper';

export class DamageReportsDataModel extends BaseDataModel {
    reportId?: string;

    reportingLocation?: LocationsDataModel;

    /**
     * airwaybill or houseAirwaybill
     */
    reportingShipmentType?: string;

    airwaybill?: AirwaybillsDataModel;

    houseAirwaybill?: HouseAirwaybillsDataModel;

    transportMeans?: TransportMeansDataModel;

    uld?: string;

    pieces?: number;

    boxes?: string;

    weight?: AmountUnitDataModel;

    recuperage?: string;

    remarks?: string;

    reportedOn?: Date;

    outerPackagingType?: Array<LookupsDataModel> = [];

    outerPackagingMaterial?: Array<LookupsDataModel> = [];

    innerPackaging?: Array<LookupsDataModel> = [];

    damageToPackaging?: Array<LookupsDataModel> = [];

    conditionOfContents?: Array<LookupsDataModel> = [];

    damageDiscovery?: Array<LookupsDataModel> = [];

    damageDueTo?: Array<LookupsDataModel> = [];

    images?: ImagesDataModel;

    createdBy?: SystemUsersDataModel;

    /**
     * last date and time of modification
     */
    modifiedOn?: Date;

    modifiedBy?: SystemUsersDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.reportId = _data['reportId'];

            let reportingLocationJSON = _data['reportingLocation'];
            if (reportingLocationJSON != null && reportingLocationJSON.constructor === {}.constructor) {
                this.reportingLocation = new LocationsDataModel();
                this.reportingLocation.parse(reportingLocationJSON);
            }

            this.reportingShipmentType = _data['reportingShipmentType'];

            let airwaybillJSON = _data['airwaybill'];
            if (airwaybillJSON != null && airwaybillJSON.constructor === {}.constructor) {
                this.airwaybill = new AirwaybillsDataModel();
                this.airwaybill.parse(airwaybillJSON);
            }

            let houseAirwaybillJSON = _data['houseAirwaybill'];
            if (houseAirwaybillJSON != null && houseAirwaybillJSON.constructor === {}.constructor) {
                this.houseAirwaybill = new HouseAirwaybillsDataModel();
                this.houseAirwaybill.parse(houseAirwaybillJSON);
            }

            let transportMeansJSON = _data['transportMeans'];
            if (transportMeansJSON != null && transportMeansJSON.constructor === {}.constructor) {
                this.transportMeans = new TransportMeansDataModel();
                this.transportMeans.parse(transportMeansJSON);
            }

            this.uld = _data['uld'];
            this.pieces = _data['pieces'];
            this.boxes = _data['boxes'];

            let weightJSON = _data['weight'];
            if (weightJSON != null && weightJSON.constructor === {}.constructor) {
                this.weight = new AmountUnitDataModel();
                this.weight.parse(weightJSON);
            }

            this.recuperage = _data['recuperage'];
            this.remarks = _data['remarks'];
            this.reportedOn = DateParserHelper.parseStringToDate(_data['reportedOn']);

            let outerPackingTypeArray = _data['outerPackagingType'];
            if (outerPackingTypeArray != null && outerPackingTypeArray instanceof Array) {
                for (let arrayItem of outerPackingTypeArray) {
                    let lookupsDataModel = new LookupsDataModel();
                    lookupsDataModel.parse(arrayItem);
                    this.outerPackagingType.push(lookupsDataModel);
                  }
            }

            let outerPackagingMaterialArray = _data['outerPackagingMaterial'];
            if (outerPackagingMaterialArray != null && outerPackagingMaterialArray instanceof Array) {
                for (let arrayItem of outerPackagingMaterialArray) {
                    let lookupsDataModel = new LookupsDataModel();
                    lookupsDataModel.parse(arrayItem);
                    this.outerPackagingMaterial.push(lookupsDataModel);
                  }
            }

            let innerPackagingArray = _data['innerPackaging'];
            if (innerPackagingArray != null && innerPackagingArray instanceof Array) {
                for (let arrayItem of innerPackagingArray) {
                    let lookupsDataModel = new LookupsDataModel();
                    lookupsDataModel.parse(arrayItem);
                    this.innerPackaging.push(lookupsDataModel);
                  }
            }

            let damageToPackagingArray = _data['damageToPackaging'];
            if (damageToPackagingArray != null && damageToPackagingArray instanceof Array) {
                for (let arrayItem of damageToPackagingArray) {
                    let lookupsDataModel = new LookupsDataModel();
                    lookupsDataModel.parse(arrayItem);
                    this.damageToPackaging.push(lookupsDataModel);
                  }
            }

            let conditionOfContentsArray = _data['conditionOfContents'];
            if (conditionOfContentsArray != null && conditionOfContentsArray instanceof Array) {
                for (let arrayItem of conditionOfContentsArray) {
                    let lookupsDataModel = new LookupsDataModel();
                    lookupsDataModel.parse(arrayItem);
                    this.conditionOfContents.push(lookupsDataModel);
                  }
            }

            let damageDiscoveryArray = _data['damageDiscovery'];
            if (damageDiscoveryArray != null && damageDiscoveryArray instanceof Array) {
                for (let arrayItem of damageDiscoveryArray) {
                    let lookupsDataModel = new LookupsDataModel();
                    lookupsDataModel.parse(arrayItem);
                    this.damageDiscovery.push(lookupsDataModel);
                  }
            }

            let damageDueToArray = _data['damageDueTo'];
            if (damageDueToArray != null && damageDueToArray instanceof Array) {
                for (let arrayItem of damageDueToArray) {
                    let lookupsDataModel = new LookupsDataModel();
                    lookupsDataModel.parse(arrayItem);
                    this.damageDueTo.push(lookupsDataModel);
                  }
            }

            let imagesArray = _data['image'];
            if (imagesArray != null && imagesArray instanceof Array) {
                this.images = new ImagesDataModel();
                this.images.parse(imagesArray);
            }

            let createdByText = _data['createdBy'];
            if (createdByText != null && createdByText.constructor === ''.constructor) {
                this.createdBy = new SystemUsersDataModel();
                this.createdBy.fullName = createdByText;
            }

            this.modifiedOn = DateParserHelper.parseStringToDate(_data['modifiedOn']);

            let modifiedByJSON = _data['modifiedBy'];
            if (modifiedByJSON != null && modifiedByJSON.constructor === {}.constructor) {
                this.modifiedBy = new SystemUsersDataModel();
                this.modifiedBy.parse(modifiedByJSON);
            }
        }
    }

    doWeHaveAnyShipmentDetailsMandatoryDataLeftToBeFilledIn(): boolean {
        return this.doWeHaveShipmentDetailsMandatoryDataLeftToBeFilledIn_Condition1();
    }

    doWeHaveAllOfShipmentDetailsMandatoryDataLeftToBeFilledIn(): boolean {
        return this.doWeHaveShipmentDetailsMandatoryDataLeftToBeFilledIn_Condition1();
    }

    doWeHaveAnyShipmentDetailsLogicalDataErrors(): boolean {
        return this.doWeHaveAnyShipmentDetailsLogicalDataErrors_Pieces();
    }

    textOfShipmentDetailsMandatoryDataThatIsFilledIn(): string|undefined {
        let arrayOfTexts: Array<string> = new Array<string>();
        if (!this.doWeHaveShipmentDetailsMandatoryDataLeftToBeFilledIn_Condition1())
            arrayOfTexts.push(this.pieces + ' ' + AppUIHelper.getInstance().i18nText('pickuppieces'));
        return this.arrayOfTextsInCSVFormat(arrayOfTexts);
    }

    doWeHaveAnyPackingDetailsMandatoryDataLeftToBeFilledIn(): boolean {
        return this.doWeHavePackingDetailsMandatoryDataLeftToBeFilledIn_Condition1() ||
               this.doWeHavePackingDetailsMandatoryDataLeftToBeFilledIn_Condition2() ||
               this.doWeHavePackingDetailsMandatoryDataLeftToBeFilledIn_Condition3();
    }

    doWeHaveAllOfPackingDetailsMandatoryDataLeftToBeFilledIn(): boolean {
        return this.doWeHavePackingDetailsMandatoryDataLeftToBeFilledIn_Condition1() &&
               this.doWeHavePackingDetailsMandatoryDataLeftToBeFilledIn_Condition2() &&
               this.doWeHavePackingDetailsMandatoryDataLeftToBeFilledIn_Condition3();
    }

    textOfPackingDetailsMandatoryDataThatIsFilledIn(): string|undefined {
        let arrayOfTexts: Array<string> = new Array<string>();
        if (!this.doWeHavePackingDetailsMandatoryDataLeftToBeFilledIn_Condition1()) {
            for (let i = 0; i < this.outerPackagingMaterial[0].qualifierType.length ; i++)
                arrayOfTexts.push(this.outerPackagingMaterial[0].qualifierType[i].name);
        }
        if (!this.doWeHavePackingDetailsMandatoryDataLeftToBeFilledIn_Condition2()) {
            for (let i = 0; i < this.outerPackagingType[0].qualifierType.length ; i++)
                arrayOfTexts.push(this.outerPackagingType[0].qualifierType[i].name);
        }
        if (!this.doWeHavePackingDetailsMandatoryDataLeftToBeFilledIn_Condition3()) {
            for (let i = 0; i < this.innerPackaging[0].qualifierType.length ; i++)
                arrayOfTexts.push(this.innerPackaging[0].qualifierType[i].name);
        }
        return this.arrayOfTextsInCSVFormat(arrayOfTexts);
    }

    doWeHaveAnyDamageDetailsMandatoryDataLeftToBeFilledIn(): boolean {
        return this.doWeHaveDamageDetailsMandatoryDataLeftToBeFilledIn_Condition1() ||
               this.doWeHaveDamageDetailsMandatoryDataLeftToBeFilledIn_Condition2();
    }

    doWeHaveAllOfDamageDetailsMandatoryDataLeftToBeFilledIn(): boolean {
        return this.doWeHaveDamageDetailsMandatoryDataLeftToBeFilledIn_Condition1() &&
               this.doWeHaveDamageDetailsMandatoryDataLeftToBeFilledIn_Condition2();
    }

    textOfDamageDetailsMandatoryDataThatIsFilledIn(): string|undefined {
        let arrayOfTexts: Array<string> = new Array<string>();
        if (!this.doWeHaveDamageDetailsMandatoryDataLeftToBeFilledIn_Condition1()) {
            for (let i = 0; i < this.damageToPackaging[0].qualifierType.length ; i++)
                arrayOfTexts.push(this.damageToPackaging[0].qualifierType[i].name);
        }
        if (!this.doWeHaveDamageDetailsMandatoryDataLeftToBeFilledIn_Condition2()) {
            for (let i = 0; i < this.conditionOfContents[0].qualifierType.length ; i++)
                arrayOfTexts.push(this.conditionOfContents[0].qualifierType[i].name);
        }
        return this.arrayOfTextsInCSVFormat(arrayOfTexts);
    }

    doWeHaveAnyDamageDetectionMandatoryDataLeftToBeFilledIn(): boolean {
        return this.doWeHaveDamageDetectionMandatoryDataLeftToBeFilledIn_Condition1() ||
               this.doWeHaveDamageDetectionMandatoryDataLeftToBeFilledIn_Condition2();
    }

    doWeHaveAllOfDamageDetectionMandatoryDataLeftToBeFilledIn(): boolean {
        return this.doWeHaveDamageDetectionMandatoryDataLeftToBeFilledIn_Condition1() &&
               this.doWeHaveDamageDetectionMandatoryDataLeftToBeFilledIn_Condition2();
    }

    textOfDamageDetectionMandatoryDataThatIsFilledIn(): string|undefined {
        let arrayOfTexts: Array<string> = new Array<string>();
        if (!this.doWeHaveDamageDetectionMandatoryDataLeftToBeFilledIn_Condition1()) {
            for (let i = 0; i < this.damageDiscovery[0].qualifierType.length ; i++)
                arrayOfTexts.push(this.damageDiscovery[0].qualifierType[i].name);
        }
        if (!this.doWeHaveDamageDetectionMandatoryDataLeftToBeFilledIn_Condition2()) {
            for (let i = 0; i < this.damageDueTo[0].qualifierType.length ; i++)
                arrayOfTexts.push(this.damageDueTo[0].qualifierType[i].name);
        }
        return this.arrayOfTextsInCSVFormat(arrayOfTexts);
    }

    doWeHaveAnyRecuperationMandatoryDataLeftToBeFilledIn(): boolean {
        return this.doWeHaveRecuperationMandatoryDataLeftToBeFilledIn_Condition1();
    }

    doWeHaveAllOfRecuperationMandatoryDataLeftToBeFilledIn(): boolean {
        return this.doWeHaveRecuperationMandatoryDataLeftToBeFilledIn_Condition1();
    }

    textOfRecuperationMandatoryDataThatIsFilledIn(): string|undefined {
        let arrayOfTexts: Array<string> = new Array<string>();
        if (!this.doWeHaveRecuperationMandatoryDataLeftToBeFilledIn_Condition1())
            arrayOfTexts.push(this.recuperage);
        return this.arrayOfTextsInCSVFormat(arrayOfTexts);
    }

    doWeHaveAnyMandatoryDataLeftToBeFilledIn(): boolean {
        const doWeHaveAnyMandatoryDataLeftToBeFilledInOnlyTextual = ((): boolean => {
            return this.doWeHaveAnyShipmentDetailsMandatoryDataLeftToBeFilledIn()
            || this.doWeHaveAnyPackingDetailsMandatoryDataLeftToBeFilledIn()
            || this.doWeHaveAnyDamageDetailsMandatoryDataLeftToBeFilledIn()
            || this.doWeHaveAnyDamageDetectionMandatoryDataLeftToBeFilledIn()
            || this.doWeHaveAnyRecuperationMandatoryDataLeftToBeFilledIn()
            || this.doWeHaveAnyShipmentDetailsLogicalDataErrors();
        });

        const doWeHaveAllOfMandatoryDataLeftToBeFilledInOnlyTextual = ((): boolean => {
            return this.doWeHaveAllOfShipmentDetailsMandatoryDataLeftToBeFilledIn()
            && this.doWeHaveAllOfPackingDetailsMandatoryDataLeftToBeFilledIn()
            && this.doWeHaveAllOfDamageDetailsMandatoryDataLeftToBeFilledIn()
            && this.doWeHaveAllOfDamageDetectionMandatoryDataLeftToBeFilledIn()
            && this.doWeHaveAllOfRecuperationMandatoryDataLeftToBeFilledIn()
            && this.doWeHaveAnyShipmentDetailsLogicalDataErrors();
        });

        let anyMandatoryDataLeftToBeFilledIn: boolean =
        doWeHaveAnyMandatoryDataLeftToBeFilledInOnlyTextual();

        if (anyMandatoryDataLeftToBeFilledIn && !this.isAnyNonMandatoryDataFilledIn()) {
          if (this.images.linkMediaDataModels.length > 0 &&
              !doWeHaveAllOfMandatoryDataLeftToBeFilledInOnlyTextual()) {
            if (this.images.doWeHaveAtLeastOneMediaGeneratedFromDevice())
                anyMandatoryDataLeftToBeFilledIn = false;
            else
                anyMandatoryDataLeftToBeFilledIn = true;
          }
        }

        return anyMandatoryDataLeftToBeFilledIn;
    }

    doWeHaveAnythingWrittenOnDamageReportsDataModelIgnoringMedia(): boolean {
        if (this.transportMeans != null
            && this.transportMeans.carriersDataModel != null
            && this.transportMeans.carriersDataModel.code != null) {
              return true;
        }
        if (this.transportMeans != null
            && this.transportMeans.transportNumber != null) {
            return true;
        }
        if (this.pieces != null)
          return true;
        if (this.uld != null)
          return true;
        if (this.outerPackagingMaterial.length > 0
        && this.outerPackagingMaterial[0].qualifierType.length > 0)
          return true;
        if (this.outerPackagingType.length > 0
            && this.outerPackagingType[0].qualifierType.length > 0)
          return true;
        if (this.innerPackaging.length > 0
            && this.innerPackaging[0].qualifierType.length > 0)
          return true;
        if (this.damageToPackaging.length > 0
            && this.damageToPackaging[0].qualifierType.length > 0)
          return true;
        if (this.conditionOfContents.length > 0
            && this.conditionOfContents[0].qualifierType.length > 0)
          return true;
        if (this.damageDiscovery.length > 0
            && this.damageDiscovery[0].qualifierType.length > 0)
          return true;
        if (this.damageDueTo.length > 0
            && this.damageDueTo[0].qualifierType.length > 0)
          return true;
        if (this.recuperage != null)
          return true;
        if (this.remarks != null)
          return true;
        if (this.boxes != null)
          return true;
        return false;
    }

    doWeHaveOnlyThePresenceOfMediaOnDamageReportsDataModel(): boolean {
        return this.images.linkMediaDataModels.length > 0 &&
               !this.doWeHaveAnythingWrittenOnDamageReportsDataModelIgnoringMedia();
    }

    doWeHaveAnyLogicalDataErrors(): Array<string> {
        let anyLogicalDataErrors: Array<string> = [];

        if (this.transportMeans != null
            && this.transportMeans.carriersDataModel != null
            && this.transportMeans.carriersDataModel.code != null) {
            if (this.transportMeans.transportNumber == null)
                anyLogicalDataErrors.push(AppUIHelper.getInstance().i18nText('flightnumbermandatory'));
        }
        if (this.transportMeans != null) {
          if (this.transportMeans.date == null)
            anyLogicalDataErrors.push(AppUIHelper.getInstance().i18nText('flightdatemandatory'));
        }
        if (this.weight != null
            && this.weight.amount != null
            && this.weight.unit == null) {
                anyLogicalDataErrors.push(AppUIHelper.getInstance().i18nText('unitofweightneedstobeputin'));
        }

        return anyLogicalDataErrors;
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        delete jsonData['uniqueObjectId'];

        jsonData['reportId'] = this.reportId;
        if (this.reportingLocation != null)
            jsonData['reportingLocation'] = this.reportingLocation.toJSON();
        jsonData['reportingShipmentType'] = this.reportingShipmentType;
        if (this.airwaybill != null)
            jsonData['airwaybill'] = this.airwaybill.toJSON();
        if (this.houseAirwaybill != null)
            jsonData['houseAirwaybill'] = this.houseAirwaybill.toJSON();
        if (this.transportMeans != null)
            jsonData['transportMeans'] = this.transportMeans.toJSON();
        jsonData['uld'] = this.uld;
        jsonData['pieces'] = this.pieces;
        jsonData['boxes'] = this.boxes;
        if (this.weight != null)
            jsonData['weight'] = this.weight.toJSON();
        jsonData['recuperage'] = this.recuperage;
        jsonData['remarks'] = this.remarks;
        if (this.reportedOn != null)
            jsonData['reportedOn'] = DateParserHelper.parseDateToString_InFormat_Network(this.reportedOn);
        if (this.outerPackagingType != null)
            jsonData['outerPackagingType'] = JSON.parse(JSON.stringify(this.outerPackagingType));
        if (this.outerPackagingMaterial != null)
            jsonData['outerPackagingMaterial'] = JSON.parse(JSON.stringify(this.outerPackagingMaterial));
        if (this.innerPackaging != null)
            jsonData['innerPackaging'] = JSON.parse(JSON.stringify(this.innerPackaging));
        if (this.damageToPackaging != null)
            jsonData['damageToPackaging'] = JSON.parse(JSON.stringify(this.damageToPackaging));
        if (this.conditionOfContents != null)
            jsonData['conditionOfContents'] = JSON.parse(JSON.stringify(this.conditionOfContents));
        if (this.damageDiscovery != null)
            jsonData['damageDiscovery'] = JSON.parse(JSON.stringify(this.damageDiscovery));
        if (this.damageDueTo != null)
            jsonData['damageDueTo'] = JSON.parse(JSON.stringify(this.damageDueTo));
        if (this.images != null)
            jsonData['image'] = this.images.toJSON();
        if (this.createdBy != null)
            jsonData['createdBy'] = this.createdBy.toJSON();
        if (this.modifiedOn != null)
            jsonData['modifiedOn'] = DateParserHelper.parseDateToString(this.modifiedOn);
        if (this.modifiedBy != null)
            jsonData['modifiedBy'] = this.modifiedBy.toJSON();

        return jsonData;
    }

    fromJSON(_json: any) {
        super.fromJSON(_json);

        this.parse(_json);
    }

    doWeHaveAnyShipmentDetailsLogicalDataErrors_Pieces(): boolean {
        if (this.pieces != null) {
            const awbPieces = this.airwaybill != null ?
                              this.airwaybill.pieces :
                              this.houseAirwaybill.masterAirwaybill.pieces;
            return this.pieces > Number(awbPieces);
        }
        return false;
    }

    private isAnyNonMandatoryDataFilledIn(): boolean {
        if (this.uld != null && this.uld.length > 0)
          return true;
        if (this.remarks != null && this.remarks.length > 0)
          return true;
        if (this.boxes != null && this.boxes.length > 0)
          return true;

        return false;
    }

    private doWeHaveShipmentDetailsMandatoryDataLeftToBeFilledIn_Condition1(): boolean {
        return (this.pieces == null
                || AppUIHelper.isNaN(this.pieces)
                || this.pieces <= 0);
    }

    private doWeHavePackingDetailsMandatoryDataLeftToBeFilledIn_Condition1(): boolean {
        return (this.outerPackagingMaterial.length === 0
                || this.outerPackagingMaterial[0].qualifierType.length === 0);
    }

    private doWeHavePackingDetailsMandatoryDataLeftToBeFilledIn_Condition2(): boolean {
        return (this.outerPackagingType.length === 0
                || this.outerPackagingType[0].qualifierType.length === 0);
    }

    private doWeHavePackingDetailsMandatoryDataLeftToBeFilledIn_Condition3(): boolean {
        return (this.innerPackaging.length === 0
                || this.innerPackaging[0].qualifierType.length === 0);
    }

    private doWeHaveDamageDetailsMandatoryDataLeftToBeFilledIn_Condition1(): boolean {
        return (this.damageToPackaging.length === 0
                || this.damageToPackaging[0].qualifierType.length === 0);
    }

    private doWeHaveDamageDetailsMandatoryDataLeftToBeFilledIn_Condition2(): boolean {
        return (this.conditionOfContents.length === 0
                || this.conditionOfContents[0].qualifierType.length === 0);
    }

    private doWeHaveDamageDetectionMandatoryDataLeftToBeFilledIn_Condition1(): boolean {
        return (this.damageDiscovery.length === 0
                || this.damageDiscovery[0].qualifierType.length === 0);
    }

    private doWeHaveDamageDetectionMandatoryDataLeftToBeFilledIn_Condition2(): boolean {
        return (this.damageDueTo.length === 0
                || this.damageDueTo[0].qualifierType.length === 0);
    }

    private doWeHaveRecuperationMandatoryDataLeftToBeFilledIn_Condition1(): boolean {
        return (this.recuperage === null || this.recuperage === undefined);
    }

    private arrayOfTextsInCSVFormat(_arrayOfTexts: Array<string>): string|undefined {
        let _arrayOfTextsInCSVFormat: string = null;
        for (let i = 0; i < _arrayOfTexts.length; i++) {
            if (_arrayOfTextsInCSVFormat == null) _arrayOfTextsInCSVFormat = '';
            if (i > 0) _arrayOfTextsInCSVFormat = _arrayOfTextsInCSVFormat + ', ';
            _arrayOfTextsInCSVFormat = _arrayOfTextsInCSVFormat + _arrayOfTexts[i];
        }
        return _arrayOfTextsInCSVFormat;
    }
}
