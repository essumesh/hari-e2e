import { BaseDataModel } from './BaseDataModel';
import { CarriersDataModel } from './CarriersDataModel';
import { DateParserHelper } from '../helpers/DateParserHelper';

export class TransportMeansDataModel extends BaseDataModel {
    /**
     * business reference for this transport
     */
    reference?: string;

    /**
     * mode of transport
     */
    mode?: string;

    carriersDataModel?: CarriersDataModel;

    /**
     * reference number for this transport (e.g. flight number)
     */
    transportNumber?: string;

    /**
     * reference date for transport
     */
    date?: Date;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.reference = _data['reference'];
            this.mode = _data['mode'];

            let carrierJSON = _data['carrier'];
            if (carrierJSON != null && carrierJSON.constructor === {}.constructor) {
                this.carriersDataModel = new CarriersDataModel();
                this.carriersDataModel.parse(carrierJSON);
            }

            this.transportNumber = _data['transportNumber'];

            this.date = DateParserHelper.parseStringToDate(_data['date']);
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['reference'] = this.reference;
        jsonData['mode'] = this.mode;
        if (this.carriersDataModel != null)
            jsonData['carrier'] = JSON.parse(JSON.stringify(this.carriersDataModel));
        jsonData['transportNumber'] = this.transportNumber;
        if (this.date != null)
            jsonData['date'] = DateParserHelper.parseDateToString_InFormat_Network(this.date);

        return jsonData;
    }
}
