import { ServerErrorResponseGlobalErrorsDataModel } from './ServerErrorResponseGlobalErrorsDataModel';

export class ServerErrorDataModel extends ServerErrorResponseGlobalErrorsDataModel {
    errorText: string;
    fieldErrors?: Array<string>;

    constructor(errorText: string) {
        super();
        this.errorText = errorText;
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        if (this.errorText != null)
            jsonData['errorText'] = this.errorText;
        if (this.fieldErrors != null)
            jsonData['fieldErrors'] = JSON.parse(JSON.stringify(this.fieldErrors));
        return jsonData;
    }

    fromJSON(json: any) {
        if (json.errorText != null)
            this.errorText = json.errorText;
        if (json.fieldErrors != null)
            this.fieldErrors = json.fieldErrors;
    }
}
