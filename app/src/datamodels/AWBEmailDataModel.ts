import { BaseDataModel } from './BaseDataModel';

export class Thumbnail extends BaseDataModel {
    base64Content: string;
    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.base64Content = _data['base64Content'];
        }
    }

    toJSON() {
        let jsonData = super.toJSON();
        delete jsonData['uniqueObjectId'];
        jsonData['base64Content'] = this.base64Content;
        return jsonData;
    }
}

export class ImageURL extends BaseDataModel {
    href: string;
    thumbnail: Thumbnail;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.href = _data['href'];
            this.thumbnail = _data['thumbnail'];
        }
    }

    toJSON() {
        let jsonData = super.toJSON();
        delete jsonData['uniqueObjectId'];
        jsonData['href'] = this.href;
        jsonData['thumbnail'] = this.thumbnail;
        return jsonData;
    }
}

export class AWBEmailDataModel extends BaseDataModel {
    imageURLs: Array<ImageURL> = [];
    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let emailArray = _data['imageURLs'];
            if (emailArray != null && emailArray instanceof Array) {
                for (let arrayItem of emailArray) {
                    let imageURL = new ImageURL();
                    imageURL.parse(arrayItem);
                    this.imageURLs.push(imageURL);
                }
            }
        }
    }
    toJSON() {
        let jsonData = super.toJSON();
        delete jsonData['uniqueObjectId'];
        if (this.imageURLs != null) {
            jsonData['imageURLs'] = JSON.parse(JSON.stringify(this.imageURLs));
        }
        return jsonData;
    }
}
