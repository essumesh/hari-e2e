import { BaseDataModel } from './BaseDataModel';
import { DamageReportsDataModel } from './DamageReportsDataModel';
import { PageMetadataDataModel } from './PageMetadataDataModel';

export class PagedResourcesdamagereportsEmbeddedDataModel extends BaseDataModel {
    damageReports: Array<DamageReportsDataModel> = [];
    pageMetadataDataModel?: PageMetadataDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let _embeddedJSON = _data['_embedded'];
            if (_embeddedJSON != null && _embeddedJSON.constructor === {}.constructor) {
                let damageReportsArray = _embeddedJSON['awbDamages'];
                if (damageReportsArray != null && damageReportsArray instanceof Array) {
                    for (let arrayItem of damageReportsArray) {
                        let damageReportsDataModel = new DamageReportsDataModel();
                        damageReportsDataModel.parse(arrayItem);
                        this.damageReports.push(damageReportsDataModel);
                      }
                }
            }

            let pageJSON = _data['page'];
            if (pageJSON != null && pageJSON.constructor === {}.constructor) {
                this.pageMetadataDataModel = new PageMetadataDataModel();
                this.pageMetadataDataModel.parse(pageJSON);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        if (this.damageReports != null)
            jsonData['awbDamages'] = JSON.parse(JSON.stringify(this.damageReports));
        if (this.pageMetadataDataModel != null)
            jsonData['page'] = JSON.stringify(this.pageMetadataDataModel);

        return jsonData;
    }
}
