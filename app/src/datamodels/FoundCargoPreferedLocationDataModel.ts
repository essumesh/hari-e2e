import { BaseDataModel } from './BaseDataModel';
import { WarehouseLocationDataModel } from './WarehouseLocationDataModel';

export class FoundCargoPreferedLocationDataModel extends BaseDataModel {
    deliveryPieces?: string;
    warehouseLocation?: WarehouseLocationDataModel ;
    euPalletCount?: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.deliveryPieces = _data['deliveryPieces'];
            this.euPalletCount = _data['euPalletCount'];

            let warehouseLocationText = _data['warehouseLocation'];
            if (warehouseLocationText != null && warehouseLocationText.constructor === ''.constructor) {
                this.warehouseLocation = new WarehouseLocationDataModel();
                this.warehouseLocation.parse(warehouseLocationText);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['deliveryPieces'] = this.deliveryPieces;
        jsonData['euPalletCount'] = this.euPalletCount;
        if (this.warehouseLocation != null)
        jsonData['warehouseLocation'] = this.warehouseLocation.toJSON();
        return jsonData;
    }
}
