import { BaseDataModel } from './BaseDataModel';

export class PickupViewModel extends BaseDataModel {
    airwaybillno: string;
    location: string;
    deliverypieces: string;
    isChecked: boolean;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.airwaybillno = _data['airwaybillno'];
            this.location = _data['location'];
            this.deliverypieces = _data['deliverypieces'];
            this.isChecked = _data['isChecked'];
        }
    }
}

export class PickupViewArrayModel extends BaseDataModel {
     pickupViewArrayModel: Array<PickupViewModel> = [];

     parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let pickupViewArray = _data['pickupViewModel'];
            if (pickupViewArray != null && pickupViewArray instanceof Array) {
                for (let arrayItem of pickupViewArray) {
                    let pickupViewData = new PickupViewModel();
                    pickupViewData.parse(arrayItem);
                    this.pickupViewArrayModel.push(pickupViewData);
                  }
            }
        }
    }
}

export class PickUpViewDataModel extends BaseDataModel {
    isChecked: boolean;
    storedPieces: string;
    collectedPieces: string;
    storedCount: string;
    pickUpViewModel: Array<PickupViewModel> = [];

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.isChecked = _data['isChecked'];
            this.storedPieces = _data['storedPieces'];
            this.collectedPieces = _data['collectedPieces'];
            this.storedCount = _data['storedCount'];
            let pickUpViewDataModelArray = _data['pickUpViewDataModel'];
            if (pickUpViewDataModelArray != null && pickUpViewDataModelArray instanceof Array) {
                for (let arrayItem of pickUpViewDataModelArray) {
                    let pickupViewData = new PickupViewModel();
                    pickupViewData.parse(arrayItem);
                    this.pickUpViewModel.push(pickupViewData);
                  }
            }
        }
    }
}

export class PickUpDeliveryViewDataModel extends BaseDataModel {
    pickupDeliveryViewModel: Array<PickUpViewDataModel> = [];
    isChecked: boolean;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.isChecked = _data['isChecked'];
            let pickupDeliveryArray = _data['pickupDeliveryViewModel'];
            if (pickupDeliveryArray != null && pickupDeliveryArray instanceof Array) {
                for (let arrayItem of pickupDeliveryArray) {
                    let pickupViewData = new PickUpViewDataModel();
                    pickupViewData.parse(arrayItem);
                    this.pickupDeliveryViewModel.push(pickupViewData);
                  }
            }
        }
    }
}
