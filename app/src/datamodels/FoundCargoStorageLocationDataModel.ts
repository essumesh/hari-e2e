import { BaseDataModel } from './BaseDataModel';
import { WarehouseLocationDataModel } from './WarehouseLocationDataModel';

export class FoundCargoStorageLocationDataModel extends BaseDataModel {
    storedPieces?: string;
    warehouseLocation?: WarehouseLocationDataModel ;
    // euPalletCount?: string;
    location?: string;
    viewDelete?: boolean;
    weight: number;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.storedPieces = _data['storedPieces'];
          //  this.euPalletCount = _data['euPalletCount'];
            this.location = _data['location'];
            this.viewDelete = _data['viewDelete'];
            this.weight = _data['weight'];

            let warehouseLocationText = _data['warehouseLocation'];
            if (warehouseLocationText != null && warehouseLocationText.constructor === ''.constructor) {
                this.warehouseLocation = new WarehouseLocationDataModel();
                this.warehouseLocation = warehouseLocationText;
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['storedPieces'] = this.storedPieces;
       // jsonData['euPalletCount'] = this.euPalletCount;
        jsonData['location'] = this.location;
        jsonData['viewDelete'] = this.viewDelete;
        jsonData['weight'] = this.weight;
        if (this.warehouseLocation != null)
        jsonData['warehouseLocation'] = this.warehouseLocation.toJSON();
        return jsonData;
    }
}
