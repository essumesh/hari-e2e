import { BaseDataModel } from './BaseDataModel';
import { CountriesDataModel } from './CountriesDataModel';
import { LinksDataModel } from './LinksDataModel';

export class LocationsDataModel extends BaseDataModel {
    /**
     * code for the location
     */
    code?: string;

    /**
     * airport or city
     */
    type?: string;

    /**
     * full name
     */
    name?: string;

    country?: CountriesDataModel;

    links?: LinksDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.code = _data['code'];
            this.type = _data['type'];
            this.name = _data['name'];

            let countryJSON = _data['country'];
            if (countryJSON != null && countryJSON.constructor === {}.constructor) {
                this.country = new CountriesDataModel();
                this.country.parse(countryJSON);
            }

            let linksJSON = _data['links'];
            if (linksJSON != null && linksJSON.constructor === {}.constructor) {
                this.links = new LinksDataModel();
                this.links.parse(linksJSON);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['code'] = this.code;
        jsonData['type'] = this.type;
        jsonData['name'] = this.name;

        if (this.country != null)
            jsonData['country'] = this.country.toJSON();

        if (this.links != null)
            jsonData['links'] = this.links.toJSON();

        return jsonData;
    }
}
