import { BaseDataModel } from './BaseDataModel';

export class LookupDataModel extends BaseDataModel {
    private static readonly OTHER_CODE = 'OTH';
    /**
     * business code for the combo
     */
    code?: string;

    /**
     * business code for the combo
     */
    type?: string;

    /**
     * description for the lookup
     */
    name?: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.code = _data['code'];
            this.type = _data['type'];
            this.name = _data['name'];
        }
    }

    isCodeOfTypeOther(): boolean {
        return this.code != null && this.code === LookupDataModel.OTHER_CODE;
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['code'] = this.code;
        jsonData['type'] = this.type;
        jsonData['name'] = this.name;

        return jsonData;
    }
}
