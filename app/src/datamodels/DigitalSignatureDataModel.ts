import { BaseDataModel } from './BaseDataModel';

export class DigitalSignatureDataModel extends BaseDataModel {
    fileName: string;
    fileType: string;
    fileContent: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.fileName = _data['fileName'];
            this.fileType = _data['fileType'];
            this.fileContent = _data['fileContent'];
        }
    }

    toJSON() {
        let jsonData = super.toJSON();
        delete jsonData['uniqueObjectId'];
        jsonData['fileName'] = this.fileName;
        jsonData['fileType'] = this.fileType;
        jsonData['fileContent'] = this.fileContent;
        // console.log(jsonData);
        return jsonData;
   }
}
