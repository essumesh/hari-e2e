export class TreeBranchDataModel {
    private _name: string;
    private _data: any;

    constructor(_name: string,
                _data: any) {
        this._name = _name;
        this.data = _data;
    }

    get name(): string {
        return this._name;
    }

    get data(): any {
        return this._data;
    }

    set data(_data: any) {
        this._data = _data;
    }
}
