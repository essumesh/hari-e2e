import { BaseDataModel } from './BaseDataModel';

export class RectDataModel extends BaseDataModel {
    top: number;
    left: number;
    width: number;
    height: number;

    constructor(top: number, left: number, width: number, height: number) {
        super();
        this.top = top;
        this.left = left;
        this.width = width;
        this.height = height;
    }
}
