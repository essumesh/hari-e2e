import { BaseDataModel } from './BaseDataModel';

export class CountriesDataModel extends BaseDataModel {
    /**
     * unique id for this countries
     */
    id?: string;

    code?: string;

    name?: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];
            this.code = _data['code'];
            this.name = _data['name'];
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;
        jsonData['code'] = this.code;
        jsonData['name'] = this.name;

        return jsonData;
    }
}
