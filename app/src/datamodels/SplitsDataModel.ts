import { BaseDataModel } from './BaseDataModel';
import { AirwaybillsDataModel } from './AirwaybillsDataModel';
import { HouseAirwaybillsDataModel } from './HouseAirwaybillsDataModel';
import { PartsDataModel } from './PartsDataModel';
import { AmountUnitDataModel } from './AmountUnitDataModel';
import { LogisticsPackagesDataModel } from './LogisticsPackagesDataModel';
import { SpecialHandlingCodesDataModel } from './SpecialHandlingCodesDataModel';
import { WarehouseLocationDataModel } from './WarehouseLocationDataModel';
import { SystemUsersDataModel } from './SystemUsersDataModel';
import { ContainersDataModel } from './ContainersDataModel';

export class SplitsDataModel extends BaseDataModel {
    /**
     * unique id for this splits
     */
    id?: string;

    airwaybill?: AirwaybillsDataModel;

    houseAirwaybill?: HouseAirwaybillsDataModel;

    part?: PartsDataModel;

    expectedPieces?: number;

    expectedWeight?: AmountUnitDataModel;

    expectedVolume?: AmountUnitDataModel;

    actualPieces?: number;

    actualWeight?: AmountUnitDataModel;

    actualVolume?: AmountUnitDataModel;

    logisticsPackages?: Array<LogisticsPackagesDataModel>;

    status?: string;

    specialHandling?: Array<SpecialHandlingCodesDataModel>;

    splitContainer?: ContainersDataModel;

    storageLocation?: WarehouseLocationDataModel;

    preferredLocation?: WarehouseLocationDataModel;

    createdOn?: string;

    createdBy?: SystemUsersDataModel;

    modifiedOn?: string;

    modifiedBy?: SystemUsersDataModel;

    plannedContainer?: ContainersDataModel;

    actualContainer?: ContainersDataModel;

    throughContainer?: boolean;

    shipperLoadedContainer?: boolean;

    blockedForManifesting?: boolean;

    blockedForPremanifesting?: boolean;

    /**
     * number of eu pallets
     */
    euPalletCount?: number;

    foundCargo?: boolean;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];

            let airwaybillJSON = _data['airwaybill'];
            if (airwaybillJSON != null && airwaybillJSON.constructor === {}.constructor) {
                this.airwaybill = new AirwaybillsDataModel();
                this.airwaybill.parse(airwaybillJSON);
            }

            let houseAirwaybillJSON = _data['houseAirwaybill'];
            if (houseAirwaybillJSON != null && houseAirwaybillJSON.constructor === {}.constructor) {
                this.houseAirwaybill = new HouseAirwaybillsDataModel();
                this.houseAirwaybill.parse(houseAirwaybillJSON);
            }

            let partJSON = _data['part'];
            if (partJSON != null && partJSON.constructor === {}.constructor) {
                this.part = new PartsDataModel();
                this.part.parse(partJSON);
            }

            this.expectedPieces = _data['expectedPieces'];

            let expectedWeightJSON = _data['expectedWeight'];
            if (expectedWeightJSON != null && expectedWeightJSON.constructor === {}.constructor) {
                this.expectedWeight = new AmountUnitDataModel();
                this.expectedWeight.parse(expectedWeightJSON);
            }

            let expectedVolumeJSON = _data['expectedVolume'];
            if (expectedVolumeJSON != null && expectedVolumeJSON.constructor === {}.constructor) {
                this.expectedVolume = new AmountUnitDataModel();
                this.expectedVolume.parse(expectedVolumeJSON);
            }

            this.actualPieces = _data['actualPieces'];

            let actualWeightJSON = _data['actualWeight'];
            if (actualWeightJSON != null && actualWeightJSON.constructor === {}.constructor) {
                this.actualWeight = new AmountUnitDataModel();
                this.actualWeight.parse(actualWeightJSON);
            }

            let actualVolumeJSON = _data['actualVolume'];
            if (actualVolumeJSON != null && actualVolumeJSON.constructor === {}.constructor) {
                this.actualVolume = new AmountUnitDataModel();
                this.actualVolume.parse(actualVolumeJSON);
            }

            let logisticsPackagesArray = _data['logisticsPackages'];
            if (logisticsPackagesArray != null && logisticsPackagesArray instanceof Array) {
                for (let arrayItem of logisticsPackagesArray) {
                    let logisticsPackagesDataModel = new LogisticsPackagesDataModel();
                    logisticsPackagesDataModel.parse(arrayItem);
                    this.logisticsPackages.push(logisticsPackagesDataModel);
                  }
            }

            this.status = _data['status'];

            let specialHandlingArray = _data['specialHandling'];
            if (specialHandlingArray != null && specialHandlingArray instanceof Array) {
                for (let arrayItem of specialHandlingArray) {
                    let specialHandlingCodesDataModel = new SpecialHandlingCodesDataModel();
                    specialHandlingCodesDataModel.parse(arrayItem);
                    this.specialHandling.push(specialHandlingCodesDataModel);
                  }
            }

            let splitContainerJSON = _data['splitContainer'];
            if (splitContainerJSON != null && splitContainerJSON.constructor === {}.constructor) {
                this.splitContainer = new ContainersDataModel();
                this.splitContainer.parse(splitContainerJSON);
            }

            let storageLocationJSON = _data['storageLocation'];
            if (storageLocationJSON != null && storageLocationJSON.constructor === {}.constructor) {
                this.storageLocation = new WarehouseLocationDataModel();
                this.storageLocation.parse(storageLocationJSON);
            }

            let preferredLocationJSON = _data['preferredLocationJSON'];
            if (preferredLocationJSON != null && preferredLocationJSON.constructor === {}.constructor) {
                this.preferredLocation = new WarehouseLocationDataModel();
                this.preferredLocation.parse(preferredLocationJSON);
            }

            this.createdOn = _data['createdOn'];

            let createdByJSON = _data['createdBy'];
            if (createdByJSON != null && createdByJSON.constructor === {}.constructor) {
                this.createdBy = new SystemUsersDataModel();
                this.createdBy.parse(createdByJSON);
            }

            this.modifiedOn = _data['modifiedOn'];

            let modifiedByJSON = _data['modifiedBy'];
            if (modifiedByJSON != null && modifiedByJSON.constructor === {}.constructor) {
                this.modifiedBy = new SystemUsersDataModel();
                this.modifiedBy.parse(modifiedByJSON);
            }

            let plannedContainerJSON = _data['plannedContainer'];
            if (plannedContainerJSON != null && plannedContainerJSON.constructor === {}.constructor) {
                this.plannedContainer = new ContainersDataModel();
                this.plannedContainer.parse(plannedContainerJSON);
            }

            let actualContainerJSON = _data['actualContainer'];
            if (actualContainerJSON != null && actualContainerJSON.constructor === {}.constructor) {
                this.actualContainer = new ContainersDataModel();
                this.actualContainer.parse(actualContainerJSON);
            }

            this.throughContainer = _data['throughContainer'];
            this.shipperLoadedContainer = _data['shipperLoadedContainer'];
            this.blockedForManifesting = _data['blockedForManifesting'];
            this.blockedForPremanifesting = _data['blockedForPremanifesting'];
            this.euPalletCount = _data['euPalletCount'];
            this.foundCargo = _data['foundCargo'];

        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;

        if (this.airwaybill != null)
            jsonData['airwaybill'] = this.airwaybill.toJSON();

        if (this.houseAirwaybill != null)
            jsonData['houseAirwaybill'] = this.houseAirwaybill.toJSON();

        if (this.part != null)
            jsonData['part'] = this.part.toJSON();

        jsonData['expectedPieces'] = this.expectedPieces;

        if (this.expectedWeight != null)
            jsonData['expectedWeight'] = this.expectedWeight.toJSON();

        if (this.expectedVolume != null)
            jsonData['expectedVolume'] = this.expectedVolume.toJSON();

        jsonData['actualPieces'] = this.actualPieces;

        if (this.actualWeight != null)
            jsonData['actualWeight'] = this.actualWeight.toJSON();

        if (this.actualVolume != null)
            jsonData['actualVolume'] = this.actualVolume.toJSON();

        if (this.logisticsPackages != null)
            jsonData['logisticsPackages'] = JSON.parse(JSON.stringify(this.logisticsPackages));

        jsonData['status'] = this.status;

        if (this.specialHandling != null)
            jsonData['specialHandling'] = JSON.parse(JSON.stringify(this.specialHandling));

        if (this.splitContainer != null)
            jsonData['splitContainer'] = this.splitContainer.toJSON();

        if (this.storageLocation != null)
            jsonData['storageLocation'] = this.storageLocation.toJSON();

        if (this.preferredLocation != null)
            jsonData['preferredLocation'] = this.preferredLocation.toJSON();

        jsonData['createdOn'] = this.createdOn;

        if (this.createdBy != null)
            jsonData['createdBy'] = this.createdBy.toJSON();

        jsonData['modifiedOn'] = this.modifiedOn;

        if (this.modifiedBy != null)
            jsonData['modifiedBy'] = this.modifiedBy.toJSON();

        if (this.plannedContainer != null)
            jsonData['plannedContainer'] = this.plannedContainer.toJSON();

        if (this.actualContainer != null)
            jsonData['actualContainer'] = this.actualContainer.toJSON();

        jsonData['throughContainer'] = this.throughContainer;
        jsonData['shipperLoadedContainer'] = this.shipperLoadedContainer;
        jsonData['blockedForManifesting'] = this.blockedForManifesting;
        jsonData['blockedForPremanifesting'] = this.blockedForPremanifesting;
        jsonData['euPalletCount'] = this.euPalletCount;
        jsonData['foundCargo'] = this.foundCargo;
    }

}
