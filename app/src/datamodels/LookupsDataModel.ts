import { BaseDataModel } from './BaseDataModel';
import { LookupDataModel } from './LookupDataModel';

export class LookupsDataModel extends BaseDataModel {
    qualifierType: Array<LookupDataModel> = [];
    other?: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let qualifierTypeArray = _data['qualifierType'];
            if (qualifierTypeArray != null && qualifierTypeArray instanceof Array) {
                for (let arrayItem of qualifierTypeArray) {
                    let lookupDataModel = new LookupDataModel();
                    lookupDataModel.parse(arrayItem);
                    this.qualifierType.push(lookupDataModel);
                  }
            }
            this.other = _data['other'];
        }
    }

    lookupDataModel_Having_CodeOfType_Other(): LookupDataModel|undefined {
        let _lookupDataModelHavingCodeOfTypeOther: LookupDataModel = null;
        for (let _lookupDataModelTemp of this.qualifierType) {
            if (_lookupDataModelTemp.isCodeOfTypeOther()) {
                _lookupDataModelHavingCodeOfTypeOther =
                _lookupDataModelTemp;
                break;
            }
        }
        return _lookupDataModelHavingCodeOfTypeOther;
    }

    addIfNotPresent(_lookupDataModel: LookupDataModel) {
        let toInclude: boolean = true;
        for (let _lookupDataModelTemp of this.qualifierType) {
            if (_lookupDataModelTemp.code === _lookupDataModel.code) {
                toInclude = false;
                break;
            }
        }
        if (toInclude) {
            this.qualifierType.push(_lookupDataModel);
        }
    }

    removeIfPresent(_lookupDataModel: LookupDataModel) {
        let toRemove: boolean = false;
        for (let _lookupDataModelTemp of this.qualifierType) {
            if (_lookupDataModelTemp.code === _lookupDataModel.code) {
                toRemove = true;
                break;
            }
        }
        if (toRemove) {
            this.qualifierType.splice(this.qualifierType.indexOf(_lookupDataModel), 1);
        }
    }

    remove_LookupDataModel_Having_CodeOfType_Other_IfPresent() {
        let _lookupDataModelHavingCodeOfTypeOther: LookupDataModel =
        this.lookupDataModel_Having_CodeOfType_Other();
        if (_lookupDataModelHavingCodeOfTypeOther != null)
            this.removeIfPresent(_lookupDataModelHavingCodeOfTypeOther);
    }

    contains(_lookupDataModel: LookupDataModel): boolean {
        let doesItContain: boolean = false;
        for (let _lookupDataModelTemp of this.qualifierType) {
            if (_lookupDataModelTemp.code === _lookupDataModel.code) {
                doesItContain = true;
                break;
            }
        }
        return doesItContain;
    }

    clearQualifierType() {
        this.qualifierType = [];
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        if (this.qualifierType != null)
            jsonData['qualifierType'] = JSON.parse(JSON.stringify(this.qualifierType));
        jsonData['other'] = this.other;

        return jsonData;
    }
}
