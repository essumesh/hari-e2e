import { BaseDataModel } from './BaseDataModel';

export class AmountUnitDataModel extends BaseDataModel {
    /**
     * the amount in the specified unit
     */
    amount?: number;

    /**
     * the unit for the amount
     */
    unit?: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.amount = _data['amount'];
            this.unit = _data['unit'];
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['amount'] = this.amount;
        jsonData['unit'] = this.unit;

        return jsonData;
    }
}
