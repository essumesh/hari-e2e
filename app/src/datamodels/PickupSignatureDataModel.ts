import { BaseDataModel } from './BaseDataModel';
import { PickupDriverInfoDataModel } from './PickupDriverInfoDataModel';

export class PickupSignatureDataModel extends BaseDataModel {
    pickupDriverInfo?: PickupDriverInfoDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.pickupDriverInfo = _data['pickupDriverInfo'];
        }
    }

    toJSON() {
        let jsonData = super.toJSON();
        delete jsonData['uniqueObjectId'];
        if (this.pickupDriverInfo != null)
        jsonData['pickupDriverInfo'] = this.pickupDriverInfo.toJSON();
        // console.log(jsonData);
        return jsonData;
    }

    fromJSON(_json: any) {
        super.fromJSON(_json);
        this.parse(_json);
    }
}
