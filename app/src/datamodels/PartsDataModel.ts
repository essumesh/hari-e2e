import { BaseDataModel } from './BaseDataModel';
import { PartsEmbeddedDataModel } from './PartsEmbeddedDataModel';
import { AirwaybillsDataModel } from './AirwaybillsDataModel';
import { HouseAirwaybillsDataModel } from './HouseAirwaybillsDataModel';
import { AmountUnitDataModel } from './AmountUnitDataModel';
import { SpecialHandlingCodesDataModel } from './SpecialHandlingCodesDataModel';
import { ReadyForCarriageChecksDataModel } from './ReadyForCarriageChecksDataModel';
import { IrregularitiesDataModel } from './IrregularitiesDataModel';
import { ItinerariesDataModel } from './ItinerariesDataModel';
import { WarehouseLocationDataModel } from './WarehouseLocationDataModel';
import { SystemUsersDataModel } from './SystemUsersDataModel';
import { HandlingInformationDataModel } from './HandlingInformationDataModel';
import { TransportMeansDataModel } from './TransportMeansDataModel';

export class PartsDataModel extends BaseDataModel {
    /**
     * unique id for this parts
     */
    id?: string;

    airwaybill?: AirwaybillsDataModel;

    houseAirwaybill?: HouseAirwaybillsDataModel;

    pieces?: number;

    weight?: AmountUnitDataModel;

    volume?: AmountUnitDataModel;

    receivedPieces?: number;

    receivedWeight?: AmountUnitDataModel;

    receivedVolume?: AmountUnitDataModel;

    status?: string;

    specialHandling?: Array<SpecialHandlingCodesDataModel>;

    xray?: boolean;

    readyForCarriage?: boolean;

    readyForCarriageChecks?: Array<ReadyForCarriageChecksDataModel>;

    blockedForManifesting?: boolean;

    security?: boolean;

    irregularity?: Array<IrregularitiesDataModel>;

    itineraries?: Array<ItinerariesDataModel>;

    numberOfSplits?: number;

    preferredLocation?: WarehouseLocationDataModel;

    createdOn?: string;

    createdBy?: SystemUsersDataModel;

    modifiedOn?: string;

    modifiedBy?: SystemUsersDataModel;

    handlingInformation?: Array<HandlingInformationDataModel>;

    transportMeans?: Array<TransportMeansDataModel>;

    embedded?: PartsEmbeddedDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];

            let airwaybillJSON = _data['airwaybill'];
            if (airwaybillJSON != null && airwaybillJSON.constructor === {}.constructor) {
                this.airwaybill = new AirwaybillsDataModel();
                this.airwaybill.parse(airwaybillJSON);
            }

            let houseAirwaybillJSON = _data['houseAirwaybill'];
            if (houseAirwaybillJSON != null && houseAirwaybillJSON.constructor === {}.constructor) {
                this.houseAirwaybill = new HouseAirwaybillsDataModel();
                this.houseAirwaybill.parse(houseAirwaybillJSON);
            }

            this.pieces = _data['pieces'];

            let weightJSON = _data['weight'];
            if (weightJSON != null && weightJSON.constructor === {}.constructor) {
                this.weight = new AmountUnitDataModel();
                this.weight.parse(weightJSON);
            }

            let volumeJSON = _data['volume'];
            if (volumeJSON != null && volumeJSON.constructor === {}.constructor) {
                this.volume = new AmountUnitDataModel();
                this.volume.parse(volumeJSON);
            }

            this.receivedPieces = _data['receivedPieces'];

            let receivedWeightJSON = _data['receivedWeight'];
            if (receivedWeightJSON != null && receivedWeightJSON.constructor === {}.constructor) {
                this.receivedWeight = new AmountUnitDataModel();
                this.receivedWeight.parse(receivedWeightJSON);
            }

            let receivedVolumeJSON = _data['receivedVolume'];
            if (receivedVolumeJSON != null && receivedVolumeJSON.constructor === {}.constructor) {
                this.receivedVolume = new AmountUnitDataModel();
                this.receivedVolume.parse(receivedVolumeJSON);
            }

            this.status = _data['status'];

            let specialHandlingArray = _data['specialHandling'];
            if (specialHandlingArray != null && specialHandlingArray instanceof Array) {
                for (let arrayItem of specialHandlingArray) {
                    let specialHandlingCodesDataModel = new SpecialHandlingCodesDataModel();
                    specialHandlingCodesDataModel.parse(arrayItem);
                    this.specialHandling.push(specialHandlingCodesDataModel);
                  }
            }

            this.xray = _data['xray'];
            this.readyForCarriage = _data['readyForCarriage'];

            let readyForCarriageChecksArray = _data['readyForCarriageChecks'];
            if (readyForCarriageChecksArray != null && readyForCarriageChecksArray instanceof Array) {
                for (let arrayItem of readyForCarriageChecksArray) {
                    let readyForCarriageChecksDataModel = new ReadyForCarriageChecksDataModel();
                    readyForCarriageChecksDataModel.parse(arrayItem);
                    this.readyForCarriageChecks.push(readyForCarriageChecksDataModel);
                  }
            }

            this.blockedForManifesting = _data['blockedForManifesting'];
            this.security = _data['security'];

            let irregularityArray = _data['irregularity'];
            if (irregularityArray != null && irregularityArray instanceof Array) {
                for (let arrayItem of irregularityArray) {
                    let irregularityDataModel = new IrregularitiesDataModel();
                    irregularityDataModel.parse(arrayItem);
                    this.irregularity.push(irregularityDataModel);
                  }
            }

            let itinerariesArray = _data['itineraries'];
            if (itinerariesArray != null && itinerariesArray instanceof Array) {
                for (let arrayItem of itinerariesArray) {
                    let itinerariesDataModel = new ItinerariesDataModel();
                    itinerariesDataModel.parse(arrayItem);
                    this.readyForCarriageChecks.push(itinerariesDataModel);
                  }
            }

            this.numberOfSplits = _data['numberOfSplits'];

            let preferredLocationJSON = _data['preferredLocationJSON'];
            if (preferredLocationJSON != null && preferredLocationJSON.constructor === {}.constructor) {
                this.preferredLocation = new WarehouseLocationDataModel();
                this.preferredLocation.parse(preferredLocationJSON);
            }

            this.createdOn = _data['createdOn'];

            let createdByJSON = _data['createdBy'];
            if (createdByJSON != null && createdByJSON.constructor === {}.constructor) {
                this.createdBy = new SystemUsersDataModel();
                this.createdBy.parse(createdByJSON);
            }

            this.modifiedOn = _data['modifiedOn'];

            let modifiedByJSON = _data['modifiedBy'];
            if (modifiedByJSON != null && modifiedByJSON.constructor === {}.constructor) {
                this.modifiedBy = new SystemUsersDataModel();
                this.modifiedBy.parse(modifiedByJSON);
            }

            let handlingInformationArray = _data['handlingInformation'];
            if (handlingInformationArray != null && handlingInformationArray instanceof Array) {
                for (let arrayItem of handlingInformationArray) {
                    let handlingInformationDataModel = new HandlingInformationDataModel();
                    handlingInformationDataModel.parse(arrayItem);
                    this.handlingInformation.push(handlingInformationDataModel);
                  }
            }

            let transportMeansArray = _data['transportMeans'];
            if (transportMeansArray != null && transportMeansArray instanceof Array) {
                for (let arrayItem of transportMeansArray) {
                    let transportMeansArrayDataModel = new TransportMeansDataModel();
                    transportMeansArrayDataModel.parse(arrayItem);
                    this.transportMeans.push(transportMeansArrayDataModel);
                  }
            }

            let embeddedJSON = _data['embedded'];
            if (embeddedJSON != null && embeddedJSON.constructor === {}.constructor) {
                this.embedded = new PartsEmbeddedDataModel();
                this.embedded.parse(embeddedJSON);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;

        if (this.airwaybill != null)
            jsonData['airwaybill'] = this.airwaybill.toJSON();

        if (this.houseAirwaybill != null)
            jsonData['houseAirwaybill'] = this.houseAirwaybill.toJSON();

        jsonData['pieces'] = this.pieces;

        if (this.weight != null)
            jsonData['weight'] = this.weight.toJSON();

        if (this.volume != null)
            jsonData['volume'] = this.volume.toJSON();

        jsonData['receivedPieces'] = this.receivedPieces;

        if (this.receivedWeight != null)
            jsonData['receivedWeight'] = this.receivedWeight.toJSON();

        if (this.receivedVolume != null)
            jsonData['receivedVolume'] = this.receivedVolume.toJSON();

        jsonData['status'] = this.status;

        if (this.specialHandling != null)
            jsonData['specialHandling'] = JSON.parse(JSON.stringify(this.specialHandling));

        jsonData['xray'] = this.xray;
        jsonData['readyForCarriage'] = this.readyForCarriage;

        if (this.readyForCarriageChecks != null)
            jsonData['readyForCarriageChecks'] = JSON.parse(JSON.stringify(this.readyForCarriageChecks));

        jsonData['blockedForManifesting'] = this.blockedForManifesting;
        jsonData['security'] = this.security;

        if (this.irregularity != null)
            jsonData['irregularity'] = JSON.parse(JSON.stringify(this.irregularity));

        if (this.itineraries != null)
            jsonData['itineraries'] = JSON.parse(JSON.stringify(this.itineraries));

        jsonData['numberOfSplits'] = this.numberOfSplits;

        if (this.preferredLocation != null)
            jsonData['preferredLocation'] = this.preferredLocation.toJSON();

        jsonData['createdOn'] = this.createdOn;

        if (this.createdBy != null)
            jsonData['createdBy'] = this.createdBy.toJSON();

        jsonData['modifiedOn'] = this.modifiedOn;

        if (this.modifiedBy != null)
            jsonData['modifiedBy'] = this.modifiedBy.toJSON();

        if (this.handlingInformation != null)
            jsonData['handlingInformation'] = JSON.parse(JSON.stringify(this.handlingInformation));

        if (this.transportMeans != null)
            jsonData['transportMeans'] = JSON.parse(JSON.stringify(this.transportMeans));

        if (this.embedded != null)
            jsonData['embedded'] = this.embedded.toJSON();

        return jsonData;
    }
}
