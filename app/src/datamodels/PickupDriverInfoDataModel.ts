import { BaseDataModel } from './BaseDataModel';
import { DigitalSignatureDataModel } from './DigitalSignatureDataModel';

export class PickupDriverInfoDataModel extends BaseDataModel {
    personID: string;
    pickupDrivingLicenseNumber: string;
    pickupVehicleLicensePlate: string;
    pickupAirportPassNumber: string;
    pickupDriver: string;
    digitalSignature: DigitalSignatureDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.personID = _data['personID'];
            this.pickupDrivingLicenseNumber = _data['pickupDrivingLicenseNumber'];
            this.pickupVehicleLicensePlate = _data['pickupVehicleLicensePlate'];
            this.pickupAirportPassNumber = _data['pickupAirportPassNumber'];
            this.pickupDriver = _data['pickupDriver'];
            this.digitalSignature = _data['digitalSignature'];
        }
    }

    toJSON() {
        let jsonData = super.toJSON();
        delete jsonData['uniqueObjectId'];
        jsonData['personID'] = this.personID;
        jsonData['pickupDrivingLicenseNumber'] = this.pickupDrivingLicenseNumber;
        jsonData['pickupVehicleLicensePlate'] = this.pickupVehicleLicensePlate;
        jsonData['pickupAirportPassNumber'] = this.pickupAirportPassNumber;
        jsonData['pickupDriver'] = this.pickupDriver;
        if (this.digitalSignature != null)
        jsonData['digitalSignature'] = this.digitalSignature.toJSON();
        // console.log(jsonData);
        return jsonData;
    }
}
