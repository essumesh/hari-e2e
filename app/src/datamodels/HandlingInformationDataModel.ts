import { BaseDataModel } from './BaseDataModel';
import { SystemUsersDataModel } from './SystemUsersDataModel';

export class HandlingInformationDataModel extends BaseDataModel {
    /**
     * unique id for this handling-information
     */
    id?: string;

    source?: string;

    description?: string;

    createdOn?: string;

    createdBy?: SystemUsersDataModel;

    modifiedOn?: string;

    modifiedBy?: SystemUsersDataModel;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];
            this.source = _data['source'];
            this.description = _data['description'];
            this.createdOn = _data['createdOn'];

            let createdByJSON = _data['createdBy'];
            if (createdByJSON != null && createdByJSON.constructor === {}.constructor) {
                this.createdBy = new SystemUsersDataModel();
                this.createdBy.parse(createdByJSON);
            }

            this.modifiedOn = _data['modifiedOn'];

            let modifiedByJSON = _data['modifiedBy'];
            if (modifiedByJSON != null && modifiedByJSON.constructor === {}.constructor) {
                this.modifiedBy = new SystemUsersDataModel();
                this.modifiedBy.parse(modifiedByJSON);
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;
        jsonData['source'] = this.source;
        jsonData['description'] = this.description;
        jsonData['createdOn'] = this.createdOn;

        if (this.createdBy != null)
            jsonData['createdBy'] = this.createdBy.toJSON();

        jsonData['modifiedOn'] = this.modifiedOn;

        if (this.modifiedBy != null)
            jsonData['modifiedBy'] = this.modifiedBy.toJSON();

        return jsonData;
    }

}
