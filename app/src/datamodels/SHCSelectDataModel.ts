import { BaseDataModel } from './BaseDataModel';
import { SHCLookupsDataModel } from './SHCLookupsDataModel';
import { FetchOriginItemDataModel } from './FetchOriginItemDataModel';

export class SHCSelectDataModel extends BaseDataModel {
    specialHandlingCode?: Array<SHCLookupsDataModel> = [];
    selectedOrigin: FetchOriginItemDataModel;

    parse(_data: any) {
        let selectedOriginJSON = _data['selectedOrigin'];
        if (selectedOriginJSON != null && selectedOriginJSON.constructor === ''.constructor) {
            this.selectedOrigin = new FetchOriginItemDataModel();
            this.selectedOrigin.parse(selectedOriginJSON);
        }
        let specialHandlingCodeArray = _data['specialHandlingCode'];
        if (specialHandlingCodeArray != null && specialHandlingCodeArray instanceof Array) {
            for (let arrayItem of specialHandlingCodeArray) {
                let shcLookupsDataModel = new SHCLookupsDataModel();
                shcLookupsDataModel.parse(arrayItem);
                this.specialHandlingCode.push(shcLookupsDataModel);
              }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        if (this.selectedOrigin != null)
        jsonData['selectedOrigin'] = this.selectedOrigin.toJSON();
        if (this.specialHandlingCode != null)
        jsonData['specialHandlingCode'] = JSON.parse(JSON.stringify(this.specialHandlingCode));
        return jsonData;
    }

    fromJSON(_json: any) {
        super.fromJSON(_json);

        this.parse(_json);
    }
}
