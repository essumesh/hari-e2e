import { BaseDataModel } from './BaseDataModel';

export class LinksDataModel extends BaseDataModel {
    /**
     * URL for the resource
     */
    self?: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.self = _data['self'];
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['self'] = this.self;

        return jsonData;
    }
}
