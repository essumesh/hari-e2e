
import { BaseDataModel } from './BaseDataModel';
import { ItinerariesDataModel } from './ItinerariesDataModel';

export class ContainersEmbeddedDataModel extends BaseDataModel {

    containerItinerary?: Array<ItinerariesDataModel>;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let containerItineraryArray = _data['containerItinerary'];
            if (containerItineraryArray != null && containerItineraryArray instanceof Array) {
                for (let arrayItem of containerItineraryArray) {
                    let itinerariesDataModel = new ItinerariesDataModel();
                    itinerariesDataModel.parse(arrayItem);
                    this.containerItinerary.push(itinerariesDataModel);
                  }
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        if (this.containerItinerary != null)
        jsonData['containerItinerary'] = JSON.parse(JSON.stringify(this.containerItinerary));

        return jsonData;
    }
}
