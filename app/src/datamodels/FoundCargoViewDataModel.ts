import { BaseDataModel } from './BaseDataModel';
import { DateTime } from 'ionic-angular';
import { FoundCargoStorageLocationDataModel } from './FoundCargoStorageLocationDataModel';
import { FoundCargoPreferedLocationDataModel } from './FoundCargoPreferedLocationDataModel';
import { AirwaybillsDataModel } from './AirwaybillsDataModel';
import { HouseAirwaybillsDataModel } from './HouseAirwaybillsDataModel';
import { TransportMeansDataModel } from './TransportMeansDataModel';
import { AmountUnitDataModel } from './AmountUnitDataModel';
import { AppUIHelper } from '../helpers/AppUIHelper';
import { SHCLookupDataModel } from './SHCLookupDataModel';

export class FoundCargoViewDataModel extends BaseDataModel {
    storageLocation?: Array<FoundCargoStorageLocationDataModel> = [];
    preferedLocation?: string;
    origin?: string;
    foundPieces?: number;
    foundWeight?: number ;
    description?: string ;
    reportedOn?: Date ;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.origin = _data['origin'];
            this.foundPieces = _data['foundPieces'];
            this.foundWeight = _data['foundWeight'];
            this.description = _data['description'];
            this.reportedOn = _data['reportedOn'];
            this.preferedLocation = _data['preferedLocation'];
            let storageLocationArray = _data['storageLocation'];
            if (storageLocationArray != null && storageLocationArray instanceof Array) {
                for (let arrayItem of storageLocationArray) {
                    let foundCargoStorageLocationDataModel = new FoundCargoStorageLocationDataModel();
                    foundCargoStorageLocationDataModel.parse(arrayItem);
                    this.storageLocation.push(foundCargoStorageLocationDataModel);
                  }
            }

    }
 }
    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['origin'] = this.origin;
        jsonData['foundPieces'] = this.foundPieces;
        jsonData['description'] = this.description;
        jsonData['reportedOn'] = this.reportedOn;
        jsonData['preferedLocation'] = this.preferedLocation;
        jsonData['foundWeight'] = this.foundWeight;
        if (this.storageLocation != null)
        jsonData['storageLocation'] = JSON.parse(JSON.stringify(this.storageLocation));
        return jsonData;
    }

    doWeHaveShipmentDetailsMandatoryDataLeftToBeFilledIn_Condition1(): boolean {
         return (this.foundPieces == null
            || AppUIHelper.isNaN(this.foundPieces)
            || this.foundPieces <= 0);
    }

    doWeHaveShipmentDetailsMandatoryDataLeftToBeFilledIn_Condition2(): boolean {
        return (this.description == null || this.description === '');
    }

    doWeHaveSelectOriginMandatoryDataLeftToBeFilledIn_Condition1(): boolean {
        return this.origin == null;
    }

    doWeHaveAllOfShipmentDetailsMandatoryDataLeftToBeFilledIn(): boolean {
        return this.doWeHaveShipmentDetailsMandatoryDataLeftToBeFilledIn_Condition1() &&
        this.doWeHaveShipmentDetailsMandatoryDataLeftToBeFilledIn_Condition2();
    }

    doWeHaveAllOfSelectOriginMandatoryDataLeftToBeFilledIn(): boolean {
        return this.doWeHaveSelectOriginMandatoryDataLeftToBeFilledIn_Condition1();
    }

    doWeHaveAnySelectOriginMandatoryDataLeftToBeFilledIn(): boolean {
        return this.doWeHaveSelectOriginMandatoryDataLeftToBeFilledIn_Condition1();
    }

    textOfSelectOriginMandatoryDataThatIsFilledIn(selectedOrigin: string): string|undefined {
        let arrayOfTexts: Array<string> = new Array<string>();
        if (!this.doWeHaveSelectOriginMandatoryDataLeftToBeFilledIn_Condition1())
                arrayOfTexts.push(selectedOrigin);
        return this.arrayOfTextsInCSVFormat(arrayOfTexts);
    }

    textOfShipmentDetailsMandatoryDataThatIsFilledIn(): string|undefined {
        let arrayOfTexts: Array<string> = new Array<string>();
        if (!this.doWeHaveShipmentDetailsMandatoryDataLeftToBeFilledIn_Condition1())
                arrayOfTexts.push(AppUIHelper.getInstance().i18nText('pieces') + this.foundPieces);
        if (this.foundWeight)
                arrayOfTexts.push(AppUIHelper.getInstance().i18nText('weightTitle') + ': ' + this.foundWeight);
        return this.arrayOfTextsInCSVFormat(arrayOfTexts);
    }

    textOfSHCDataThatIsFilledIn(shcType: SHCLookupDataModel[]): string|undefined {
        let arrayOfTexts: Array<string> = new Array<string>();
        if (shcType.length !== 0) {
        for (let addSHC of shcType)
                arrayOfTexts.push(addSHC.code);
            }else {
                arrayOfTexts.push(AppUIHelper.getInstance().i18nText('shcsubtext'));
            }
        return this.arrayOfTextsInCSVFormat(arrayOfTexts);
    }

    doWeHaveAnyShipmentDetailsMandatoryDataLeftToBeFilledIn(): boolean {
        return this.doWeHaveShipmentDetailsMandatoryDataLeftToBeFilledIn_Condition1() ||
        this.doWeHaveShipmentDetailsMandatoryDataLeftToBeFilledIn_Condition2();
    }

    private arrayOfTextsInCSVFormat(_arrayOfTexts: Array<string>): string|undefined {
        let _arrayOfTextsInCSVFormat: string = null;
        for (let i = 0; i < _arrayOfTexts.length; i++) {
            if (_arrayOfTextsInCSVFormat == null) _arrayOfTextsInCSVFormat = '';
            if (i > 0) _arrayOfTextsInCSVFormat = _arrayOfTextsInCSVFormat + ' , ';
            _arrayOfTextsInCSVFormat = _arrayOfTextsInCSVFormat + _arrayOfTexts[i];
        }
        return _arrayOfTextsInCSVFormat;
    }
}
