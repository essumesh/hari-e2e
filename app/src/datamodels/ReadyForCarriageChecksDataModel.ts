import { BaseDataModel } from './BaseDataModel';

export class ReadyForCarriageChecksDataModel extends BaseDataModel {
    /**
     * unique id for this ready-for-carriage-checks
     */
    id?: string;

    name?: string;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            this.id = _data['id'];
            this.name = _data['name'];
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['id'] = this.id;
        jsonData['name'] = this.name;

        return jsonData;
    }
}
