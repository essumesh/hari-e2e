import { BaseDataModel } from './BaseDataModel';
import { WarehouseLocationDataModel } from './WarehouseLocationDataModel';

export class PickUpPiecesLocationDataModel extends BaseDataModel {
    deliveryPieces: string ;
    warehouseLocation: WarehouseLocationDataModel;
    euPalletCount: string;
    isChecked: boolean;

    parse(_data: any) {
       if (_data != null && _data.constructor === {}.constructor) {
           this.deliveryPieces = _data['deliveryPieces'];
           this.warehouseLocation = _data['warehouseLocation'];
           this.euPalletCount = _data['euPalletCount'];
           this.isChecked = _data['isChecked'];

       }
   }
}
