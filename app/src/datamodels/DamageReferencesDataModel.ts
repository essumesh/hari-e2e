import { BaseDataModel } from './BaseDataModel';
import { LookupDataModel } from './LookupDataModel';
import { LookupsDataModel } from './LookupsDataModel';

export class DamageReferencesDataModel extends BaseDataModel {
    outerPackingMaterials: LookupsDataModel = new LookupsDataModel();
    outerPackingTypes: LookupsDataModel = new LookupsDataModel();
    innerPackings: LookupsDataModel = new LookupsDataModel();
    damageToPackings: LookupsDataModel = new LookupsDataModel();
    conditionOfContents: LookupsDataModel = new LookupsDataModel();
    damageDiscovered: LookupsDataModel = new LookupsDataModel();
    damageDueTo: LookupsDataModel = new LookupsDataModel();
    recuperation: LookupsDataModel = new LookupsDataModel();

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let outerPackingMaterialsArray = _data['outerPackingMaterials'];
            if (outerPackingMaterialsArray != null && outerPackingMaterialsArray instanceof Array) {
                for (let arrayItem of outerPackingMaterialsArray) {
                    let lookupDataModel = new LookupDataModel();
                    lookupDataModel.parse(arrayItem);
                    this.outerPackingMaterials.addIfNotPresent(lookupDataModel);
                  }
            }

            let outerPackingTypesArray = _data['outerPackingTypes'];
            if (outerPackingTypesArray != null && outerPackingTypesArray instanceof Array) {
                for (let arrayItem of outerPackingTypesArray) {
                    let lookupDataModel = new LookupDataModel();
                    lookupDataModel.parse(arrayItem);
                    this.outerPackingTypes.addIfNotPresent(lookupDataModel);
                  }
            }

            let innerPackingsArray = _data['innerPackings'];
            if (innerPackingsArray != null && innerPackingsArray instanceof Array) {
                for (let arrayItem of innerPackingsArray) {
                    let lookupDataModel = new LookupDataModel();
                    lookupDataModel.parse(arrayItem);
                    this.innerPackings.addIfNotPresent(lookupDataModel);
                  }
            }

            let damageToPackingsArray = _data['damageToPackings'];
            if (damageToPackingsArray != null && damageToPackingsArray instanceof Array) {
                for (let arrayItem of damageToPackingsArray) {
                    let lookupDataModel = new LookupDataModel();
                    lookupDataModel.parse(arrayItem);
                    this.damageToPackings.addIfNotPresent(lookupDataModel);
                  }
            }

            let conditionOfContentsArray = _data['conditionOfContents'];
            if (conditionOfContentsArray != null && conditionOfContentsArray instanceof Array) {
                for (let arrayItem of conditionOfContentsArray) {
                    let lookupDataModel = new LookupDataModel();
                    lookupDataModel.parse(arrayItem);
                    this.conditionOfContents.addIfNotPresent(lookupDataModel);
                  }
            }

            let damageDiscoveredArray = _data['damageDiscovered'];
            if (damageDiscoveredArray != null && damageDiscoveredArray instanceof Array) {
                for (let arrayItem of damageDiscoveredArray) {
                    let lookupDataModel = new LookupDataModel();
                    lookupDataModel.parse(arrayItem);
                    this.damageDiscovered.addIfNotPresent(lookupDataModel);
                  }
            }

            let damageDueToArray = _data['damageDueTo'];
            if (damageDueToArray != null && damageDueToArray instanceof Array) {
                for (let arrayItem of damageDueToArray) {
                    let lookupDataModel = new LookupDataModel();
                    lookupDataModel.parse(arrayItem);
                    this.damageDueTo.addIfNotPresent(lookupDataModel);
                  }
            }

            let recuperationArray = _data['recuperation'];
            if (recuperationArray != null && recuperationArray instanceof Array) {
                for (let arrayItem of recuperationArray) {
                    let lookupDataModel = new LookupDataModel();
                    lookupDataModel.parse(arrayItem);
                    this.recuperation.addIfNotPresent(lookupDataModel);
                  }
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        if (this.outerPackingMaterials != null)
            jsonData['outerPackingMaterials'] = JSON.parse(JSON.stringify(this.outerPackingMaterials.qualifierType));
        if (this.outerPackingTypes != null)
            jsonData['outerPackingTypes'] = JSON.parse(JSON.stringify(this.outerPackingTypes.qualifierType));
        if (this.innerPackings != null)
            jsonData['innerPackings'] = JSON.parse(JSON.stringify(this.innerPackings.qualifierType));
        if (this.damageToPackings != null)
            jsonData['damageToPackings'] = JSON.parse(JSON.stringify(this.damageToPackings.qualifierType));
        if (this.conditionOfContents != null)
            jsonData['conditionOfContents'] = JSON.parse(JSON.stringify(this.conditionOfContents.qualifierType));
        if (this.damageDiscovered != null)
            jsonData['damageDiscovered'] = JSON.parse(JSON.stringify(this.damageDiscovered.qualifierType));
        if (this.damageDueTo != null)
            jsonData['damageDueTo'] = JSON.parse(JSON.stringify(this.damageDueTo.qualifierType));
        if (this.recuperation != null)
            jsonData['recuperation'] = JSON.parse(JSON.stringify(this.recuperation.qualifierType));

        return jsonData;
    }
}
