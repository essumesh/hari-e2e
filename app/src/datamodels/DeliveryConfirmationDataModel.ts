import { BaseDataModel } from './BaseDataModel';

export class DeliveryConfirmationItemDataModel extends BaseDataModel {
airwaybillNumber: string;
collectedPieces: string;

   parse(_data: any) {
       if (_data != null && _data.constructor === {}.constructor) {
           this.airwaybillNumber = _data['airwaybillNumber'];
           this.collectedPieces = _data['collectedPieces'];
       }
   }

}

export class DeliveryConfirmationDataModel extends BaseDataModel {
   deliveryConfirmationArray: Array<DeliveryConfirmationItemDataModel> = [];

   parse(_data: any) {
       if (_data != null && _data.constructor === {}.constructor) {
           let pickupDeliveryArray = _data['deliveryConfirmationArray'];
           if (pickupDeliveryArray != null && pickupDeliveryArray instanceof Array) {
               for (let arrayItem of pickupDeliveryArray) {
                   let pickupViewData = new DeliveryConfirmationItemDataModel();
                   pickupViewData.parse(arrayItem);
                   this.deliveryConfirmationArray.push(pickupViewData);
                 }
           }
       }
   }
}
