
import { BaseDataModel } from './BaseDataModel';
import { SplitsDataModel } from './SplitsDataModel';

export class PartsEmbeddedDataModel extends BaseDataModel {

    splits?: Array<SplitsDataModel>;

    parse(_data: any) {
        if (_data != null && _data.constructor === {}.constructor) {
            let splitsArray = _data['splits'];
            if (splitsArray != null && splitsArray instanceof Array) {
                for (let arrayItem of splitsArray) {
                    let splitsDataModel = new SplitsDataModel();
                    splitsDataModel.parse(arrayItem);
                    this.splits.push(splitsDataModel);
                  }
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();

        if (this.splits != null)
            jsonData['splits'] = JSON.parse(JSON.stringify(this.splits));

        return jsonData;
    }
}
