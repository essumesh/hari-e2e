import { BaseUIView } from '../BaseUIView';
import { ScreenOrientationHelper } from '../../../helpers/ScreenOrientationHelper';
import { PlatformHelper } from '../../../helpers/PlatformHelper';

export class BaseUIPageView extends BaseUIView {
    protected toRegisterForHardwareBackButtonAction: boolean = false;
    private hardwareBackButtonAction: any;

    constructor() {
        super();
    }

    ionViewDidLoad() {
        super.ionViewDidLoad();

        if (!PlatformHelper.isDevice_A_Desktop()) {
            if (PlatformHelper.isDevice_A_iPad() ||
            (PlatformHelper.isPlatform_Android() && PlatformHelper.isDevice_A_Tablet()))
                ScreenOrientationHelper.unlock_ScreenOrientation();
            else if (PlatformHelper.isDevice_A_Phone())
                ScreenOrientationHelper.lock_ScreenOrientation_To_Portrait();
        }
    }

    ionViewWillEnter() {
        super.ionViewWillEnter();
        if (this.toRegisterForHardwareBackButtonAction) {
            this.hardwareBackButtonAction = PlatformHelper.getInstance().platform.registerBackButtonAction(() => {
                this.onHardwareBackButtonPressed();
            }, 1);
        }
    }

    ionViewDidEnter() {
        super.ionViewDidEnter();
    }

    ionViewWillLeave() {
        super.ionViewWillLeave();
        if (this.toRegisterForHardwareBackButtonAction) {
            /* tslint:disable:no-unused-expression */
            this.hardwareBackButtonAction && this.hardwareBackButtonAction();
            /* tslint:enable:no-unused-expression */
        }
    }

    ionViewDidLeave() {
        super.ionViewDidLeave();
    }

    ionViewWillUnload() {
        super.ionViewWillUnload();
    }
}
