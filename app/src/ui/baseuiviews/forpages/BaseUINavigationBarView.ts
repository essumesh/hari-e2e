import { ViewChild } from '@angular/core';
import { BaseUIPageView } from './BaseUIPageView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { NavigationBarComponent } from '../../components/navigation-bar/navigation-bar';

export class BaseUINavigationBarView extends BaseUIPageView {
    protected navigationBarViewModel: NavigationBarViewModel;
    @ViewChild('navigationBarComponent') protected navigationBarComponent: NavigationBarComponent;

    constructor() {
        super();
    }

    ionViewDidLoad() {
        super.ionViewDidLoad();
    }

    ionViewWillEnter() {
        super.ionViewWillEnter();

        if (this.navigationBarComponent != null)
            this.navigationBarComponent.ionViewWillEnter();
    }

    ionViewWillLeave() {
        super.ionViewWillLeave();

        if (this.navigationBarComponent != null)
            this.navigationBarComponent.ionViewWillLeave();
    }
}
