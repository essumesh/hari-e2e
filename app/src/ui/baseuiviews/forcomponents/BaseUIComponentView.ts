import { BaseUIView } from '../BaseUIView';

export class BaseUIComponentView extends BaseUIView {
    constructor() {
        super();
    }

    ionViewDidLoad() {
        super.ionViewDidLoad();
    }

    ionViewWillEnter() {
        super.ionViewWillEnter();
    }

    ionViewDidEnter() {
        super.ionViewDidEnter();
    }

    ionViewWillLeave() {
        super.ionViewWillLeave();
    }

    ionViewDidLeave() {
        super.ionViewDidLeave();
    }

    ionViewWillUnload() {
        super.ionViewWillUnload();
    }
}
