import { ScreenOrientationHelper, ScreenOrientationType } from '../../helpers/ScreenOrientationHelper';
import { KeyboardHelper } from '../../helpers/KeyboardHelper';

export class BaseUIView {
    private _screenOrientationChangeSubscription: any;
    private _showHideKeyboardSubscription: any;

    constructor() {
    }

    ionViewDidLoad() {
    }

    ionViewWillEnter() {
    }

    ionViewDidEnter() {
        this.cancelScreenOrientationSubscription();
        this._screenOrientationChangeSubscription =
        ScreenOrientationHelper.getInstance().screenOrientationChange.subscribe
        ((_screenOrientationType: ScreenOrientationType) => {
            if (_screenOrientationType === ScreenOrientationType.PORTRAIT)
                this.onOrientationChangedTo_Portrait();
            else if (_screenOrientationType === ScreenOrientationType.LANDSCAPE)
                this.onOrientationChangedTo_Landscape();
        });
    }

    ionViewWillLeave() {
    }

    ionViewDidLeave() {
        this.cancelScreenOrientationSubscription();
    }

    ionViewWillUnload() {
    }

    ionViewCanEnter(): boolean {
        return true;
    }

    ionViewCanLeave(): boolean {
        return true;
    }

    onHardwareBackButtonPressed() {
    }

    onOrientationChangedTo_Portrait() {
    }

    onOrientationChangedTo_Landscape() {
    }

    onKeyboardShowHideEvent(_keyboard: any) {
    }

    subscribeToKeyboardShowHideEvents() {
        this.unsubscribeToKeyboardShowHideEvents();

        this._showHideKeyboardSubscription =
        KeyboardHelper.getInstance().showHide.subscribe((_keyboard: any) => {
            this.onKeyboardShowHideEvent(_keyboard);
        });
      }

    unsubscribeToKeyboardShowHideEvents() {
        if (this._showHideKeyboardSubscription != null)
          this._showHideKeyboardSubscription.unsubscribe();
        this._showHideKeyboardSubscription = null;
    }

    private cancelScreenOrientationSubscription() {
        if (this._screenOrientationChangeSubscription != null)
            this._screenOrientationChangeSubscription.unsubscribe();
        this._screenOrientationChangeSubscription = null;
    }
}
