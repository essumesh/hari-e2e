import { AppUIHelper } from '../../helpers/AppUIHelper';

export class PageConstants {

     public static TO_USE_SCREEN_ID: number = 1;
     static defaultPage: number = 0;
     static damageReportSearchPage: number = 1;
     static irregularitySearchPage: number = 2;
     static deliveriesAndTransferPage: number = 3;
     static airlineTransferPage: number = 4;
    public static getScreenID(_screenName: string) {
        if (_screenName === AppUIHelper.getInstance().i18nText('damage')) {
            this.TO_USE_SCREEN_ID = this.damageReportSearchPage;
        } else if (_screenName === AppUIHelper.getInstance().i18nText('irregularities')) {
            this.TO_USE_SCREEN_ID = this.irregularitySearchPage;
        } else if (_screenName === AppUIHelper.getInstance().i18nText('receiptbondtransfer')) {
            this.TO_USE_SCREEN_ID = this.deliveriesAndTransferPage;
        }else if (_screenName === AppUIHelper.getInstance().i18nText('airlinetransfer')) {
            this.TO_USE_SCREEN_ID = this.airlineTransferPage;
        } else {
            this.TO_USE_SCREEN_ID = this.damageReportSearchPage;
        }
    }
}
