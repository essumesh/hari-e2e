import { BaseViewModel } from './BaseViewModel';
import { GenericListItemViewModel } from './GenericListItemViewModel';

export class ListPopoverViewModel extends BaseViewModel {
    title: string;
    genericListItemsViewModel: Array<GenericListItemViewModel>;
    buttonText: string;

    constructor(title: string, genericListItemsViewModel: Array<GenericListItemViewModel>, buttonText: string) {
        super();
        this.title = title;
        this.genericListItemsViewModel = genericListItemsViewModel;
        this.buttonText = buttonText;
    }
}
