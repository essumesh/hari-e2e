import { BaseViewModel } from './BaseViewModel';
import { LinkMediaDataModel } from '../../datamodels/LinkMediaDataModel';

export class EmailCheckBoxViewModel extends BaseViewModel {
    linkMediaDataModel: LinkMediaDataModel;
    isChecked: boolean;

    constructor(linkMediaDataModel: LinkMediaDataModel, isChecked: boolean) {
        super();

        this.linkMediaDataModel = linkMediaDataModel;
        this.isChecked = isChecked;
    }
}

export class EmailCheckBoxesViewModel extends BaseViewModel {
    checkBoxViewModels: Array<EmailCheckBoxViewModel> = [];

    constructor() {
        super();
    }

    addCheckBoxViewModel(linkMediaDataModel: LinkMediaDataModel, isChecked: boolean) {
        this.checkBoxViewModels.push(new EmailCheckBoxViewModel(linkMediaDataModel, isChecked));
    }
}
