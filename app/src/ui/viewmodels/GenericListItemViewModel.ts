import { BaseViewModel } from './BaseViewModel';
import { ImagesDataModel } from '../../datamodels/ImagesDataModel';
import { LinkMediaDataModel } from '../../datamodels/LinkMediaDataModel';

export enum GenericListItemViewModelType {
    MainText,
    Icon_MainText_SubText,
    HorizontalListing_Of_Media_ThumbNails,
    NotificationGenericListItemViewModel,
    LeftText_MainText_SubText,
}

export class GenericListItemViewModel extends BaseViewModel {
    icon: string;
    mainText: string;
    subText: string;
    horizontalListingOfMediaThumbNailsURLs: ImagesDataModel = new ImagesDataModel();
    marginBottom: number = 0;
    borderRadius: number = 0;
    backgroundColor: string = '#fff';
    shouldShowRightArrow: boolean = false;
    shouldTakeActionForHamBurgerMenuClose: boolean = false;
    shouldShowMandatoryStar: boolean = false;
    thumbnailBadgeImage: string;
    leftTopText: string;
    leftBottomText: string;
    textWrap: boolean = false;

    private _index: number = 0;
    private _tag: string = '';
    private _genericListItemViewModelType: GenericListItemViewModelType;

    constructor(_index: number,
                _tag: string,
                _genericListItemViewModelType: GenericListItemViewModelType) {
        super();

        this._index = _index;
        this._tag = _tag;
        this._genericListItemViewModelType = _genericListItemViewModelType;

    }

    public get index(): number  {
        return this._index;
    }

    public get tag(): string  {
        return this._tag;
    }

    public get genericListItemViewModelType(): GenericListItemViewModelType {
        return this._genericListItemViewModelType;
    }

    public isGenericListItemViewModelType_MainText(): boolean {
        return this.genericListItemViewModelType ===
               GenericListItemViewModelType.MainText;
    }

    public isGenericListItemViewModelType_Icon_MainText_SubText(): boolean {
        return this.genericListItemViewModelType ===
               GenericListItemViewModelType.Icon_MainText_SubText;
    }

    public isGenericListItemViewModelType_HorizontalListing_Of_Media_ThumbNails(): boolean {
        return this.genericListItemViewModelType ===
               GenericListItemViewModelType.HorizontalListing_Of_Media_ThumbNails;
    }

    public isGenericListItemViewModelType_NotificationGenericListItemViewModel(): boolean {
        return this.genericListItemViewModelType ===
               GenericListItemViewModelType.NotificationGenericListItemViewModel;
    }

    public isGenericListItemViewModelType_LeftText_MainText_SubText(): boolean {
        return this.genericListItemViewModelType ===
               GenericListItemViewModelType.LeftText_MainText_SubText;
    }
}
