import { BaseViewModel } from './BaseViewModel';
import { GenericListItemViewModel } from './GenericListItemViewModel';

export class NavigationBarViewModel extends BaseViewModel {
    hamburgerIcon: boolean;
    title: string;
    toShowNotificationsIcon: boolean;
    toShowOverFlowIcon: boolean;
    toShowOnlineOfflineIcon: boolean;
    toShowBackButton: boolean = false;
    toShowTitleOnLeft: boolean = true;
    private _overFlowIconGenericListItemViewModels: Array<GenericListItemViewModel> = [];
    private _overFlowIconGenericListItemViewModelsCallBacks:
    ((_genericListItemViewModel: GenericListItemViewModel) => void)[] = [];

    constructor(hamburgerIcon: boolean,
                title: string,
                toShowNotificationsIcon: boolean,
                toShowOverFlowIcon: boolean,
                _toShowOnlineOfflineIcon: boolean) {
        super();

        _toShowOnlineOfflineIcon = false;

        this.hamburgerIcon = hamburgerIcon;
        this.title = title;
        this.toShowNotificationsIcon = toShowNotificationsIcon;

        this.toShowOverFlowIcon = toShowOverFlowIcon;
        this.toShowOnlineOfflineIcon = _toShowOnlineOfflineIcon;
    }

    get overFlowIconGenericListItemViewModels(): Array<GenericListItemViewModel> {
        return this._overFlowIconGenericListItemViewModels;
    }

    set overFlowIconGenericListItemViewModels(value: Array<GenericListItemViewModel> ) {
        if (value == null) value = [];
        this._overFlowIconGenericListItemViewModels = value;
    }

    addOverFlowIconGenericListItemViewModel(_genericListItemViewModel: GenericListItemViewModel,
                                            _overFlowIconGenericListItemViewModelsCallBack:
                                            (_genericListItemViewModel: GenericListItemViewModel) => void):
                                            GenericListItemViewModel {
        this._overFlowIconGenericListItemViewModels.push(_genericListItemViewModel);
        this._overFlowIconGenericListItemViewModelsCallBacks.push(_overFlowIconGenericListItemViewModelsCallBack);
        return _genericListItemViewModel;
    }

    removeOverFlowIconGenericListItemViewModel(_genericListItemViewModel: GenericListItemViewModel) {
        for (let i = 0; i < this._overFlowIconGenericListItemViewModels.length ; i++) {
            if (this._overFlowIconGenericListItemViewModels[i] === _genericListItemViewModel) {
                this._overFlowIconGenericListItemViewModels.splice(i, 1);
                this._overFlowIconGenericListItemViewModelsCallBacks.splice(i, 1);
                break;
            }
        }
    }

    overFlowIconGenericListItemViewModelsCallBackFor(_genericListItemViewModel: GenericListItemViewModel):
    ((_genericListItemViewModel: GenericListItemViewModel) => void) {
        for (let i = 0; i < this._overFlowIconGenericListItemViewModels.length ; i++) {
            if (this._overFlowIconGenericListItemViewModels[i] === _genericListItemViewModel) {
                return this._overFlowIconGenericListItemViewModelsCallBacks[i];
            }
        }
    }
}
