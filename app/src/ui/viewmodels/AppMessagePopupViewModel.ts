import { BaseViewModel } from './BaseViewModel';

export enum AppMessagePopupViewModelType {
    LoadingPopup,
    MessagePopup,
}

export class AppMessagePopupViewModel extends BaseViewModel {
  text: string;
  imagePath: string;
  appMessagePopupViewModelType: AppMessagePopupViewModelType;

  constructor(text: string,
              imagePath: string,
              appMessagePopupViewModelType: AppMessagePopupViewModelType) {
          super();
          this.text = text;
          this.imagePath = imagePath;
          this.appMessagePopupViewModelType = appMessagePopupViewModelType;
  }
}
