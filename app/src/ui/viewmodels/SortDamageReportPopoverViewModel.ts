import { BaseViewModel } from './BaseViewModel';

export class SortDamageReportPopoverViewModel extends BaseViewModel {
    allCheckBox: boolean = false;
    dateCheckBox: boolean = false;
    stationCheckBox: boolean = false;

    checkUnCheckHappenedOnAllCheckBox() {
        if (this.allCheckBox) {
          this.dateCheckBox = true;
          this.stationCheckBox = true;
        } else {
          this.dateCheckBox = false;
          this.stationCheckBox = false;
        }
    }

    checkUnCheckHappenedOnCheckBoxes_OtherThan_AllCheckBox() {
        let checkBoxesArray: Array<boolean> = [this.dateCheckBox,
                                                this.stationCheckBox];

        const areAllValuesInCheckBoxesArrayTrue = (() => {
            for (let checkBoxValue of checkBoxesArray) {
            if (!checkBoxValue) return false;
            }
            return true;
        });

        const isAnyOneValueInCheckBoxesArrayFalse = (() => {
            for (let checkBoxValue of checkBoxesArray) {
            if (!checkBoxValue) return true;
            }
            return false;
        });

        if (areAllValuesInCheckBoxesArrayTrue()) {
            this.allCheckBox = true;
        }
        if (isAnyOneValueInCheckBoxesArrayFalse()) {
            this.allCheckBox = false;
        }
    }
}
