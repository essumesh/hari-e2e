import { BaseViewModel } from './BaseViewModel';
import { RadioButtonsViewModel } from './RadioButtonsViewModel';

export class FilterRadioButtonsPopoverViewModel extends BaseViewModel {
    title: string;
    radioButtonsViewModel: RadioButtonsViewModel;
    buttonText: string;

    constructor(title: string, radioButtonsViewModel: RadioButtonsViewModel, buttonText: string) {
        super();
        this.title = title;
        this.radioButtonsViewModel = radioButtonsViewModel;
        this.buttonText = buttonText;
    }
}
