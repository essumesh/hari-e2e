import { BaseViewModel } from './BaseViewModel';

export class RadioButtonViewModel extends BaseViewModel {
    name: string;
    isSelected: boolean;
    thumbnailBadgeImage?: string;
    subText?: string;
    extras?: any;

    constructor(name: string, isSelected: boolean, extras?: any, thumbnailBadgeImage?: string, subText?: string) {
        super();

        this.name = name;
        this.isSelected = isSelected;
        this.extras = extras;
        this.thumbnailBadgeImage = thumbnailBadgeImage;
        this.subText = subText;
    }
}

export class RadioButtonsViewModel extends BaseViewModel {
    radioButtonViewModels: Array<RadioButtonViewModel> = [];
    uniqueObjectIdOfRadioButtonViewModel: string;

    constructor() {
        super();
    }

    addRadioButtonViewModel(name: string, isSelected: boolean, extras?: any,
        thumbnailBadgeImage?: string, subText?: string) {
        let radioButtonViewModel: RadioButtonViewModel =
        new RadioButtonViewModel(name, isSelected, extras, thumbnailBadgeImage, subText);
        if (radioButtonViewModel.isSelected) {
            this.uniqueObjectIdOfRadioButtonViewModel = radioButtonViewModel.getUniqueObjectId();
        }
        this.radioButtonViewModels.push(radioButtonViewModel);
    }

    setSelectedRadioButtonViewModel(radioButtonViewModel: RadioButtonViewModel) {
        if (radioButtonViewModel.isSelected) {
            this.uniqueObjectIdOfRadioButtonViewModel = radioButtonViewModel.getUniqueObjectId();
        } else {
            this.uniqueObjectIdOfRadioButtonViewModel = null;
        }
    }
}
