import { GenericListItemViewModel } from './GenericListItemViewModel';
import { GenericListItemViewModelType } from './GenericListItemViewModel';
import { LocalStorageModel, LocalStorageModelSyncStatus }
from '../../localstorage/localstoragemodels/LocalStorageModel';
import { AppUIHelper } from '../../helpers/AppUIHelper';
import { BaseDataObject } from '../../network/dataobjects/BaseDataObject';

export class NotificationGenericListItemViewModel extends GenericListItemViewModel {
    action: string;
    private _localStorageModel: LocalStorageModel;

    constructor(_index: number,
                _tag: string,
                _localStorageModel: LocalStorageModel) {
        super(_index, _tag, GenericListItemViewModelType.NotificationGenericListItemViewModel);
        this._localStorageModel = _localStorageModel;
    }

    get localStorageModel(): LocalStorageModel {
        return this._localStorageModel;
    }

    set localStorageModel(_localStorageModel: LocalStorageModel) {
        this._localStorageModel = _localStorageModel;
    }

    syncStatus(): string {
        if (this._localStorageModel.localStorageModelSyncStatus
            === LocalStorageModelSyncStatus.SyncPending)
            return AppUIHelper.getInstance().i18nText('pendingSync');
        else if (this._localStorageModel.localStorageModelSyncStatus
            === LocalStorageModelSyncStatus.SyncFailed) {
            let dataObject: BaseDataObject = this._localStorageModel.objectToBeLocallyStored;
            if (dataObject.serverErrorDataModel != null
                && dataObject.serverErrorDataModel.errorText != null
                && AppUIHelper.isString(dataObject.serverErrorDataModel.errorText)
                && dataObject.serverErrorDataModel.errorText.length > 0)
                return dataObject.serverErrorDataModel.errorText;
            else
                return AppUIHelper.getInstance().i18nText('syncFailed');
        } else if (this._localStorageModel.localStorageModelSyncStatus
            === LocalStorageModelSyncStatus.SyncInProgress)
            return AppUIHelper.getInstance().i18nText('syncInProgress');
        return null;
    }

    syncStatusColor(): string {
        if (this._localStorageModel.localStorageModelSyncStatus
            === LocalStorageModelSyncStatus.SyncPending)
            return '#cf8f4d';
        else if (this._localStorageModel.localStorageModelSyncStatus
            === LocalStorageModelSyncStatus.SyncFailed)
            return '#ff0000';
        else if (this._localStorageModel.localStorageModelSyncStatus
            === LocalStorageModelSyncStatus.SyncInProgress)
            return '#00ff00';
        return null;
    }

    syncStatusIcon(): string {
        if (this._localStorageModel.localStorageModelSyncStatus
            === LocalStorageModelSyncStatus.SyncPending)
            return 'assets/images/info.svg';
        else if (this._localStorageModel.localStorageModelSyncStatus
            === LocalStorageModelSyncStatus.SyncFailed)
            return 'assets/images/info.svg';
        else if (this._localStorageModel.localStorageModelSyncStatus
            === LocalStorageModelSyncStatus.SyncInProgress)
            return 'assets/images/sync.svg';
        return null;
    }
}
