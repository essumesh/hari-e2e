import { BaseViewModel } from './BaseViewModel';

export class CheckBoxViewModel extends BaseViewModel {
    name: string;
    isSelected: boolean;
    extras?: any;

    constructor(name: string, isSelected: boolean, extras?: any) {
        super();

        this.name = name;
        this.isSelected = isSelected;
        this.extras = extras;
    }
}

export class CheckBoxesViewModel extends BaseViewModel {
    checkBoxViewModels: Array<CheckBoxViewModel> = [];

    constructor() {
        super();
    }

    addCheckBoxViewModel(name: string, isSelected: boolean, extras?: any) {
        this.checkBoxViewModels.push(new CheckBoxViewModel(name, isSelected, extras));
    }

    selectedCheckBoxViewModels(): Array<CheckBoxViewModel> {
        let checkBoxViewModelsTemp: Array<CheckBoxViewModel> = [];
        this.checkBoxViewModels.forEach((_checkBoxViewModel, _index) => {
            if (_checkBoxViewModel.isSelected)
                checkBoxViewModelsTemp.push(_checkBoxViewModel);
        });
        return checkBoxViewModelsTemp;
    }

    doesASelectedCheckBoxViewModelContainThisText(_text: string): boolean {
        let doesitContain:  boolean = false;
        const selectedCheckBoxViewModels: Array<CheckBoxViewModel> =
        this.selectedCheckBoxViewModels();

        for (let i = 0 ; i < selectedCheckBoxViewModels.length ; i++) {
            if (selectedCheckBoxViewModels[i].name === _text) {
                doesitContain = true;
                break;
            }
        }
        return doesitContain;
    }

    selectCheckBoxViewModel(_checkBoxViewModel: CheckBoxViewModel) {
        _checkBoxViewModel.isSelected = true;
    }

    unSelectCheckBoxViewModel(_checkBoxViewModel: CheckBoxViewModel) {
        _checkBoxViewModel.isSelected = false;
    }

    unSelectAllCheckBoxViewModel() {
        for (let i = 0 ; i < this.checkBoxViewModels.length ; i++)
            this.unSelectCheckBoxViewModel(this.checkBoxViewModels[i]);
    }
}
