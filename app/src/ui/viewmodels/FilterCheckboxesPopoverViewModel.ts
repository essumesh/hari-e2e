import { BaseViewModel } from './BaseViewModel';
import { CheckBoxesViewModel } from './CheckBoxesViewModel';

export class FilterCheckboxesPopoverViewModel extends BaseViewModel {
    title: string;
    checkBoxesViewModel: CheckBoxesViewModel;
    buttonText: string;

    constructor(title: string, checkBoxViewModel: CheckBoxesViewModel, buttonText: string) {
        super();
        this.title = title;
        this.checkBoxesViewModel = checkBoxViewModel;
        this.buttonText = buttonText;
    }
}
