import { AppUIHelper } from '../../helpers/AppUIHelper';

export class BaseViewModel {
    private uniqueObjectId: string = this.getUniqueObjectId();

    constructor() {
    }

    getUniqueObjectId(): string {
        if (this.uniqueObjectId == null) {
            this.uniqueObjectId = this.constructor.name + '_' + AppUIHelper.guid();
        }
        return this.uniqueObjectId;
    }
}
