import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DateParserHelper } from '../../../helpers/DateParserHelper';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { BaseUIComponentView } from '../../baseuiviews/forcomponents/BaseUIComponentView';
import { IrregularitiesDataModel } from '../../../datamodels/IrregularitiesDataModel';

@Component({
  selector: 'champ-irregularity-report-list-item',
  templateUrl: 'irregularity-report-list-item.html',
})
export class IrregularityReportListItemComponent extends BaseUIComponentView {
  @Input() irregularityReportsDataModel: IrregularitiesDataModel;
  @Output() tapEvent: EventEmitter<IrregularitiesDataModel> =
  new EventEmitter<IrregularitiesDataModel>();

  dateToBeShown(): string {
    return DateParserHelper.parseDateToString_InFormat_ddMMMyy(this.irregularityReportsDataModel.createdOn);
  }

  timeToBeShown(): string {
    return DateParserHelper.parseDateToString_InFormat_HHmm(this.irregularityReportsDataModel.createdOn);
  }

  piecesToBeShown(): string {
    return this.irregularityReportsDataModel.pieces != null ?
           AppUIHelper.getInstance().i18nText('pieces') + this.irregularityReportsDataModel.pieces.toString() :
           '';
  }

  weightToBeShown(): string {
    return this.irregularityReportsDataModel.weight != null &&
           this.irregularityReportsDataModel.weight.unit != null &&
           this.irregularityReportsDataModel.weight.amount != null ?
           this.irregularityReportsDataModel.weight.unit + ': '
           + this.irregularityReportsDataModel.weight.amount.toString() :
           '';
  }

  reportingLocationToBeShown(): string {
    return this.irregularityReportsDataModel.reportingLocation != null ?
            this.irregularityReportsDataModel.reportingLocation.code.toString() :
           '';
  }
  createdByToBeShown(): string {
    return this.irregularityReportsDataModel.createdBy != null ?
            this.irregularityReportsDataModel.createdBy.fullName.toString() :
           '';
  }

  irregularityDetailsToBeShown(): string {
    return this.irregularityReportsDataModel.irregularityDetails != null ?
    AppUIHelper.getInstance().i18nText('irregularityinfo') + ': ' +
    this.irregularityReportsDataModel.irregularityDetails.code.toString() + ' ' +
    this.irregularityReportsDataModel.irregularityDetails.name.toString() :
           '';
  }
  onTap() {
    this.tapEvent.emit(this.irregularityReportsDataModel);
  }
}
