import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { RadioButtonsViewModel, RadioButtonViewModel } from '../../viewmodels/RadioButtonsViewModel';
import { BaseUIComponentView } from '../../baseuiviews/forcomponents/BaseUIComponentView';

@Component({
  selector: 'champ-radio-buttons-item',
  templateUrl: 'radio-buttons-item.html',
})
export class RadioButtonsItemComponent extends BaseUIComponentView {
  @Input() radioButtonsViewModel: RadioButtonsViewModel;
  @Output() radioButtonViewModelTapEvent: EventEmitter<RadioButtonViewModel> =
  new EventEmitter<RadioButtonViewModel>();

  radioButtonTapped(_radioButtonViewModel: RadioButtonViewModel) {
    this.clearAllSelections();
    if (this.radioButtonsViewModel.uniqueObjectIdOfRadioButtonViewModel ===
        _radioButtonViewModel.getUniqueObjectId()) {
        _radioButtonViewModel.isSelected = true;
    }
    this.radioButtonViewModelTapEvent.emit(_radioButtonViewModel);
  }

  private clearAllSelections() {
    for (let radioButtonViewModel of this.radioButtonsViewModel.radioButtonViewModels) {
        radioButtonViewModel.isSelected = false;
    }
  }
}
