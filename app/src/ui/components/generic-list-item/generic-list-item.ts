import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { GenericListItemViewModel } from '../../viewmodels/GenericListItemViewModel';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';
import { BaseUIComponentView } from '../../baseuiviews/forcomponents/BaseUIComponentView';
import { LinkMediaDataModel } from '../../../datamodels/LinkMediaDataModel';

@Component({
  selector: 'champ-generic-list-item',
  templateUrl: 'generic-list-item.html',
})
export class GenericListItemComponent extends BaseUIComponentView {
  @Input() genericListItemViewModels: Array<GenericListItemViewModel>;
  @Output() tapEvent: EventEmitter<GenericListItemViewModel> =
  new EventEmitter<GenericListItemViewModel>();
  @Output() tapEventOnDeleteButtonofHorizontalListingOfMediaThumbNailsURLs: EventEmitter<number> =
  new EventEmitter<number>();
  @Output() tapEventOnUserAction: EventEmitter<GenericListItemViewModel> =
  new EventEmitter<GenericListItemViewModel>();
  private tapEventFunction: (_genericListItemViewModel: GenericListItemViewModel) => void;
  private toRenderUnderScroll: boolean = false;

  constructor(public viewController: ViewController, public navParams: NavParams) {
    super();

    if (this.navParams.get('genericListItemViewModels') != null) {
      this.genericListItemViewModels = this.navParams.get('genericListItemViewModels');
      this.tapEventFunction = this.navParams.get('tapEvent');
    }
    if (this.navParams.get('toRenderUnderScroll') != null) {
      this.toRenderUnderScroll = this.navParams.get('toRenderUnderScroll');
    }
  }

  onTap(_genericListItemViewModel: GenericListItemViewModel) {
    if (this.tapEventFunction != null) {
      this.tapEventFunction(_genericListItemViewModel);
    } else {
      this.tapEvent.emit(_genericListItemViewModel);
    }
  }

  deleteButtonofHorizontalListingOfMediaThumbNailsURLsClicked(event: any, index: number) {
    event.stopPropagation();
    this.tapEventOnDeleteButtonofHorizontalListingOfMediaThumbNailsURLs.emit(index);
  }

  actionClicked(_genericListItemViewModel: GenericListItemViewModel) {
    this.tapEventOnUserAction.emit(_genericListItemViewModel);
  }

  private horizontalListingOfMediaThumbNailsURLsDataForIndex
  (_linkMediaDataModel: LinkMediaDataModel,
   _index: number): string {
    if (_linkMediaDataModel.haveWeGotMediaDataFromServer()) {
      return 'data:image/jpeg;base64,' + _linkMediaDataModel.thumbnailBase64Data;
    } else {
      return _linkMediaDataModel.fromUIhref;
    }
  }
}
