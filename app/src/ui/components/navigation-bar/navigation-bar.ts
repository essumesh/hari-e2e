import { Component, Input, Output, EventEmitter, NgZone } from '@angular/core';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { ModalController, PopoverController, ViewController } from 'ionic-angular';
import { NotificationsPage } from '../../pages/notifications/notifications';
import { GenericListItemComponent } from '../generic-list-item/generic-list-item';
import { GenericListItemViewModel, GenericListItemViewModelType } from '../../viewmodels/GenericListItemViewModel';
import { APIConstants } from '../../../network/constants/APIConstants';
import { NetworkConnectivityMonitorHelper } from '../../../helpers/NetworkConnectivityMonitorHelper';
import { BaseUIComponentView } from '../../baseuiviews/forcomponents/BaseUIComponentView';
import { LocalStorageHelper } from '../../../localstorage/helpers/LocalStorageHelper';
import { LocalStorageModel } from '../../../localstorage/localstoragemodels/LocalStorageModel';
import { AppUIHelper } from '../../../helpers/AppUIHelper';

@Component({
  selector: 'champ-navigation-bar',
  templateUrl: 'navigation-bar.html',
})
export class NavigationBarComponent extends BaseUIComponentView {
  @Input() navigationBarViewModel: NavigationBarViewModel;
  @Output() backButtonTapEvent: EventEmitter<NavigationBarViewModel> =
  new EventEmitter<NavigationBarViewModel>();
  private _isOnline: boolean;
  private _noOfDataObjectsInSyncTray: number = 0;
  private networkConnectivityMonitorSubscriber: any;
  private localStorageModelAddedOrDeletedSubscriber: any;

  constructor(private modalCtrl: ModalController,
              private popoverController: PopoverController,
              private ngZone: NgZone) {
    super();
  }

  ionViewWillEnter() {
    super.ionViewWillEnter();

    if (this.navigationBarViewModel.toShowOnlineOfflineIcon) {
      this._isOnline = !NetworkConnectivityMonitorHelper.isNoConnection();
      if (this.networkConnectivityMonitorSubscriber == null) {
        this.networkConnectivityMonitorSubscriber =
        NetworkConnectivityMonitorHelper.getInstance().onlineOffline.subscribe((_isOnline: boolean) => {
          this.ngZone.run(() => {
            this._isOnline = _isOnline;
          });
        });
      }
    }

    if (this.navigationBarViewModel.toShowNotificationsIcon) {
      this.updateNoOfDataObjectsInSyncTray();
      if (this.localStorageModelAddedOrDeletedSubscriber == null) {
        this.localStorageModelAddedOrDeletedSubscriber =
        LocalStorageHelper.getInstance().localStorageModelAddedOrDeleted.subscribe
        ((_localStorageModel: LocalStorageModel) => {
          this.ngZone.run(() => {
            this.updateNoOfDataObjectsInSyncTray();
          });
        });
      }
    }
  }

  ionViewWillLeave() {
    super.ionViewWillLeave();

    if (this.navigationBarViewModel.toShowOnlineOfflineIcon) {
      if (this.networkConnectivityMonitorSubscriber != null) {
        this.networkConnectivityMonitorSubscriber.unsubscribe();
        this.networkConnectivityMonitorSubscriber = null;
      }
    }

    if (this.navigationBarViewModel.toShowNotificationsIcon) {
      if (this.localStorageModelAddedOrDeletedSubscriber != null) {
        this.localStorageModelAddedOrDeletedSubscriber.unsubscribe();
        this.localStorageModelAddedOrDeletedSubscriber = null;
      }
    }
  }

  private onBackButtonTap() {
    this.backButtonTapEvent.emit(this.navigationBarViewModel);
  }

  private notificationsIconTapped() {
    let notificationsModal = this.modalCtrl.create(NotificationsPage, { });
    notificationsModal.present();
  }

  private overFlowIconTapped(event: any) {
    // this should be coming from UI instead :: But for now just putting it here
    this.navigationBarViewModel.overFlowIconGenericListItemViewModels = null;
    const _genericListItemViewModelTmp: GenericListItemViewModel =
    this.navigationBarViewModel.addOverFlowIconGenericListItemViewModel(
      new GenericListItemViewModel(0, '0', GenericListItemViewModelType.MainText),
      (_genericListItemViewModel: GenericListItemViewModel) => {
        APIConstants.TO_USE_MOCKS = !APIConstants.TO_USE_MOCKS;
        _genericListItemViewModel.mainText =
        APIConstants.TO_USE_MOCKS ? 'Using Mocks <Tap To Change>' : 'Using API <Tap To Change>';
      },
    );
    _genericListItemViewModelTmp.mainText =
    APIConstants.TO_USE_MOCKS ? 'Using Mocks <Tap To Change>' : 'Using API <Tap To Change>';
    _genericListItemViewModelTmp.textWrap = true;
    if (this.navigationBarViewModel.toShowNotificationsIcon) {
      const _genericListItemViewModelTemp: GenericListItemViewModel =
      this.navigationBarViewModel.addOverFlowIconGenericListItemViewModel(
        new GenericListItemViewModel(1, '1', GenericListItemViewModelType.MainText),
        (_genericListItemViewModel: GenericListItemViewModel) => {
          this.notificationsIconTapped();
        },
      );
      _genericListItemViewModelTemp.mainText =
      AppUIHelper.getInstance().i18nText('notificationss') + '(' + this._noOfDataObjectsInSyncTray + ')';
      _genericListItemViewModelTemp.textWrap = true;
    }
    if (this.navigationBarViewModel.toShowOnlineOfflineIcon) {
      const _genericListItemViewModelTemp: GenericListItemViewModel =
      this.navigationBarViewModel.addOverFlowIconGenericListItemViewModel(
        new GenericListItemViewModel(2, '2', GenericListItemViewModelType.Icon_MainText_SubText),
        (_genericListItemViewModel: GenericListItemViewModel) => {
        },
      );
      _genericListItemViewModelTemp.mainText =
      this._isOnline ?
      AppUIHelper.getInstance().i18nText('online') :
      AppUIHelper.getInstance().i18nText('offline') ;
      _genericListItemViewModelTemp.icon =
      this._isOnline ?
      'assets/images/online.svg' :
      'assets/images/offline.svg' ;
      _genericListItemViewModelTemp.textWrap = true;
    }

    let popover = this.popoverController.create(GenericListItemComponent,
                                                {'genericListItemViewModels':
                                                 this.navigationBarViewModel.overFlowIconGenericListItemViewModels,
                                                 'tapEvent': (_genericListItemViewModel: GenericListItemViewModel) => {
                                                    popover.dismiss();
                                                    (this.navigationBarViewModel.
                                                    overFlowIconGenericListItemViewModelsCallBackFor
                                                    (_genericListItemViewModel))(_genericListItemViewModel);
                                                 }});
    popover.present({
      ev: event,
    });
  }

  private updateNoOfDataObjectsInSyncTray() {
    LocalStorageHelper.getInstance().numberOfLocalStorageModelsStored
      ((numberOfLocalStorageModelsStored: number) => {
        this._noOfDataObjectsInSyncTray = numberOfLocalStorageModelsStored;
    });
  }
}
