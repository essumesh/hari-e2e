import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { CheckBoxesViewModel, CheckBoxViewModel } from '../../viewmodels/CheckBoxesViewModel';
import { BaseUIComponentView } from '../../baseuiviews/forcomponents/BaseUIComponentView';

@Component({
  selector: 'champ-check-boxes-item',
  templateUrl: 'check-boxes-item.html',
})
export class CheckBoxesItemComponent extends BaseUIComponentView {
  @Input() checkBoxesViewModel: CheckBoxesViewModel;
  @Output() checkBoxViewModelTapEvent: EventEmitter<CheckBoxViewModel> =
  new EventEmitter<CheckBoxViewModel>();

  checkBoxTapped(_checkBoxViewModel: CheckBoxViewModel) {
    this.checkBoxViewModelTapEvent.emit(_checkBoxViewModel);
  }
}
