import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DamageReportsDataModel } from '../../../datamodels/DamageReportsDataModel';
import { DateParserHelper } from '../../../helpers/DateParserHelper';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { BaseUIComponentView } from '../../baseuiviews/forcomponents/BaseUIComponentView';

@Component({
  selector: 'champ-damage-report-list-item',
  templateUrl: 'damage-report-list-item.html',
})
export class DamageReportListItemComponent extends BaseUIComponentView {
  @Input() damageReportsDataModel: DamageReportsDataModel;
  @Output() tapEvent: EventEmitter<DamageReportsDataModel> =
  new EventEmitter<DamageReportsDataModel>();

  dateToBeShown(): string {
    return DateParserHelper.parseDateToString_InFormat_ddMMMyy(this.damageReportsDataModel.reportedOn);
  }

  timeToBeShown(): string {
    return DateParserHelper.parseDateToString_InFormat_HHmm(this.damageReportsDataModel.reportedOn);
  }

  piecesToBeShown(): string {
    return this.damageReportsDataModel.pieces != null ?
           AppUIHelper.getInstance().i18nText('pieces') + this.damageReportsDataModel.pieces.toString() :
           '';
  }

  weightToBeShown(): string {
    return this.damageReportsDataModel.weight != null &&
           this.damageReportsDataModel.weight.unit != null &&
           this.damageReportsDataModel.weight.amount != null ?
           this.damageReportsDataModel.weight.unit + ': ' + this.damageReportsDataModel.weight.amount.toString() :
           '';
  }

  onTap() {
    this.tapEvent.emit(this.damageReportsDataModel);
  }
}
