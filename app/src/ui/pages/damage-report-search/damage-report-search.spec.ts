import {  } from 'jasmine';
import { DamageReportSearchPage } from './damage-report-search';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { NavController, NavParams, ModalController, PopoverController } from 'ionic-angular';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpModule, Http } from '@angular/http';
import { Keyboard } from '@ionic-native/keyboard';

export class NavParamsMock {
    static returnParam: any = null;

    static setParams(value: any) {
      NavParamsMock.returnParam = value;
    }

    public get(_key: any): any {
        if (NavParamsMock.returnParam) {
           return NavParamsMock.returnParam;
        }
        return 'default';
      }
  }

describe('DamageReportSearchPage:', () => {

    let comp: DamageReportSearchPage;
    let fixture: ComponentFixture<DamageReportSearchPage>;
    let de: DebugElement;

    beforeEach(() => {
        TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            declarations: [DamageReportSearchPage],
            providers: [
                {provide: NavController, useValue: NavController},
                {provide: NavParams, useClass: NavParamsMock},
                {provide: ModalController, useValue: ModalController},
                {provide: PopoverController, useValue: PopoverController},
                {provide: Keyboard, useValue: Keyboard},
            ],
            imports: [
                HttpModule,
                TranslateModule.forRoot({
                loader: {
                    provide: TranslateLoader,
                    useFactory: (http: Http) => new TranslateHttpLoader(http, 'assets/i18n/', '.json'),
                    deps: [Http],
                },
                }),
            ],
        });
        fixture = TestBed.createComponent(DamageReportSearchPage);
        // #trick
        // If you want to trigger ionViewWillEnter automatically de-comment the following line
        // fixture.componentInstance.ionViewWillEnter();
        fixture.detectChanges();
        comp = fixture.componentInstance;
        de = fixture.debugElement;
    });

    describe('.constructor()', () => {
        it('Should be defined', () => {
            expect(comp).toBeDefined();
        });
    });
});
