import { Component, ViewChild, ChangeDetectorRef, NgZone, ElementRef } from '@angular/core';
import filter from 'lodash-es/filter';
import { NavController, NavParams, PopoverController } from 'ionic-angular';
import { DamageReportSearchResultsPage } from '../damage-report-search-results/damage-report-search-results';
import { GenericListItemComponent } from '../../components/generic-list-item/generic-list-item';
import { Searchbar } from 'ionic-angular';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { FetchAWBDataObject } from '../../../network/dataobjects/FetchAWBDataObject';
import { FetchAWBResponseObject } from '../../../network/responseobjects/FetchAWBResponseObject';
import { FetchHWBDataObject } from '../../../network/dataobjects/FetchHWBDataObject';
import { FetchHWBResponseObject } from '../../../network/responseobjects/FetchHWBResponseObject';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { PlatformHelper } from '../../../helpers/PlatformHelper';
import { AppMessagePopupHelper } from '../../../helpers/AppMessagePopupHelper';
import { BarcodeScannerHelper } from '../../../helpers/BarcodeScannerHelper';
import { BarcodeParserHelper , BarcodeType} from '../../../helpers/BarcodeParserHelper';
import { AirwaybillsDataModel } from '../../../datamodels/AirwaybillsDataModel';
import { HouseAirwaybillsDataModel } from '../../../datamodels/HouseAirwaybillsDataModel';
import { FetchDamageReportsDataObject } from '../../../network/dataobjects/FetchDamageReportsDataObject';
import { GenericListItemViewModel } from '../../viewmodels/GenericListItemViewModel';
import { GenericListItemViewModelType } from '../../viewmodels/GenericListItemViewModel';
import { RectDataModel } from '../../../datamodels/RectDataModel';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavigationBarComponent } from '../../components/navigation-bar/navigation-bar';
import { AppMessagePopupViewModel, AppMessagePopupViewModelType } from '../../viewmodels/AppMessagePopupViewModel';
import { ListPopoverPage } from '../list-popover/list-popover';
import { Keyboard } from '@ionic-native/keyboard';
import { KeyboardHelper } from '../../../helpers/KeyboardHelper';
import { IrregularitySearchResultsPage } from '../irregularity-search-results/irregularity-search-results';
import { PageConstants } from '../../constants/PageConstants';
import { NetworkConnectivityMonitorHelper } from '../../../helpers/NetworkConnectivityMonitorHelper';

@Component({
  selector: 'champ-page-damage-report-search',
  templateUrl: 'damage-report-search.html',
})
export class DamageReportSearchPage extends BaseUINavigationBarView {
  readonly AWB_SEGMENTTYPE: string = 'awb';
  readonly HWB_SEGMENTTYPE: string = 'hwb';
  @ViewChild('awbPrefixSearchbar') awbPrefixSearchbar: Searchbar;
  @ViewChild('awbSerialSearchbar') awbSerialSearchbar: Searchbar;
  @ViewChild('hwbSearchBar') hwbSearchBar: Searchbar;
  awbHwbSegmentControlValue: string = this.AWB_SEGMENTTYPE;
  awbPrefixSearchbarText: string;
  awbSerialSearchbarText: string;
  hwbSearchBarText: any;
  isAWBSerialDisabled: boolean = true;
  airwaybillsDataModels: Array<AirwaybillsDataModel> = null;
  houseAirwaybillsDataModels: Array<HouseAirwaybillsDataModel> = null;
  tempHouseAirwaybillsDataModels: Array<HouseAirwaybillsDataModel> = new Array<HouseAirwaybillsDataModel>();
  awbORHWBGenericListItemViewModels: Array<GenericListItemViewModel> = null;
  multipleAWBForHWBGenericListItemViewModels: Array<GenericListItemViewModel> =
                                              new Array<GenericListItemViewModel>();
  private fetchAWBDataObject: FetchAWBDataObject;
  private fetchHWBDataObject: FetchHWBDataObject;
  private rectDataModel: RectDataModel;
  private shouldShowLoadingContainerInAWBorHWBList: boolean = false;
  private toResetControlsAndDataForAWBHWBAutoSuggest: boolean = false;
  private awbHwbListHeightWhenKeyboardNotShown: number;
  private awbHWBListHeight: number;
  private AWBHWBLISTMARGINBOTTOM = 50;
  private _barCodeForInternalHardwareScannerForZebraDevicesSubscriber: any;
  private intentionToHaveAWBSerialSearchbarInFocusForiOS: boolean = true;
  private tapEventOnAWBGenericListItemViewModelsCalledDueToBarCodeScanning: boolean = false;
  private tapEventOnHWBGenericListItemViewModelsCalledDueToBarCodeScanning: boolean = false;
  private createOrViewReport: string;
  private invalidawborhwbnumbermessage: string = null;
  @ViewChild('containerAWB') private containerAWBElementRef: ElementRef;

  constructor(public navController: NavController,
              public navParams: NavParams,
              public changeDetectorRef: ChangeDetectorRef,
              public popoverController: PopoverController,
              private keyboard: Keyboard,
              private ngZone: NgZone) {
    super();

    this.navigationBarViewModel =
    new NavigationBarViewModel(true,
                               '',
                               true,
                               true,
                               true);

  }

  ionViewDidLoad() {
    super.ionViewDidLoad();

    AppUIHelper.getInstance().i18nTextWithCallBack(this.getTitle(), (i18nTextValue: string) => {
      this.navigationBarViewModel.title = i18nTextValue;
    });
    AppUIHelper.getInstance().i18nTextWithCallBack(this.getContentTitle(), (i18nTextValue: string) => {
      this.createOrViewReport = i18nTextValue;
    });

     // Add extra bottom margin to avoid list overflow in iOS. ionic bug for iOS
    if (PlatformHelper.isPlatform_iOS())
      this.AWBHWBLISTMARGINBOTTOM = this.AWBHWBLISTMARGINBOTTOM + 30;

    this.calculateAWBHWBListHeightWhenKeyboardNotShown();
  }

  ionViewDidEnter() {
    super.ionViewDidEnter();

    this.subscribeToKeyboardShowHideEvents();
    this.registerForBarCodeForInternalHardwareScannerForZebraDevices();
  }

  ionViewDidLeave() {
    super.ionViewDidLeave();

    this.unsubscribeToKeyboardShowHideEvents();
    this.unregisterForBarCodeForInternalHardwareScannerForZebraDevices();
  }

  ionViewCanLeave(): boolean {
    if (this.toResetControlsAndDataForAWBHWBAutoSuggest) {
      this.toResetControlsAndDataForAWBHWBAutoSuggest = false;
      this.resetControlsAndData_For_AWBHWB_AutoSuggest();
    }
    return super.ionViewCanLeave();
  }

  onOrientationChangedTo_Portrait() {
    super.onOrientationChangedTo_Portrait();

    this.onOrientationChange();
  }

  onOrientationChangedTo_Landscape() {
    super.onOrientationChangedTo_Landscape();

    this.onOrientationChange();
  }

  onKeyboardShowHideEvent(_keyboard: any) {
    super.onKeyboardShowHideEvent(_keyboard);

    if (this.intentionToHaveAWBSerialSearchbarInFocusForiOS) {
      this.intentionToHaveAWBSerialSearchbarInFocusForiOS = false;
      this.awbSerialSearchbar.setFocus();
    } else {
      this.calculateAWBHWBListHeight(_keyboard.showing, _keyboard.height);
    }
  }

  awbHwbSegmentControlChanged(event: any) {
    this.resetInvalidawborhwbnumbermessage();
    this.resetControlsAndData_For_AWBHWB_AutoSuggest();
    if (event.value === this.AWB_SEGMENTTYPE) {
    } else {
    }
  }

  onAWBPrefixSearchbarTextChanged() {
    this.awbPrefixSearchbarText = this.awbPrefixSearchbarText.replace(/[^a-z0-9]+/gi, '');
    if (this.awbPrefixSearchbarText.length >= 3) {
      this.isAWBSerialDisabled = false;
      setTimeout(() => {
        if (PlatformHelper.isPlatform_iOS()) {
          this.intentionToHaveAWBSerialSearchbarInFocusForiOS = true;
          this.awbSerialSearchbar.setFocus();
          KeyboardHelper.getInstance().showKeyboard();
        } else this.awbSerialSearchbar.setFocus();
      }, 10);
    } else {
      this.isAWBSerialDisabled = true;
      this.awbSerialSearchbarText = '';
    }
    this.cancelAutoCompleteNetworkCalls();
    this.deInit_AWB_HWB_DataModels();
    if (this.awbPrefixSearchbar.value + '' + this.awbSerialSearchbar.value === '') {
      this.resetControlsAndData_For_AWBHWB_AutoSuggest();
    }
  }

  changedAWBPrefix(value: any) {
    this.changeDetectorRef.detectChanges();
    this.awbPrefixSearchbarText = value.length > 3 ? value.substring(0, 3) : value;
  }

  onAWBSerialSearchbarTextChanged() {
    this.resetInvalidawborhwbnumbermessage();

    this.awbSerialSearchbarText = this.awbSerialSearchbarText.replace(/[^0-9]+/gi, '');

    if (this.awbPrefixSearchbar.value + '' + this.awbSerialSearchbar.value === '') {
      this.resetControlsAndData_For_AWBHWB_AutoSuggest();
    } else {
      if (this.awbSerialSearchbarText.length >= 3) {
        this.do_AWB_HWB_AutoSuggest(true);
      } else {
        this.cancelAutoCompleteNetworkCalls();
        this.deInit_AWB_HWB_DataModels();
      }
    }
  }

  onAWBSerialChanged() {
    this.changeDetectorRef.detectChanges();
  }

  onAWBSerialSearchbarBackspacePressed() {
    if (this.awbSerialSearchbarText.length >= 3) {
      this.do_AWB_HWB_AutoSuggest(true);
    } else {
      this.cancelAutoCompleteNetworkCalls();
      this.deInit_AWB_HWB_DataModels();
    //   if (this.awbSerialSearchbarText.length === 0) {
    //     this.awbPrefixSearchbar.setFocus();
    //  }
    }
   }

  onHWBSearchbarTextChanged() {
    this.resetInvalidawborhwbnumbermessage();
    this.hwbSearchBarText = this.hwbSearchBarText.replace(/[^a-z0-9]+/gi, '');
    if (this.hwbSearchBarText.length >= 3) {
      this.do_AWB_HWB_AutoSuggest(true);
    } else {
      this.cancelAutoCompleteNetworkCalls();
      this.deInit_AWB_HWB_DataModels();
    }
    if (this.hwbSearchBar.value === '') {
      this.resetControlsAndData_For_AWBHWB_AutoSuggest();
    }
  }

  onHWBTextChanged() {
    this.changeDetectorRef.detectChanges();
  }

  onSearchClicked() {
    if (this.awbHwbSegmentControlValue === this.AWB_SEGMENTTYPE) {
      if (AppUIHelper.isValidAWBNumber(this.awbPrefixSearchbar.value, this.awbSerialSearchbar.value)) {
            const _value = this.awbPrefixSearchbar.value + this.awbSerialSearchbar.value;
            this.resetControlsAndData_For_AWBHWB_AutoSuggest();
            this.do_AWB_HWB_AutoSuggest(false, _value);
      } else {
        this.hideAWBHWBList();

        this.invalidawborhwbnumbermessage =
        AppUIHelper.getInstance().i18nText('invalidawbnumber');
      }
    } else if (this.awbHwbSegmentControlValue === this.HWB_SEGMENTTYPE) {
      if (BarcodeParserHelper.barcodeType(this.hwbSearchBar.value) === BarcodeType.HWB) {
            const _value = this.hwbSearchBar.value;
            this.resetControlsAndData_For_AWBHWB_AutoSuggest();
            this.do_AWB_HWB_AutoSuggest(false, _value);
      } else {
        this.hideAWBHWBList();

        this.invalidawborhwbnumbermessage =
        AppUIHelper.getInstance().i18nText('invalidhwbnumber');
      }
    }
  }

  tapEvent_On_AWBORHWBGenericListItemViewModels(_genericListItemViewModel: GenericListItemViewModel) {
    this.toResetControlsAndDataForAWBHWBAutoSuggest = true;

    const doAWB = (() => {
      if (PageConstants.TO_USE_SCREEN_ID === 2) {
        this.navController.push(IrregularitySearchResultsPage,
          {'airwaybillsDataModel': this.airwaybillsDataModels[_genericListItemViewModel.index]});
      }else {
        this.navController.push(DamageReportSearchResultsPage,
          {'airwaybillsDataModel': this.airwaybillsDataModels[_genericListItemViewModel.index]});
      }
    });

    const doHWB = (() => {
      this.showMultipleMasterAWBsForHWBIfRequired(_genericListItemViewModel.index);
    });

    if (this.tapEventOnAWBGenericListItemViewModelsCalledDueToBarCodeScanning) {
      doAWB();
      this.tapEventOnAWBGenericListItemViewModelsCalledDueToBarCodeScanning = false;
    } else if (this.tapEventOnHWBGenericListItemViewModelsCalledDueToBarCodeScanning) {
      doHWB();
      this.tapEventOnHWBGenericListItemViewModelsCalledDueToBarCodeScanning = false;
    } else if (this.awbHwbSegmentControlValue === this.AWB_SEGMENTTYPE)
        doAWB();
      else
        doHWB();
  }

  onScanClicked() {
    this.resetControlsAndData_For_AWBHWB_AutoSuggest();

    BarcodeScannerHelper.getInstance().scan((barcodeData: string) => {
      this.do_AWB_HWB_AutoSuggest(false, barcodeData);
    }, (_errorText: string) => {
    });
  }

  calculateAWBHWBListHeight(isKeyboardShown: boolean, keyboardHeight: number) {
    if (isKeyboardShown && PlatformHelper.isOrientation_Portrait()) {
      this.awbHWBListHeight = this.awbHwbListHeightWhenKeyboardNotShown - keyboardHeight;
    } else {
      this.awbHWBListHeight = this.awbHwbListHeightWhenKeyboardNotShown ;
    }

    this.initRectDataModel();
  }

  calculateAWBHWBListHeightWhenKeyboardNotShown() {
    /*
      'getBoundingClientRect' is giving wrong value in Portrait mode when queried
       2nd time from landscape to Portrait

       It needs to be check upon again :: There's a conceptual bug on this in Ionic
    */
    let searchbarContainer = this.awbHwbSegmentControlValue === this.AWB_SEGMENTTYPE
    ? document.getElementById('containerAWB').getBoundingClientRect()
    : document.getElementById('containerHWB').getBoundingClientRect();

    let navBarHeight = document.getElementById('searchBarHeader').getBoundingClientRect().height;

    let containerAWBElementRefBottom = this.containerAWBElementRef.nativeElement.getBoundingClientRect().bottom;

    // Calculate the height of the list by subtracting the bottom position of the awb search bar from the screenheight
    /*
    this.awbHwbListHeightWhenKeyboardNotShown = PlatformHelper.screenHeight() -
    navBarHeight - searchbarContainer.bottom - this.AWBHWBLISTMARGINBOTTOM;
    */
    this.awbHwbListHeightWhenKeyboardNotShown = PlatformHelper.screenHeight() -
    navBarHeight - containerAWBElementRefBottom - this.AWBHWBLISTMARGINBOTTOM;
  }

  private do_AWB_HWB_AutoSuggest(calledByUIOrBarcodeScanner: boolean, _valueFromBarcodeScanner?: string) {
    const doAWBNetworkCall = ((_awbPrefix: string, _awbSerial: string) => {
      const _success = ((fetchAWBResponseObject: FetchAWBResponseObject) => {
        if (fetchAWBResponseObject == null) {
          fetchAWBResponseObject = new FetchAWBResponseObject();
          fetchAWBResponseObject.awbNotFound = true;
        }

        if (fetchAWBResponseObject.awbNotFound) {
          const fakeAirwaybillsDataModel: AirwaybillsDataModel =
          new AirwaybillsDataModel();
          fakeAirwaybillsDataModel.airwaybillPrefix = _awbPrefix;
          fakeAirwaybillsDataModel.airwaybillSerial = _awbSerial;
          fetchAWBResponseObject.airwaybillsDataModels.push(fakeAirwaybillsDataModel);
        }
        this.airwaybillsDataModels = fetchAWBResponseObject.airwaybillsDataModels;

        if (calledByUIOrBarcodeScanner)
          this.prepare_AirwaybillsGenericListItemViewModels();
        else {
          let fakeGenericListItemViewModel: GenericListItemViewModel =
          new GenericListItemViewModel(0, '0', GenericListItemViewModelType.MainText);
          this.tapEvent_On_AWBORHWBGenericListItemViewModels(fakeGenericListItemViewModel);
        }
      });

      if (NetworkConnectivityMonitorHelper.isNoConnection())
        _success(null);
      else {
        if (!calledByUIOrBarcodeScanner)
        AppMessagePopupHelper.showLoadingAppMessagePopup(AppUIHelper.getInstance().i18nText('loading'));
        this.shouldShowLoadingContainerInAWBorHWBList = true;
        this.fetchAWBDataObject =
        ManualDIHelper.getInstance().createObject('FetchAWBDataObject');
        this.fetchAWBDataObject.prepareRequestWithParameters
        (_awbPrefix, _awbSerial);
        this.fetchAWBDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
          this.shouldShowLoadingContainerInAWBorHWBList = false;
          if (!calledByUIOrBarcodeScanner)
            AppMessagePopupHelper.getInstance().hideAppMessagePopup();
          if (_dataObject.serverErrorDataModel != null) {
            this.invalidawborhwbnumbermessage = _dataObject.serverErrorDataModel.errorText;
            this.hideAWBHWBList();
          } else {
            _success(_dataObject.responseObject as FetchAWBResponseObject);
          }
        });
      }
    });

    const doHWBNetworkCall = ((_hwb: string) => {
      const _success = ((fetchHWBResponseObject: FetchHWBResponseObject) => {
        if (fetchHWBResponseObject == null) {
          fetchHWBResponseObject = new FetchHWBResponseObject();
          fetchHWBResponseObject.hwbNotFound = true;
        }

        if (fetchHWBResponseObject.hwbNotFound) {
          const fakeHouseAirwaybillsDataModel: HouseAirwaybillsDataModel =
          new HouseAirwaybillsDataModel();
          fakeHouseAirwaybillsDataModel.houseAirwaybillNumber = _hwb;
          fetchHWBResponseObject.houseAirwaybillsDataModels.push(fakeHouseAirwaybillsDataModel);
        }
        this.houseAirwaybillsDataModels = fetchHWBResponseObject.houseAirwaybillsDataModels;

        if (calledByUIOrBarcodeScanner)
          this.prepare_HouseAirwaybillsGenericListItemViewModels();
        else {
          let fakeGenericListItemViewModel: GenericListItemViewModel =
          new GenericListItemViewModel(0, '0', GenericListItemViewModelType.MainText);
          this.tapEvent_On_AWBORHWBGenericListItemViewModels(fakeGenericListItemViewModel);
        }
      });

      if (NetworkConnectivityMonitorHelper.isNoConnection())
        _success(null);
      else {
        if (!calledByUIOrBarcodeScanner)
          AppMessagePopupHelper.showLoadingAppMessagePopup(AppUIHelper.getInstance().i18nText('loading'));
        this.shouldShowLoadingContainerInAWBorHWBList = true;
        this.fetchHWBDataObject =
        ManualDIHelper.getInstance().createObject('FetchHWBDataObject');
        this.fetchHWBDataObject.prepareRequestWithParameters(_hwb);
        this.fetchHWBDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
          this.shouldShowLoadingContainerInAWBorHWBList = false;
          if (!calledByUIOrBarcodeScanner)
          AppMessagePopupHelper.getInstance().hideAppMessagePopup();
          if (_dataObject.serverErrorDataModel != null) {
            this.invalidawborhwbnumbermessage = AppUIHelper.getInstance().i18nText('invalidhwbnumber');
            this.hideAWBHWBList();
          } else {
            _success(_dataObject.responseObject as FetchHWBResponseObject);
          }
        });
      }
    });

    if (calledByUIOrBarcodeScanner) {
      if (this.awbHwbSegmentControlValue === this.AWB_SEGMENTTYPE) {
        this.cancelAutoCompleteNetworkCalls();
        this.initAWBORHWBGenericListItemViewModels();
        doAWBNetworkCall(this.awbPrefixSearchbar.value, this.awbSerialSearchbar.value);
      } else {
        this.cancelAutoCompleteNetworkCalls();
        this.initAWBORHWBGenericListItemViewModels();
        doHWBNetworkCall(this.hwbSearchBar.value.toUpperCase());
      }
    } else {
      let barcodeType = BarcodeParserHelper.barcodeType(_valueFromBarcodeScanner);
      if (barcodeType === BarcodeType.AWB) {
        const barcodeValues = BarcodeParserHelper.barcodeValues(_valueFromBarcodeScanner, barcodeType);
        this.tapEventOnAWBGenericListItemViewModelsCalledDueToBarCodeScanning = true;
        doAWBNetworkCall(barcodeValues[0], barcodeValues[1]);
      } else if (barcodeType === BarcodeType.HWB) {
        const barcodeValues = BarcodeParserHelper.barcodeValues(_valueFromBarcodeScanner, barcodeType);
        this.tapEventOnHWBGenericListItemViewModelsCalledDueToBarCodeScanning = true;
        doHWBNetworkCall(barcodeValues[0]);
      }
    }
  }

  private initAWBORHWBGenericListItemViewModels() {
    this.initRectDataModel();
    this.awbORHWBGenericListItemViewModels = [];
  }

  private initRectDataModel() {
    let searchbarContainer = this.awbHwbSegmentControlValue === this.AWB_SEGMENTTYPE
    ? document.getElementById('containerAWB').getBoundingClientRect()
    : document.getElementById('containerHWB').getBoundingClientRect();

    let navBarHeight = document.getElementById('searchBarHeader').getBoundingClientRect().height;
    /*
        The navBarHeight is subtracted from the searchbar's bottom point as the list was rendered
        'navBarHeight' point below the 'searchbarContainer.bottom' :: Hence such a solution
    */
    this.rectDataModel = new RectDataModel(searchbarContainer.bottom - navBarHeight,
                                           searchbarContainer.left,
                                           searchbarContainer.width,
                                           this.awbHWBListHeight);
  }

  private prepare_AirwaybillsGenericListItemViewModels() {
    this.initAWBORHWBGenericListItemViewModels();
    this.airwaybillsDataModels.forEach((airwaybillsDataModel, index) => {
      let genericListItemViewModel: GenericListItemViewModel =
      new GenericListItemViewModel(index, '', GenericListItemViewModelType.MainText);
      genericListItemViewModel.mainText =
      airwaybillsDataModel.airwaybillPrefix
      + ' '
      + airwaybillsDataModel.airwaybillSerial;
      this.awbORHWBGenericListItemViewModels.push(genericListItemViewModel);
    });
  }

  private prepare_HouseAirwaybillsGenericListItemViewModels() {
    this.initAWBORHWBGenericListItemViewModels();
    this.houseAirwaybillsDataModels.forEach((houseAirwaybillsDataModel, index) => {
      let genericListItemViewModel: GenericListItemViewModel =
      new GenericListItemViewModel(index, '', GenericListItemViewModelType.MainText);
      genericListItemViewModel.mainText =
      houseAirwaybillsDataModel.houseAirwaybillNumber;
      this.awbORHWBGenericListItemViewModels.push(genericListItemViewModel);
    });
  }

  private cancelAutoCompleteNetworkCalls() {
    if (this.fetchAWBDataObject != null) {
      this.fetchAWBDataObject.cancelRequest();
      this.fetchAWBDataObject = null;
    }
    if (this.fetchHWBDataObject != null) {
      this.fetchHWBDataObject.cancelRequest();
      this.fetchHWBDataObject = null;
    }
  }

  private deInit_AWB_HWB_DataModels() {
    this.airwaybillsDataModels = null;
    this.houseAirwaybillsDataModels = null;
    this.awbORHWBGenericListItemViewModels = null;
  }

  private resetControlsAndData_For_AWBHWB_AutoSuggest() {
    this.awbPrefixSearchbarText = '';
    this.awbSerialSearchbarText = '';
    this.hwbSearchBarText = '';

    this.deInit_AWB_HWB_DataModels();

    this.cancelAutoCompleteNetworkCalls();
  }

  private showMultipleMasterAWBsForHWBIfRequired(selectedIndex: number) {
    this.resetTempArrayOfMultipleAWBsForHWB();
    let selectedHouseAirwayBillNumber =
        this.houseAirwaybillsDataModels[selectedIndex].houseAirwaybillNumber;
    for (let i = 0; i < this.houseAirwaybillsDataModels.length; i++) {
      if (this.houseAirwaybillsDataModels[i].houseAirwaybillNumber === selectedHouseAirwayBillNumber) {
        this.tempHouseAirwaybillsDataModels.push(this.houseAirwaybillsDataModels[i]);
      }
    }

    if (this.tempHouseAirwaybillsDataModels.length > 1) {
      this.showPopupWithMultipleAWBsForHWB(this.tempHouseAirwaybillsDataModels);
    } else {
      this.showDamageReportSearchResultsScreenForHWB(this.houseAirwaybillsDataModels[selectedIndex]);
    }
  }

  private showPopupWithMultipleAWBsForHWB(houseAirwaybillsDataModels: Array<HouseAirwaybillsDataModel>) {
     houseAirwaybillsDataModels.forEach((houseAirwaybillsDataModel, index) => {
        let genericListItemViewModel: GenericListItemViewModel =
        new GenericListItemViewModel(index, '', GenericListItemViewModelType.MainText);
        genericListItemViewModel.mainText =
        houseAirwaybillsDataModel.masterAirwaybill.airwaybillPrefix
        + ' ' +
        houseAirwaybillsDataModel.masterAirwaybill.airwaybillSerial;
        genericListItemViewModel.shouldShowRightArrow = true;
        this.multipleAWBForHWBGenericListItemViewModels.push(genericListItemViewModel);
      });

     this.presentPopupWithMultipleAWBNumbersForHWB(houseAirwaybillsDataModels,
                                                   houseAirwaybillsDataModels[0].houseAirwaybillNumber);
  }

  private presentPopupWithMultipleAWBNumbersForHWB(hwbDataModels: Array<HouseAirwaybillsDataModel>, hwbNumber: string) {
    let popover = this.popoverController.create(ListPopoverPage, {
      'title': hwbDataModels.length + ' AWB Results For ' + hwbNumber ,
      'listItems': this.multipleAWBForHWBGenericListItemViewModels,
      'buttonText': AppUIHelper.getInstance().i18nText('cancel'),
      'genericListItemViewModelTapEvent':
        (_genericListItemViewModel: GenericListItemViewModel) => {
          this.resetTempArrayOfMultipleAWBsForHWB();
          this.showDamageReportSearchResultsScreenForHWB(hwbDataModels[_genericListItemViewModel.index]);
        }},
      );
    popover.present({
    });

    popover.onDidDismiss(() => {
      this.resetTempArrayOfMultipleAWBsForHWB();
    },
   );
  }

  private resetInvalidawborhwbnumbermessage() {
    this.invalidawborhwbnumbermessage = null;
  }

  private hideAWBHWBList() {
    this.deInit_AWB_HWB_DataModels();
  }

  private resetTempArrayOfMultipleAWBsForHWB() {
    this.tempHouseAirwaybillsDataModels = [];
    this.multipleAWBForHWBGenericListItemViewModels = [];
  }

  private showDamageReportSearchResultsScreenForHWB(hwbDataModel: HouseAirwaybillsDataModel) {
    if (PageConstants.TO_USE_SCREEN_ID === 2) {
      this.navController.push(IrregularitySearchResultsPage,
        {'houseAirwaybillsDataModel': hwbDataModel});
      } else {
        this.navController.push(DamageReportSearchResultsPage,
          {'houseAirwaybillsDataModel': hwbDataModel});
      }
  }

  private onOrientationChange() {
    this.unsubscribeToKeyboardShowHideEvents();
    KeyboardHelper.getInstance().hideKeyboard();
    setTimeout(() => {
      this.ngZone.run(() => {
        this.calculateAWBHWBListHeightWhenKeyboardNotShown();
        this.calculateAWBHWBListHeight(false, 0);

        this.subscribeToKeyboardShowHideEvents();
      });
    }, 50);
  }

  private registerForBarCodeForInternalHardwareScannerForZebraDevices() {
    this.unregisterForBarCodeForInternalHardwareScannerForZebraDevices();

    this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber =
    BarcodeScannerHelper.getInstance()
    .barCodeForInternalHardwareScannerForZebraDevices.subscribe
    ((_barcodeData: string) => {
      this.resetControlsAndData_For_AWBHWB_AutoSuggest();
      this.do_AWB_HWB_AutoSuggest(false, _barcodeData);
    });
  }

  private unregisterForBarCodeForInternalHardwareScannerForZebraDevices() {
    if (this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber != null) {
      this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber.unsubscribe();
      this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber = null;
    }
  }

  private getTitle(): string {
    let title = 'damagereport';
    if (PageConstants.TO_USE_SCREEN_ID === 2) {
        title = 'irregularities';
      }else if (PageConstants.TO_USE_SCREEN_ID === 1) {
        title = 'damagereport';
      }
    return title;
  }
  private getContentTitle(): string {
    let createOrViewReportTitle = 'createOrViewDamageReport';
    if (PageConstants.TO_USE_SCREEN_ID === 2) {
        createOrViewReportTitle = 'createOrViewIrregularityReport';
      }else if (PageConstants.TO_USE_SCREEN_ID === 1) {
        createOrViewReportTitle = 'createOrViewDamageReport';
      }
    return createOrViewReportTitle;
  }
}
