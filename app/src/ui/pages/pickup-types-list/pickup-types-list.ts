import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, Navbar } from 'ionic-angular';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { FetchPickUpDeliveryResponseObject } from '../../../network/responseobjects/FetchPickUpDeliveryResponseObject';
import { PickUpDeliveryDataModel } from '../../../datamodels/PickUpDeliveryDataModel';
import { PickupViewListPage } from '../pickup-view-list/pickup-view-list';
import { PickUpDeliveryReceiptDataModel } from '../../../datamodels/PickUpDeliveryReceiptDataModel';

@Component({
  selector: 'champ-page-pickup-types-list',
  templateUrl: 'pickup-types-list.html',
})
export class PickupTypeListPage extends BaseUINavigationBarView {
  private pickupReferencesDataModel: PickUpDeliveryReceiptDataModel;
  constructor(private navParams: NavParams,
    private navCtrl: NavController,
    private viewCtrl: ViewController) {
    super();

    this.pickupReferencesDataModel =
    this.navParams.get('pickupReferencesDataModel');
    this.setTitle();
    this.navigationBarViewModel =
      new NavigationBarViewModel(false,
        AppUIHelper.getInstance().i18nText('Receipt Number ' +
        this.pickupReferencesDataModel.deliveryReceipt[0].receiptOrTransferNumber),
        false,
        true,
        true);
    this.navigationBarViewModel.toShowBackButton = true;

  }
  backButtonClicked() {
    console.log('back');
    this.navCtrl.pop({animate: true});
  }
  nextPage(deliveryReceiptDataModel: PickUpDeliveryDataModel) {
    deliveryReceiptDataModel.pickupDriverInfo != null &&
    deliveryReceiptDataModel.pickupDriverInfo.pickupDriver != null ?
      AppUIHelper.getInstance().showAlert(AppUIHelper.getInstance().i18nText('error'),
        'Record Already Commited', [AppUIHelper.getInstance().i18nText('ok')]) :
      this.navCtrl.push(PickupViewListPage,
        {
          'pickUpDeliveryDataModel': deliveryReceiptDataModel,
        });
  }

  setTitle() {
    (this.pickupReferencesDataModel.deliveryReceipt[0].pickupType === 'deliveryReceipt') ?
      this.pickupReferencesDataModel.deliveryReceipt[0].pickupType = 'Delivery Receipt' :
      this.pickupReferencesDataModel.deliveryReceipt[0].pickupType = 'Bond Transfer';

    (this.pickupReferencesDataModel.deliveryReceipt[1].pickupType === 'deliveryReceipt') ?
      this.pickupReferencesDataModel.deliveryReceipt[1].pickupType = 'Delivery Receipt' :
      this.pickupReferencesDataModel.deliveryReceipt[1].pickupType = 'Bond Transfer';
  }

}
