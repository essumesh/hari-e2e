import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { GenericListItemViewModel, GenericListItemViewModelType } from '../../viewmodels/GenericListItemViewModel';

@Component({
  selector: 'champ-page-export-acceptance-reports',
  templateUrl: 'export-acceptance-reports.html',
})
export class ExportAcceptanceReportsPage extends BaseUINavigationBarView {
  genericListItemViewModels: Array<GenericListItemViewModel> = null;
  private reportCount: string;
  private readonly LISTMARGINBOTTOM = 10;
  private readonly LISTBORDERRADIUS = 5;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    super();

    this.navigationBarViewModel =
    new NavigationBarViewModel(false,
                        '',
                        true,
                        true,
                        true);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExportAcceptanceReportsPage');
    this.prepare_GenericListItemViewModels();
  }

  presentPopover(_event: any) {

  }

  tapEvent_On_GenericListItemComponent() {

  }

  prepare_GenericListItemViewModels() {
    this.genericListItemViewModels = [];

    let genericListViewModel =
    new GenericListItemViewModel(0, '', GenericListItemViewModelType.LeftText_MainText_SubText);
    genericListViewModel.mainText = 'mainText';
    genericListViewModel.subText = 'subText';
    genericListViewModel.leftTopText = '100';
    genericListViewModel.leftBottomText = 'Pcs';
    genericListViewModel.shouldShowRightArrow = true;
    genericListViewModel.marginBottom = this.LISTMARGINBOTTOM;
    genericListViewModel.borderRadius = this.LISTBORDERRADIUS;

    this.genericListItemViewModels.push(genericListViewModel);

    genericListViewModel.mainText = 'mainText';
    genericListViewModel.subText = 'subText';
    genericListViewModel.leftTopText = '100';
    genericListViewModel.leftBottomText = 'Pcs';
    genericListViewModel.shouldShowRightArrow = true;
    genericListViewModel.marginBottom = this.LISTMARGINBOTTOM;
    genericListViewModel.borderRadius = this.LISTBORDERRADIUS;

    this.genericListItemViewModels.push(genericListViewModel);

  }

  private navigationBarTitle() {
  }

}
