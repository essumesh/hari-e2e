import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { DamageDiscoveryPage } from '../damage-discovery/damage-discovery';
import { PackagingDetailPage } from '../packaging-details/packaging-details';
import { PreFetchDataObjectsHelper } from '../../../network/helpers/PreFetchDataObjectsHelper';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { FetchAWBDamageQualifiersResponseObject }
from '../../../network/responseobjects/FetchAWBDamageQualifiersResponseObject';
import { LookupDataModel } from '../../../datamodels/LookupDataModel';
import { LookupsDataModel } from '../../../datamodels/LookupsDataModel';
import { DamageReportsDataModel } from '../../../datamodels/DamageReportsDataModel';
import { CheckBoxesViewModel, CheckBoxViewModel } from '../../viewmodels/CheckBoxesViewModel';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { PlatformHelper } from '../../../helpers/PlatformHelper';
import { ScreenOrientationHelper, ScreenOrientationType } from '../../../helpers/ScreenOrientationHelper';
import { KeyboardHelper } from '../../../helpers/KeyboardHelper';

@Component({
  selector: 'champ-page-damage-report',
  templateUrl: 'damage-report.html',
})
export class DamageReportPage extends BaseUINavigationBarView {
  fetchAWBDamageQualifiersResponseObject: FetchAWBDamageQualifiersResponseObject =
  PreFetchDataObjectsHelper.getInstance().fetchAWBDamageQualifiersDataObject.responseObject  as
  FetchAWBDamageQualifiersResponseObject;
  private damageToPackingCheckBoxesViewModel: CheckBoxesViewModel;
  private conditionOfContentCheckBoxesViewModel: CheckBoxesViewModel;
  private damageReportsDataModel: DamageReportsDataModel;
  private checkBoxViewModelOtherOfdamageToPacking: CheckBoxViewModel;
  private checkBoxViewModelOtherOfconditionOfContent: CheckBoxViewModel;
  private isDeviceATablet: boolean = false;
  private isDeviceInLandScapeOrientation: boolean = false;
  private toshowFooter: boolean = true;

  constructor(private navParams: NavParams,
              private navController: NavController,
              private viewCtrl: ViewController) {
    super();

    this.damageReportsDataModel =
    this.navParams.get('damageReportsDataModel');
    this.navigationBarViewModel =
        new NavigationBarViewModel(true,
                               AppUIHelper.getInstance().i18nText('damagedetails'),
                               true,
                               true,
                               true);
    if (PlatformHelper.isDevice_A_Tablet() &&
        ScreenOrientationHelper.currentScreenOrientation() === ScreenOrientationType.LANDSCAPE) {
        this.isDeviceATablet = true;
        this.isDeviceInLandScapeOrientation = true;
    }

    if (this.damageReportsDataModel.damageToPackaging.length === 0) {
      this.damageReportsDataModel.damageToPackaging.push(new LookupsDataModel());
    }
    if (this.damageReportsDataModel.conditionOfContents.length === 0) {
      this.damageReportsDataModel.conditionOfContents.push(new LookupsDataModel());
    }

    this.prepareDamageToPackingCheckBoxesViewModel();
    this.preparecConditionOfContentCheckBoxesViewModel();
  }

  ionViewDidEnter() {
    super.ionViewDidEnter();

    this.subscribeToKeyboardShowHideEvents();
  }

  ionViewDidLeave() {
    super.ionViewDidLeave();

    this.unsubscribeToKeyboardShowHideEvents();
  }

  onOrientationChangedTo_Landscape() {
    super.onOrientationChangedTo_Landscape();

    try {
        if (PlatformHelper.isDevice_A_Tablet() &&
            this.navController.canGoBack())
              this.navController.pop();
    } catch (e) {
    }
  }

  onKeyboardShowHideEvent(_keyboard: any) {
    super.onKeyboardShowHideEvent(_keyboard);

    if (_keyboard.showing) {
        this.toshowFooter = false;
    } else {
        this.toshowFooter = true;
    }
    this.doACallTo_OnInputFocusedInAnyOfTheChilds();
  }

  private prepareDamageToPackingCheckBoxesViewModel() {
    this.damageToPackingCheckBoxesViewModel = new CheckBoxesViewModel();

    for (let lookupDataModel of
      this.fetchAWBDamageQualifiersResponseObject.damageReferencesDataModel.damageToPackings.qualifierType) {
      this.damageToPackingCheckBoxesViewModel.addCheckBoxViewModel(
        lookupDataModel.name,
        this.damageReportsDataModel.damageToPackaging[0].contains(lookupDataModel),
        lookupDataModel,
      );
      if (lookupDataModel.isCodeOfTypeOther()) {
                this.checkBoxViewModelOtherOfdamageToPacking =
                this.damageToPackingCheckBoxesViewModel.checkBoxViewModels
                [this.damageToPackingCheckBoxesViewModel.checkBoxViewModels.length - 1];
      }
    }
  }

  private tapEvent_On_DamageToPackingCheckBoxesViewModel(_checkBoxViewModel: CheckBoxViewModel) {
    let _lookupDataModel: LookupDataModel = _checkBoxViewModel.extras as LookupDataModel;
    if (_checkBoxViewModel.isSelected) {
        this.damageReportsDataModel.damageToPackaging[0].addIfNotPresent(_lookupDataModel);
    } else
        this.damageReportsDataModel.damageToPackaging[0].removeIfPresent(_lookupDataModel);

    if (_lookupDataModel.isCodeOfTypeOther() && !_checkBoxViewModel.isSelected)
        this.damageReportsDataModel.damageToPackaging[0].other = null;

    this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds')();
  }

  private preparecConditionOfContentCheckBoxesViewModel() {
    this.conditionOfContentCheckBoxesViewModel = new CheckBoxesViewModel();

    for (let lookupDataModel of
      this.fetchAWBDamageQualifiersResponseObject.damageReferencesDataModel.conditionOfContents.qualifierType) {
      this.conditionOfContentCheckBoxesViewModel.addCheckBoxViewModel(
        lookupDataModel.name,
        this.damageReportsDataModel.conditionOfContents[0].contains(lookupDataModel),
        lookupDataModel,
      );
      if (lookupDataModel.isCodeOfTypeOther()) {
                this.checkBoxViewModelOtherOfconditionOfContent =
                this.conditionOfContentCheckBoxesViewModel.checkBoxViewModels
                [this.conditionOfContentCheckBoxesViewModel.checkBoxViewModels.length - 1];
      }
    }
  }

  private tapEvent_On_ConditionOfContentCheckBoxesViewModel(_checkBoxViewModel: CheckBoxViewModel) {
    let _lookupDataModel: LookupDataModel = _checkBoxViewModel.extras as LookupDataModel;
    if (_checkBoxViewModel.isSelected) {
        this.damageReportsDataModel.conditionOfContents[0].addIfNotPresent(_lookupDataModel);
    } else
        this.damageReportsDataModel.conditionOfContents[0].removeIfPresent(_lookupDataModel);

    if (_lookupDataModel.isCodeOfTypeOther() && !_checkBoxViewModel.isSelected)
        this.damageReportsDataModel.conditionOfContents[0].other = null;

    this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds')();
  }

  private onOtherFieldOfDamageToPackingChanged() {
    if (this.damageReportsDataModel.damageToPackaging[0].other)
      this.checkBoxViewModelOtherOfdamageToPacking.isSelected = true;
    else
      this.checkBoxViewModelOtherOfdamageToPacking.isSelected = false;
    this.tapEvent_On_DamageToPackingCheckBoxesViewModel(this.checkBoxViewModelOtherOfdamageToPacking);
  }

  private onOtherFieldOfConditionOfContentChanged() {
    if (this.damageReportsDataModel.conditionOfContents[0].other)
      this.checkBoxViewModelOtherOfconditionOfContent.isSelected = true;
    else
      this.checkBoxViewModelOtherOfconditionOfContent.isSelected = false;
    this.tapEvent_On_ConditionOfContentCheckBoxesViewModel(this.checkBoxViewModelOtherOfconditionOfContent);
  }

  private closePage() {
    this.navController.push(PackagingDetailPage,
       {'damageReportsDataModel': this.damageReportsDataModel,
        'onSaveClickedOnOneOfTheChilds': this.navParams.get('onSaveClickedOnOneOfTheChilds'),
        'someUIElementsHaveChangedItsStateOnOneOfTheChilds':
        this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds'),
        'doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds':
        this.navParams.get('doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds'),
        'doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds':
        this.navParams.get('doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds'),
        'onInputFocusedInAnyOfTheChilds': this.navParams.get('onInputFocusedInAnyOfTheChilds'),
        'onPrintClickedOnOneOfTheChilds': this.navParams.get('onPrintClickedOnOneOfTheChilds'),
        'onEmailClickedOnOneOfTheChilds': this.navParams.get('onEmailClickedOnOneOfTheChilds')}).then(() => {
    this.viewCtrl.dismiss();
      });
  }

  private nextPage() {
    this.navController.push(DamageDiscoveryPage,
       {'damageReportsDataModel': this.damageReportsDataModel,
        'onSaveClickedOnOneOfTheChilds': this.navParams.get('onSaveClickedOnOneOfTheChilds'),
        'someUIElementsHaveChangedItsStateOnOneOfTheChilds':
        this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds'),
        'doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds':
        this.navParams.get('doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds'),
        'doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds':
        this.navParams.get('doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds'),
        'onInputFocusedInAnyOfTheChilds': this.navParams.get('onInputFocusedInAnyOfTheChilds'),
        'onPrintClickedOnOneOfTheChilds': this.navParams.get('onPrintClickedOnOneOfTheChilds'),
        'onEmailClickedOnOneOfTheChilds': this.navParams.get('onEmailClickedOnOneOfTheChilds')}).then(() => {
    this.viewCtrl.dismiss();
    });
  }

  private swiped(event: any) {
    if (event.direction === 2) {
        this.nextPage();
    } else if (event.direction === 4) {
        this.closePage();
    }
  }

  private contentTitle(): string {
    if (this.damageReportsDataModel.airwaybill != null) {
      return AppUIHelper.getInstance().i18nText('damagereportfor')
             + ' '
             + this.damageReportsDataModel.airwaybill.airwaybillPrefix
             + '-'
             + this.damageReportsDataModel.airwaybill.airwaybillSerial;
    } else if (this.damageReportsDataModel.houseAirwaybill != null) {
            return AppUIHelper.getInstance().i18nText('damagereportfor')
                  + ' '
                  + this.damageReportsDataModel.houseAirwaybill.masterAirwaybill.airwaybillPrefix
                  + '-'
                  + this.damageReportsDataModel.houseAirwaybill.masterAirwaybill.airwaybillSerial;
    }
  }

  /*
    PATCH :: Related to MOB-224, MOB-258 in JIRA
    Q) Why this function ? Why this Patch ?
    A) In iOS, when the keyboard is hidden and we focus on any input for the first time, the keyboard show callback
    doesnt come for the first time, and the 'toshowFooter' doesnt get set to false. Hence we are setting it to false
    when any input gets focused.
  */
  private ionInputFocused() {
    if (!KeyboardHelper.getInstance().isKeyboardShown) {
      this.toshowFooter = false;
      this.doACallTo_OnInputFocusedInAnyOfTheChilds();
    }
  }

  /* Related to MOB-224, MOB-258 in JIRA
     Q) Why this function?
     A) This is to call the show or hide footer function in
     create-damage-report screen on tablet in landscape mode based on keyboard position
  */
  private doACallTo_OnInputFocusedInAnyOfTheChilds() {
    if (PlatformHelper.isDevice_A_Tablet() &&
    ScreenOrientationHelper.currentScreenOrientation() === ScreenOrientationType.LANDSCAPE)
    this.navParams.get('onInputFocusedInAnyOfTheChilds')(this.toshowFooter);
  }
}
