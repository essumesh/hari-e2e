import { Component, NgZone } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { IrregularityQualifierDataObject } from '../../../network/dataobjects/IrregularityQualifierDataObject';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { FetchIrregularityResponseObject } from '../../../network/responseobjects/FetchIrregularityResponseObject';
import { CarriersDataModel } from '../../../datamodels/CarriersDataModel';
import { IrregularitiesDataModel } from '../../../datamodels/IrregularitiesDataModel';
import { ScreenOrientationHelper, ScreenOrientationType } from '../../../helpers/ScreenOrientationHelper';
import { PlatformHelper } from '../../../helpers/PlatformHelper';
import { RadioButtonsViewModel } from '../../viewmodels/RadioButtonsViewModel';

@Component({
    selector: 'champ-page-irregularity-detail',
    templateUrl: 'irregularities-detail.html',
})
export class IrregularityDetailsPage extends BaseUINavigationBarView {
    uniqueObjectIdOfRadioButtonViewModel: string;
    irregularitiesDataModel: IrregularitiesDataModel;
    private irregularityInput: string;
    private selectIrregularityFor: string;
    private fetchIrregularityResponseObject: FetchIrregularityResponseObject;
    private irregularityDetailList: Array<CarriersDataModel> = [];
    private isDeviceATablet: boolean = false;
    private isDeviceInLandScapeOrientation: boolean = false;
    constructor(public navController: NavController,
        public navParams: NavParams) {
        super();
        this.irregularitiesDataModel = this.navParams.get('irregularitiesDataModel');
        console.log(this.irregularitiesDataModel);
        this.navigationBarViewModel =
        new NavigationBarViewModel(true,
            '',
            true,
            true,
            true);
        if (PlatformHelper.isDevice_A_Tablet() &&
        ScreenOrientationHelper.currentScreenOrientation() === ScreenOrientationType.LANDSCAPE) {
        this.isDeviceATablet = true;
        this.isDeviceInLandScapeOrientation = true;
        }
        this.irregularityQualifiersResponse();
    }

    onOrientationChangedTo_Landscape() {
        super.onOrientationChangedTo_Landscape();

        try {
            if (PlatformHelper.isDevice_A_Tablet() &&
                this.navController.canGoBack())
                  this.navController.pop();
        } catch (e) {
        }
    }

    ionViewDidLoad() {
        super.ionViewDidLoad();
        let contentTitle = '';
        AppUIHelper.getInstance().i18nTextWithCallBack('irregularitydetailscode', (i18nTextValue: string) => {
                  this.navigationBarViewModel.title = i18nTextValue;
          });
        this.irregularitiesDataModel.houseAirwaybill == null ?
          contentTitle = '' + this.irregularitiesDataModel.airwaybill.airwaybillPrefix
           + '-' + this.irregularitiesDataModel.airwaybill.airwaybillSerial
           : contentTitle = '' + this.irregularitiesDataModel.houseAirwaybill.houseAirwaybillNumber;
        AppUIHelper.getInstance().i18nTextWithCallBack('selectIrregularityFor', (i18nTextValue: string) => {
            this.selectIrregularityFor = i18nTextValue + '  ' + contentTitle;
        });
    }

    irregularityQualifiersResponse() {
            let irregularityQualifierDataObject: IrregularityQualifierDataObject =
              ManualDIHelper.getInstance().createObject('IrregularityQualifierDataObject');
            irregularityQualifierDataObject.prepareRequestWithParameters();

            irregularityQualifierDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
              if (_dataObject.serverErrorDataModel != null) {
                  AppUIHelper.getInstance().showAlert(
                    AppUIHelper.getInstance().i18nText('error'),
                    _dataObject.serverErrorDataModel.errorText,
                    [AppUIHelper.getInstance().i18nText('ok')]);
              } else {
                this.fetchIrregularityResponseObject =
                  _dataObject.responseObject as FetchIrregularityResponseObject;
                this.onGetIrregularityDetails();
              }
            });
        }
        onGetIrregularityDetails() {
            this.irregularityDetailList = this.fetchIrregularityResponseObject.
            irregularityQualifierDataModel.irregularityDetailList;
            this.setSelectedRadioButtonViewModel();
        }

        private onSearchIrregularityCode() {
            this.onGetIrregularityDetails();
            if (this.irregularityInput != null && this.irregularityInput.trim() !== '') {
                this.irregularityDetailList = this.irregularityDetailList.filter((item) => {
                    if (item.code.toLowerCase().includes(this.irregularityInput.toLocaleLowerCase()))
                    return (item.code.toLowerCase().startsWith(this.irregularityInput.toLocaleLowerCase()));
                    else if (item.name.toLowerCase().includes(this.irregularityInput.toLocaleLowerCase())) {
                        return (item.name.toLowerCase().startsWith(this.irregularityInput.toLocaleLowerCase()));
                    } else {
                        return '';
                    }
                });
              }
        }

        private onSelectIrregularityCode(_irregularityCode: CarriersDataModel) {
             this.irregularitiesDataModel.irregularityDetails = _irregularityCode;
             if (!this.isDeviceATablet && !this.isDeviceInLandScapeOrientation)
            this.navController.pop();
        }

        private setSelectedRadioButtonViewModel() {
            for (let i = 0; i < this.irregularityDetailList.length; i++) {
                if (this.irregularitiesDataModel.irregularityDetails.code === this.irregularityDetailList[i].code) {
                   this.uniqueObjectIdOfRadioButtonViewModel =
                   this.irregularityDetailList[i].getUniqueObjectId().toString();
                   break;
                  }
            }
        }

}
