import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { BarcodeScannerHelper } from '../../../helpers/BarcodeScannerHelper';
import { FetchPickUpDeliveryDataObject } from '../../../network/dataobjects/FetchPickUpDeliveryDataObject';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { FetchPickUpDeliveryResponseObject } from
  '../../../network/responseobjects/FetchPickUpDeliveryResponseObject';
import { PickupDriverInfoDataObject } from '../../../network/dataobjects/PickupDriverInfoDataObject';
import { PickupTypeListPage } from '../pickup-types-list/pickup-types-list';
import { PickupViewListPage } from '../pickup-view-list/pickup-view-list';
import { PickUpDeliveryDataModel } from '../../../datamodels/PickUpDeliveryDataModel';
import { PageConstants } from '../../constants/PageConstants';
import { FetchAirlineTransferDataObject } from '../../../network/dataobjects/FetchAirlineTransferDataObject';

@Component({
  selector: 'champ-page-delivery-and-transfers',
  templateUrl: 'deliveries-and-transfers.html',
})
export class DeliveryTransferPage extends BaseUINavigationBarView {
  receiptManifestText: string;
  fetchPickUpDeliveryResponseObject: FetchPickUpDeliveryResponseObject;
  driverInfo: boolean;
  noRecord: boolean;
  isAirlineTransfer: boolean;
  receiptlabel: string ;
  private _barCodeForInternalHardwareScannerForZebraDevicesSubscriber: any;
  constructor(private navParams: NavParams,
    private navCtrl: NavController,
    private viewCtrl: ViewController) {
    super();

    this.navigationBarViewModel =
      new NavigationBarViewModel(true,
        AppUIHelper.getInstance().i18nText('Deliveries & Transfers'),
        true,
        true,
        true);

    this.airlineTransferValidation();
  }

  ionViewDidLoad() {
    super.ionViewDidLoad();

    AppUIHelper.getInstance().i18nTextWithCallBack(this.getTitle(), (i18nTextValue: string) => {
      this.receiptlabel = i18nTextValue;
    });

  }
  ionViewDidEnter() {
    super.ionViewDidEnter();
    this.registerForBarCodeForInternalHardwareScannerForZebraDevices();
  }

  ionViewDidLeave() {
    super.ionViewDidLeave();
    this.unregisterForBarCodeForInternalHardwareScannerForZebraDevices();
  }
  onSearchClicked() {
    typeof this.receiptManifestText === 'undefined' || this.receiptManifestText === null ||
      this.receiptManifestText === '' ?
      AppUIHelper.getInstance().showAlert(AppUIHelper.getInstance().i18nText('error'),
        AppUIHelper.getInstance().i18nText('delivceryReceiptError'), [AppUIHelper.getInstance().i18nText('ok')]) :
        this.isAirlineTransfer ? this.getAirlineTransferResponse(this.receiptManifestText) :
        this.deliveryReceiptResponse(this.receiptManifestText);
  }
  onScanClicked() {
    BarcodeScannerHelper.getInstance().scan((barcodeData: string) => {
      this.receiptManifestText = barcodeData;
      this.isAirlineTransfer ? this.getAirlineTransferResponse(barcodeData) :
      this.deliveryReceiptResponse(barcodeData);
    }, (errorText: string) => {
      alert(errorText);
    });
  }

  deliveryReceiptResponse(deliveryReceiptNumber: string) {

    let fetchPickUpDeliveryDataObject: FetchPickUpDeliveryDataObject =
      ManualDIHelper.getInstance().createObject('FetchPickUpDeliveryDataObject');

    fetchPickUpDeliveryDataObject.prepareRequestWithParameters(deliveryReceiptNumber);

    fetchPickUpDeliveryDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {

      if (_dataObject.serverErrorDataModel != null) {
        if (_dataObject.serverErrorDataModel.errorText === 'Record not found') {
          this.noRecord = true;
          this.driverInfo = false;
        } else {
          this.disableErrorInfo();
          AppUIHelper.getInstance().showAlert(
            AppUIHelper.getInstance().i18nText('error'),
            _dataObject.serverErrorDataModel.errorText,
            [AppUIHelper.getInstance().i18nText('ok')]);
        }
      } else {
        this.disableErrorInfo();
        this.fetchPickUpDeliveryResponseObject =
          _dataObject.responseObject as FetchPickUpDeliveryResponseObject;
        this.receiptManifestText = '';
        if (this.fetchPickUpDeliveryResponseObject.pickupReferencesDataModel.deliveryReceipt.length === 2)
          this.navigateToPickupTypeListPage();
        else
          this.navigateToPickupViewListPage();
      }
    });
  }

  navigateToPickupTypeListPage() {
    this.navCtrl.push(PickupTypeListPage,
      {
        'pickupReferencesDataModel': this.fetchPickUpDeliveryResponseObject.pickupReferencesDataModel,
      });
  }

  navigateToPickupViewListPage() {
    let pickUpDeliveryDataModel: PickUpDeliveryDataModel;
    pickUpDeliveryDataModel = this.fetchPickUpDeliveryResponseObject.pickupReferencesDataModel.deliveryReceipt[0];
    if (pickUpDeliveryDataModel.pickupDriverInfo == null && pickUpDeliveryDataModel.airwaybillArray.length === 0) {
      this.disableDriverInfo();
    }
    // tslint:disable-next-line:one-line
    else if (pickUpDeliveryDataModel.pickupDriverInfo.pickupDriver != null) {
      this.disableRecordInfo();
    }
    // tslint:disable-next-line:one-line
    else {
      (pickUpDeliveryDataModel.pickupType === 'deliveryReceipt') ?
        pickUpDeliveryDataModel.pickupType = 'Delivery Receipt' :
        pickUpDeliveryDataModel.pickupType = 'Bond Transfer';

      this.navCtrl.push(PickupViewListPage, {
        'pickUpDeliveryDataModel': pickUpDeliveryDataModel,
      });
    }
  }
  disableErrorInfo() {
    this.noRecord = false;
    this.driverInfo = false;
  }
  disableDriverInfo() {
    this.noRecord = true;
    this.driverInfo = false;
  }
  disableRecordInfo() {
    this.noRecord = false;
    this.driverInfo = true;
  }

  private registerForBarCodeForInternalHardwareScannerForZebraDevices() {
    this.unregisterForBarCodeForInternalHardwareScannerForZebraDevices();

    this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber =
    BarcodeScannerHelper.getInstance()
    .barCodeForInternalHardwareScannerForZebraDevices.subscribe
    ((_barcodeData: string) => {
      this.isAirlineTransfer ? this.getAirlineTransferResponse(_barcodeData) :
      this.deliveryReceiptResponse(_barcodeData);
    });
  }

  private unregisterForBarCodeForInternalHardwareScannerForZebraDevices() {
    if (this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber != null) {
      this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber.unsubscribe();
      this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber = null;
    }
  }

  private airlineTransferValidation() {
    PageConstants.TO_USE_SCREEN_ID === 4 ? this.isAirlineTransfer = true : this.isAirlineTransfer = false;
  }

  private getTitle(): string {
    let title = 'receiptlabel';
    if (PageConstants.TO_USE_SCREEN_ID === 4) {
        title = 'airlinetransfermanifestnumber';
      }else if (PageConstants.TO_USE_SCREEN_ID === 3) {
        title = 'receiptlabel';
      }
    return title;
  }

  private getAirlineTransferResponse(manifestNumber: string) {
    let fetchAirlineTransferDataObject: FetchAirlineTransferDataObject =
    ManualDIHelper.getInstance().createObject('FetchAirlineTransferDataObject');

    fetchAirlineTransferDataObject.prepareRequestWithParameters(manifestNumber);

    fetchAirlineTransferDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {

    if (_dataObject.serverErrorDataModel != null) {
      if (_dataObject.serverErrorDataModel.errorText === 'Record not found') {
        this.noRecord = true;
      } else {
        this.disableErrorInfo();
        AppUIHelper.getInstance().showAlert(
          AppUIHelper.getInstance().i18nText('error'),
          _dataObject.serverErrorDataModel.errorText,
          [AppUIHelper.getInstance().i18nText('ok')]);
      }
    } else {
      this.disableErrorInfo();
      this.fetchPickUpDeliveryResponseObject =
        _dataObject.responseObject as FetchPickUpDeliveryResponseObject;
      this.receiptManifestText = '';
      this.navCtrl.push(PickupViewListPage, {
        'pickUpDeliveryDataModel': this.fetchPickUpDeliveryResponseObject.pickUpReferencesAirlineTransferDataModel,
      });
    }
  });
  }
}
