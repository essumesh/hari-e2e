import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { ListPopoverViewModel } from '../../viewmodels/ListPopoverViewModel';
import { GenericListItemViewModel } from '../../viewmodels/GenericListItemViewModel';
import { BaseUIPageView } from '../../baseuiviews/forpages/BaseUIPageView';
/**
 * Generated class for the ListPopoverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'champ-page-list-popover',
  templateUrl: 'list-popover.html',
})
export class ListPopoverPage  extends BaseUIPageView {

   listPopoverViewModel: ListPopoverViewModel;

    private genericListViewModelTapEvent: (_genericListItemViewModel: GenericListItemViewModel) => void;

    constructor(public viewController: ViewController, public navParams: NavParams) {
      super();

      this.populateDataInGenericListViewModel(
        this.navParams.get('title'),
        this.navParams.get('listItems'),
        this.navParams.get('buttonText')),
        this.genericListViewModelTapEvent = this.navParams.get('genericListItemViewModelTapEvent');
    }

    populateDataInGenericListViewModel(title: string,
                                       genericListItemViewModels: Array<GenericListItemViewModel>,
                                       buttonText: string) {
      this.listPopoverViewModel = new ListPopoverViewModel(title, genericListItemViewModels, buttonText);
    }

    buttonClicked() {
      this.viewController.dismiss();
    }

    tapEvent_On_GenericList_ViewModel(genericListItemViewModel: GenericListItemViewModel) {
      if (this.genericListViewModelTapEvent != null) {
       this.viewController.dismiss();
       this.genericListViewModelTapEvent(genericListItemViewModel);
      }
    }
}
