import { Component, ViewChild, ChangeDetectorRef, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { NavigationBarComponent } from '../../components/navigation-bar/navigation-bar';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { Searchbar } from 'ionic-angular';
import { FetchAWBDataObject } from '../../../network/dataobjects/FetchAWBDataObject';
import { FetchAWBResponseObject } from '../../../network/responseobjects/FetchAWBResponseObject';
import { FetchHWBDataObject } from '../../../network/dataobjects/FetchHWBDataObject';
import { FetchHWBResponseObject } from '../../../network/responseobjects/FetchHWBResponseObject';
import { AirwaybillsDataModel } from '../../../datamodels/AirwaybillsDataModel';
import { HouseAirwaybillsDataModel } from '../../../datamodels/HouseAirwaybillsDataModel';
import { GenericListItemViewModel } from '../../viewmodels/GenericListItemViewModel';
import { GenericListItemViewModelType } from '../../viewmodels/GenericListItemViewModel';
import { RectDataModel } from '../../../datamodels/RectDataModel';
import { PlatformHelper } from '../../../helpers/PlatformHelper';
import { KeyboardHelper } from '../../../helpers/KeyboardHelper';
import { ExportAcceptanceReportsPage } from '../export-acceptance-reports/export-acceptance-reports';
import { BarcodeScannerHelper } from '../../../helpers/BarcodeScannerHelper';
import { AppMessagePopupHelper } from '../../../helpers/AppMessagePopupHelper';
import { BarcodeParserHelper, BarcodeType } from '../../../helpers/BarcodeParserHelper';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { ListPopoverPage } from '../list-popover/list-popover';

@Component({
  selector: 'champ-page-export-acceptance',
  templateUrl: 'export-acceptance.html',
})
export class ExportAcceptancePage extends BaseUINavigationBarView {
  readonly AWB_SEGMENTTYPE: string = 'awb';
  readonly HWB_SEGMENTTYPE: string = 'hwb';
  readonly BUP_SEGMENTTYPE: string = 'bup';
  @ViewChild('awbPrefixSearchbar') awbPrefixSearchbar: Searchbar;
  @ViewChild('awbSerialSearchbar') awbSerialSearchbar: Searchbar;
  @ViewChild('hwbSearchBar') hwbSearchBar: Searchbar;
  @ViewChild('bupSearchBar') bupSearchBar: Searchbar;
  awbHwbSegmentControlValue: string = this.AWB_SEGMENTTYPE;
  awbPrefixSearchbarText: string;
  awbSerialSearchbarText: string;
  hwbSearchBarText: any;
  bupSearchBarText: any;
  isAWBSerialDisabled: boolean = true;
  airwaybillsDataModels: Array<AirwaybillsDataModel> = null;
  houseAirwaybillsDataModels: Array<HouseAirwaybillsDataModel> = null;
  awbORHWBGenericListItemViewModels: Array<GenericListItemViewModel> = null;
  tempHouseAirwaybillsDataModels: Array<HouseAirwaybillsDataModel> = new Array<HouseAirwaybillsDataModel>();
  multipleAWBForHWBGenericListItemViewModels: Array<GenericListItemViewModel> =
  new Array<GenericListItemViewModel>();
  private fetchAWBDataObject: FetchAWBDataObject;
  private fetchHWBDataObject: FetchHWBDataObject;
  private rectDataModel: RectDataModel;
  private awbHwbListHeightWhenKeyboardNotShown: number;
  private awbHWBListHeight: number;
  private readonly AWBHWBLISTMARGINBOTTOM = 50;
  private toResetControlsAndDataForAWBHWBAutoSuggest: boolean = false;
  private shouldShowLoadingContainerInAWBorHWBList: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public changeDetectorRef: ChangeDetectorRef,
    public popoverController: PopoverController,
    private ngZone: NgZone) {

    super();

    this.navigationBarViewModel =
      new NavigationBarViewModel(true,
        AppUIHelper.getInstance().i18nText('exportacceptancetitle'),
        true,
        true,
        true);
  }

  // TODO: Add external scanner and tablet code

  ionViewDidLoad() {
    this.calculateAWBHWBListHeightWhenKeyboardNotShown();

    this.subscribeToKeyboardShowHideEvents();
  }

  ionViewCanLeave(): boolean {
    if (this.toResetControlsAndDataForAWBHWBAutoSuggest) {
      this.toResetControlsAndDataForAWBHWBAutoSuggest = false;
      this.resetControlsAndData_For_AWBHWB_AutoSuggest();
    }
    return super.ionViewCanLeave();
  }

  onKeyboardShowHideEvent(_keyboard: any) {
    super.onKeyboardShowHideEvent(_keyboard);
    this.calculateAWBHWBListHeight(_keyboard.showing, _keyboard.height);
  }

  onSearchClicked() {
    if (this.awbHwbSegmentControlValue === this.AWB_SEGMENTTYPE) {
      if (AppUIHelper.isValidAWBNumber(this.awbPrefixSearchbar.value, this.awbSerialSearchbar.value)) {
        const _value = this.awbPrefixSearchbar.value + this.awbSerialSearchbar.value;
        this.resetControlsAndData_For_AWBHWB_AutoSuggest();
        this.do_AWB_HWB_AutoSuggest(false, _value);
      }
    }
  }

  onAWBPrefixSearchbarTextChanged() {
    if (this.awbPrefixSearchbarText.length >= 3) {
      this.isAWBSerialDisabled = false;
      setTimeout(() => {
        this.awbSerialSearchbar.setFocus();
      }, 10);
    } else {
      this.isAWBSerialDisabled = true;
      this.awbSerialSearchbarText = '';
    }
    this.cancelAutoCompleteNetworkCalls();
    this.deInit_AWB_HWB_DataModels();
    if (this.awbPrefixSearchbar.value + '' + this.awbSerialSearchbar.value === '') {
      this.resetControlsAndData_For_AWBHWB_AutoSuggest();
    }
  }

  changedAWBPrefix(value: any) {
    this.changeDetectorRef.detectChanges();
    this.awbPrefixSearchbarText = value.length > 3 ? value.substring(0, 3) : value;
  }

  onAWBSerialSearchbarTextChanged() {
    if (this.awbPrefixSearchbar.value + '' + this.awbSerialSearchbar.value === '') {
      this.resetControlsAndData_For_AWBHWB_AutoSuggest();
    } else {
      if (this.awbSerialSearchbarText.length >= 3) {
        this.do_AWB_HWB_AutoSuggest(true);
      } else {
        this.cancelAutoCompleteNetworkCalls();
        this.deInit_AWB_HWB_DataModels();
      }
    }
  }

  onAWBSerialSearchbarBackspacePressed() {
    if (this.awbSerialSearchbarText.length >= 3) {
      this.do_AWB_HWB_AutoSuggest(true);
    } else {
      this.cancelAutoCompleteNetworkCalls();
      this.deInit_AWB_HWB_DataModels();
      if (this.awbSerialSearchbarText.length === 0) {
        this.awbPrefixSearchbar.setFocus();
      }
    }
  }

  onHWBSearchbarTextChanged() {
    if (this.hwbSearchBarText.length >= 3) {
      this.do_AWB_HWB_AutoSuggest(true);
    } else {
      this.cancelAutoCompleteNetworkCalls();
      this.deInit_AWB_HWB_DataModels();
    }
    if (this.hwbSearchBar.value === '') {
      this.resetControlsAndData_For_AWBHWB_AutoSuggest();
    }
  }

  onBUPSearchbarTextChanged() {
    if (this.bupSearchBarText.length >= 3) {
      this.do_AWB_HWB_AutoSuggest(true);
    } else {
      this.cancelAutoCompleteNetworkCalls();
      this.deInit_AWB_HWB_DataModels();
    }
    if (this.bupSearchBar.value === '') {
      this.resetControlsAndData_For_AWBHWB_AutoSuggest();
    }
  }

  awbHwbSegmentControlChanged(_event: any) {
    this.resetControlsAndData_For_AWBHWB_AutoSuggest();
  }

  onScanClicked() {
    this.resetControlsAndData_For_AWBHWB_AutoSuggest();

    BarcodeScannerHelper.getInstance().scan((barcodeData: string) => {
      this.do_AWB_HWB_AutoSuggest(false, barcodeData);
    }, (_errorText: string) => {
    });
  }

  calculateAWBHWBListHeightWhenKeyboardNotShown() {
    /*
      'getBoundingClientRect' is giving wrong value in Portrait mode when queried
       2nd time from landscape to Portrait

       It needs to be check upon again :: There's a conceptual bug on this in Ionic
    */
    let searchbarContainer;
    if (this.awbHwbSegmentControlValue === this.AWB_SEGMENTTYPE)
      searchbarContainer = document.getElementById('containerAWB').getBoundingClientRect();
    else if (this.awbHwbSegmentControlValue === this.HWB_SEGMENTTYPE)
      searchbarContainer = document.getElementById('containerHWB').getBoundingClientRect();
    else
      searchbarContainer = document.getElementById('containerBUP').getBoundingClientRect();

    let navBarHeight = document.getElementById('searchBarHeader').getBoundingClientRect().height;

    // Calculate the height of the list by subtracting the bottom position of the awb search bar from the screenheight
    this.awbHwbListHeightWhenKeyboardNotShown = PlatformHelper.screenHeight() -
      navBarHeight - searchbarContainer.bottom - this.AWBHWBLISTMARGINBOTTOM;
  }

  calculateAWBHWBListHeight(isKeyboardShown: boolean, keyboardHeight: number) {
    if (isKeyboardShown && PlatformHelper.isOrientation_Portrait()) {
      this.awbHWBListHeight = this.awbHwbListHeightWhenKeyboardNotShown - keyboardHeight;
    } else {
      this.awbHWBListHeight = this.awbHwbListHeightWhenKeyboardNotShown;
    }

    this.initRectDataModel();
  }

  tapEvent_On_AWBORHWBGenericListItemViewModels(_genericListItemViewModel: GenericListItemViewModel) {
    this.toResetControlsAndDataForAWBHWBAutoSuggest = true;
    if (this.awbHwbSegmentControlValue === this.AWB_SEGMENTTYPE)
      this.navCtrl.push(ExportAcceptanceReportsPage,
        {
          'airwaybillsDataModel':
          this.awbHwbSegmentControlValue === this.AWB_SEGMENTTYPE ?
            this.airwaybillsDataModels[_genericListItemViewModel.index] :
            null,
        });
    else if (this.awbHwbSegmentControlValue === this.HWB_SEGMENTTYPE)
      this.showMultipleMasterAWBsForHWBIfRequired(_genericListItemViewModel.index);
    else { // For BUP

    }

  }

  private do_AWB_HWB_AutoSuggest(calledByUIOrBarcodeScanner: boolean, _valueFromBarcodeScanner?: string) {
    const doAWBNetworkCall = ((_awbPrefix: string, _awbSerial: string) => {
      if (!calledByUIOrBarcodeScanner)
        AppMessagePopupHelper.showLoadingAppMessagePopup(AppUIHelper.getInstance().i18nText('loading'));
      this.shouldShowLoadingContainerInAWBorHWBList = true;
      this.fetchAWBDataObject =
        ManualDIHelper.getInstance().createObject('FetchAWBDataObject');
      this.fetchAWBDataObject.prepareRequestWithParameters
        (_awbPrefix, _awbSerial);
      this.fetchAWBDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
        this.shouldShowLoadingContainerInAWBorHWBList = false;
        if (!calledByUIOrBarcodeScanner)
          AppMessagePopupHelper.getInstance().hideAppMessagePopup();
        if (_dataObject.serverErrorDataModel != null) {
          AppUIHelper.getInstance().showAlert('',
            _dataObject.serverErrorDataModel.errorText,
            [AppUIHelper.getInstance().i18nText('ok')]);
        } else {
          let fetchAWBResponseObject: FetchAWBResponseObject = _dataObject.responseObject as FetchAWBResponseObject;
          if (fetchAWBResponseObject.awbNotFound) {
            const fakeAirwaybillsDataModel: AirwaybillsDataModel =
              new AirwaybillsDataModel();
            fakeAirwaybillsDataModel.airwaybillPrefix = _awbPrefix;
            fakeAirwaybillsDataModel.airwaybillSerial = _awbSerial;
            fetchAWBResponseObject.airwaybillsDataModels.push(fakeAirwaybillsDataModel);
          }
          this.airwaybillsDataModels = fetchAWBResponseObject.airwaybillsDataModels;

          if (calledByUIOrBarcodeScanner)
            this.prepare_AirwaybillsGenericListItemViewModels();
          else {
            let fakeGenericListItemViewModel: GenericListItemViewModel =
              new GenericListItemViewModel(0, '0', GenericListItemViewModelType.MainText);
            this.tapEvent_On_AWBORHWBGenericListItemViewModels(fakeGenericListItemViewModel);
          }
        }
      });
    });

    const doHWBNetworkCall = ((_hwb: string) => {
      if (!calledByUIOrBarcodeScanner)
        AppMessagePopupHelper.showLoadingAppMessagePopup(AppUIHelper.getInstance().i18nText('loading'));
      this.shouldShowLoadingContainerInAWBorHWBList = true;
      this.fetchHWBDataObject =
        ManualDIHelper.getInstance().createObject('FetchHWBDataObject');
      this.fetchHWBDataObject.prepareRequestWithParameters(_hwb);
      this.fetchHWBDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
        this.shouldShowLoadingContainerInAWBorHWBList = false;
        if (!calledByUIOrBarcodeScanner)
          AppMessagePopupHelper.getInstance().hideAppMessagePopup();
        if (_dataObject.serverErrorDataModel != null) {
          AppUIHelper.getInstance().showAlert('',
            AppUIHelper.getInstance().i18nText('invalidhwbnumber'),
            [AppUIHelper.getInstance().i18nText('ok')]);
        } else {
          let fetchHWBResponseObject: FetchHWBResponseObject = _dataObject.responseObject as FetchHWBResponseObject;
          this.houseAirwaybillsDataModels = fetchHWBResponseObject.houseAirwaybillsDataModels;
          if (calledByUIOrBarcodeScanner)
            this.prepare_HouseAirwaybillsGenericListItemViewModels();
          else {
            let fakeGenericListItemViewModel: GenericListItemViewModel =
              new GenericListItemViewModel(0, '0', GenericListItemViewModelType.MainText);
            this.tapEvent_On_AWBORHWBGenericListItemViewModels(fakeGenericListItemViewModel);
          }
        }
      });
    });

    const doBUPNetworkCall = ((_bup: string) => {
    });

    if (calledByUIOrBarcodeScanner) {
      if (this.awbHwbSegmentControlValue === this.AWB_SEGMENTTYPE) {
        this.cancelAutoCompleteNetworkCalls();
        this.initAWBORHWBGenericListItemViewModels();
        doAWBNetworkCall(this.awbPrefixSearchbar.value, this.awbSerialSearchbar.value);
      } else if (this.awbHwbSegmentControlValue === this.HWB_SEGMENTTYPE) {
        this.cancelAutoCompleteNetworkCalls();
        this.initAWBORHWBGenericListItemViewModels();
        doHWBNetworkCall(this.hwbSearchBar.value.toUpperCase());
      } else {
        this.cancelAutoCompleteNetworkCalls();
        this.initAWBORHWBGenericListItemViewModels();
        doBUPNetworkCall(this.bupSearchBar.value.toUpperCase());
      }
    } else {
      let barcodeType = BarcodeParserHelper.barcodeType(_valueFromBarcodeScanner);
      if (barcodeType === BarcodeType.AWB) {
        const barcodeValues = BarcodeParserHelper.barcodeValues(_valueFromBarcodeScanner, barcodeType);
        doAWBNetworkCall(barcodeValues[0], barcodeValues[1]);
      } else if (barcodeType === BarcodeType.HWB) {
        const barcodeValues = BarcodeParserHelper.barcodeValues(_valueFromBarcodeScanner, barcodeType);
        doHWBNetworkCall(barcodeValues[0]);
      }
      // TODO: Add condition for BUP
    }
  }

  private showMultipleMasterAWBsForHWBIfRequired(selectedIndex: number) {
    this.resetTempArrayOfMultipleAWBsForHWB();
    let selectedHouseAirwayBillNumber =
      this.houseAirwaybillsDataModels[selectedIndex].houseAirwaybillNumber;
    for (let i = 0; i < this.houseAirwaybillsDataModels.length; i++) {
      if (this.houseAirwaybillsDataModels[i].houseAirwaybillNumber === selectedHouseAirwayBillNumber) {
        this.tempHouseAirwaybillsDataModels.push(this.houseAirwaybillsDataModels[i]);
      }
    }

    if (this.tempHouseAirwaybillsDataModels.length > 1) {
      this.showPopupWithMultipleAWBsForHWB(this.tempHouseAirwaybillsDataModels);
    } else {
      this.showExportAcceptanceReportsScreenForHWB(this.houseAirwaybillsDataModels[selectedIndex]);
    }
  }

  private showPopupWithMultipleAWBsForHWB(houseAirwaybillsDataModels: Array<HouseAirwaybillsDataModel>) {
    houseAirwaybillsDataModels.forEach((houseAirwaybillsDataModel, index) => {
      let genericListItemViewModel: GenericListItemViewModel =
        new GenericListItemViewModel(index, '', GenericListItemViewModelType.MainText);
      genericListItemViewModel.mainText =
        houseAirwaybillsDataModel.masterAirwaybill.airwaybillPrefix
        + ' ' +
        houseAirwaybillsDataModel.masterAirwaybill.airwaybillSerial;
      genericListItemViewModel.shouldShowRightArrow = true;
      this.multipleAWBForHWBGenericListItemViewModels.push(genericListItemViewModel);
    });

    this.presentPopupWithMultipleAWBNumbersForHWB(houseAirwaybillsDataModels,
      houseAirwaybillsDataModels[0].houseAirwaybillNumber);
  }

  private presentPopupWithMultipleAWBNumbersForHWB(hwbDataModels: Array<HouseAirwaybillsDataModel>, hwbNumber: string) {
    let popover = this.popoverController.create(ListPopoverPage, {
      'title': hwbDataModels.length + ' AWB Results For ' + hwbNumber,
      'listItems': this.multipleAWBForHWBGenericListItemViewModels,
      'buttonText': AppUIHelper.getInstance().i18nText('cancel'),
      'genericListItemViewModelTapEvent':
      (_genericListItemViewModel: GenericListItemViewModel) => {
        this.resetTempArrayOfMultipleAWBsForHWB();
        this.showExportAcceptanceReportsScreenForHWB(hwbDataModels[_genericListItemViewModel.index]);
      },
    },
    );
    popover.present({
    });

    popover.onDidDismiss(() => {
      this.resetTempArrayOfMultipleAWBsForHWB();
    },
    );
  }

  private resetTempArrayOfMultipleAWBsForHWB() {
    this.tempHouseAirwaybillsDataModels = [];
    this.multipleAWBForHWBGenericListItemViewModels = [];
  }

  private showExportAcceptanceReportsScreenForHWB(hwbDataModel: HouseAirwaybillsDataModel) {
    this.navCtrl.push(ExportAcceptanceReportsPage,
      {
        'houseAirwaybillsDataModel':
        this.awbHwbSegmentControlValue === this.HWB_SEGMENTTYPE ?
          hwbDataModel :
          null,
      });
  }

  private onOrientationChange() {
    this.unsubscribeToKeyboardShowHideEvents();
    KeyboardHelper.getInstance().hideKeyboard();
    setTimeout(() => {
      this.ngZone.run(() => {
        this.calculateAWBHWBListHeightWhenKeyboardNotShown();
        this.calculateAWBHWBListHeight(false, 0);

        this.subscribeToKeyboardShowHideEvents();
      });
    }, 50);
  }

  private initAWBORHWBGenericListItemViewModels() {
    this.initRectDataModel();
    this.awbORHWBGenericListItemViewModels = [];
  }

  private initRectDataModel() {
    let searchbarContainer;
    if (this.awbHwbSegmentControlValue === this.AWB_SEGMENTTYPE)
      searchbarContainer = document.getElementById('containerAWB').getBoundingClientRect();
    else if (this.awbHwbSegmentControlValue === this.HWB_SEGMENTTYPE)
      searchbarContainer = document.getElementById('containerHWB').getBoundingClientRect();
    else
      searchbarContainer = document.getElementById('containerBUP').getBoundingClientRect();

    let navBarHeight = document.getElementById('searchBarHeader').getBoundingClientRect().height;
    /*
        The navBarHeight is subtracted from the searchbar's bottom point as the list was rendered
        'navBarHeight' point below the 'searchbarContainer.bottom' :: Hence such a solution
    */

    this.rectDataModel = new RectDataModel(searchbarContainer.bottom - navBarHeight,
      searchbarContainer.left,
      searchbarContainer.width,
      this.awbHWBListHeight);
  }

  private resetControlsAndData_For_AWBHWB_AutoSuggest() {
    this.awbPrefixSearchbarText = '';
    this.awbSerialSearchbarText = '';
    this.hwbSearchBarText = '';
    this.bupSearchBarText = '';

    this.deInit_AWB_HWB_DataModels();

    this.cancelAutoCompleteNetworkCalls();
  }

  private deInit_AWB_HWB_DataModels() {
    this.airwaybillsDataModels = null;
    this.houseAirwaybillsDataModels = null;
    this.awbORHWBGenericListItemViewModels = null;
  }

  private cancelAutoCompleteNetworkCalls() {
    if (this.fetchAWBDataObject != null) {
      this.fetchAWBDataObject.cancelRequest();
      this.fetchAWBDataObject = null;
    }
    if (this.fetchHWBDataObject != null) {
      this.fetchHWBDataObject.cancelRequest();
      this.fetchHWBDataObject = null;
    }
  }

  private prepare_AirwaybillsGenericListItemViewModels() {
    this.initAWBORHWBGenericListItemViewModels();
    this.airwaybillsDataModels.forEach((airwaybillsDataModel, index) => {
      let genericListItemViewModel: GenericListItemViewModel =
        new GenericListItemViewModel(index, '', GenericListItemViewModelType.MainText);
      genericListItemViewModel.mainText =
        airwaybillsDataModel.airwaybillPrefix
        + ' '
        + airwaybillsDataModel.airwaybillSerial;
      this.awbORHWBGenericListItemViewModels.push(genericListItemViewModel);
    });
  }

  private prepare_HouseAirwaybillsGenericListItemViewModels() {
    this.initAWBORHWBGenericListItemViewModels();
    this.houseAirwaybillsDataModels.forEach((houseAirwaybillsDataModel, index) => {
      let genericListItemViewModel: GenericListItemViewModel =
        new GenericListItemViewModel(index, '', GenericListItemViewModelType.MainText);
      genericListItemViewModel.mainText =
        houseAirwaybillsDataModel.houseAirwaybillNumber;
      this.awbORHWBGenericListItemViewModels.push(genericListItemViewModel);
    });
  }

}
