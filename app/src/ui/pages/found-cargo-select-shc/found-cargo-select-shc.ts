import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { FoundCargoSHCDataObject } from '../../../network/dataobjects/FoundCargoSHCDataObject';
import { FetchSHCResponseObject } from '../../../network/responseobjects/FetchSHCResponseObject';
import { CarriersDataModel } from '../../../datamodels/CarriersDataModel';
import { CheckBoxesViewModel, CheckBoxViewModel } from '../../viewmodels/CheckBoxesViewModel';
import { PreFetchDataObjectsHelper } from '../../../network/helpers/PreFetchDataObjectsHelper';
import { SHCSelectDataModel } from '../../../datamodels/SHCSelectDataModel';
import { SHCLookupsDataModel } from '../../../datamodels/SHCLookupsDataModel';
import { SHCLookupDataModel } from '../../../datamodels/SHCLookupDataModel';
import { BarcodeScannerHelper } from '../../../helpers/BarcodeScannerHelper';
import { PlatformHelper } from '../../../helpers/PlatformHelper';
@Component({
    selector: 'champ-page-select-shc',
    templateUrl: 'found-cargo-select-shc.html',
})
export class SelectSHCPage extends BaseUINavigationBarView {
    fetchSHCResponseObject: FetchSHCResponseObject =
    PreFetchDataObjectsHelper.getInstance().fetchFoundCargoSHCDataObject.responseObject as
    FetchSHCResponseObject;

    foudCargoSHCCheckBoxesViewModel: CheckBoxesViewModel;
    shcSelectDataModel: SHCSelectDataModel;
    shcCheckBoxViewModels: Array<CheckBoxViewModel> = [];
    shcInput: string;
    airwaybillPrefix: string;
    airwaybillSerial: string;
    selectshcFor: string;
    toShowEnabledSaveButton: boolean;
    private _barCodeForInternalHardwareScannerForZebraDevicesSubscriber: any;

    constructor(public navController: NavController,
        public navParams: NavParams) {
        super();
        this.airwaybillPrefix = this.navParams.get('airwaybillPrefix');
        this.airwaybillSerial = this.navParams.get('airwaybillSerial');
        this.shcSelectDataModel = this.navParams.get('shcSelectDataModel');
        this.navigationBarViewModel =
            new NavigationBarViewModel(true,
                '',
                true,
                true,
                true);
        if (this.shcSelectDataModel.specialHandlingCode.length === 0) {
            this.shcSelectDataModel.specialHandlingCode.push(new SHCLookupsDataModel());
        }
        this.prepareSHCCheckBoxesViewModel();
    }

    ionViewDidLoad() {
        super.ionViewDidLoad();
        AppUIHelper.getInstance().i18nTextWithCallBack('addshc', (i18nTextValue: string) => {
            this.navigationBarViewModel.title = i18nTextValue;
        });
        AppUIHelper.getInstance().i18nTextWithCallBack('selectshcFor', (i18nTextValue: string) => {
            this.selectshcFor = i18nTextValue + '  ' + this.airwaybillPrefix + '-' + this.airwaybillSerial;
        });
    }

    ionViewDidEnter() {
        super.ionViewDidEnter();
        this.registerForBarCodeForInternalHardwareScannerForZebraDevices();
      }

      ionViewDidLeave() {
        super.ionViewDidLeave();
        this.unregisterForBarCodeForInternalHardwareScannerForZebraDevices();
      }

    onOrientationChangedTo_Landscape() {
        super.onOrientationChangedTo_Landscape();

        try {
            if (PlatformHelper.isDevice_A_Tablet() &&
                this.navController.canGoBack())
                  this.navController.pop();
        } catch (e) {
        }
    }

    private prepareSHCCheckBoxesViewModel() {
        this.foudCargoSHCCheckBoxesViewModel = new CheckBoxesViewModel();
        for (let shcDataModel of
            this.fetchSHCResponseObject.foundCargoSHCDataModel.specialHandling.shcType) {
            this.foudCargoSHCCheckBoxesViewModel.addCheckBoxViewModel(
                shcDataModel.code,
                this.shcSelectDataModel.specialHandlingCode[0].contains(shcDataModel),
                shcDataModel);
        }
        console.log(this.shcSelectDataModel.specialHandlingCode[0].shcType);
        this.onEnableSaveButton();
        this.onGetSHCDetails();
    }

    private onGetSHCDetails() {
        this.shcCheckBoxViewModels = this.foudCargoSHCCheckBoxesViewModel.checkBoxViewModels;
    }

    private onSelectedSHC(_checkBoxViewModel: CheckBoxViewModel, event: any) {
        console.log('1');
        if (this.shcSelectDataModel.specialHandlingCode[0].shcType.length === 9 && event.checked) {
        this.showErrorMessgae('Only 9 SHC selections allowed ');
        event.checked = false ;
        } else {
        let _shcLookupDataModel: SHCLookupDataModel = _checkBoxViewModel.extras as SHCLookupDataModel;
        if (_checkBoxViewModel.isSelected) {
                this.shcSelectDataModel.specialHandlingCode[0].addIfNotPresent(_shcLookupDataModel);
                this.shcInput = '';
            } else {
                this.shcSelectDataModel.specialHandlingCode[0].removeIfPresent(_shcLookupDataModel);
            }
        this.onEnableSaveButton();
        }

    }
    private onRemoveSHC(_shcLookupDataModel: SHCLookupDataModel) {
        let index = this.findScanedSHCIndex(this.shcCheckBoxViewModels, _shcLookupDataModel.code);
        this.shcCheckBoxViewModels[index].isSelected = false;
        this.shcSelectDataModel.specialHandlingCode[0].removeIfPresent(_shcLookupDataModel);

    }
    private onSearchSHC() {
        this.onGetSHCDetails();
        if (this.shcInput != null && this.shcInput.trim() !== '') {
            this.shcCheckBoxViewModels = this.shcCheckBoxViewModels.filter((item) => {
                return (item.name.toLowerCase().startsWith(this.shcInput.toLocaleLowerCase()));
            });
        }
    }
    private onEnableSaveButton() {
        if (this.shcSelectDataModel.specialHandlingCode[0].shcType.length === 0)
            this.toShowEnabledSaveButton = false;
        else this.toShowEnabledSaveButton = true;
    }
    private onSaveClicked() {
        this.navController.pop();
    }

    private onScanClicked() {
        console.log(this.shcCheckBoxViewModels.length);
        BarcodeScannerHelper.getInstance().scan((barcodeData: string) => {
            this.shcInput = barcodeData;
            let toIndex = 0;
            let fromIndex = this.findScanedSHCIndex(this.shcCheckBoxViewModels, barcodeData);
            console.log(fromIndex);
            this.shcCheckBoxViewModels.splice(toIndex, 0,
                this.shcCheckBoxViewModels.splice(fromIndex, 1)[0]);
        }, (_errorText: string) => {
    });
        console.log(this.shcCheckBoxViewModels.length);

    }
    private findScanedSHCIndex(array: Array<CheckBoxViewModel>, value: string) {
        for (let i = 0; i < array.length; i += 1) {
            if (array[i].name === value) {
                return i;
            }
        }
        return -1;
    }

    private shcCountValidate(_checkBoxViewModel: CheckBoxViewModel) {
       /* if (this.shcSelectDataModel.specialHandlingCode[0].shcType.length >= 9) {
            let _shcLookupDataModel: SHCLookupDataModel = _checkBoxViewModel.extras as SHCLookupDataModel;
            this.onRemoveSHC(_shcLookupDataModel);

        } */
    }

    private showErrorMessgae(msg: string) {
        AppUIHelper.getInstance().showAlert(
            AppUIHelper.getInstance().i18nText('error'), msg,
            [AppUIHelper.getInstance().i18nText('ok')]);
    }

    private registerForBarCodeForInternalHardwareScannerForZebraDevices() {
        this.unregisterForBarCodeForInternalHardwareScannerForZebraDevices();
        this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber =
        BarcodeScannerHelper.getInstance()
        .barCodeForInternalHardwareScannerForZebraDevices.subscribe
        ((_barcodeData: string) => {
            this.shcInput = _barcodeData;
            let toIndex = 0;
            let fromIndex = this.findScanedSHCIndex(this.shcCheckBoxViewModels, _barcodeData);
            console.log(fromIndex);
            this.shcCheckBoxViewModels.splice(toIndex, 0,
                this.shcCheckBoxViewModels.splice(fromIndex, 1)[0]);
        });
      }

      private unregisterForBarCodeForInternalHardwareScannerForZebraDevices() {
        if (this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber != null) {
          this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber.unsubscribe();
          this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber = null;
        }
      }
}
