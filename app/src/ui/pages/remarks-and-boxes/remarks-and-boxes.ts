import { Component } from '@angular/core';
import filter from 'lodash-es/filter';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { DamageReportsDataModel } from '../../../datamodels/DamageReportsDataModel';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { RecuperationPage } from '../recuperation/recuperation';
import { PlatformHelper } from '../../../helpers/PlatformHelper';
import { ScreenOrientationHelper, ScreenOrientationType } from '../../../helpers/ScreenOrientationHelper';
import { KeyboardHelper } from '../../../helpers/KeyboardHelper';

@Component({
  selector: 'champ-page-remarks-and-boxes',
  templateUrl: 'remarks-and-boxes.html',
})
export class RemarksPage extends BaseUINavigationBarView {
  damageReportsDataModel: DamageReportsDataModel;
  private toShowEnabledSaveButton: boolean = false;
  private toShowEmailAndPrintOptions: boolean = false;
  private isDeviceATablet: boolean = false;
  private isDeviceInLandScapeOrientation: boolean = false;
  private toshowFooter: boolean = true;

  constructor(public navController: NavController,
              public navParams: NavParams,
              private viewCtrl: ViewController) {
    super();

    this.damageReportsDataModel =
    this.navParams.get('damageReportsDataModel');
    this.toShowEnabledSaveButton =
    this.navParams.get('doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds')();
    this.toShowEmailAndPrintOptions =
    this.navParams.get('doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds')();
    this.navigationBarViewModel =
        new NavigationBarViewModel(true,
                               AppUIHelper.getInstance().i18nText('remarksandboxes'),
                               true,
                               true,
                               true);
    if (PlatformHelper.isDevice_A_Tablet() &&
        ScreenOrientationHelper.currentScreenOrientation() === ScreenOrientationType.LANDSCAPE) {
        this.isDeviceATablet = true;
        this.isDeviceInLandScapeOrientation = true;
    }
  }

  ionViewDidEnter() {
    super.ionViewDidEnter();

    this.subscribeToKeyboardShowHideEvents();
  }

  ionViewDidLeave() {
    super.ionViewDidLeave();

    this.unsubscribeToKeyboardShowHideEvents();
  }

  onOrientationChangedTo_Landscape() {
    super.onOrientationChangedTo_Landscape();

    try {
        if (PlatformHelper.isDevice_A_Tablet() &&
            this.navController.canGoBack())
              this.navController.pop();
    } catch (e) {
    }
  }

  onKeyboardShowHideEvent(_keyboard: any) {
    super.onKeyboardShowHideEvent(_keyboard);

    if (_keyboard.showing) {
        this.toshowFooter = false;
    } else {
        this.toshowFooter = true;
    }
    this.doACallTo_OnInputFocusedInAnyOfTheChilds();
  }

  onSaveClicked() {
    this.navParams.get('onSaveClickedOnOneOfTheChilds')();
    this.viewCtrl.dismiss();
  }

  onPrintButtonClicked() {
    this.navParams.get('onPrintClickedOnOneOfTheChilds')();
    this.viewCtrl.dismiss();
  }

  onEmailButtonClicked() {
    this.navParams.get('onEmailClickedOnOneOfTheChilds')();
    this.viewCtrl.dismiss();
  }

  closePage() {
    this.navController.push(RecuperationPage,
            {'damageReportsDataModel': this.damageReportsDataModel,
             'onSaveClickedOnOneOfTheChilds': this.navParams.get('onSaveClickedOnOneOfTheChilds'),
             'someUIElementsHaveChangedItsStateOnOneOfTheChilds':
             this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds'),
             'doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds':
             this.navParams.get('doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds'),
             'doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds':
             this.navParams.get('doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds'),
             'onInputFocusedInAnyOfTheChilds': this.navParams.get('onInputFocusedInAnyOfTheChilds'),
             'onPrintClickedOnOneOfTheChilds': this.navParams.get('onPrintClickedOnOneOfTheChilds'),
             'onEmailClickedOnOneOfTheChilds': this.navParams.get('onEmailClickedOnOneOfTheChilds')}).then(() => {
            this.viewCtrl.dismiss();
    });
  }

  private swiped(event: any) {
    if (event.direction === 4) {
          this.closePage();
    }
  }

  private contentTitle(): string {
    if (this.damageReportsDataModel.airwaybill != null) {
      return AppUIHelper.getInstance().i18nText('damagereportfor')
             + ' '
             + this.damageReportsDataModel.airwaybill.airwaybillPrefix
             + '-'
             + this.damageReportsDataModel.airwaybill.airwaybillSerial;
    } else if (this.damageReportsDataModel.houseAirwaybill != null) {
            return AppUIHelper.getInstance().i18nText('damagereportfor')
                  + ' '
                  + this.damageReportsDataModel.houseAirwaybill.masterAirwaybill.airwaybillPrefix
                  + '-'
                  + this.damageReportsDataModel.houseAirwaybill.masterAirwaybill.airwaybillSerial;
    }
  }

  /*
    PATCH :: Related to MOB-224, MOB-258 in JIRA
    Q) Why this function ? Why this Patch ?
    A) In iOS, when the keyboard is hidden and we focus on any input for the first time, the keyboard show callback
    doesnt come for the first time, and the 'toshowFooter' doesnt get set to false. Hence we are setting it to false
    when any input gets focused.
  */
  private ionInputFocused() {
    if (!KeyboardHelper.getInstance().isKeyboardShown) {
      this.toshowFooter = false;
      this.doACallTo_OnInputFocusedInAnyOfTheChilds();
    }
  }

  /* Related to MOB-224, MOB-258 in JIRA
     Q) Why this function?
     A) This is to call the show or hide footer function in
     create-damage-report screen on tablet in landscape mode based on keyboard position
  */
  private doACallTo_OnInputFocusedInAnyOfTheChilds() {
    if (PlatformHelper.isDevice_A_Tablet() &&
    ScreenOrientationHelper.currentScreenOrientation() === ScreenOrientationType.LANDSCAPE)
    this.navParams.get('onInputFocusedInAnyOfTheChilds')(this.toshowFooter);
  }
}
