import { Component, ViewChild, ViewChildren, QueryList, AfterViewInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav } from 'ionic-angular';
import { DamageReportsDataModel } from '../../../datamodels/DamageReportsDataModel';
import { LinkMediaDataModel } from '../../../datamodels/LinkMediaDataModel';
import { RectDataModel } from '../../../datamodels/RectDataModel';
import { ImagesDataModel } from '../../../datamodels/ImagesDataModel';
import { GenericListItemViewModel } from '../../viewmodels/GenericListItemViewModel';
import { AppMessagePopupViewModel, AppMessagePopupViewModelType } from '../../viewmodels/AppMessagePopupViewModel';
import { GenericListItemViewModelType } from '../../viewmodels/GenericListItemViewModel';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { PlatformHelper } from '../../../helpers/PlatformHelper';
import { AppMessagePopupHelper } from '../../../helpers/AppMessagePopupHelper';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { DateParserHelper } from '../../../helpers/DateParserHelper';
import { CameraHelper } from '../../../helpers/CameraHelper';
import { ShipmentDetailsPage } from '../../../ui/pages/shipment-details/shipment-details';
import { PackagingDetailPage } from '../../../ui/pages/packaging-details/packaging-details';
import { DamageReportPage } from '../../../ui/pages/damage-report/damage-report';
import { DamageDiscoveryPage } from '../../../ui/pages/damage-discovery/damage-discovery';
import { RecuperationPage } from '../../../ui/pages/recuperation/recuperation';
import { RemarksPage } from '../../../ui/pages/remarks-and-boxes/remarks-and-boxes';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { FetchAWBDamageQualifiersDataObject } from '../../../network/dataobjects/FetchAWBDamageQualifiersDataObject';
import { FetchAWBDamageQualifiersResponseObject } from
'../../../network/responseobjects/FetchAWBDamageQualifiersResponseObject';
import { AWBDamageAPIDataObject } from '../../../network/dataobjects/AWBDamageAPIDataObject';
import { AWBDamageAPIRequestObject } from
'../../../network/requestobjects/AWBDamageAPIRequestObject';
import { AWBDamageAPIResponseObject } from
'../../../network/responseobjects/AWBDamageAPIResponseObject';
import { PreFetchDataObjectsHelper } from
'../../../network/helpers/PreFetchDataObjectsHelper';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from
'@ionic-native/media-capture';
import { AlertController, Alert } from 'ionic-angular';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { FileTransferRequestObject, FileTransferType, FileTransferCategory } from
'../../../network/requestobjects/FileTransferRequestObject';
import { FileTransferDataObject } from
'../../../network/dataobjects/FileTransferDataObject';
import { DamageReportEmailPage } from '../damage-report-email/damage-report-email';
import { GenericListItemComponent } from '../../components/generic-list-item/generic-list-item';
import { AWBEmailDataObject } from '../../../network/dataobjects/AWBEmailDataObject';
import { AWBEmailDataModel } from '../../../datamodels/AWBEmailDataModel';
import { ScreenOrientationHelper, ScreenOrientationType } from '../../../helpers/ScreenOrientationHelper';

@Component({
  selector: 'champ-page-create-damage-report',
  templateUrl: 'create-damage-report.html',
})
export class CreateDamageReportPage extends BaseUINavigationBarView implements AfterViewInit {
  damageReportsDataModel: DamageReportsDataModel;
  genericListItemViewModels: Array<GenericListItemViewModel> = null;
  genericListItemViewModelsForTabletDevices: Array<GenericListItemViewModel> = null;
  private genericListItemShowingMenuComponentRectDataModel: RectDataModel =
  new RectDataModel(0, 0, 0, 0);
  private wasDamageReportsDataModelNewlyCreated: boolean = false;
  private imageOrVideoOptionAlert: Alert;
  private fileSizeLimitExceededAlert: Alert;
  private readonly LISTITEMTAGHORIZONTALIMAGES = 'LISTITEMTAGHORIZONTALIMAGES';
  private readonly LISTITEMTAGSHIPMENTDETAILS = 'LISTITEMTAGSHIPMENTDETAILS';
  private readonly LISTITEMTAGPACKINGDETAILS = 'LISTITEMTAGPACKINGDETAILS';
  private readonly LISTITEMTAGDAMAGEREPORT = 'LISTITEMTAGDAMAGEREPORT';
  private readonly LISTITEMTAGDAMAGEDISCOVERY = 'LISTITEMTAGDAMAGEDISCOVERY';
  private readonly LISTITEMTAGRECUPERATION = 'LISTITEMTAGRECUPERATION';
  private readonly LISTITEMTAGREMARKSANDBOXES = 'LISTITEMTAGREMARKSANDBOXES';
  private readonly LISTMARGINBOTTOM = 2;
  private readonly LISTBORDERRADIUS = 5;
  private toShowEmailAndPrintOptions: boolean;
  private readonly FILESIZE20MB: number = 20000000;
  private onSaveClickedOnOneOfTheChilds: (() => void) = (() => {
    this.onSaveClicked();
  });
  private onPrintClickedOnOneOfTheChilds: (() => void) = (() => {
    this.onPrintButtonClicked();
  });
  private onEmailClickedOnOneOfTheChilds: (() => void) = (() => {
    this.onEmailButtonClicked();
  });
  private onInputFocusedInAnyOfTheChilds: ((toShowFooter: boolean) => void) = ((toShowFooter: boolean) => {
    this.showOrHideFooter(toShowFooter);
  });
  private someUIElementsHaveChangedItsStateOnOneOfTheChilds:
  (() => void) = (() => {
    this.someUIElementsHaveChangedItsState_ChangeStateOfOtherUIElementsAccordingly(true);
    this.someUIElementsHaveChangedItsState_PrintEmailUIElementsAccordingly();
  });
  private doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds: (() => boolean) = (() => {
    return this.toShowEnabledSaveButton;
  });
  private doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds: (() => boolean) = (() => {
    return this.toShowEmailAndPrintOptions;
  });
  private toShowEnabledSaveButton: boolean = true;
  private isDeviceATablet: boolean = false;
  private isDeviceInLandScapeOrientation: boolean = false;
  private listItemShownCurrentlyForTabletLandScapeMode: string = this.LISTITEMTAGSHIPMENTDETAILS;
  private shouldClickEmailButton: boolean = true;
  @ViewChildren('splitPaneMasterComponentNav', {read: Nav}) private splitPaneMasterDetailNavs: QueryList<Nav>;
  @ViewChild('splitPaneMasterComponentNav') private splitPaneMasterComponentNav: Nav;
  @ViewChild('splitPaneDetailComponentNav') private splitPaneDetailComponentNav: Nav;
  private isIonViewDidEnterEverBeenCalledBefore: boolean = false;
  private dateWhenIonViewDidEnterFirst: Date = null;
  private toShowFooter: boolean = true;
  constructor(public navController: NavController,
              public navParams: NavParams,
              private alertCtrl: AlertController) {
    super();

    this.wasDamageReportsDataModelNewlyCreated =
    this.navParams.get('wasDamageReportsDataModelNewlyCreated');
    this.damageReportsDataModel =
    this.navParams.get('damageReportsDataModel');

    this.navigationBarViewModel =
        new NavigationBarViewModel(true,
                               this.navigationBarTitle(),
                               true,
                               true,
                               true);

    this.toRegisterForHardwareBackButtonAction = true;
  }

  ngAfterViewInit() {
    this.splitPaneMasterDetailNavs.changes.subscribe((_comps: QueryList <Nav>) => {
      this.workOn_IsDeviceInLandScapeOrientationFlag();
    });
    this.highlightSelectedList(this.LISTITEMTAGSHIPMENTDETAILS);
  }

  ionViewDidLoad() {
    super.ionViewDidLoad();
    this.fetch_FetchAWBDamageQualifiersDataObject_IfRequired();
  }

  ionViewWillEnter() {
    super.ionViewWillEnter();
    this.someUIElementsHaveChangedItsState_ChangeStateOfOtherUIElementsAccordingly(false);
  }

  ionViewDidEnter() {
    super.ionViewDidEnter();

    if (!this.isIonViewDidEnterEverBeenCalledBefore) {
      this.isIonViewDidEnterEverBeenCalledBefore = true;
      this.dateWhenIonViewDidEnterFirst = new Date();
      this.disableSaveButtonForOnceForExistingDamageReportsDataModel();
    }
    this.workOn_IsDeviceInLandScapeOrientationFlag();
  }

  onHardwareBackButtonPressed() {
    super.onHardwareBackButtonPressed();

    if (this.imageOrVideoOptionAlert != null) {
      this.imageOrVideoOptionAlert.dismiss();
    } else if (this.fileSizeLimitExceededAlert != null) {
      this.fileSizeLimitExceededAlert.dismiss();
    } else {
      this.navController.pop();
    }
  }

  onOrientationChangedTo_Portrait() {
    super.onOrientationChangedTo_Portrait();
    this.workOn_IsDeviceInLandScapeOrientationFlag();
    this.showOrHideFooter(true);
  }

  onOrientationChangedTo_Landscape() {
    super.onOrientationChangedTo_Landscape();
    this.workOn_IsDeviceInLandScapeOrientationFlag();
    this.showOrHideFooter(true);
  }

  highlightSelectedList(listItemShownCurrentlyForTabletLandScapeMode: any) {
    // added to highlight the selected option from list in tablet landscape mode
    setTimeout(() => {
    if (this.isDeviceATablet && this.isDeviceInLandScapeOrientation) {
      let createDamageListDom = document.getElementById('generic-report-list-left-pane');
      for (let i = 0; i < createDamageListDom.children.length; i++)
        createDamageListDom.children[i].attributes[0].value = 'set-opacity-for-non-selected';
      document.getElementById(listItemShownCurrentlyForTabletLandScapeMode).
      attributes[0].value = 'set-opacity-for-selected';
    }
  }, 200);
  }

  navigationBarTitle(): string {
     if (this.damageReportsDataModel.houseAirwaybill != null) {
      return AppUIHelper.getInstance().i18nText('hwb')
             + ' - '
             + this.damageReportsDataModel.houseAirwaybill.houseAirwaybillNumber;
    } else if (this.damageReportsDataModel.airwaybill != null) {
      return AppUIHelper.getInstance().i18nText('awb')
             + ' '
             + this.damageReportsDataModel.airwaybill.airwaybillPrefix
             + '-'
             + this.damageReportsDataModel.airwaybill.airwaybillSerial;
    }
  }

  tapEvent_On_GenericListItemComponent(_genericListItemViewModel: GenericListItemViewModel) {
    const navParamsExtras = {'damageReportsDataModel': this.damageReportsDataModel,
                              'onSaveClickedOnOneOfTheChilds': this.onSaveClickedOnOneOfTheChilds,
                              'someUIElementsHaveChangedItsStateOnOneOfTheChilds':
                              this.someUIElementsHaveChangedItsStateOnOneOfTheChilds,
                              'doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds':
                              this.doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds,
                              'doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds':
                              this.doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds,
                              'onPrintClickedOnOneOfTheChilds': this.onPrintClickedOnOneOfTheChilds,
                              'onEmailClickedOnOneOfTheChilds': this.onEmailClickedOnOneOfTheChilds,
                              'onInputFocusedInAnyOfTheChilds': this.onInputFocusedInAnyOfTheChilds,
                            };
    this.highlightSelectedList(_genericListItemViewModel.tag);
    if (_genericListItemViewModel.tag !== this.LISTITEMTAGHORIZONTALIMAGES)
      this.listItemShownCurrentlyForTabletLandScapeMode = _genericListItemViewModel.tag;
    if (_genericListItemViewModel.tag === this.LISTITEMTAGHORIZONTALIMAGES) {
      this.showImageOrVideoOptionAlert();
    } else if (_genericListItemViewModel.tag === this.LISTITEMTAGSHIPMENTDETAILS) {
      if (this.isDeviceATablet && this.isDeviceInLandScapeOrientation)
        this.splitPaneDetailComponentNav.setRoot(ShipmentDetailsPage, navParamsExtras);
      else
        this.navController.push(ShipmentDetailsPage, navParamsExtras);
    } else if (_genericListItemViewModel.tag === this.LISTITEMTAGPACKINGDETAILS) {
      if (this.isDeviceATablet && this.isDeviceInLandScapeOrientation)
        this.splitPaneDetailComponentNav.setRoot(PackagingDetailPage, navParamsExtras);
      else
        this.navController.push(PackagingDetailPage, navParamsExtras);
    } else if (_genericListItemViewModel.tag === this.LISTITEMTAGDAMAGEREPORT) {
      if (this.isDeviceATablet && this.isDeviceInLandScapeOrientation)
        this.splitPaneDetailComponentNav.setRoot(DamageReportPage, navParamsExtras);
      else
        this.navController.push(DamageReportPage, navParamsExtras);
    } else if (_genericListItemViewModel.tag === this.LISTITEMTAGDAMAGEDISCOVERY) {
      if (this.isDeviceATablet && this.isDeviceInLandScapeOrientation)
        this.splitPaneDetailComponentNav.setRoot(DamageDiscoveryPage, navParamsExtras);
      else
        this.navController.push(DamageDiscoveryPage, navParamsExtras);
    } else if (_genericListItemViewModel.tag === this.LISTITEMTAGRECUPERATION) {
      if (this.isDeviceATablet && this.isDeviceInLandScapeOrientation)
        this.splitPaneDetailComponentNav.setRoot(RecuperationPage, navParamsExtras);
      else
        this.navController.push(RecuperationPage, navParamsExtras);
    } else if (_genericListItemViewModel.tag === this.LISTITEMTAGREMARKSANDBOXES) {
      if (this.isDeviceATablet && this.isDeviceInLandScapeOrientation)
        this.splitPaneDetailComponentNav.setRoot(RemarksPage, navParamsExtras);
      else
        this.navController.push(RemarksPage, navParamsExtras);
    }
  }

  tapEventOnDeleteButtonofHorizontalListingOfMediaThumbNailsURLs(_index: number) {
    AppUIHelper.getInstance().showAlert
    (AppUIHelper.getInstance().i18nText('suretodeletemedia'),
    '',
    [
      {
        text: AppUIHelper.getInstance().i18nText('yes'),
        handler: () => {
          const _linkMediaDataModels: Array<LinkMediaDataModel> =
          this.addProofGenericListItemViewModel().horizontalListingOfMediaThumbNailsURLs.
          linkMediaDataModels;
          const _linkMediaDataModel: LinkMediaDataModel = _linkMediaDataModels[_index];
          const _deleteMediaFromUI = (() => {
            _linkMediaDataModels.splice(_index, 1);
            this.someUIElementsHaveChangedItsState_ChangeStateOfOtherUIElementsAccordingly(false);
          });
          if (_linkMediaDataModel.haveWeGotMediaDataFromServer()) {
            const _awbDamageAPIDataObject: AWBDamageAPIDataObject =
            ManualDIHelper.getInstance().createObject('AWBDamageAPIDataObject');
            _awbDamageAPIDataObject.prepareRequestWithParameters
            (this.wasDamageReportsDataModelNewlyCreated,
             this.damageReportsDataModel,
             false);

            AppMessagePopupHelper.showLoadingAppMessagePopup(AppUIHelper.getInstance().i18nText('deletingmedia'));
            let fileTransferDataObject: FileTransferDataObject =
            ManualDIHelper.getInstance().createObject('FileTransferDataObject');
            fileTransferDataObject.prepareRequestWithParameters
            (FileTransferType.DELETE, _awbDamageAPIDataObject.urlToDELETEMediafrom(_linkMediaDataModel));
            fileTransferDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
              AppMessagePopupHelper.getInstance().hideAppMessagePopup();
              if (_dataObject.serverErrorDataModel != null) {
                AppUIHelper.getInstance().showAlert(
                  AppUIHelper.getInstance().i18nText('error'),
                  _dataObject.serverErrorDataModel.errorText,
                [AppUIHelper.getInstance().i18nText('ok')]);
              } else _deleteMediaFromUI();
            });
          } else _deleteMediaFromUI();
        },
      },
      {
        text: AppUIHelper.getInstance().i18nText('no'),
      },
    ],
    );
  }

  onSaveClicked() {
   let awbDamageAPIDataObject: AWBDamageAPIDataObject =
    ManualDIHelper.getInstance().createObject('AWBDamageAPIDataObject');
   awbDamageAPIDataObject.prepareRequestWithParameters(this.wasDamageReportsDataModelNewlyCreated,
                                                        this.damageReportsDataModel,
                                                        false);

   if (!this.wasDamageReportsDataModelNewlyCreated) {
      if (this.damageReportsDataModel.doWeHaveOnlyThePresenceOfMediaOnDamageReportsDataModel()) {
        (awbDamageAPIDataObject.requestObject as AWBDamageAPIRequestObject)
        .directlySyncMediaWithoutAnySyncingAnythingOnDamageReportData = true;
      }
   }

   if (this.wasDamageReportsDataModelNewlyCreated) {
    this.damageReportsDataModel.reportedOn = new Date();
   }

   AppMessagePopupHelper.showLoadingAppMessagePopup(AppUIHelper.getInstance().i18nText('saveinprogress'));
   awbDamageAPIDataObject.queryTheServer(true, (_dataObject: BaseDataObject) => {
    AppMessagePopupHelper.getInstance().hideAppMessagePopup();
    if (_dataObject.serverErrorDataModel != null) {
      AppUIHelper.getInstance().showAlert(
        AppUIHelper.getInstance().i18nText('error'),
        _dataObject.serverErrorDataModel.errorText,
      [AppUIHelper.getInstance().i18nText('ok')]);
    } else {
      this.toShowEmailAndPrintOptions = true;
      this.damageReportsDataModel.reportId =
      (awbDamageAPIDataObject.responseObject as AWBDamageAPIResponseObject).reportId;

      AppMessagePopupHelper.getInstance().showAppMessagePopup(
        new AppMessagePopupViewModel(
          AppUIHelper.getInstance().i18nText('savedsuccessfully'),
          'assets/images/green-tick-small.svg',
          AppMessagePopupViewModelType.MessagePopup,
        ), 4);
      }
    });
  }

  showImageOrVideoOptionAlert() {
    this.imageOrVideoOptionAlert = this.alertCtrl.create({
    title: AppUIHelper.getInstance().i18nText('selectOption'),
    buttons: [
      {
        text: AppUIHelper.getInstance().i18nText('captureImage'),
        handler: _data => {
          this.showCameraWithImageOption();
        },
      },
      {
        text: AppUIHelper.getInstance().i18nText('recordVideo'),
        handler: _data => {
          this.showCameraWithVideoOption();
        },
      },
      ],
    });
    this.imageOrVideoOptionAlert.onDidDismiss(() => {
      this.imageOrVideoOptionAlert = null;
    });
    this.imageOrVideoOptionAlert.present();
  }

  showCameraWithImageOption() {
    CameraHelper.getInstance().captureImage(true, (_mediaFile: MediaFile, _reSizedImageFullPath: string) => {
      if (AppUIHelper.getInstance().isMediaFileSizeLessThanLimit(_mediaFile, this.FILESIZE20MB)) {
        const linkMediaDataModel: LinkMediaDataModel = new LinkMediaDataModel();
        const hreftoUse: string = _reSizedImageFullPath != null ? _reSizedImageFullPath : _mediaFile.fullPath;
        linkMediaDataModel.href = hreftoUse;
        linkMediaDataModel.fromUIhref = hreftoUse;
        linkMediaDataModel.fileTransferCategory = FileTransferCategory.IMAGE;
        this.addThisLinkMediaDataModelToGenericListItemViewModel
        (linkMediaDataModel);
      } else {
        this.showFileSizeLimitExceededAlert();
      }
    }, (_errorText: string) => {
    });
  }

  showCameraWithVideoOption() {
     CameraHelper.getInstance().captureVideo((_mediaFile: MediaFile) => {
       if (AppUIHelper.getInstance().isMediaFileSizeLessThanLimit(_mediaFile, this.FILESIZE20MB)) {
        CameraHelper.getInstance().createThumbnailForAVideoFile(_mediaFile, 500, 500,
          (_thumbNailURL: string) => {
            const linkMediaDataModel: LinkMediaDataModel = new LinkMediaDataModel();
            linkMediaDataModel.href = _mediaFile.fullPath;
            linkMediaDataModel.fromUIhref = _thumbNailURL;
            linkMediaDataModel.fileTransferCategory = FileTransferCategory.VIDEO;
            this.addThisLinkMediaDataModelToGenericListItemViewModel
            (linkMediaDataModel);
          }, (_errorText: string) => {
            AppUIHelper.getInstance().showAlert(
              AppUIHelper.getInstance().i18nText('error'),
              _errorText,
              [AppUIHelper.getInstance().i18nText('ok')]);
          });
       } else {
        this.showFileSizeLimitExceededAlert();
      }
    }, (_errorText: string) => {
    });
  }

  showFileSizeLimitExceededAlert() {
    this.fileSizeLimitExceededAlert = this.alertCtrl.create({
      title: AppUIHelper.getInstance().i18nText('filesizeexceededmsg'),
      buttons: [
        {
          text: AppUIHelper.getInstance().i18nText('yes'),
          handler: _data => {
            this.showImageOrVideoOptionAlert();
          },
        },
        {
          text: AppUIHelper.getInstance().i18nText('no'),
          handler: _data => {
          },
        },
        ],
      });

    this.fileSizeLimitExceededAlert.onDidDismiss(() => {
      this.fileSizeLimitExceededAlert = null;
    });
    this.fileSizeLimitExceededAlert.present();
  }

  private addThisLinkMediaDataModelToGenericListItemViewModel
  (linkMediaDataModel: LinkMediaDataModel) {
    this.addProofGenericListItemViewModel().horizontalListingOfMediaThumbNailsURLs.
    linkMediaDataModels.push(linkMediaDataModel);

    this.someUIElementsHaveChangedItsState_ChangeStateOfOtherUIElementsAccordingly(true);
    this.someUIElementsHaveChangedItsState_PrintEmailUIElementsAccordingly();
  }

  private prepare_GenericListItemViewModels() {
    this.genericListItemViewModels = [];
    this.genericListItemViewModelsForTabletDevices = [];

    if (PlatformHelper.isDevice_A_Tablet()) {
      this.isDeviceATablet = true;
      this.isDeviceInLandScapeOrientation = true;
    }

    let dataArray = [[GenericListItemViewModelType.HorizontalListing_Of_Media_ThumbNails
                     , this.LISTITEMTAGHORIZONTALIMAGES
                     , 'assets/images/camerawhite.svg'
                     , AppUIHelper.getInstance().i18nText('addproof')
                     , AppUIHelper.getInstance().i18nText('photosandvideos')
                     , this.LISTMARGINBOTTOM * 4
                     , this.LISTBORDERRADIUS
                     , false
                     , '#c03'],
                     [GenericListItemViewModelType.Icon_MainText_SubText
                      , this.LISTITEMTAGSHIPMENTDETAILS
                      , 'assets/images/shipmentdetails.svg'
                      , AppUIHelper.getInstance().i18nText('shipmentdetails')
                      , AppUIHelper.getInstance().i18nText('shipmentdetailssubtext')
                      , this.LISTMARGINBOTTOM
                      , this.LISTBORDERRADIUS
                      , true
                      , true
                      , ''
                      , false],
                    [GenericListItemViewModelType.Icon_MainText_SubText
                      , this.LISTITEMTAGPACKINGDETAILS
                      , 'assets/images/packingdetails.svg'
                      , AppUIHelper.getInstance().i18nText('packingdetails')
                      , AppUIHelper.getInstance().i18nText('packingdetailssubtext')
                      , this.LISTMARGINBOTTOM
                      , this.LISTBORDERRADIUS
                      , true
                      , true
                      , ''
                      , false],
                    [GenericListItemViewModelType.Icon_MainText_SubText
                      , this.LISTITEMTAGDAMAGEREPORT
                      , 'assets/images/damagereport.svg'
                      , AppUIHelper.getInstance().i18nText('damagedetails')
                      , AppUIHelper.getInstance().i18nText('damagereportsubtext')
                      , this.LISTMARGINBOTTOM
                      , this.LISTBORDERRADIUS
                      , true
                      , true
                      , ''
                      , false],
                    [GenericListItemViewModelType.Icon_MainText_SubText
                      , this.LISTITEMTAGDAMAGEDISCOVERY
                      , 'assets/images/damagediscovery.svg'
                      , AppUIHelper.getInstance().i18nText('damagedetection')
                      , AppUIHelper.getInstance().i18nText('damagediscoverysubtext')
                      , this.LISTMARGINBOTTOM
                      , this.LISTBORDERRADIUS
                      , true
                      , true
                      , ''
                      , true],
                    [GenericListItemViewModelType.Icon_MainText_SubText
                      , this.LISTITEMTAGRECUPERATION
                      , 'assets/images/recuperation.svg'
                      , AppUIHelper.getInstance().i18nText('recuperation')
                      , AppUIHelper.getInstance().i18nText('recuperationsubtext')
                      , this.LISTMARGINBOTTOM
                      , this.LISTBORDERRADIUS
                      , true
                      , true
                      , ''
                      , false],
                    [GenericListItemViewModelType.Icon_MainText_SubText
                      , this.LISTITEMTAGREMARKSANDBOXES
                      , 'assets/images/remarks.svg'
                      , AppUIHelper.getInstance().i18nText('remarksandboxes')
                      , AppUIHelper.getInstance().i18nText('remarksandboxessubtext')
                      , this.LISTMARGINBOTTOM
                      , this.LISTBORDERRADIUS
                      , false
                      , true
                      , ''
                      , true],
    ];

    for (let i = 0; i < dataArray.length ; i++) {
      let array = dataArray[i];

      let genericListItemViewModel: GenericListItemViewModel =
      new GenericListItemViewModel(0, array[1] as string, array[0] as GenericListItemViewModelType);
      if (genericListItemViewModel.genericListItemViewModelType ===
          GenericListItemViewModelType.MainText) {
        genericListItemViewModel.mainText = array[2] as string;
      } else if (genericListItemViewModel.genericListItemViewModelType ===
        GenericListItemViewModelType.Icon_MainText_SubText ||
        genericListItemViewModel.genericListItemViewModelType ===
        GenericListItemViewModelType.HorizontalListing_Of_Media_ThumbNails) {
        genericListItemViewModel.icon = array[2] as string;
        genericListItemViewModel.mainText = array[3] as string;
        genericListItemViewModel.subText = array[4] as string;
        genericListItemViewModel.marginBottom = array[5] as number;
        genericListItemViewModel.borderRadius = array[6] as number;
        genericListItemViewModel.shouldShowMandatoryStar = array[7] as boolean;
        genericListItemViewModel.shouldShowRightArrow = array[8] as boolean;
        genericListItemViewModel.thumbnailBadgeImage = array[9] as string;
        genericListItemViewModel.textWrap = array[10] as boolean;
        if (genericListItemViewModel.genericListItemViewModelType ===
            GenericListItemViewModelType.HorizontalListing_Of_Media_ThumbNails) {
          genericListItemViewModel.backgroundColor = array[8] as string;
        }
      }

      if (this.isDeviceATablet && i > 0)
        this.genericListItemViewModelsForTabletDevices.push(genericListItemViewModel);
      else
        this.genericListItemViewModels.push(genericListItemViewModel);
    }

    if (this.damageReportsDataModel.images == null)
      this.damageReportsDataModel.images = new ImagesDataModel();
    this.addProofGenericListItemViewModel().horizontalListingOfMediaThumbNailsURLs =
    this.damageReportsDataModel.images;

    this.someUIElementsHaveChangedItsState_ChangeStateOfOtherUIElementsAccordingly(false);
  }

  private fetch_FetchAWBDamageQualifiersDataObject_IfRequired() {
    let proceedFurther = (() => {
      this.prepare_GenericListItemViewModels();
      this.workOn_IsDeviceInLandScapeOrientationFlag();
    });

    if (PreFetchDataObjectsHelper.getInstance().doWeHave_Valid_Data_For_FetchAWBDamageQualifiersDataObject()) {
      proceedFurther();
    } else {
      PreFetchDataObjectsHelper.getInstance().preFetch_FetchAWBDamageQualifiersDataObject(
        (_dataObject: BaseDataObject) => {
          if (_dataObject.serverErrorDataModel != null) {
            AppUIHelper.getInstance().showAlert(
              AppUIHelper.getInstance().i18nText('error'),
              _dataObject.serverErrorDataModel.errorText,
            [
              {
              text: AppUIHelper.getInstance().i18nText('retry'),
              handler: () => {
                this.fetch_FetchAWBDamageQualifiersDataObject_IfRequired();
              },
            },
            ]);
          } else {
            proceedFurther();
          }
      });
    }
  }

  private addProofGenericListItemViewModel(): GenericListItemViewModel {
    return this.genericListItemViewModels[0];
  }

  private onPrintButtonClicked() {
  }

  private onEmailButtonClicked() {
    if (this.shouldClickEmailButton) {
      this.setUnsetClickOnEmailButton(false);
      if (this.damageReportsDataModel.images.linkMediaDataModels.length === 0) {
      this.onTriggerAWBEmailService();
      }else {
      this.navController.push(DamageReportEmailPage,
        {'damageReportsDataModel': this.damageReportsDataModel});
      this.setUnsetClickOnEmailButton(true);
     }
    }
  }

  private setUnsetClickOnEmailButton(shouldClick: boolean) {
    this.shouldClickEmailButton = shouldClick;
  }

  private someUIElementsHaveChangedItsState_ChangeStateOfOtherUIElementsAccordingly
  (_isThisCalledDueToARealChange: boolean) {
    let _genericListItemViewModelByTag: GenericListItemViewModel =
    this.genericListItemViewModelByTag(this.LISTITEMTAGSHIPMENTDETAILS);
    if (_genericListItemViewModelByTag != null) {
      if (this.damageReportsDataModel.doWeHaveAllOfShipmentDetailsMandatoryDataLeftToBeFilledIn()) {
        _genericListItemViewModelByTag.thumbnailBadgeImage = null;
        _genericListItemViewModelByTag.subText =
        AppUIHelper.getInstance().i18nText('shipmentdetailssubtext');
      } else if (this.damageReportsDataModel.doWeHaveAnyShipmentDetailsMandatoryDataLeftToBeFilledIn()) {
        _genericListItemViewModelByTag.thumbnailBadgeImage = 'assets/images/pendingdot.svg';
        _genericListItemViewModelByTag.subText =
        this.damageReportsDataModel.textOfShipmentDetailsMandatoryDataThatIsFilledIn();
      } else {
        _genericListItemViewModelByTag.thumbnailBadgeImage =
        (this.damageReportsDataModel.doWeHaveAnyShipmentDetailsLogicalDataErrors() ?
         'assets/images/error_icon.svg' : 'assets/images/completed.svg');
        _genericListItemViewModelByTag.subText =
        this.damageReportsDataModel.textOfShipmentDetailsMandatoryDataThatIsFilledIn();
      }
    }

    _genericListItemViewModelByTag =
    this.genericListItemViewModelByTag(this.LISTITEMTAGPACKINGDETAILS);
    if (_genericListItemViewModelByTag != null) {
      if (this.damageReportsDataModel.doWeHaveAllOfPackingDetailsMandatoryDataLeftToBeFilledIn()) {
        _genericListItemViewModelByTag.thumbnailBadgeImage = null;
        _genericListItemViewModelByTag.subText =
        AppUIHelper.getInstance().i18nText('packingdetailssubtext');
      } else {
        _genericListItemViewModelByTag.thumbnailBadgeImage =
        (this.damageReportsDataModel.doWeHaveAnyPackingDetailsMandatoryDataLeftToBeFilledIn() ?
        'assets/images/pendingdot.svg' : 'assets/images/completed.svg');
        _genericListItemViewModelByTag.subText =
        this.damageReportsDataModel.textOfPackingDetailsMandatoryDataThatIsFilledIn();
      }
    }

    _genericListItemViewModelByTag =
    this.genericListItemViewModelByTag(this.LISTITEMTAGDAMAGEREPORT);
    if (_genericListItemViewModelByTag != null) {
      if (this.damageReportsDataModel.doWeHaveAllOfDamageDetailsMandatoryDataLeftToBeFilledIn()) {
        _genericListItemViewModelByTag.thumbnailBadgeImage = null;
        _genericListItemViewModelByTag.subText =
        AppUIHelper.getInstance().i18nText('damagereportsubtext');
      } else {
        _genericListItemViewModelByTag.thumbnailBadgeImage =
        (this.damageReportsDataModel.doWeHaveAnyDamageDetailsMandatoryDataLeftToBeFilledIn() ?
        'assets/images/pendingdot.svg' : 'assets/images/completed.svg');
        _genericListItemViewModelByTag.subText =
        this.damageReportsDataModel.textOfDamageDetailsMandatoryDataThatIsFilledIn();
      }
    }

    _genericListItemViewModelByTag =
    this.genericListItemViewModelByTag(this.LISTITEMTAGDAMAGEDISCOVERY);
    if (_genericListItemViewModelByTag != null) {
      if (this.damageReportsDataModel.doWeHaveAllOfDamageDetectionMandatoryDataLeftToBeFilledIn()) {
        _genericListItemViewModelByTag.thumbnailBadgeImage = null;
        _genericListItemViewModelByTag.subText =
        AppUIHelper.getInstance().i18nText('damagediscoverysubtext');
      } else {
        _genericListItemViewModelByTag.thumbnailBadgeImage =
        (this.damageReportsDataModel.doWeHaveAnyDamageDetectionMandatoryDataLeftToBeFilledIn() ?
        'assets/images/pendingdot.svg' : 'assets/images/completed.svg');
        _genericListItemViewModelByTag.subText =
        this.damageReportsDataModel.textOfDamageDetectionMandatoryDataThatIsFilledIn();
      }
    }

    _genericListItemViewModelByTag =
    this.genericListItemViewModelByTag(this.LISTITEMTAGRECUPERATION);
    if (_genericListItemViewModelByTag != null) {
      if (this.damageReportsDataModel.doWeHaveAllOfRecuperationMandatoryDataLeftToBeFilledIn()) {
        _genericListItemViewModelByTag.thumbnailBadgeImage = null;
        _genericListItemViewModelByTag.subText =
        AppUIHelper.getInstance().i18nText('recuperationsubtext');
      } else {
        _genericListItemViewModelByTag.thumbnailBadgeImage =
        (this.damageReportsDataModel.doWeHaveAnyRecuperationMandatoryDataLeftToBeFilledIn() ?
        'assets/images/pendingdot.svg' : 'assets/images/completed.svg');
        _genericListItemViewModelByTag.subText =
        this.damageReportsDataModel.textOfRecuperationMandatoryDataThatIsFilledIn();
      }
    }

    if (!this.wasDamageReportsDataModelNewlyCreated && !_isThisCalledDueToARealChange) {
    } else {
      this.toShowEnabledSaveButton =
      !this.damageReportsDataModel.doWeHaveAnyMandatoryDataLeftToBeFilledIn();
    }

    this.disableSaveButtonForOnceForExistingDamageReportsDataModel();
  }

  private someUIElementsHaveChangedItsState_PrintEmailUIElementsAccordingly() {
    if (this.toShowEmailAndPrintOptions) {
      this.toShowEmailAndPrintOptions = false;
      this.wasDamageReportsDataModelNewlyCreated = false;
    }
  }

  private  onTriggerAWBEmailService() {
    let awbEmailDataModel = new AWBEmailDataModel();
    let awbEmailREquestObject: AWBEmailDataObject =
      ManualDIHelper.getInstance().createObject('AWBEmailDataObject');

    awbEmailREquestObject.prepareRequestWithParameters(this.damageReportsDataModel.reportId, awbEmailDataModel);

    awbEmailREquestObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
      this.setUnsetClickOnEmailButton(true);
      if (_dataObject.serverErrorDataModel != null) {
        if (_dataObject.serverErrorDataModel.errorText === 'success') {
          AppMessagePopupHelper.getInstance().showAppMessagePopup(
            new AppMessagePopupViewModel(
              AppUIHelper.getInstance().i18nText('Email Sent Successfully'),
              'assets/images/green-tick-small.svg', AppMessagePopupViewModelType.MessagePopup), 4);
        } else {
          AppUIHelper.getInstance().showAlert(
            AppUIHelper.getInstance().i18nText('error'),
            _dataObject.serverErrorDataModel.errorText,
            [AppUIHelper.getInstance().i18nText('ok')]);
        }
      }
    });
  }

  private workOn_IsDeviceInLandScapeOrientationFlag() {
    if (this.isDeviceATablet) {
      this.isDeviceInLandScapeOrientation =
      ScreenOrientationHelper.currentScreenOrientation() ===
      ScreenOrientationType.LANDSCAPE;

      const addGenericListItemViewModels = (() => {
        if (this.genericListItemViewModels.length === 1) {
          for (let i = 0; i < this.genericListItemViewModelsForTabletDevices.length; i++)
            this.genericListItemViewModels.push
            (this.genericListItemViewModelsForTabletDevices[i]);
        }
      });

      const removeGenericListItemViewModels = (() => {
        if (this.genericListItemViewModels.length > 1) {
          this.genericListItemViewModels.splice(1, 6);
        }
      });

      const calc = (() => {
        if (document.getElementById('genericListItemShowingMenuComponent') != null) {
          this.genericListItemShowingMenuComponentRectDataModel.top =
          document.getElementById('genericListItemShowingMenuComponent').getBoundingClientRect().bottom -
          document.getElementById('navigationBarHeader').getBoundingClientRect().height -
          8;
          // '8' --> Above is hardcoded --> Need to figure out where this padding is coming in from
        }
      });

      const showListItemShownCurrentlyForTabletLandScapeMode = (() => {
        if (this.listItemShownCurrentlyForTabletLandScapeMode != null)
          this.tapEvent_On_GenericListItemComponent
          (this.genericListItemViewModelByTag(this.listItemShownCurrentlyForTabletLandScapeMode));
      });

      setTimeout(() => {
        this.someUIElementsHaveChangedItsState_ChangeStateOfOtherUIElementsAccordingly(false);
        if (this.isDeviceInLandScapeOrientation) {
          if (this.splitPaneMasterComponentNav != null) {
            this.splitPaneMasterComponentNav.setRoot
            (GenericListItemComponent,
              {'genericListItemViewModels':
                this.genericListItemViewModelsForTabletDevices,
               'tapEvent': (_genericListItemViewModel: GenericListItemViewModel) => {
                 this.tapEvent_On_GenericListItemComponent(_genericListItemViewModel);
               },
               'toRenderUnderScroll': true});

            addGenericListItemViewModels();
            showListItemShownCurrentlyForTabletLandScapeMode();
            removeGenericListItemViewModels();

            setTimeout(() => {
              calc();
            }, 100); // 300
          }
        } else {
          addGenericListItemViewModels();
        }
      }, 200);
    }
  }

  private genericListItemViewModelByTag(_tag: string): GenericListItemViewModel {
    let genericListItemViewModelByTag: GenericListItemViewModel = null;
    for (let _genericListItemViewModelByTag of this.genericListItemViewModels)
      if (_genericListItemViewModelByTag.tag === _tag) {
        genericListItemViewModelByTag = _genericListItemViewModelByTag;
        break;
      }
    if (this.genericListItemViewModelsForTabletDevices != null && genericListItemViewModelByTag == null) {
      for (let _genericListItemViewModelByTag of this.genericListItemViewModelsForTabletDevices)
        if (_genericListItemViewModelByTag.tag === _tag) {
          genericListItemViewModelByTag = _genericListItemViewModelByTag;
          break;
      }
    }
    return genericListItemViewModelByTag;
  }

  private disableSaveButtonForOnceForExistingDamageReportsDataModel() {
    if (this.isIonViewDidEnterEverBeenCalledBefore) {
      if (this.dateWhenIonViewDidEnterFirst != null &&
          DateParserHelper.difference_Between_TwoDates(new Date(), this.dateWhenIonViewDidEnterFirst) <= 2) {
        if (!this.wasDamageReportsDataModelNewlyCreated) {
          this.toShowEnabledSaveButton = false;
        }
      }
    }
  }

  /* Related to MOB-224, MOB-258 in JIRA
    Q) Why this function ?
    A) In Tablet landscape mode, there is a common footer
    for all pages from Shipment details to Remarks and  boxes. When any of the inputs is focused in any of these pages,
    we need to hide the footer. This function is called from the individual pages.
  */
  private showOrHideFooter(_toShowFooter: boolean) {
    if (PlatformHelper.isDevice_A_Tablet()) {
      this.toShowFooter = _toShowFooter;
    }
  }
}
