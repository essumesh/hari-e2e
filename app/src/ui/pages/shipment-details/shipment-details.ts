import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import filter from 'lodash-es/filter';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { PackagingDetailPage } from '../packaging-details/packaging-details';
import { DamageReportsDataModel } from '../../../datamodels/DamageReportsDataModel';
import { AmountUnitDataModel } from '../../../datamodels/AmountUnitDataModel';
import { TransportMeansDataModel } from '../../../datamodels/TransportMeansDataModel';
import { CarriersDataModel } from '../../../datamodels/CarriersDataModel';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { DatePickerHelper } from '../../../helpers/DatePickerHelper';
import { DateParserHelper } from '../../../helpers/DateParserHelper';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { PlatformHelper } from '../../../helpers/PlatformHelper';
import { ScreenOrientationHelper, ScreenOrientationType } from '../../../helpers/ScreenOrientationHelper';
import { KeyboardHelper } from '../../../helpers/KeyboardHelper';

@Component({
  selector: 'champ-page-shipment-details',
  templateUrl: 'shipment-details.html',
})
export class ShipmentDetailsPage extends BaseUINavigationBarView {
  private damageReportsDataModel: DamageReportsDataModel;
  private isDeviceATablet: boolean = false;
  private isDeviceInLandScapeOrientation: boolean = false;
  private flightDateText: string;
  private readonly DEFAULT_WEIGHT_KG = 'KG';
  private piecesLogicalError: string = null;
  private toshowFooter: boolean = true;

  constructor(public navController: NavController,
              public navParams: NavParams,
              private viewCtrl: ViewController,
              public changeDetectorRef: ChangeDetectorRef) {
    super();

    this.damageReportsDataModel =
    this.navParams.get('damageReportsDataModel');
    this.navigationBarViewModel =
    new NavigationBarViewModel(true,
                               AppUIHelper.getInstance().i18nText('shipmentdetails'),
                               true,
                               true,
                               true);
    if (PlatformHelper.isDevice_A_Tablet() &&
        ScreenOrientationHelper.currentScreenOrientation() === ScreenOrientationType.LANDSCAPE) {
      this.isDeviceATablet = true;
      this.isDeviceInLandScapeOrientation = true;
    }

    this.prepareNecessaries();
    this.checkFor_PiecesLogicalError();
  }

  ionViewDidLoad() {
    super.ionViewDidLoad();

    this.flightDateText = DateParserHelper.parseDateToString_InFormat_ddMMMyy
    (this.damageReportsDataModel.transportMeans.date);
  }

  ionViewDidEnter() {
    super.ionViewDidEnter();

    this.subscribeToKeyboardShowHideEvents();
  }

  ionViewDidLeave() {
    super.ionViewDidLeave();

    this.unsubscribeToKeyboardShowHideEvents();
  }

  onOrientationChangedTo_Landscape() {
    super.onOrientationChangedTo_Landscape();

    try {
        if (PlatformHelper.isDevice_A_Tablet() &&
            this.navController.canGoBack())
              this.navController.pop();
    } catch (e) {
    }
  }

  onKeyboardShowHideEvent(_keyboard: any) {
    super.onKeyboardShowHideEvent(_keyboard);

    if (_keyboard.showing) {
      this.toshowFooter = false;
    } else {
      this.toshowFooter = true;
    }
    this.doACallTo_OnInputFocusedInAnyOfTheChilds();
  }

  nextpage() {
    this.navController.push(PackagingDetailPage,
       {'damageReportsDataModel': this.damageReportsDataModel,
        'onSaveClickedOnOneOfTheChilds': this.navParams.get('onSaveClickedOnOneOfTheChilds'),
        'someUIElementsHaveChangedItsStateOnOneOfTheChilds':
        this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds'),
        'doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds':
        this.navParams.get('doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds'),
        'doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds':
        this.navParams.get('doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds'),
        'onInputFocusedInAnyOfTheChilds': this.navParams.get('onInputFocusedInAnyOfTheChilds'),
        'onPrintClickedOnOneOfTheChilds': this.navParams.get('onPrintClickedOnOneOfTheChilds'),
        'onEmailClickedOnOneOfTheChilds': this.navParams.get('onEmailClickedOnOneOfTheChilds')}).then(() => {
      this.viewCtrl.dismiss();
    });
  }

  showDatePicker() {
    DatePickerHelper.getInstance().showDatePicker((date: Date) => {
      this.damageReportsDataModel.transportMeans.date = date;
      this.flightDateText = DateParserHelper.parseDateToString_InFormat_ddMMMyy
      (this.damageReportsDataModel.transportMeans.date);
      this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds')();
    }, (_errorText: string) => {
    });
  }

  onPiecesTextChanged() {
    this.changeDetectorRef.detectChanges();

    this.checkFor_PiecesLogicalError();

    this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds')();
  }

  onWeightTextChanged() {
    this.changeDetectorRef.detectChanges();
    this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds')();
  }

  onUldTextChanged() {
    this.changeDetectorRef.detectChanges();
    this.damageReportsDataModel.uld = this.damageReportsDataModel.uld.replace(/[^a-z0-9]+/gi, '');
    this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds')();
  }

  onFlightCodeTextChanged() {
    this.changeDetectorRef.detectChanges();
    this.damageReportsDataModel.transportMeans.carriersDataModel.code =
    this.damageReportsDataModel.transportMeans.carriersDataModel.code.replace(/[^a-z0-9]+/gi, '');
    this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds')();
  }

  onFlightNumberTextChanged() {
    this.changeDetectorRef.detectChanges();
    this.damageReportsDataModel.transportMeans.transportNumber =
    this.damageReportsDataModel.transportMeans.transportNumber.replace(/[^a-z0-9]+/gi, '');
    this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds')();
  }

  private swiped (event: any) {
    if (event.direction === 2) {
      this.nextpage();
    } else if (event.direction === 4) {
      this.viewCtrl.dismiss();
    }
  }

  private prepareNecessaries() {
    if (this.damageReportsDataModel.weight == null) {
      this.damageReportsDataModel.weight = new AmountUnitDataModel();
      this.damageReportsDataModel.weight.unit = this.DEFAULT_WEIGHT_KG;
    }
    if (this.damageReportsDataModel.transportMeans == null)
      this.damageReportsDataModel.transportMeans = new TransportMeansDataModel();
    if (this.damageReportsDataModel.transportMeans.date == null)
      this.damageReportsDataModel.transportMeans.date = new Date();
    if (this.damageReportsDataModel.transportMeans.carriersDataModel == null)
      this.damageReportsDataModel.transportMeans.carriersDataModel =
      new CarriersDataModel();
  }

  private contentTitle(): string {
    if (this.damageReportsDataModel.airwaybill != null) {
      return AppUIHelper.getInstance().i18nText('damagereportfor')
             + ' '
             + this.damageReportsDataModel.airwaybill.airwaybillPrefix
             + '-'
             + this.damageReportsDataModel.airwaybill.airwaybillSerial;
    } else if (this.damageReportsDataModel.houseAirwaybill != null) {
            return AppUIHelper.getInstance().i18nText('damagereportfor')
                  + ' '
                  + this.damageReportsDataModel.houseAirwaybill.masterAirwaybill.airwaybillPrefix
                  + '-'
                  + this.damageReportsDataModel.houseAirwaybill.masterAirwaybill.airwaybillSerial;
    }
  }

  private checkFor_PiecesLogicalError() {
    if (this.damageReportsDataModel.doWeHaveAnyShipmentDetailsLogicalDataErrors_Pieces())
      this.piecesLogicalError = AppUIHelper.getInstance().i18nText('piecesuivalidation');
    else this.piecesLogicalError = null;
  }

  /*
    PATCH :: Related to MOB-224, MOB-258 in JIRA
    Q) Why this function ? Why this Patch ?
    A) In iOS, when the keyboard is hidden and we focus on any input for the first time, the keyboard show callback
    doesnt come for the first time, and the 'toshowFooter' doesnt get set to false. Hence we are setting it to false
    when any input gets focused.
  */
  private ionInputFocused() {
    if (!KeyboardHelper.getInstance().isKeyboardShown) {
      this.toshowFooter = false;
      this.doACallTo_OnInputFocusedInAnyOfTheChilds();
    }
  }

  /* Related to MOB-224, MOB-258 in JIRA
     Q) Why this function?
     A) This is to call the show or hide footer function in
     create-damage-report screen on tablet in landscape mode based on keyboard position
  */
  private doACallTo_OnInputFocusedInAnyOfTheChilds() {
    if (PlatformHelper.isDevice_A_Tablet() &&
    ScreenOrientationHelper.currentScreenOrientation() === ScreenOrientationType.LANDSCAPE)
    this.navParams.get('onInputFocusedInAnyOfTheChilds')(this.toshowFooter);
  }
}
