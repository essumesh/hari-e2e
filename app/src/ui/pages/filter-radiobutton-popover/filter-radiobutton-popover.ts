import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';
import { FilterRadioButtonsPopoverViewModel } from '../../viewmodels/FilterRadioButtonsPopoverViewModel';
import { RadioButtonsViewModel, RadioButtonViewModel } from '../../viewmodels/RadioButtonsViewModel';
import { BaseUIPageView } from '../../baseuiviews/forpages/BaseUIPageView';

/**
 * Generated class for the SelectNotificationSectionPopoverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'champ-filter-radiobutton-popover',
  templateUrl: 'filter-radiobutton-popover.html',
})
export class FilterRadioButtonPopover extends BaseUIPageView {

  filterRadioButtonsPopoverViewModel: FilterRadioButtonsPopoverViewModel;

  private radioButtonViewModelTapEvent: (_radioButtonViewModel: RadioButtonViewModel) => void;

  constructor(public viewController: ViewController, public navParams: NavParams) {
    super();

    this.populateDataInFilterRadioButtonViewModel(
      this.navParams.get('title'),
      this.navParams.get('radioButtons'),
      this.navParams.get('buttonText')),
      this.radioButtonViewModelTapEvent = this.navParams.get('radioButtonViewModelTapEvent');
  }

  populateDataInFilterRadioButtonViewModel(title: string, radioButtons: RadioButtonsViewModel, buttonText: string) {
    this.filterRadioButtonsPopoverViewModel = new FilterRadioButtonsPopoverViewModel(title, radioButtons, buttonText);
  }

  buttonClicked() {
    this.viewController.dismiss();
  }

  tapEvent_On_RadioButton_ViewModel(radioButtonViewModel: RadioButtonViewModel) {
    if (this.radioButtonViewModelTapEvent != null) {
      if (this.filterRadioButtonsPopoverViewModel.buttonText === null) {
        this.viewController.dismiss();
      }
      this.radioButtonViewModelTapEvent(radioButtonViewModel);
    }
  }
}
