import { Component, Pipe, PipeTransform, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, Content } from 'ionic-angular';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { PickUpViewDataModel } from '../../../datamodels/PickUpDeliveryViewDataModel';
import { DeliveryConfirmationDataModel } from '../../../datamodels/DeliveryConfirmationDataModel';
import { PickupDeliveryReceiptDataObject } from '../../../network/dataobjects/PickupDeliveryReceiptDataObject';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { PickupDriverInfoDataObject } from '../../../network/dataobjects/PickupDriverInfoDataObject';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { AppMessagePopupHelper } from '../../../helpers/AppMessagePopupHelper';
import { AppMessagePopupViewModel, AppMessagePopupViewModelType } from '../../viewmodels/AppMessagePopupViewModel';
import { DeliveryTransferPage } from '../deliveries-and-transfers/deliveries-and-transfers';
import { PickupSignatureDataModel } from '../../../datamodels/PickupSignatureDataModel';
import { DigitalSignatureDataModel } from '../../../datamodels/DigitalSignatureDataModel';
import { PickupDriverInfoDataModel } from '../../../datamodels/PickupDriverInfoDataModel';

@Component({
  selector: 'champ-page-delivery-confirmation',
  templateUrl: 'delivery-confirmation.html',
})
export class DeliveryConfirmationPage extends BaseUINavigationBarView {
  @ViewChild(SignaturePad) public signaturePad: SignaturePad;
  @ViewChild(Content) public content: Content;
  @ViewChild('pickerinfocontent') public pickerinfocontent: any;
  deliveryConfirmationDataModel: DeliveryConfirmationDataModel;
  pickupSignatureDataModel: PickupSignatureDataModel;
  _receiptNumber: any;
  _totalPieces: any;
  driverIDText: any;
  driverNameText: any;
  driverPassText: any;
  driverLicenseText: any;
  signatureText: any;
  isDriverValid: any = 0;
  _headerText: any;
  isSummit: boolean = false;
  public signatureImage: string;
  /* tslint:disable:max-line-length*/
  public defaultImage: string = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAVQAAADICAYAAAC3QRk5AAAGWklEQVR4Xu3UsQ0AAAjDMPr/0zyR0RzQwULZOQIECBBIBJasGCFAgACBE1RPQIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQEFQ/QIAAgUhAUCNIMwQIEBBUP0CAAIFIQFAjSDMECBAQVD9AgACBSEBQI0gzBAgQeKu3AMmYkQkPAAAAAElFTkSuQmCC';
  /* tslint:enable:max-line-length*/

  public signaturePadOptions: Object = {
    'minWidth': 2,
    'canvasWidth': 340,
    'canvasHeight': 200,
  };

  constructor(private navParams: NavParams,
    private navCtrl: NavController,
    private viewCtrl: ViewController) {
    super();

    this.deliveryConfirmationDataModel = this.navParams.get('deliveryConfirmationDataModel');
    this._receiptNumber = this.navParams.get('receiptNumber');
    this._totalPieces = this.navParams.get('totalPieces');
    this._headerText = this.navParams.get('headerText');

    this.navigationBarViewModel =
      new NavigationBarViewModel(true,
        AppUIHelper.getInstance().i18nText('Confirmation'),
        true,
        true,
        true);

  }

  canvasResize() {
    let canvas = document.querySelector('canvas');
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.set('canvasWidth', canvas.offsetWidth);
    this.signaturePad.set('canvasHeight', canvas.offsetHeight);
  }

  drawComplete() {
    let signValid: boolean = false;
    this.signatureImage = this.signaturePad.toDataURL();
    if (this.signatureImage === this.defaultImage) {
      signValid = false;
    } else {
      signValid = true;
    }
    return signValid;
  }

  drawClear() {
    this.signaturePad.clear();
  }

  validateDriverInfo() {
    if (this.driverIDText == null)
      return;
    let pickupDriverInfoDataObject: PickupDriverInfoDataObject =
      ManualDIHelper.getInstance().createObject('PickupDriverInfoDataObject');

    pickupDriverInfoDataObject.prepareRequestWithParameters(this.driverIDText);

    pickupDriverInfoDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
      if (_dataObject.serverErrorDataModel != null) {
        _dataObject.serverErrorDataModel.errorText === 'success' ? this.isDriverValid = 1 : this.isDriverValid = 2;
      }
    });
  }
  onValidateInputs(_event: any) {
    // console.log(this.driverIDText);
    // if ((this.driverIDText == null || this.driverIDText === '') ||
    //   (this.driverNameText == null || this.driverNameText === '') ||
    //   (this.driverPassText == null || this.driverPassText === '') ||
    //   this.driverLicenseText == null || this.driverLicenseText === '') {
    //   this.isSummit = false;
    //   return;
    // } else {
    //   this.isSummit = true;
    // }
    // console.log(this.isSummit);
    // this.content.scrollTo(0, this.pickerinfocontent.nativeElement.offsetTop
    //   + event.getNativeElement().parentElement.parentElement.parentElement.parentElement.parentElement.offsetTop,
    //   1000);
  }
  onSubmitDeliveryReceipt() {
    if (this.driverIDText == null || this.driverIDText === '')
      this.showErrorMessage('Enter Driver ID');
    else if (this.driverNameText == null || this.driverNameText === '')
      this.showErrorMessage('Enter Driver Name');
    else if (this.driverPassText == null || this.driverPassText === '')
      this.showErrorMessage('Enter Driver Pass Number');
    else if (this.driverLicenseText == null || this.driverLicenseText === '')
      this.showErrorMessage('Enter Driver License Number');
    else if (!this.drawComplete())
      this.showErrorMessage('Please Sign');
    else {
      this.prepareRequestParameters();
      let pickupDeliveryReceiptDataObject: PickupDeliveryReceiptDataObject =
        ManualDIHelper.getInstance().createObject('PickupDeliveryReceiptDataObject');

      pickupDeliveryReceiptDataObject.prepareRequestWithParameters(this._receiptNumber, this.pickupSignatureDataModel);

      pickupDeliveryReceiptDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
        if (_dataObject.serverErrorDataModel != null) {
          if (_dataObject.serverErrorDataModel.errorText === 'success') {
            console.log(this.viewCtrl);
            this.timeout();
            AppMessagePopupHelper.getInstance().showAppMessagePopup(
              new AppMessagePopupViewModel(
                AppUIHelper.getInstance().i18nText('savedsuccessfully'),
                'assets/images/green-tick-small.svg', AppMessagePopupViewModelType.MessagePopup), 4);
          } else {
            AppUIHelper.getInstance().showAlert(
              AppUIHelper.getInstance().i18nText('error'),
              _dataObject.serverErrorDataModel.errorText,
              [AppUIHelper.getInstance().i18nText('ok')]);
          }
        }
      });
    }
  }

  prepareRequestParameters() {
    this.pickupSignatureDataModel = new PickupSignatureDataModel();
    let digitalSignature = new DigitalSignatureDataModel();
    digitalSignature.fileName = 'signature.png';
    digitalSignature.fileContent = this.signatureImage;
    digitalSignature.fileType = 'image/png';

    let pickupDriverInfo = new PickupDriverInfoDataModel();
    pickupDriverInfo.personID = this.driverIDText;
    pickupDriverInfo.pickupDrivingLicenseNumber = this.driverLicenseText;
    pickupDriverInfo.pickupVehicleLicensePlate = '';
    pickupDriverInfo.pickupAirportPassNumber = this.driverPassText;
    pickupDriverInfo.pickupDriver = this.driverNameText;
    pickupDriverInfo.digitalSignature = digitalSignature;
    this.pickupSignatureDataModel.pickupDriverInfo = pickupDriverInfo;
    console.log(this.pickupSignatureDataModel);
  }
  onCancelClicked() {
    this.drawClear();
    this.driverIDText = '';
    this.driverNameText = '';
    this.driverLicenseText = '';
    this.driverPassText = '';
    this.isDriverValid = 0;
    this.leaveComponent();
  }
  showErrorMessage(error: string) {
    AppUIHelper.getInstance().showAlert(
      AppUIHelper.getInstance().i18nText('error'), error,
      [AppUIHelper.getInstance().i18nText('ok')]);
  }
  timeout() {
    setTimeout(() => {
      this.leaveComponent();
    }, 4 * 1000);
  }

  leaveComponent() {
    this.navCtrl.setRoot(DeliveryTransferPage, {}, { animate: true });
  }
}
