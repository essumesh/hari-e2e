import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { DamageReportPage } from '../damage-report/damage-report';
import { ShipmentDetailsPage } from '../shipment-details/shipment-details';
import { PreFetchDataObjectsHelper } from '../../../network/helpers/PreFetchDataObjectsHelper';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { FetchAWBDamageQualifiersResponseObject } from
'../../../network/responseobjects/FetchAWBDamageQualifiersResponseObject';
import { LookupDataModel } from '../../../datamodels/LookupDataModel';
import { DamageReferencesDataModel } from '../../../datamodels/DamageReferencesDataModel';
import { FetchAWBDamageQualifiersDataObject } from '../../../network/dataobjects/FetchAWBDamageQualifiersDataObject';
import { DamageReportsDataModel } from '../../../datamodels/DamageReportsDataModel';
import { LookupsDataModel } from '../../../datamodels/LookupsDataModel';
import { RadioButtonsViewModel, RadioButtonViewModel } from '../../viewmodels/RadioButtonsViewModel';
import { CheckBoxesViewModel, CheckBoxViewModel } from '../../viewmodels/CheckBoxesViewModel';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { PlatformHelper } from '../../../helpers/PlatformHelper';
import { ScreenOrientationHelper, ScreenOrientationType } from '../../../helpers/ScreenOrientationHelper';
import { KeyboardHelper } from '../../../helpers/KeyboardHelper';

@Component({
  selector: 'champ-page-package-details',
  templateUrl: 'packaging-details.html',

})
export class PackagingDetailPage extends BaseUINavigationBarView {
    fetchAWBDamageQualifiersResponseObject: FetchAWBDamageQualifiersResponseObject =
    PreFetchDataObjectsHelper.getInstance().fetchAWBDamageQualifiersDataObject.responseObject  as
    FetchAWBDamageQualifiersResponseObject;
    private outerPackagingMaterialCheckBoxesViewModel: CheckBoxesViewModel;
    private outerPackagingTypeRadioButtonsViewModel: RadioButtonsViewModel;
    private innerPackagingCheckBoxesViewModel: CheckBoxesViewModel;
    private damageReportsDataModel: DamageReportsDataModel;
    private checkBoxViewModelOtherOfouterPackagingMaterial: CheckBoxViewModel;
    private radioButtonViewModelOtherOfOuterPackagingType: RadioButtonViewModel;
    private checkBoxViewModelOtherOfInnerPackaging: CheckBoxViewModel;
    private isDeviceATablet: boolean = false;
    private isDeviceInLandScapeOrientation: boolean = false;
    private toshowFooter: boolean = true;

    constructor(private navController: NavController,
                private navParams: NavParams,
                private viewCtrl: ViewController) {
        super();

        this.damageReportsDataModel =
        this.navParams.get('damageReportsDataModel');
        this.navigationBarViewModel =
        new NavigationBarViewModel(true,
                               AppUIHelper.getInstance().i18nText('packingdetails'),
                               true,
                               true,
                               true);
        if (PlatformHelper.isDevice_A_Tablet() &&
            ScreenOrientationHelper.currentScreenOrientation() === ScreenOrientationType.LANDSCAPE) {
            this.isDeviceATablet = true;
            this.isDeviceInLandScapeOrientation = true;
        }

        if (this.damageReportsDataModel.outerPackagingMaterial.length === 0) {
            this.damageReportsDataModel.outerPackagingMaterial.push(new LookupsDataModel());
        }
        if (this.damageReportsDataModel.outerPackagingType.length === 0) {
            this.damageReportsDataModel.outerPackagingType.push(new LookupsDataModel());
        }
        if (this.damageReportsDataModel.innerPackaging.length === 0) {
            this.damageReportsDataModel.innerPackaging.push(new LookupsDataModel());
        }

        this.prepareOuterPackagingMaterialCheckBoxesViewModel();
        this.prepareOuterPackagingTypeRadioButtonsViewModel();
        this.prepareInnerPackagingCheckBoxesViewModel();
    }

    ionViewDidEnter() {
        super.ionViewDidEnter();

        this.subscribeToKeyboardShowHideEvents();
      }

    ionViewDidLeave() {
        super.ionViewDidLeave();

        this.unsubscribeToKeyboardShowHideEvents();
    }

    onOrientationChangedTo_Landscape() {
        super.onOrientationChangedTo_Landscape();

        try {
            if (PlatformHelper.isDevice_A_Tablet() &&
                this.navController.canGoBack())
                  this.navController.pop();
        } catch (e) {
        }
    }

    onKeyboardShowHideEvent(_keyboard: any) {
        super.onKeyboardShowHideEvent(_keyboard);

        if (_keyboard.showing) {
          this.toshowFooter = false;
        } else {
          this.toshowFooter = true;
        }
        this.doACallTo_OnInputFocusedInAnyOfTheChilds();
    }

    private prepareOuterPackagingMaterialCheckBoxesViewModel() {
        this.outerPackagingMaterialCheckBoxesViewModel = new CheckBoxesViewModel();

        for (let lookupDataModel of
            this.fetchAWBDamageQualifiersResponseObject.damageReferencesDataModel.outerPackingMaterials.qualifierType) {
            this.outerPackagingMaterialCheckBoxesViewModel.addCheckBoxViewModel(
                lookupDataModel.name,
                this.damageReportsDataModel.outerPackagingMaterial[0].contains(lookupDataModel),
                lookupDataModel,
            );
            if (lookupDataModel.isCodeOfTypeOther()) {
                this.checkBoxViewModelOtherOfouterPackagingMaterial =
                this.outerPackagingMaterialCheckBoxesViewModel.checkBoxViewModels
                [this.outerPackagingMaterialCheckBoxesViewModel.checkBoxViewModels.length - 1];
            }
        }
    }

    private tapEvent_On_OuterPackagingMaterialCheckBoxesViewModel(_checkBoxViewModel: CheckBoxViewModel) {
        let _lookupDataModel: LookupDataModel = _checkBoxViewModel.extras as LookupDataModel;
        if (_checkBoxViewModel.isSelected) {
            this.damageReportsDataModel.outerPackagingMaterial[0].addIfNotPresent(_lookupDataModel);
        } else
            this.damageReportsDataModel.outerPackagingMaterial[0].removeIfPresent(_lookupDataModel);

        if (_lookupDataModel.isCodeOfTypeOther() && !_checkBoxViewModel.isSelected)
            this.damageReportsDataModel.outerPackagingMaterial[0].other = null;

        this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds')();
    }

    private prepareOuterPackagingTypeRadioButtonsViewModel() {
        this.outerPackagingTypeRadioButtonsViewModel = new RadioButtonsViewModel();

        for (let lookupDataModel of
            this.fetchAWBDamageQualifiersResponseObject.damageReferencesDataModel.outerPackingTypes.qualifierType) {
            this.outerPackagingTypeRadioButtonsViewModel.addRadioButtonViewModel(
                lookupDataModel.name,
                this.damageReportsDataModel.outerPackagingType[0].contains(lookupDataModel),
                lookupDataModel,
            );
            if (lookupDataModel.isCodeOfTypeOther()) {
                this.radioButtonViewModelOtherOfOuterPackagingType =
                this.outerPackagingTypeRadioButtonsViewModel.radioButtonViewModels
                [this.outerPackagingTypeRadioButtonsViewModel.radioButtonViewModels.length - 1];
            }
        }
    }

    private tapEvent_On_OuterPackagingTypeRadioButtonsViewModel(_radioButtonViewModel: RadioButtonViewModel) {
        this.damageReportsDataModel.outerPackagingType[0].clearQualifierType();
        if (_radioButtonViewModel.isSelected) {
            this.damageReportsDataModel.outerPackagingType[0].addIfNotPresent(
                _radioButtonViewModel.extras as LookupDataModel,
            );

            if (_radioButtonViewModel !== this.radioButtonViewModelOtherOfOuterPackagingType) {
                this.damageReportsDataModel.outerPackagingType[0].other = null;
            }
        }
        this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds')();
    }

    private prepareInnerPackagingCheckBoxesViewModel() {
        this.innerPackagingCheckBoxesViewModel = new CheckBoxesViewModel();

        for (let lookupDataModel of
            this.fetchAWBDamageQualifiersResponseObject.damageReferencesDataModel.innerPackings.qualifierType) {
            this.innerPackagingCheckBoxesViewModel.addCheckBoxViewModel(
                lookupDataModel.name,
                this.damageReportsDataModel.innerPackaging[0].contains(lookupDataModel),
                lookupDataModel,
            );
            if (lookupDataModel.isCodeOfTypeOther()) {
                this.checkBoxViewModelOtherOfInnerPackaging =
                this.innerPackagingCheckBoxesViewModel.checkBoxViewModels
                [this.innerPackagingCheckBoxesViewModel.checkBoxViewModels.length - 1];
            }
        }
    }

    private tapEvent_On_InnerPackagingCheckBoxesViewModel(_checkBoxViewModel: CheckBoxViewModel) {
        let _lookupDataModel: LookupDataModel = _checkBoxViewModel.extras as LookupDataModel;
        if (_checkBoxViewModel.isSelected) {
            this.damageReportsDataModel.innerPackaging[0].addIfNotPresent(_lookupDataModel);
        } else
            this.damageReportsDataModel.innerPackaging[0].removeIfPresent(_lookupDataModel);

        if (_lookupDataModel.isCodeOfTypeOther() && !_checkBoxViewModel.isSelected)
            this.damageReportsDataModel.innerPackaging[0].other = null;

        this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds')();
    }

    private onOtherFieldOfOuterPackagingMaterialChanged() {
        if (this.damageReportsDataModel.outerPackagingMaterial[0].other)
            this.checkBoxViewModelOtherOfouterPackagingMaterial.isSelected = true;
        else
            this.checkBoxViewModelOtherOfouterPackagingMaterial.isSelected = false;
        this.tapEvent_On_OuterPackagingMaterialCheckBoxesViewModel(this.checkBoxViewModelOtherOfouterPackagingMaterial);
    }

    private onOtherFieldOfOuterPackagingTypeChanged() {
        if (this.damageReportsDataModel.outerPackagingType[0].other)
            this.radioButtonViewModelOtherOfOuterPackagingType.isSelected = true;
        else
            this.radioButtonViewModelOtherOfOuterPackagingType.isSelected = false;
        this.outerPackagingTypeRadioButtonsViewModel
        .setSelectedRadioButtonViewModel(this.radioButtonViewModelOtherOfOuterPackagingType);
        this.tapEvent_On_OuterPackagingTypeRadioButtonsViewModel(this.radioButtonViewModelOtherOfOuterPackagingType);
    }

    private onOtherFieldOfInnerPackagingChanged() {
        if (this.damageReportsDataModel.innerPackaging[0].other)
            this.checkBoxViewModelOtherOfInnerPackaging.isSelected = true;
        else
            this.checkBoxViewModelOtherOfInnerPackaging.isSelected = false;
        this.tapEvent_On_InnerPackagingCheckBoxesViewModel(this.checkBoxViewModelOtherOfInnerPackaging);
    }

    private closePage() {
        this.navController.push(ShipmentDetailsPage,
            {'damageReportsDataModel': this.damageReportsDataModel,
             'onSaveClickedOnOneOfTheChilds': this.navParams.get('onSaveClickedOnOneOfTheChilds'),
             'someUIElementsHaveChangedItsStateOnOneOfTheChilds':
             this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds'),
             'doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds':
             this.navParams.get('doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds'),
             'doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds':
             this.navParams.get('doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds'),
             'onInputFocusedInAnyOfTheChilds': this.navParams.get('onInputFocusedInAnyOfTheChilds'),
             'onPrintClickedOnOneOfTheChilds': this.navParams.get('onPrintClickedOnOneOfTheChilds'),
             'onEmailClickedOnOneOfTheChilds': this.navParams.get('onEmailClickedOnOneOfTheChilds')}).then(() => {
        this.viewCtrl.dismiss();
        });
    }

    private nextPage() {
        this.navController.push(DamageReportPage,
            {'damageReportsDataModel': this.damageReportsDataModel,
             'onSaveClickedOnOneOfTheChilds': this.navParams.get('onSaveClickedOnOneOfTheChilds'),
             'someUIElementsHaveChangedItsStateOnOneOfTheChilds':
             this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds'),
             'doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds':
             this.navParams.get('doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds'),
             'doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds':
             this.navParams.get('doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds'),
             'onInputFocusedInAnyOfTheChilds': this.navParams.get('onInputFocusedInAnyOfTheChilds'),
             'onPrintClickedOnOneOfTheChilds': this.navParams.get('onPrintClickedOnOneOfTheChilds'),
             'onEmailClickedOnOneOfTheChilds': this.navParams.get('onEmailClickedOnOneOfTheChilds')}).then(() => {
        this.viewCtrl.dismiss();
        });
    }

    private swiped(event: any) {
        if (event.direction === 2) {
            this.nextPage();
        } else if (event.direction === 4) {
            this.closePage();
        }
    }

    private contentTitle(): string {
        if (this.damageReportsDataModel.airwaybill != null) {
          return AppUIHelper.getInstance().i18nText('damagereportfor')
                 + ' '
                 + this.damageReportsDataModel.airwaybill.airwaybillPrefix
                 + '-'
                 + this.damageReportsDataModel.airwaybill.airwaybillSerial;
        } else if (this.damageReportsDataModel.houseAirwaybill != null) {
                return AppUIHelper.getInstance().i18nText('damagereportfor')
                      + ' '
                      + this.damageReportsDataModel.houseAirwaybill.masterAirwaybill.airwaybillPrefix
                      + '-'
                      + this.damageReportsDataModel.houseAirwaybill.masterAirwaybill.airwaybillSerial;
        }
      }

    /*
        PATCH :: Related to MOB-224, MOB-258 in JIRA
        Q) Why this function ? Why this Patch ?
        A) In iOS, when the keyboard is hidden and we focus on any input for the first time, the keyboard show callback
        doesnt come for the first time, and the 'toshowFooter' doesnt get set to false. Hence we are setting it to false
        when any input gets focused.
    */
      private ionInputFocused() {
        if (!KeyboardHelper.getInstance().isKeyboardShown) {
          this.toshowFooter = false;
          this.doACallTo_OnInputFocusedInAnyOfTheChilds();
        }
      }

    /*  Related to MOB-224, MOB-258 in JIRA
        Q) Why this function?
        A) This is to call the show or hide footer function in
        create-damage-report screen on tablet in landscape mode based on keyboard position
    */
      private doACallTo_OnInputFocusedInAnyOfTheChilds() {
        if (PlatformHelper.isDevice_A_Tablet() &&
        ScreenOrientationHelper.currentScreenOrientation() === ScreenOrientationType.LANDSCAPE)
        this.navParams.get('onInputFocusedInAnyOfTheChilds')(this.toshowFooter);
      }
}
