import { Component, NgZone, ViewChild, TemplateRef } from '@angular/core';
import { NavController, NavParams, Searchbar } from 'ionic-angular';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { DatePickerHelper } from '../../../helpers/DatePickerHelper';
import { DateParserHelper } from '../../../helpers/DateParserHelper';
import { SelectOriginPage } from '../found-cargo-select-origin/found-cargo-select-origin';
import { SelectSHCPage } from '../found-cargo-select-shc/found-cargo-select-shc';
import { FoundCargoDataModel } from '../../../datamodels/FoundCargoDataModel';
import { FetchSHCResponseObject } from '../../../network/responseobjects/FetchSHCResponseObject';
import { AmountUnitDataModel } from '../../../datamodels/AmountUnitDataModel';
import { FoundCargoSHCDataObject } from '../../../network/dataobjects/FoundCargoSHCDataObject';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { FoundCargoPreferedLocationDataModel } from '../../../datamodels/FoundCargoPreferedLocationDataModel';
import { WarehouseLocationDataModel } from '../../../datamodels/WarehouseLocationDataModel';
import { FoundCargoStorageLocationDataModel } from '../../../datamodels/FoundCargoStorageLocationDataModel';
import { SHCSelectDataModel } from '../../../datamodels/SHCSelectDataModel';
import { CreateFoundCargoDataObject } from '../../../network/dataobjects/CreateFoundCargoDataObject';
import { AppMessagePopupHelper } from '../../../helpers/AppMessagePopupHelper';
import { AppMessagePopupViewModel, AppMessagePopupViewModelType } from '../../viewmodels/AppMessagePopupViewModel';
import { PlatformHelper } from '../../../helpers/PlatformHelper';
import { ScreenOrientationHelper, ScreenOrientationType } from '../../../helpers/ScreenOrientationHelper';
import { RectDataModel } from '../../../datamodels/RectDataModel';
import { FoundCargoViewDataModel } from '../../../datamodels/FoundCargoViewDataModel';
import { BarcodeScannerHelper } from '../../../helpers/BarcodeScannerHelper';
import { FetchOriginItemDataModel } from '../../../datamodels/FetchOriginItemDataModel';
import { AirwaybillsDataModel } from '../../../datamodels/AirwaybillsDataModel';
import { CheckBoxesViewModel, CheckBoxViewModel } from '../../viewmodels/CheckBoxesViewModel';
import { SHCLookupsDataModel } from '../../../datamodels/SHCLookupsDataModel';
import { SHCLookupDataModel } from '../../../datamodels/SHCLookupDataModel';
import { PreFetchDataObjectsHelper } from '../../../network/helpers/PreFetchDataObjectsHelper';
import { RadioButtonsViewModel, RadioButtonViewModel } from '../../viewmodels/RadioButtonsViewModel';
import { FetchOriginResponseObject } from '../../../network/responseobjects/FetchOriginResponseObject';
import { GenericListItemViewModel, GenericListItemViewModelType } from '../../viewmodels/GenericListItemViewModel';

@Component({
    selector: 'champ-page-create-found-cargo',
    templateUrl: 'create-found-cargo.html',
})
export class CreateFoundCargoPage extends BaseUINavigationBarView {
    @ViewChild('schSearchBar') schSearchBar: Searchbar;
    @ViewChild('originSearchBar') originSearchBar: Searchbar;
    @ViewChild('selectOrigin') selectOrigin: TemplateRef<any>;
    @ViewChild('foundDetails') foundDetails: TemplateRef<any>;
    @ViewChild('selectSHC') selectSHC: TemplateRef<any>;
    @ViewChild('storageLocation') storageLocation: TemplateRef<any>;
    foundCargoDataModel: FoundCargoDataModel;
    foundCargoViewDataModel: FoundCargoViewDataModel;
    originDataModel: FetchOriginItemDataModel;
    genericListItemViewModelsForTabletDevices: Array<GenericListItemViewModel> = null;
    fetchOriginResponseObject: FetchOriginResponseObject =
        PreFetchDataObjectsHelper.getInstance().fetchFoundCargoOrignDataObject.responseObject as
        FetchOriginResponseObject;
    shcSelectDataModel: SHCSelectDataModel;
    fetchSHCResponseObject: FetchSHCResponseObject =
        PreFetchDataObjectsHelper.getInstance().fetchFoundCargoSHCDataObject.responseObject as
        FetchSHCResponseObject;
    private readonly DEFAULT_WEIGHT_KG = 'KG';
    private readonly LISTITEMTAGORIGIN = 'LISTITEMTAGORIGIN';
    private readonly LISTITEMTAGSHIPMENTDETAILS = 'LISTITEMTAGSHIPMENTDETAILS';
    private readonly LISTITEMTAGSHC = 'LISTITEMTAGSHC';
    private readonly LISTITEMTAGSTORAGELOCATION = 'LISTITEMTAGSTORAGELOCATION';
    private addStorageLocationRow: Array<string> = [];
    private toShowEnabledSaveButton: boolean;
    private selectedOrign: string;
    private airwaybillText: string;
    private airwaybillPrefix: string;
    private airwaybillSerial: string;
    private errorPopupPieces: number = 499;
    private errorPopupWeight: number = 599;
    private toShowSplitPaneInTabletLandScapeOrientation: boolean = false;
    private detailPaneContenttoLoadInTabletLandScapeOrientation: TemplateRef<any>;
    private originRadioButtonsViewModel: RadioButtonsViewModel;
    private shcCheckBoxesViewModel: CheckBoxesViewModel;
    private fakeshcCheckBoxesViewModel: CheckBoxesViewModel;
    private fakeoriginRadioButtonsViewModel: RadioButtonsViewModel;
    private _barCodeForInternalHardwareScannerForZebraDevicesSubscriber: any;
    private hardwareScanIndexForLocation: number;
    private readonly LISTMARGINBOTTOM = 2;
    private readonly LISTBORDERRADIUS = 5;
    private rectDataModel: RectDataModel =
        new RectDataModel(0, 0, 0, 0);

    constructor(public navController: NavController,
        public navParams: NavParams,
        private ngZone: NgZone) {
        super();
        this.foundCargoViewDataModel = new FoundCargoViewDataModel();
        this.shcSelectDataModel = new SHCSelectDataModel();
        this.airwaybillText = this.navParams.get('airwaybillText');
        this.airwaybillPrefix = this.navParams.get('airwaybillPrefix');
        this.airwaybillSerial = this.navParams.get('airwaybillSerial');
        this.navigationBarViewModel =
            new NavigationBarViewModel(true,
                '',
                true,
                true,
                true);

        if (this.shcSelectDataModel.specialHandlingCode.length === 0) {
            this.shcSelectDataModel.specialHandlingCode.push(new SHCLookupsDataModel());
        }
        this.initDataModel();
        this.prepare_GenericListItemViewModels();
        this.shcSelectDataModel.selectedOrigin.code = 'Origin';
        this.prepareSHCCheckBoxesViewModel();
        this.prepareOriginRadioButtonsViewModel();
    }

    ionViewDidLoad() {
        super.ionViewDidLoad();
        AppUIHelper.getInstance().i18nTextWithCallBack(this.airwaybillText + '  '
            + this.airwaybillPrefix + '-' + this.airwaybillSerial, (i18nTextValue: string) => {
                this.navigationBarViewModel.title = i18nTextValue;
            });
        this.workOn_IsDeviceInLandScapeOrientationFlag();
    }

    ionViewDidEnter() {
        super.ionViewDidEnter();
        this.workOn_IsDeviceInLandScapeOrientationFlag();
        this.registerForBarCodeForInternalHardwareScannerForZebraDevices();
        this.detailPaneContenttoLoadInTabletLandScapeOrientation = this.selectOrigin;
    }

    ionViewWillEnter() {
        super.ionViewWillEnter();
        this.someUIElementsHaveChangedItsState_ChangeStateOfOtherUIElementsAccordingly(false);
    }

      ionViewDidLeave() {
        super.ionViewDidLeave();
        this.unregisterForBarCodeForInternalHardwareScannerForZebraDevices();
      }
    onOrientationChangedTo_Portrait() {
        super.onOrientationChangedTo_Portrait();

        this.workOn_IsDeviceInLandScapeOrientationFlag();
    }

    onOrientationChangedTo_Landscape() {
        super.onOrientationChangedTo_Landscape();

        this.workOn_IsDeviceInLandScapeOrientationFlag();
    }

    onSelectOrigin() {
        this.navController.push(SelectOriginPage, {
            'airwaybillPrefix': this.airwaybillPrefix,
            'airwaybillSerial': this.airwaybillSerial,
            'shcSelectDataModel': this.shcSelectDataModel,
        });
    }
    onAddSHC() {
        this.navController.push(SelectSHCPage,
            {
                'airwaybillPrefix': this.airwaybillPrefix,
                'airwaybillSerial': this.airwaybillSerial,
                'shcSelectDataModel': this.shcSelectDataModel,
            });
    }

    onSaveClicked() {
        let storagePiecesCount = 0;
        for (let i = 0; i < this.foundCargoViewDataModel.storageLocation.length; i++) {
            // console.log(this.foundCargoViewDataModel.storageLocation[i].storedPieces);
            storagePiecesCount = storagePiecesCount +
                parseInt(this.foundCargoViewDataModel.storageLocation[i].storedPieces, 10);
        }
        let storagePiecesWeight = 0;
        for (let i = 0; i < this.foundCargoViewDataModel.storageLocation.length; i++) {
           // console.log(this.foundCargoViewDataModel.storageLocation[i].weight);
            storagePiecesWeight = storagePiecesWeight +
            parseInt( '' + this.foundCargoViewDataModel.storageLocation[i].weight, 10);
        }
        console.log(storagePiecesCount);
        console.log(storagePiecesWeight);
        if (storagePiecesCount > this.foundCargoViewDataModel.foundPieces) {
            this.showErrorMessgae('Entered Pieces count is greater than the found pieces count');
            return;
        } else if (storagePiecesWeight > this.foundCargoViewDataModel.foundWeight) {
        this.showErrorMessgae('Entered Pieces weight is greater than the found pieces weight');
        return;
        }

        this.foundCargoDataModel = new FoundCargoDataModel();
        this.foundCargoDataModel = this.prepareDataModel();
        console.log(this.foundCargoDataModel);
        let createFoundCargoDataObject: CreateFoundCargoDataObject =
            ManualDIHelper.getInstance().createObject('CreateFoundCargoDataObject');

        createFoundCargoDataObject.prepareRequestWithParameters(this.foundCargoDataModel);
        AppMessagePopupHelper.showLoadingAppMessagePopup(AppUIHelper.getInstance().i18nText('saveinprogress'));
        createFoundCargoDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
            AppMessagePopupHelper.getInstance().hideAppMessagePopup();
            if (_dataObject.serverErrorDataModel != null) {
                if (_dataObject.serverErrorDataModel.errorText === 'success') {
                    this.leaveComponent();
                    AppMessagePopupHelper.getInstance().showAppMessagePopup(
                        new AppMessagePopupViewModel(
                            AppUIHelper.getInstance().i18nText('savedsuccessfully'),
                            'assets/images/green-tick-small.svg', AppMessagePopupViewModelType.MessagePopup), 4);
                } else {
                    AppUIHelper.getInstance().showAlert(
                        AppUIHelper.getInstance().i18nText('error'),
                        _dataObject.serverErrorDataModel.errorText,
                        [AppUIHelper.getInstance().i18nText('ok')]);
                }
            }
        });

    }

    onSearchbarTextChanged() {
        if (this.foundCargoViewDataModel.foundPieces != null && this.foundCargoViewDataModel.foundPieces > 0 &&
            this.foundCargoViewDataModel.description != null && this.foundCargoViewDataModel.description !== '' &&
            this.shcSelectDataModel.selectedOrigin.code != null &&
            this.shcSelectDataModel.selectedOrigin.code !== 'Origin') {
            this.toShowEnabledSaveButton = true;
        } else {
            this.toShowEnabledSaveButton = false;
        }

        this.foundCargoViewDataModel.storageLocation = [];
        let foundCargoStorageLocationDataModel = new FoundCargoStorageLocationDataModel();
        this.foundCargoViewDataModel.storageLocation.push(foundCargoStorageLocationDataModel);
        if (this.foundCargoViewDataModel.foundPieces != null) {
        this.foundCargoViewDataModel.storageLocation[0].storedPieces = '' +
        this.foundCargoViewDataModel.foundPieces;
        if (this.foundCargoViewDataModel.foundWeight != null)
        this.foundCargoViewDataModel.storageLocation[0].weight = this.foundCargoViewDataModel.foundWeight;
        else
        this.foundCargoViewDataModel.storageLocation[0].weight = 0;
        foundCargoStorageLocationDataModel.location = '';
        }
      /*  if (this.foundCargoViewDataModel.foundPieces != null)
            this.foundCargoViewDataModel.storageLocation[0].storedPieces = '' +
                this.foundCargoViewDataModel.foundPieces;
        if (this.foundCargoViewDataModel.foundWeight != null)
            this.foundCargoViewDataModel.storageLocation[0].weight = this.foundCargoViewDataModel.foundWeight; */
        this.someUIElementsHaveChangedItsState_ChangeStateOfOtherUIElementsAccordingly(false);
    }

    onStoragePiecesChanged(index: number) {
        console.log('index1:' + index + '' + this.foundCargoViewDataModel.storageLocation.length);
        let storagePiecesCount = 0;
        for (let i = 0; i < this.foundCargoViewDataModel.storageLocation.length; i++) {
            storagePiecesCount = storagePiecesCount +
                parseInt(this.foundCargoViewDataModel.storageLocation[i].storedPieces, 10);
        }
        // console.log(this.foundCargoViewDataModel.storageLocation.length);
        // console.log(storagePiecesCount);
        let weightPerUnit = this.foundCargoViewDataModel.foundWeight /
            this.foundCargoViewDataModel.foundPieces;
        if (storagePiecesCount > this.foundCargoViewDataModel.foundPieces && this.errorPopupPieces !== index) {
            this.errorPopupPieces = index;
            this.showErrorMessgae('Entered Pieces count is greater than the found pieces count');
            // this.foundCargoViewDataModel.storageLocation[index].viewDelete = true;
        } else {
            let foundCargoStorageLocationDataModel = new FoundCargoStorageLocationDataModel();
            foundCargoStorageLocationDataModel.location = '';
            if (storagePiecesCount < this.foundCargoViewDataModel.foundPieces) {
                let storageLocationcount = this.foundCargoViewDataModel.storageLocation.length;
                foundCargoStorageLocationDataModel.storedPieces = '' +
                    (this.foundCargoViewDataModel.foundPieces - storagePiecesCount);
                this.foundCargoViewDataModel.storageLocation.push(foundCargoStorageLocationDataModel);
            }
            for (let i = 0; i < this.foundCargoViewDataModel.storageLocation.length; i++) {
                if (this.foundCargoViewDataModel.storageLocation.length !== 1) {
                    console.log(this.foundCargoViewDataModel.storageLocation[i].storedPieces);
                    if (isNaN(parseInt(this.foundCargoViewDataModel.storageLocation[i].storedPieces, 10))) {
                        // this.foundCargoViewDataModel.storageLocation[i].weight = ;
                    } else {
                        this.foundCargoViewDataModel.storageLocation[i].weight =
                            (parseInt(this.foundCargoViewDataModel.storageLocation[i].storedPieces, 10)
                                * weightPerUnit);
                    }
                }
            }
        }
        // this.onStorageWeightChanged(index);
    }
    onStorageWeightChanged(index: number) {
        console.log('index2:' + index);
        let storagePiecesWeight = 0;
        for (let i = 0; i < this.foundCargoViewDataModel.storageLocation.length; i++) {
            storagePiecesWeight = storagePiecesWeight +
            parseInt( '' + this.foundCargoViewDataModel.storageLocation[i].weight, 10);
        }
        console.log(storagePiecesWeight);
        console.log(this.foundCargoViewDataModel.foundWeight);
        if (storagePiecesWeight > this.foundCargoViewDataModel.foundWeight && this.errorPopupWeight !== index) {
        this.errorPopupWeight = index;
        this.showErrorMessgae('Entered Pieces weight is greater than the found pieces weight');
        }
    }
    onScanClicked(index: number) {
        console.log(index);
        BarcodeScannerHelper.getInstance().scan((barcodeData: string) => {
            this.foundCargoViewDataModel.storageLocation[index].location = '' + barcodeData;
        }, (_errorText: string) => {
        });
    }
    onSHCScanClicked() {
        BarcodeScannerHelper.getInstance().scan((barcodeData: string) => {
            for (let i = 0; i < this.shcCheckBoxesViewModel.checkBoxViewModels.length; i++) {
                if (this.shcCheckBoxesViewModel.checkBoxViewModels[i].name.match('' + barcodeData.toUpperCase())) {
                    this.shcCheckBoxesViewModel.checkBoxViewModels[i].isSelected = true;
                    this.tapEvent_On_SHCCheckBoxesViewModel(this.shcCheckBoxesViewModel.checkBoxViewModels[i]);
                }
                }
        }, (_errorText: string) => {
        });
    }
    onLocationTextChanged(index: number) {
        this.hardwareScanIndexForLocation = index;
        console.log(this.foundCargoViewDataModel.storageLocation[index].location);
        if (typeof (this.foundCargoViewDataModel.storageLocation[index].location) === undefined ||
            this.foundCargoViewDataModel.storageLocation[index].location === null ||
            this.foundCargoViewDataModel.storageLocation[index].location === '')
            this.foundCargoViewDataModel.storageLocation[index].viewDelete = true;
        else this.foundCargoViewDataModel.storageLocation[index].viewDelete = false;

        for (let i = 0; i < this.foundCargoViewDataModel.storageLocation.length; i++) {
            if (i !== index)
                this.foundCargoViewDataModel.storageLocation[i].viewDelete = false;
        }
        console.log(this.foundCargoViewDataModel.storageLocation[index].viewDelete);
    }
    showErrorMessgae(msg: string) {
        AppUIHelper.getInstance().showAlert(
            AppUIHelper.getInstance().i18nText('error'), msg,
            [AppUIHelper.getInstance().i18nText('ok')]);
    }
    onRemoveStorageLocation(index: number) {
        console.log(this.foundCargoViewDataModel.storageLocation.length);
        this.foundCargoViewDataModel.storageLocation.splice(index);
        console.log(this.foundCargoViewDataModel.storageLocation.length);
        this.onStoragePiecesChanged(index);

    }
    prepareDataModel(): FoundCargoDataModel {
        let fetchOriginItemDataModel = new FetchOriginItemDataModel();
        fetchOriginItemDataModel.code = this.shcSelectDataModel.selectedOrigin.code;
        fetchOriginItemDataModel.type = this.shcSelectDataModel.selectedOrigin.type;
        this.foundCargoDataModel.origin = fetchOriginItemDataModel;
        this.foundCargoDataModel.reportingLocation = fetchOriginItemDataModel;
        let airwaybillsDataModel = new AirwaybillsDataModel();
        airwaybillsDataModel.airwaybillPrefix = this.airwaybillPrefix;
        airwaybillsDataModel.airwaybillSerial = this.airwaybillSerial;
        this.foundCargoDataModel.airwaybill = airwaybillsDataModel;
        this.foundCargoDataModel.natureOfGoods = this.foundCargoViewDataModel.description;
        this.foundCargoDataModel.reportingShipmentType = 'airwaybill';
        this.foundCargoDataModel.preferedLocation = this.foundCargoViewDataModel.preferedLocation;
        this.foundCargoDataModel.shc = [];
        if (this.shcSelectDataModel.specialHandlingCode[0] != null &&
            this.shcSelectDataModel.specialHandlingCode[0].shcType.length !== 0) {
            for (let i = 0; i < this.shcSelectDataModel.specialHandlingCode[0].shcType.length; i++) {
                this.foundCargoDataModel.shc.push(this.shcSelectDataModel.specialHandlingCode[0].shcType[i].code);
            }
        }
        this.foundCargoDataModel.foundPieces = '' + this.foundCargoViewDataModel.foundPieces;
        let amountUnitDataModel = new AmountUnitDataModel();
        this.foundCargoViewDataModel.foundWeight == null ?
          amountUnitDataModel.amount = 0 : amountUnitDataModel.amount = this.foundCargoViewDataModel.foundWeight;
        amountUnitDataModel.unit = 'KG';
        this.foundCargoDataModel.foundWeight = amountUnitDataModel;
        for (let k = 0; k < this.foundCargoViewDataModel.storageLocation.length; k++) {
            let foundCargoStorageLocationDataModel = new FoundCargoStorageLocationDataModel();
            foundCargoStorageLocationDataModel.storedPieces =
                this.foundCargoViewDataModel.storageLocation[k].storedPieces;
            foundCargoStorageLocationDataModel.weight =
                this.foundCargoViewDataModel.storageLocation[k].weight;
            foundCargoStorageLocationDataModel.warehouseLocation = new WarehouseLocationDataModel();
            foundCargoStorageLocationDataModel.warehouseLocation.location =
                this.foundCargoViewDataModel.storageLocation[k].location;
            this.foundCargoDataModel.storageLocation.push(foundCargoStorageLocationDataModel);
        }

        return this.foundCargoDataModel;
    }
    initDataModel() {
        let foundCargoStorageLocationDataModel = new FoundCargoStorageLocationDataModel();
        // foundCargoStorageLocationDataModel.warehouseLocation = new WarehouseLocationDataModel();
        foundCargoStorageLocationDataModel.location = '';
        this.foundCargoViewDataModel.storageLocation.push(foundCargoStorageLocationDataModel);
        let fetchOriginItemDataModel = new FetchOriginItemDataModel();
        this.shcSelectDataModel.selectedOrigin = fetchOriginItemDataModel;

    }

    originInputTextChanged() {
        if (this.originSearchBar.value.length >= 2) {
            this.fakeoriginRadioButtonsViewModel = new RadioButtonsViewModel();
            for (let i = 0; i < this.originRadioButtonsViewModel.radioButtonViewModels.length; i++) {
                if (this.originRadioButtonsViewModel.radioButtonViewModels[i].name.
                    match(this.originSearchBar.value.toUpperCase()))
                    this.fakeoriginRadioButtonsViewModel.addRadioButtonViewModel(
                        this.originRadioButtonsViewModel.radioButtonViewModels[i].name,
                        this.originRadioButtonsViewModel.radioButtonViewModels[i].isSelected,
                        this.originRadioButtonsViewModel.radioButtonViewModels[i].extras,
                        this.originRadioButtonsViewModel.radioButtonViewModels[i].thumbnailBadgeImage,
                        this.originRadioButtonsViewModel.radioButtonViewModels[i].subText);
            }
        } else {
            this.fakeoriginRadioButtonsViewModel = null;
        }
    }

    shcInputTextChanged() {
        if (this.schSearchBar.value.length >= 2) {
            this.fakeshcCheckBoxesViewModel = new CheckBoxesViewModel();
            for (let i = 0; i < this.shcCheckBoxesViewModel.checkBoxViewModels.length; i++) {
                if (this.shcCheckBoxesViewModel.checkBoxViewModels[i].name.match(this.schSearchBar.value.toUpperCase()))
                    this.fakeshcCheckBoxesViewModel.addCheckBoxViewModel(
                        this.shcCheckBoxesViewModel.checkBoxViewModels[i].name,
                        this.shcCheckBoxesViewModel.checkBoxViewModels[i].isSelected,
                        this.shcCheckBoxesViewModel.checkBoxViewModels[i].extras);
            }
        } else {
            this.fakeshcCheckBoxesViewModel = null;
        }
    }

    tapEvent_On_GenericListItemComponent(_genericListItemViewModel: GenericListItemViewModel) {
        if (_genericListItemViewModel.tag === this.LISTITEMTAGORIGIN)
            this.detailPaneContenttoLoadInTabletLandScapeOrientation = this.selectOrigin;
        else if (_genericListItemViewModel.tag === this.LISTITEMTAGSHC) {
            if (this.detailPaneContenttoLoadInTabletLandScapeOrientation !== this.selectSHC)
            this.fakeshcCheckBoxesViewModel = null;
            this.detailPaneContenttoLoadInTabletLandScapeOrientation = this.selectSHC;
        } else if (_genericListItemViewModel.tag === this.LISTITEMTAGSHIPMENTDETAILS)
            this.detailPaneContenttoLoadInTabletLandScapeOrientation = this.foundDetails;
        else if (_genericListItemViewModel.tag === this.LISTITEMTAGSTORAGELOCATION)
            this.detailPaneContenttoLoadInTabletLandScapeOrientation = this.storageLocation;
    }

    private prepare_GenericListItemViewModels() {
        this.genericListItemViewModelsForTabletDevices = [];
        let dataArray = [[GenericListItemViewModelType.Icon_MainText_SubText
            , this.LISTITEMTAGORIGIN
            , 'assets/images/Origin.svg'
            , AppUIHelper.getInstance().i18nText('origintitle')
            , AppUIHelper.getInstance().i18nText('originsubtext')
            , this.LISTMARGINBOTTOM
            , this.LISTBORDERRADIUS
            , true
            , true
            , ''
            , false],
        [GenericListItemViewModelType.Icon_MainText_SubText
            , this.LISTITEMTAGSHIPMENTDETAILS
            , 'assets/images/shipmentdetails.svg'
            , AppUIHelper.getInstance().i18nText('shipmentdetails')
            , AppUIHelper.getInstance().i18nText('founddetailssubtext')
            , this.LISTMARGINBOTTOM
            , this.LISTBORDERRADIUS
            , true
            , true
            , ''
            , false],
        [GenericListItemViewModelType.Icon_MainText_SubText
            , this.LISTITEMTAGSHC
            , 'assets/images/SHC.svg'
            , AppUIHelper.getInstance().i18nText('shc')
            , AppUIHelper.getInstance().i18nText('shcsubtext')
            , this.LISTMARGINBOTTOM
            , this.LISTBORDERRADIUS
            , false
            , true
            , ''
            , false],
        [GenericListItemViewModelType.Icon_MainText_SubText
            , this.LISTITEMTAGSTORAGELOCATION
            , 'assets/images/Locationdetails.svg'
            , AppUIHelper.getInstance().i18nText('storagelocation')
            , AppUIHelper.getInstance().i18nText('storagelocationsubtext')
            , this.LISTMARGINBOTTOM
            , this.LISTBORDERRADIUS
            , false
            , true
            , ''
            , true],
        ];

        for (let i = 0; i < dataArray.length; i++) {
            let array = dataArray[i];

            let genericListItemViewModel: GenericListItemViewModel =
                new GenericListItemViewModel(0, array[1] as string, array[0] as GenericListItemViewModelType);
            if (genericListItemViewModel.genericListItemViewModelType ===
                GenericListItemViewModelType.MainText) {
                genericListItemViewModel.mainText = array[2] as string;
            } else if (genericListItemViewModel.genericListItemViewModelType ===
                GenericListItemViewModelType.Icon_MainText_SubText ||
                genericListItemViewModel.genericListItemViewModelType ===
                GenericListItemViewModelType.HorizontalListing_Of_Media_ThumbNails) {
                genericListItemViewModel.icon = array[2] as string;
                genericListItemViewModel.mainText = array[3] as string;
                genericListItemViewModel.subText = array[4] as string;
                genericListItemViewModel.marginBottom = array[5] as number;
                genericListItemViewModel.borderRadius = array[6] as number;
                genericListItemViewModel.shouldShowMandatoryStar = array[7] as boolean;
                genericListItemViewModel.shouldShowRightArrow = array[8] as boolean;
                genericListItemViewModel.thumbnailBadgeImage = array[9] as string;
                genericListItemViewModel.textWrap = array[10] as boolean;
                if (genericListItemViewModel.genericListItemViewModelType ===
                    GenericListItemViewModelType.HorizontalListing_Of_Media_ThumbNails) {
                    genericListItemViewModel.backgroundColor = array[8] as string;
                }
            }
            this.genericListItemViewModelsForTabletDevices.push(genericListItemViewModel);
        }
        this.someUIElementsHaveChangedItsState_ChangeStateOfOtherUIElementsAccordingly(false);
    }

    private genericListItemViewModelByTag(_tag: string): GenericListItemViewModel {
        let genericListItemViewModelByTag: GenericListItemViewModel = null;
        for (let _genericListItemViewModelByTag of this.genericListItemViewModelsForTabletDevices)
            if (_genericListItemViewModelByTag.tag === _tag) {
                genericListItemViewModelByTag = _genericListItemViewModelByTag;
                break;
            }
        if (this.genericListItemViewModelsForTabletDevices != null && genericListItemViewModelByTag == null) {
            for (let _genericListItemViewModelByTag of this.genericListItemViewModelsForTabletDevices)
                if (_genericListItemViewModelByTag.tag === _tag) {
                    genericListItemViewModelByTag = _genericListItemViewModelByTag;
                    break;
                }
        }
        return genericListItemViewModelByTag;
    }

    private someUIElementsHaveChangedItsState_ChangeStateOfOtherUIElementsAccordingly
        (_isThisCalledDueToARealChange: boolean) {
        let _genericListItemViewModelByTag: GenericListItemViewModel =
            this.genericListItemViewModelByTag(this.LISTITEMTAGSHIPMENTDETAILS);
        if (_genericListItemViewModelByTag != null) {
            if (this.foundCargoViewDataModel.doWeHaveAllOfShipmentDetailsMandatoryDataLeftToBeFilledIn()) {
                _genericListItemViewModelByTag.thumbnailBadgeImage = null;
                _genericListItemViewModelByTag.subText =
                    AppUIHelper.getInstance().i18nText('founddetailssubtext');
            } else {
                _genericListItemViewModelByTag.thumbnailBadgeImage =
                    (this.foundCargoViewDataModel.doWeHaveAnyShipmentDetailsMandatoryDataLeftToBeFilledIn() ?
                        'assets/images/pendingdot.svg' : 'assets/images/completed.svg');
                _genericListItemViewModelByTag.subText =
                    this.foundCargoViewDataModel.textOfShipmentDetailsMandatoryDataThatIsFilledIn();
            }
        }

        _genericListItemViewModelByTag =
            this.genericListItemViewModelByTag(this.LISTITEMTAGORIGIN);
        if (_genericListItemViewModelByTag != null) {
            if (this.foundCargoViewDataModel.doWeHaveAllOfSelectOriginMandatoryDataLeftToBeFilledIn()) {
                _genericListItemViewModelByTag.thumbnailBadgeImage = null;
                _genericListItemViewModelByTag.subText =
                    AppUIHelper.getInstance().i18nText('originsubtext');
            } else {
                _genericListItemViewModelByTag.thumbnailBadgeImage =
                    (this.foundCargoViewDataModel.doWeHaveAnySelectOriginMandatoryDataLeftToBeFilledIn() ?
                        'assets/images/pendingdot.svg' : 'assets/images/completed.svg');
                _genericListItemViewModelByTag.subText =
                    this.foundCargoViewDataModel.textOfSelectOriginMandatoryDataThatIsFilledIn(
                        this.shcSelectDataModel.selectedOrigin.code + ' - ' +
                        this.shcSelectDataModel.selectedOrigin.name);
            }
        }

        _genericListItemViewModelByTag =
        this.genericListItemViewModelByTag(this.LISTITEMTAGSHC);
        if (_genericListItemViewModelByTag != null) {
            if (this.shcSelectDataModel.specialHandlingCode[0] != null
                && this.shcSelectDataModel.specialHandlingCode[0].shcType.length !== 0) {
                _genericListItemViewModelByTag.thumbnailBadgeImage = null;
                _genericListItemViewModelByTag.subText =
                    this.foundCargoViewDataModel.textOfSHCDataThatIsFilledIn(
                        this.shcSelectDataModel.specialHandlingCode[0].shcType);
            } else {
                _genericListItemViewModelByTag.subText =
                this.foundCargoViewDataModel.textOfSHCDataThatIsFilledIn(
                    this.shcSelectDataModel.specialHandlingCode[0].shcType);
            }
        }
    }

    private prepareOriginRadioButtonsViewModel() {
        this.originRadioButtonsViewModel = new RadioButtonsViewModel();

        for (let lookupDataModel of
            this.fetchOriginResponseObject.foundCargoStationDataModel.locations) {
            this.originRadioButtonsViewModel.addRadioButtonViewModel(
                lookupDataModel.code + ' - ' + lookupDataModel.name + ', ' + lookupDataModel.country.name,
                false,
                lookupDataModel,
                'assets/images/flight.svg',
                lookupDataModel.name + ' Airport',
            );
        }
    }

    private tapEvent_On_OriginRadioButtonsViewModel(_radioButtonViewModel: RadioButtonViewModel) {
        _radioButtonViewModel.extras.isSelected = true;
        this.foundCargoViewDataModel.origin = _radioButtonViewModel.extras.code;
        if (this.shcSelectDataModel.selectedOrigin.code !== 'Origin') {
            for (let radioButtonViewModel of this.originRadioButtonsViewModel.radioButtonViewModels) {
                radioButtonViewModel.isSelected = false;
            }
        }
        this.shcSelectDataModel.selectedOrigin = _radioButtonViewModel.extras;
        let actualoriginRadioButtonViewModel = this.originRadioButtonsViewModel.radioButtonViewModels
        .filter((item) => {
            return (item.name.startsWith(_radioButtonViewModel.name));
        });
        actualoriginRadioButtonViewModel[0].isSelected = true;
        this.fakeoriginRadioButtonsViewModel = null;
        this.detailPaneContenttoLoadInTabletLandScapeOrientation = this.foundDetails;
        this.someUIElementsHaveChangedItsState_ChangeStateOfOtherUIElementsAccordingly(false);
    }

    private prepareSHCCheckBoxesViewModel() {
        this.shcCheckBoxesViewModel = new CheckBoxesViewModel();
        for (let shcDataModel of
            this.fetchSHCResponseObject.foundCargoSHCDataModel.specialHandling.shcType) {
            this.shcCheckBoxesViewModel.addCheckBoxViewModel(
                shcDataModel.code,
                this.shcSelectDataModel.specialHandlingCode[0].contains(shcDataModel),
                shcDataModel);
        }
    }

    private tapEvent_On_SHCCheckBoxesViewModel(_fakecheckBoxViewModel: CheckBoxViewModel) {
        if (this.shcSelectDataModel.specialHandlingCode[0].shcType.length === 9 && _fakecheckBoxViewModel.isSelected) {
            this.fakeshcCheckBoxesViewModel = null;
            this.schSearchBar.value = null;
            this.showErrorMessgae('Only 9 SHC selections allowed ');
        } else {
        let _shcLookupDataModel: SHCLookupDataModel = _fakecheckBoxViewModel.extras as SHCLookupDataModel;
        let actualshcCheckBoxesViewModel = this.shcCheckBoxesViewModel.checkBoxViewModels.filter((item) => {
            return (item.name.startsWith(_fakecheckBoxViewModel.name));
        });
        if (_fakecheckBoxViewModel.isSelected) {
            this.shcSelectDataModel.specialHandlingCode[0].addIfNotPresent(_shcLookupDataModel);
            this.shcCheckBoxesViewModel.selectCheckBoxViewModel(actualshcCheckBoxesViewModel[0]);
        } else {
            this.shcSelectDataModel.specialHandlingCode[0].removeIfPresent(_shcLookupDataModel);
            this.shcCheckBoxesViewModel.unSelectCheckBoxViewModel(actualshcCheckBoxesViewModel[0]);
        }
        this.schSearchBar.value = '';
        this.fakeshcCheckBoxesViewModel = null;
        this.someUIElementsHaveChangedItsState_ChangeStateOfOtherUIElementsAccordingly(false);
    }
    }

    private onRemoveSHC(_shcLookupDataModel: SHCLookupDataModel) {
        let unselectshcCheckBoxesViewModel = this.shcCheckBoxesViewModel.checkBoxViewModels.filter((item) => {
            return (item.name.startsWith(_shcLookupDataModel.code));
        });
        if (this.fakeshcCheckBoxesViewModel) {
        let unselectfakeshcCheckBoxesViewModel = this.fakeshcCheckBoxesViewModel.checkBoxViewModels.filter((item) => {
            return (item.name.startsWith(_shcLookupDataModel.code));
        });
        if (unselectfakeshcCheckBoxesViewModel[0])
        this.fakeshcCheckBoxesViewModel.unSelectCheckBoxViewModel(unselectfakeshcCheckBoxesViewModel[0]);
    }
        this.shcCheckBoxesViewModel.unSelectCheckBoxViewModel(unselectshcCheckBoxesViewModel[0]);
        this.shcSelectDataModel.specialHandlingCode[0].removeIfPresent(_shcLookupDataModel);
        this.someUIElementsHaveChangedItsState_ChangeStateOfOtherUIElementsAccordingly(false);
    }

    private workOn_IsDeviceInLandScapeOrientationFlag() {
        if (PlatformHelper.isDevice_A_Tablet()) {
            const isDeviceInLandScapeOrientation =
                ScreenOrientationHelper.currentScreenOrientation() ===
                ScreenOrientationType.LANDSCAPE;

            const calc = (() => {
                if (document.getElementById('foundCargoLabel') != null) {
                    const top =
                        (document.getElementById('foundCargoLabel').getBoundingClientRect().bottom +
                            document.getElementById('navigationBarHeader').getBoundingClientRect().height) +
                        40;

                    this.rectDataModel.height = PlatformHelper.screenHeight() - top;
                }
            });

            if (isDeviceInLandScapeOrientation) {
                setTimeout(() => {
                    calc();
                    this.ngZone.run(() => {
                        this.toShowSplitPaneInTabletLandScapeOrientation = true;
                    });
                }, 200);
            } else this.toShowSplitPaneInTabletLandScapeOrientation = false;
        }
    }

    private leaveComponent() {
        setTimeout(() => {
        this.navController.pop();
        }, 4 * 1000);
    }

    private registerForBarCodeForInternalHardwareScannerForZebraDevices() {
        this.unregisterForBarCodeForInternalHardwareScannerForZebraDevices();
        this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber =
        BarcodeScannerHelper.getInstance()
        .barCodeForInternalHardwareScannerForZebraDevices.subscribe
        ((_barcodeData: string) => {
            if (this.hardwareScanIndexForLocation != null && this.hardwareScanIndexForLocation >= 0 &&
                this.foundCargoViewDataModel != null && this.foundCargoViewDataModel.storageLocation != null &&
                this.foundCargoViewDataModel.storageLocation[this.hardwareScanIndexForLocation] != null &&
                this.foundCargoViewDataModel.storageLocation[this.hardwareScanIndexForLocation].location != null) {
            this.foundCargoViewDataModel.storageLocation[this.hardwareScanIndexForLocation].location =
            '' + _barcodeData;
            }
        });
      }

      private unregisterForBarCodeForInternalHardwareScannerForZebraDevices() {
        if (this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber != null) {
          this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber.unsubscribe();
          this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber = null;
        }
      }

}
