import { Component, ViewChild, ChangeDetectorRef, NgZone } from '@angular/core';
import { NavController, NavParams, Searchbar } from 'ionic-angular';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { AppMessagePopupHelper } from '../../../helpers/AppMessagePopupHelper';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { FetchAWBDataObject } from '../../../network/dataobjects/FetchAWBDataObject';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { FetchAWBResponseObject } from '../../../network/responseobjects/FetchAWBResponseObject';
import { AirwaybillsDataModel } from '../../../datamodels/AirwaybillsDataModel';
import { GenericListItemViewModel, GenericListItemViewModelType } from '../../viewmodels/GenericListItemViewModel';
import { RectDataModel } from '../../../datamodels/RectDataModel';
import { Keyboard } from '@ionic-native/keyboard';
import { KeyboardHelper } from '../../../helpers/KeyboardHelper';
import { PlatformHelper } from '../../../helpers/PlatformHelper';
import { BarcodeParserHelper, BarcodeType } from '../../../helpers/BarcodeParserHelper';
import { BarcodeScannerHelper } from '../../../helpers/BarcodeScannerHelper';
import { CreateFoundCargoPage } from '../create-found-cargo/create-found-cargo';
import { FoundCargoUnKnownAWBDataObject } from '../../../network/dataobjects/FoundCargoUnKnownAWBDataObject';
import { FetchUnKnownAWBResponseObject } from '../../../network/responseobjects/FetchUnKnownAWBResponseObject';
import { FoundCargoDataModel } from '../../../datamodels/FoundCargoDataModel';

@Component({
    selector: 'champ-page-found-cargo-search',
    templateUrl: 'found-cargo-search.html',
})
export class FoundCargoSearchPage extends BaseUINavigationBarView {
    @ViewChild('awbPrefixSearchbar') awbPrefixSearchbar: Searchbar;
    @ViewChild('awbSerialSearchbar') awbSerialSearchbar: Searchbar;
    awbPrefixSearchbarText: string;
    awbSerialSearchbarText: string;
    isAWBSerialDisabled: boolean = true;
    private fetchAWBDataObject: FetchAWBDataObject;
    private shouldShowLoadingContainerInAWBList: boolean = false;
    private toResetControlsAndDataForAWBAutoSuggest: boolean = false;
    private airwaybillsDataModels: Array<AirwaybillsDataModel> = null;
    private awbGenericListItemViewModels: Array<GenericListItemViewModel> = null;
    private rectDataModel: RectDataModel;
    private awbListHeightWhenKeyboardNotShown: number;
    private awbListHeight: number;
    private readonly AWBLISTMARGINBOTTOM = 50;
    private awbHWBListHeight: number;
    private _barCodeForInternalHardwareScannerForZebraDevicesSubscriber: any;

    constructor(public navController: NavController,
        public navParams: NavParams,
        public changeDetectorRef: ChangeDetectorRef,
        private keyboard: Keyboard,
        private ngZone: NgZone) {
        super();
        this.navigationBarViewModel =
            new NavigationBarViewModel(true,
                '',
                true,
                true,
                true);
    }

    ionViewDidLoad() {
        super.ionViewDidLoad();

        AppUIHelper.getInstance().i18nTextWithCallBack('foundcargo', (i18nTextValue: string) => {
            this.navigationBarViewModel.title = i18nTextValue;
        });

        this.calculateAWBHWBListHeightWhenKeyboardNotShown();
        this.subscribeToKeyboardShowHideEvents();
    }
    ionViewCanLeave(): boolean {
        if (this.toResetControlsAndDataForAWBAutoSuggest) {
          this.toResetControlsAndDataForAWBAutoSuggest = false;
          this.resetControlsAndData_For_AWB_AutoSuggest();
        }
        return super.ionViewCanLeave();
      }
    onOrientationChangedTo_Portrait() {
        super.onOrientationChangedTo_Portrait();
        this.onOrientationChange();
    }

    onOrientationChangedTo_Landscape() {
        super.onOrientationChangedTo_Landscape();
        this.onOrientationChange();
    }

    ionViewDidEnter() {
        super.ionViewDidEnter();
        this.registerForBarCodeForInternalHardwareScannerForZebraDevices();
      }

      ionViewDidLeave() {
        super.ionViewDidLeave();
        this.unregisterForBarCodeForInternalHardwareScannerForZebraDevices();
      }

    onKeyboardShowHideEvent(_keyboard: any) {
        super.onKeyboardShowHideEvent(_keyboard);
        this.calculateAWBHWBListHeight(_keyboard.showing, _keyboard.height);
    }

    onSearchClicked() {
        if (AppUIHelper.isValidAWBNumber(this.awbPrefixSearchbar.value, this.awbSerialSearchbar.value)) {
            const _value = this.awbPrefixSearchbar.value + this.awbSerialSearchbar.value;
            this.resetControlsAndData_For_AWB_AutoSuggest();
            this.do_AWB_AutoSuggest(false, _value);
        }
    }

    onScanClicked() {
        this.resetControlsAndData_For_AWB_AutoSuggest();
        BarcodeScannerHelper.getInstance().scan((barcodeData: string) => {
            this.do_AWB_AutoSuggest(false, barcodeData);
        }, (_errorText: string) => {
        });
    }

    onAWBPrefixSearchbarTextChanged() {
        if (this.awbPrefixSearchbarText.length >= 3) {
            this.isAWBSerialDisabled = false;
            setTimeout(() => {
                this.awbSerialSearchbar.setFocus();
            }, 10);
        } else {
            this.isAWBSerialDisabled = true;
            this.awbSerialSearchbarText = '';
        }
        this.cancelAutoCompleteNetworkCalls();
        this.deInit_AWB_HWB_DataModels();
        if (this.awbPrefixSearchbar.value + '' + this.awbSerialSearchbar.value === '') {
            this.resetControlsAndData_For_AWB_AutoSuggest();
        }
    }

    changedAWBPrefix(value: any) {
        this.changeDetectorRef.detectChanges();
        this.awbPrefixSearchbarText = value.length > 3 ? value.substring(0, 3) : value;
    }

    onAWBSerialSearchbarTextChanged() {
        if (this.awbPrefixSearchbar.value + '' + this.awbSerialSearchbar.value === '') {
            this.resetControlsAndData_For_AWB_AutoSuggest();
        } else {
            if (this.awbSerialSearchbarText.length >= 3) {
                this.do_AWB_AutoSuggest(true);
            } else {
                this.cancelAutoCompleteNetworkCalls();
                this.deInit_AWB_HWB_DataModels();
            }
        }
    }

    onAWBSerialSearchbarBackspacePressed() {
        if (this.awbSerialSearchbarText.length >= 3) {
            this.do_AWB_AutoSuggest(true);
        } else {
            this.cancelAutoCompleteNetworkCalls();
            this.deInit_AWB_HWB_DataModels();
            if (this.awbSerialSearchbarText.length === 0) {
                this.awbPrefixSearchbar.setFocus();
            }
        }
    }
    private do_AWB_AutoSuggest(calledByUIOrBarcodeScanner: boolean, _valueFromBarcodeScanner?: string) {
        const doAWBNetworkCall = ((_awbPrefix: string, _awbSerial: string) => {
            if (!calledByUIOrBarcodeScanner)
                AppMessagePopupHelper.showLoadingAppMessagePopup(AppUIHelper.getInstance().i18nText('loading'));
            this.shouldShowLoadingContainerInAWBList = true;
            this.fetchAWBDataObject = ManualDIHelper.getInstance().createObject('FetchAWBDataObject');
            this.fetchAWBDataObject.prepareRequestWithParameters
                (_awbPrefix, _awbSerial);
            this.fetchAWBDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
                this.shouldShowLoadingContainerInAWBList = false;
                if (!calledByUIOrBarcodeScanner)
                    AppMessagePopupHelper.getInstance().hideAppMessagePopup();
                if (_dataObject.serverErrorDataModel != null) {
                    AppUIHelper.getInstance().showAlert('', _dataObject.serverErrorDataModel.errorText,
                        [AppUIHelper.getInstance().i18nText('ok')]);
                } else {
                    let fetchAWBResponseObject: FetchAWBResponseObject =
                        _dataObject.responseObject as FetchAWBResponseObject;
                    if (fetchAWBResponseObject.awbNotFound) {
                        const fakeAirwaybillsDataModel: AirwaybillsDataModel =
                            new AirwaybillsDataModel();
                        fakeAirwaybillsDataModel.airwaybillPrefix = _awbPrefix;
                        fakeAirwaybillsDataModel.airwaybillSerial = _awbSerial;
                        fetchAWBResponseObject.airwaybillsDataModels.push(fakeAirwaybillsDataModel);
                    }
                    this.airwaybillsDataModels = fetchAWBResponseObject.airwaybillsDataModels;

                    if (calledByUIOrBarcodeScanner)
                        this.prepare_AirwaybillsGenericListItemViewModels();
                    else {
                        let fakeGenericListItemViewModel: GenericListItemViewModel =
                            new GenericListItemViewModel(0, '0', GenericListItemViewModelType.MainText);
                        this.tapEvent_On_AWBGenericListItemViewModels(fakeGenericListItemViewModel);
                    }
                }
            });
        });

        if (calledByUIOrBarcodeScanner) {
            this.cancelAutoCompleteNetworkCalls();
            this.initAWBORHWBGenericListItemViewModels();
            doAWBNetworkCall(this.awbPrefixSearchbar.value, this.awbSerialSearchbar.value);
        } else {
            let barcodeType = BarcodeParserHelper.barcodeType(_valueFromBarcodeScanner);
            const barcodeValues = BarcodeParserHelper.barcodeValues(_valueFromBarcodeScanner, barcodeType);
            doAWBNetworkCall(barcodeValues[0], barcodeValues[1]);

        }
    }
    private prepare_AirwaybillsGenericListItemViewModels() {
        this.initAWBORHWBGenericListItemViewModels();
        this.airwaybillsDataModels.forEach((airwaybillsDataModel, index) => {
            let genericListItemViewModel: GenericListItemViewModel =
                new GenericListItemViewModel(index, '', GenericListItemViewModelType.MainText);
            genericListItemViewModel.mainText =
                airwaybillsDataModel.airwaybillPrefix
                + ' '
                + airwaybillsDataModel.airwaybillSerial;
            this.awbGenericListItemViewModels.push(genericListItemViewModel);
        });
    }

    private tapEvent_On_AWBGenericListItemViewModels(_genericListItemViewModel: GenericListItemViewModel) {
        this.toResetControlsAndDataForAWBAutoSuggest = true;
        this.navController.push(CreateFoundCargoPage,
        {'airwaybillText': 'AWB',
        'airwaybillPrefix': this.airwaybillsDataModels[_genericListItemViewModel.index].airwaybillPrefix,
        'airwaybillSerial': this.airwaybillsDataModels[_genericListItemViewModel.index].airwaybillSerial});
    }
    private cancelAutoCompleteNetworkCalls() {
        if (this.fetchAWBDataObject != null) {
            this.fetchAWBDataObject.cancelRequest();
            this.fetchAWBDataObject = null;
        }
    }

    private deInit_AWB_HWB_DataModels() {
        this.airwaybillsDataModels = null;
        this.awbGenericListItemViewModels = null;
    }

    private resetControlsAndData_For_AWB_AutoSuggest() {
        this.awbPrefixSearchbarText = '';
        this.awbSerialSearchbarText = '';
        this.deInit_AWB_HWB_DataModels();
        this.cancelAutoCompleteNetworkCalls();
    }
    // keyboard events
    private initAWBORHWBGenericListItemViewModels() {
        this.initRectDataModel();
        this.awbGenericListItemViewModels = [];
    }
    private initRectDataModel() {
        let searchbarContainer = document.getElementById('containerAWB').getBoundingClientRect();
        let navBarHeight = document.getElementById('searchBarHeader').getBoundingClientRect().height;
        /*
            The navBarHeight is subtracted from the searchbar's bottom point as the list was rendered
            'navBarHeight' point below the 'searchbarContainer.bottom' :: Hence such a solution
        */
        this.rectDataModel = new RectDataModel(searchbarContainer.bottom - navBarHeight,
            searchbarContainer.left,
            searchbarContainer.width,
            this.awbHWBListHeight);
    }

    private calculateAWBHWBListHeightWhenKeyboardNotShown() {
        let searchbarContainer = document.getElementById('containerAWB').getBoundingClientRect();
        let navBarHeight = document.getElementById('searchBarHeader').getBoundingClientRect().height;
        this.awbListHeightWhenKeyboardNotShown = PlatformHelper.screenHeight() -
            navBarHeight - searchbarContainer.bottom - this.AWBLISTMARGINBOTTOM;
    }
    private onOrientationChange() {
        this.unsubscribeToKeyboardShowHideEvents();
        KeyboardHelper.getInstance().hideKeyboard();
        setTimeout(() => {
            this.ngZone.run(() => {
                this.calculateAWBHWBListHeightWhenKeyboardNotShown();
                this.calculateAWBHWBListHeight(false, 0);
                this.subscribeToKeyboardShowHideEvents();
            });
        }, 50);
    }

    private calculateAWBHWBListHeight(isKeyboardShown: boolean, keyboardHeight: number) {
        if (isKeyboardShown && PlatformHelper.isOrientation_Portrait()) {
            this.awbHWBListHeight = this.awbListHeightWhenKeyboardNotShown - keyboardHeight;
        } else {
            this.awbHWBListHeight = this.awbListHeightWhenKeyboardNotShown;
        }
        this.initRectDataModel();
    }

    private onGenerateAWB() {
        let foundCargoUnKnownAWBDataObject: FoundCargoUnKnownAWBDataObject =
        ManualDIHelper.getInstance().createObject('FoundCargoUnKnownAWBDataObject');
        foundCargoUnKnownAWBDataObject.prepareRequestWithParameters();
        AppMessagePopupHelper.showLoadingAppMessagePopup(AppUIHelper.getInstance().i18nText('unknownawbloadingtext'));
        foundCargoUnKnownAWBDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
        AppMessagePopupHelper.getInstance().hideAppMessagePopup();
        if (_dataObject.serverErrorDataModel != null) {
            AppUIHelper.getInstance().showAlert(AppUIHelper.getInstance().i18nText('error'),
              _dataObject.serverErrorDataModel.errorText, [AppUIHelper.getInstance().i18nText('ok')]);
        } else {
          let fetchUnKnownAWBResponseObject =
            _dataObject.responseObject as FetchUnKnownAWBResponseObject;
          this.navController.push(CreateFoundCargoPage,
            {'airwaybillText': 'AWB',
            'airwaybillPrefix': fetchUnKnownAWBResponseObject.airwaybillsDataModel.airwaybillPrefix,
            'airwaybillSerial': fetchUnKnownAWBResponseObject.airwaybillsDataModel.airwaybillSerial});
        }

      });
    }
    private registerForBarCodeForInternalHardwareScannerForZebraDevices() {
        this.unregisterForBarCodeForInternalHardwareScannerForZebraDevices();

        this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber =
        BarcodeScannerHelper.getInstance()
        .barCodeForInternalHardwareScannerForZebraDevices.subscribe
        ((_barcodeData: string) => {
            this.resetControlsAndData_For_AWB_AutoSuggest();
            this.do_AWB_AutoSuggest(false, _barcodeData);
        });
      }

      private unregisterForBarCodeForInternalHardwareScannerForZebraDevices() {
        if (this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber != null) {
          this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber.unsubscribe();
          this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber = null;
        }
      }
   /* private initAWBFoundCargoDataModel(airwaybillPrefix: string, airwaybillSerial: string): FoundCargoDataModel {
        console.log(airwaybillPrefix + '//' + airwaybillSerial);
        let airwaybillsDataModel = new AirwaybillsDataModel();
        airwaybillsDataModel.airwaybillPrefix = airwaybillPrefix;
        airwaybillsDataModel.airwaybillSerial = airwaybillSerial;
        this.foundCargoDataModel.airwaybill = airwaybillsDataModel;
        return this.foundCargoDataModel;
    } */
}
