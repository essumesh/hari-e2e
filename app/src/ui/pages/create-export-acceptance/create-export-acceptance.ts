import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { BarcodeScannerHelper } from '../../../helpers/BarcodeScannerHelper';

@Component({
  selector: 'champ-page-create-export-acceptance',
  templateUrl: 'create-export-acceptance.html',
})
export class CreateExportAcceptancePage extends BaseUINavigationBarView {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    super();

    this.navigationBarViewModel =
    new NavigationBarViewModel(true,
                           '',
                           true,
                           true,
                           true);
  }

  onScanClicked() {
    BarcodeScannerHelper.getInstance().scan((_barcodeData: string) => {
    }, (errorText: string) => {
      alert(errorText);
    });

  }
}
