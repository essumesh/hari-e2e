import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { FoundCargoOrignDataObject } from '../../../network/dataobjects/FoundCargoOrignDataObject';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { FetchOriginResponseObject } from '../../../network/responseobjects/FetchOriginResponseObject';
import { CarriersDataModel } from '../../../datamodels/CarriersDataModel';
import { PreFetchDataObjectsHelper } from '../../../network/helpers/PreFetchDataObjectsHelper';
import { FoundCargoDataModel } from '../../../datamodels/FoundCargoDataModel';
import { FetchOriginItemDataModel } from '../../../datamodels/FetchOriginItemDataModel';
import { SHCSelectDataModel } from '../../../datamodels/SHCSelectDataModel';
import { PlatformHelper } from '../../../helpers/PlatformHelper';

@Component({
    selector: 'champ-page-select-origin',
    templateUrl: 'found-cargo-select-origin.html',
})
export class SelectOriginPage extends BaseUINavigationBarView {
    fetchOriginResponseObject: FetchOriginResponseObject =
    PreFetchDataObjectsHelper.getInstance().fetchFoundCargoOrignDataObject.responseObject as
    FetchOriginResponseObject;
    uniqueObjectIdOfRadioButtonViewModel: string;
    shcSelectDataModel: SHCSelectDataModel;
    selectOriginFor: string;
    originList: Array<FetchOriginItemDataModel> = [];
    originInput: string;
    airwaybillPrefix: string;
    airwaybillSerial: string;
    constructor(public navController: NavController,
        public navParams: NavParams) {
        super();
        this.shcSelectDataModel = this.navParams.get('shcSelectDataModel');
        this.airwaybillPrefix = this.navParams.get('airwaybillPrefix');
        this.airwaybillSerial = this.navParams.get('airwaybillSerial');
        console.log(this.fetchOriginResponseObject);
        this.navigationBarViewModel =
            new NavigationBarViewModel(true,
                '',
                true,
                true,
                true);
        this.onGetOriginDetails();
    }

    ionViewDidLoad() {
        super.ionViewDidLoad();
        AppUIHelper.getInstance().i18nTextWithCallBack('Select Origin', (i18nTextValue: string) => {
            this.navigationBarViewModel.title = i18nTextValue;
        });
        AppUIHelper.getInstance().i18nTextWithCallBack('selectoriginfor', (i18nTextValue: string) => {
            this.selectOriginFor = i18nTextValue + '  ' +
                this.airwaybillPrefix + '-' + this.airwaybillSerial;
        });

    }

    onOrientationChangedTo_Landscape() {
        super.onOrientationChangedTo_Landscape();

        try {
            if (PlatformHelper.isDevice_A_Tablet() &&
                this.navController.canGoBack())
                  this.navController.pop();
        } catch (e) {
        }
    }

    selectedOrigin(origin: any) {
        console.log(origin);
        this.shcSelectDataModel.selectedOrigin = origin;
        this.navController.pop();
    }
    ionViewWillLeave() {
        super.ionViewWillLeave();
        console.log('pause');
    }
    preSelectOrigin() {

    }
    onGetOriginDetails() {
        this.originList = this.fetchOriginResponseObject.foundCargoStationDataModel.locations;
        this.setSelectedRadioButtonViewModel();
    }
    private onSearchOrigin() {
        this.onGetOriginDetails();
        if (this.originInput != null && this.originInput.trim() !== '') {
            this.originList = this.originList.filter((item) => {
                // return (item.code.toLowerCase().startsWith(this.originInput.toLocaleLowerCase()));
                if (this.originInput.length >= 2) {
                    if (item.code.toLowerCase().includes(this.originInput.toLocaleLowerCase()))
                        return (item.code.toLowerCase().startsWith(this.originInput.toLocaleLowerCase()));
                    else if (item.name.toLowerCase().includes(this.originInput.toLocaleLowerCase())) {
                        return (item.name.toLowerCase().startsWith(this.originInput.toLocaleLowerCase()));
                    } else if (item.country.name.toLowerCase().includes(this.originInput.toLocaleLowerCase())) {
                        return (item.country.name.toLowerCase().startsWith(this.originInput.toLocaleLowerCase()));
                    } else {
                        return '';
                    }
                }
            });
        }
    }

    private setSelectedRadioButtonViewModel() {
        for (let i = 0; i < this.originList.length; i++) {
            if (this.shcSelectDataModel.selectedOrigin.code === this.originList[i].code) {
                this.uniqueObjectIdOfRadioButtonViewModel =
                    this.originList[i].getUniqueObjectId().toString();
                break;
            }
        }
    }
}
