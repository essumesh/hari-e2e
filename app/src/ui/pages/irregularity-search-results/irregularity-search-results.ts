import { Component } from '@angular/core';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavController, PopoverController, NavParams } from 'ionic-angular';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { FetchIrregularityDataObject } from '../../../network/dataobjects/FetchIrregularityDataObject';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { AirwaybillsDataModel } from '../../../datamodels/AirwaybillsDataModel';
import { HouseAirwaybillsDataModel } from '../../../datamodels/HouseAirwaybillsDataModel';
import { AppMessagePopupHelper } from '../../../helpers/AppMessagePopupHelper';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { FetchDamageReportsRequestObject } from '../../../network/requestobjects/FetchDamageReportsRequestObject';
import { IrregularitiesDataModel } from '../../../datamodels/IrregularitiesDataModel';
import { FetchIrregularityReportResponseObject } from
  '../../../network/responseobjects/FetchIrregularityReportResponseObject';
import { RadioButtonsViewModel, RadioButtonViewModel } from '../../viewmodels/RadioButtonsViewModel';
import { FilterRadioButtonPopover } from '../filter-radiobutton-popover/filter-radiobutton-popover';
import { CreateIrregularityPage } from '../create-irregularities/create-irregularities';
import { LocationsDataModel } from '../../../datamodels/LocationsDataModel';
import { AmountUnitDataModel } from '../../../datamodels/AmountUnitDataModel';
import { CarriersDataModel } from '../../../datamodels/CarriersDataModel';

@Component({
  selector: 'champ-page-irregularity-search-results',
  templateUrl: 'irregularity-search-results.html',
})
export class IrregularitySearchResultsPage extends BaseUINavigationBarView {
  fetchIrregularityDataObject: FetchIrregularityDataObject;
  radioButtonsViewModel: RadioButtonsViewModel = new RadioButtonsViewModel();
  private arrayOfSortItems: Array<string> = [];
  private airwaybillsDataModel: AirwaybillsDataModel;
  private houseAirwaybillsDataModel: HouseAirwaybillsDataModel;
  private irregularityReportCount: string;
  private sortSelectedItem: string = AppUIHelper.getInstance().i18nText('date');

  constructor(public navController: NavController,
    public navParams: NavParams,
    public popoverController: PopoverController) {
    super();

    this.airwaybillsDataModel =
      this.navParams.get('airwaybillsDataModel');
    this.houseAirwaybillsDataModel =
      this.navParams.get('houseAirwaybillsDataModel');
    this.navigationBarViewModel =
      new NavigationBarViewModel(false,
        '',
        true,
        true,
        true);
  }

  ionViewDidEnter() {
    super.ionViewDidEnter();
    this.refreshFetchIrregularityDataObject();
  }

  private prepare_ArrayOfSortItems() {
    this.arrayOfSortItems = [];

    this.arrayOfSortItems.push(AppUIHelper.getInstance().i18nText('date'));
    this.arrayOfSortItems.push(AppUIHelper.getInstance().i18nText('station'));

    this.radioButtonsViewModel = new RadioButtonsViewModel();
    for (let i = 0; i < this.arrayOfSortItems.length; i++) {
      this.radioButtonsViewModel.addRadioButtonViewModel(this.arrayOfSortItems[i], i === 0, null);
    }

    this.sortItemsBasedOn(this.radioButtonsViewModel.radioButtonViewModels[0].name);
  }
  private presentPopover(event: any) {
    let popover = this.popoverController.create(FilterRadioButtonPopover, {
                                                'title': AppUIHelper.getInstance().i18nText('sortby'),
                                                'radioButtons': this.radioButtonsViewModel,
                                                'buttonText': null,
                                                'radioButtonViewModelTapEvent':
                                                  (_radioButtonViewModel: RadioButtonViewModel) => {
                                                    this.sortItemsBasedOn(_radioButtonViewModel.name);
                                                  }},
                                                );
    popover.present({
      ev: event,
    });

  }
  private sortItemsBasedOn(sortBy: string) {
    let fetchIrregularityReportResponseObject: FetchIrregularityReportResponseObject =
      this.fetchIrregularityDataObject.responseObject as FetchIrregularityReportResponseObject;

    if (fetchIrregularityReportResponseObject != null) {
      if (sortBy === AppUIHelper.getInstance().i18nText('date')) {
        this.sortSelectedItem = AppUIHelper.getInstance().i18nText('date');
        fetchIrregularityReportResponseObject
          .irregularityReportListDataModel
          .irregularityList.sort((irregularitiesDataModel1: IrregularitiesDataModel,
            irregularitiesDataModel2: IrregularitiesDataModel) => {
            if (irregularitiesDataModel1.createdOn
              > irregularitiesDataModel2.createdOn) return -1;
            else if (irregularitiesDataModel1.createdOn
              < irregularitiesDataModel2.createdOn) return 1;
            else return 0;
          });
      } else if (sortBy === AppUIHelper.getInstance().i18nText('station')) {
        this.sortSelectedItem = AppUIHelper.getInstance().i18nText('station');
        fetchIrregularityReportResponseObject
          .irregularityReportListDataModel
          .irregularityList.sort((irregularitiesDataModel1: IrregularitiesDataModel,
            irregularitiesDataModel2: IrregularitiesDataModel) => {
            if (irregularitiesDataModel1.reportingLocation.name
              < irregularitiesDataModel2.reportingLocation.name) return -1;
            else if (irregularitiesDataModel1.reportingLocation.name
              > irregularitiesDataModel2.reportingLocation.name) return 1;
            else return 0;
          });
      }
    }
  }

  private irregularityReportsDataModels(): Array<IrregularitiesDataModel> {
    if (this.fetchIrregularityDataObject != null
      && this.fetchIrregularityDataObject.responseObject != null) {
      let fetchIrregularityReportResponseObject: FetchIrregularityReportResponseObject =
        this.fetchIrregularityDataObject.responseObject as FetchIrregularityReportResponseObject;
      console.log(fetchIrregularityReportResponseObject);
      return fetchIrregularityReportResponseObject
        .irregularityReportListDataModel
        .irregularityList;
    }
    return [];
  }
  private tapEvent_On_IrregularitiesDataModel(_irregularitiesDataModel: IrregularitiesDataModel) {

   /* this.navController.push(CreateIrregularityPage,
                            {'irregularitiesDataModel': _irregularitiesDataModel}); */
  }

  private createNewReportButtonClicked() {
   // let _irregularitiesDataModel: IrregularitiesDataModel = new IrregularitiesDataModel();
    let fetchDamageReportsRequestObject: FetchDamageReportsRequestObject =
    this.fetchIrregularityDataObject.requestObject as FetchDamageReportsRequestObject;

    let _irregularitiesDataModel: IrregularitiesDataModel = new IrregularitiesDataModel();
    if (this.airwaybillsDataModel != null) {
      console.log(this.airwaybillsDataModel);
      _irregularitiesDataModel.airwaybill = this.airwaybillsDataModel;
      // _irregularitiesDataModel.reportingShipmentType = 'airwaybill';

      _irregularitiesDataModel.reportingLocation = new LocationsDataModel();
      _irregularitiesDataModel.reportingLocation.code = 'FRA';
      _irregularitiesDataModel.reportingLocation.type = 'Airport';
      _irregularitiesDataModel.reportingLocation.name = 'Frankfurt am Main Flughafen';
    } else if (this.houseAirwaybillsDataModel != null) {
      _irregularitiesDataModel.houseAirwaybill = this.houseAirwaybillsDataModel;
      // _irregularitiesDataModel.reportingShipmentType = 'houseairwaybill';

      _irregularitiesDataModel.reportingLocation = new LocationsDataModel();
      _irregularitiesDataModel.reportingLocation.code = 'FRA';
      _irregularitiesDataModel.reportingLocation.type = 'Airport';
      _irregularitiesDataModel.reportingLocation.name = 'Frankfurt am Main Flughafen';
    }

    let amountUnitDataModel = new AmountUnitDataModel();
    _irregularitiesDataModel.weight = amountUnitDataModel;
    let carriersDataModel = new CarriersDataModel();
    _irregularitiesDataModel.irregularityDetails = carriersDataModel;
    console.log(_irregularitiesDataModel);
    this.navController.push(CreateIrregularityPage,
                           {'irregularitiesDataModel': _irregularitiesDataModel});
  }

  private refreshFetchIrregularityDataObject() {
    this.fetchIrregularityDataObject = null;
    this.arrayOfSortItems = [];
    this.irregularityReportCount = '0';
    this.fetchIrregularityDataObject =
      ManualDIHelper.getInstance().createObject('FetchIrregularityDataObject');
    if (this.airwaybillsDataModel != null) {
      this.fetchIrregularityDataObject.
        prepareRequestWithParameters(this.airwaybillsDataModel.airwaybillPrefix,
        this.airwaybillsDataModel.airwaybillSerial,
        null,
        null,
        null,
        null);
    } else {
      this.fetchIrregularityDataObject.
        prepareRequestWithParameters(this.houseAirwaybillsDataModel.masterAirwaybill.airwaybillPrefix,
        this.houseAirwaybillsDataModel.masterAirwaybill.airwaybillSerial,
        this.houseAirwaybillsDataModel.houseAirwaybillNumber,
        null,
        null,
        null);
    }
    AppMessagePopupHelper.showLoadingAppMessagePopup(AppUIHelper.getInstance().i18nText('loading'));
    this.fetchIrregularityDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
      AppMessagePopupHelper.getInstance().hideAppMessagePopup();
      console.log(_dataObject.serverErrorDataModel);
      if (_dataObject.serverErrorDataModel != null) {
        console.log(_dataObject.serverErrorDataModel.errorText);
        AppUIHelper.getInstance().showAlert(
          AppUIHelper.getInstance().i18nText('error'),
          _dataObject.serverErrorDataModel.errorText,
          [
            {
              text: AppUIHelper.getInstance().i18nText('retry'),
              handler: () => {
                this.refreshFetchIrregularityDataObject();
              },
            },
            AppUIHelper.getInstance().i18nText('cancel'),
          ]);
      } else {
        this.irregularityReportCount =
          AppUIHelper.prependZeroToThisNumberIfRequired(this.irregularityReportsDataModels().length);
        this.navigationBarViewModel.title = this.navigationBarTitle();
        this.prepare_ArrayOfSortItems();
      }
    });
  }

  private navigationBarTitle() {
    let fetchDamageReportsRequestObject: FetchDamageReportsRequestObject =
      this.fetchIrregularityDataObject.requestObject as FetchDamageReportsRequestObject;
    return fetchDamageReportsRequestObject.
      houseAirwaybillNumber == null
      ? AppUIHelper.getInstance().i18nText('awb')
      + ' '
      + fetchDamageReportsRequestObject.airwaybillPrefix
      + '-'
      + fetchDamageReportsRequestObject.airwaybillSerial
      : AppUIHelper.getInstance().i18nText('hwb')
      + ' '
      + fetchDamageReportsRequestObject.houseAirwaybillNumber;
  }
}
