import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';
import { FilterCheckboxesPopoverViewModel } from '../../viewmodels/FilterCheckboxesPopoverViewModel';
import { CheckBoxesViewModel, CheckBoxViewModel } from '../../viewmodels/CheckBoxesViewModel';
import { BaseUIPageView } from '../../baseuiviews/forpages/BaseUIPageView';

@Component({
  selector: 'champ-filter-checkbox-popover',
  templateUrl: 'filter-checkbox-popover.html',
})
export class FilterCheckboxPopover extends BaseUIPageView {
  filterCheckboxesPopoverViewModel: FilterCheckboxesPopoverViewModel;
  private checkBoxViewModelTapEvent: (_checkBoxViewModel: CheckBoxViewModel) => void;

  constructor(public viewController: ViewController, public navParams: NavParams) {
    super();

    this.populateDataInFilterCheckboxViewModel(
      this.navParams.get('title'),
      this.navParams.get('checkboxes'),
      this.navParams.get('buttonText')),
      this.checkBoxViewModelTapEvent = this.navParams.get('checkBoxViewModelTapEvent');
  }

  populateDataInFilterCheckboxViewModel(title: string, checkBoxes: CheckBoxesViewModel, buttonText: string) {
    this.filterCheckboxesPopoverViewModel = new FilterCheckboxesPopoverViewModel(title, checkBoxes, buttonText);
  }

  buttonClicked() {
    this.viewController.dismiss();
  }

  tapEvent_On_CheckBoxesViewModel(_checkBoxViewModel: CheckBoxViewModel) {
    if (this.checkBoxViewModelTapEvent != null) {
      this.checkBoxViewModelTapEvent(_checkBoxViewModel);
    }
  }
}
