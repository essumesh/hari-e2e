import { Component, NgZone, ViewChild, AfterViewInit, QueryList, ViewChildren } from '@angular/core';
import { NavController, NavParams, Nav } from 'ionic-angular';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { IrregularityDetailsPage } from '../irregularities-detail/irregularities-detail';
import { IrregularityApiDataObject } from '../../../network/dataobjects/IrregularityApiDataObject';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { AppMessagePopupHelper } from '../../../helpers/AppMessagePopupHelper';
import { AppMessagePopupViewModel, AppMessagePopupViewModelType } from '../../viewmodels/AppMessagePopupViewModel';
import { IrregularityApiDataModel } from '../../../datamodels/IrregularityApiDataModel';
import { LocationsDataModel } from '../../../datamodels/LocationsDataModel';
import { AmountUnitDataModel } from '../../../datamodels/AmountUnitDataModel';
import { IrregularitiesDataModel } from '../../../datamodels/IrregularitiesDataModel';
import { PlatformHelper } from '../../../helpers/PlatformHelper';
import { ScreenOrientationHelper, ScreenOrientationType } from '../../../helpers/ScreenOrientationHelper';
import { RectDataModel } from '../../../datamodels/RectDataModel';
import { RadioButtonsViewModel } from '../../viewmodels/RadioButtonsViewModel';
import { FetchIrregularityResponseObject } from '../../../network/responseobjects/FetchIrregularityResponseObject';

@Component({
    selector: 'champ-page-create-irregularity',
    templateUrl: 'create-irregularities.html',
})
export class CreateIrregularityPage extends BaseUINavigationBarView implements AfterViewInit {
    irregularityApiDataModel: IrregularityApiDataModel;
    irregularitiesDataModel: IrregularitiesDataModel;
    @ViewChildren('splitPaneDetailComponentNav', {read: Nav}) splitPaneMasterDetailNavs: QueryList<Nav>;
    @ViewChild('splitPaneDetailComponentNav') splitPaneDetailComponentNav: Nav;
    private toShowSplitPaneInTabletLandScapeOrientation: boolean = false;
    private fetchIrregularityResponseObject: FetchIrregularityResponseObject;
    private irregularityRadioButtonsViewModel: RadioButtonsViewModel;
    private fakeIrregularityRadioButtonsViewModel: RadioButtonsViewModel;
    private rectDataModel: RectDataModel =
        new RectDataModel(0, 0, 0, 0);
    private toShowEnabledSaveButton: boolean;
    private toShowAWBPiecesError: boolean;
    // private irregularityApiDataModel: IrregularityApiDataModel;
    constructor(public navController: NavController,
        public navParams: NavParams,
        private ngZone: NgZone) {
        super();
        this.irregularitiesDataModel = this.navParams.get('irregularitiesDataModel');
        console.log(this.irregularitiesDataModel);
        this.navigationBarViewModel =
            new NavigationBarViewModel(true,
                '',
                true,
                true,
                true);
    }

    ionViewDidLoad() {
        super.ionViewDidLoad();
        this.irregularitiesDataModel.weight.unit = 'KG';
        this.irregularitiesDataModel.houseAirwaybill == null ?
          this.irregularitiesDataModel.pieces = parseInt(this.irregularitiesDataModel.airwaybill.pieces, 10) :
          this.irregularitiesDataModel.pieces =
          parseInt(this.irregularitiesDataModel.houseAirwaybill.masterAirwaybill.pieces, 10);

        let title = '';
        this.irregularitiesDataModel.houseAirwaybill == null ?
            title = 'AWB  ' + this.irregularitiesDataModel.airwaybill.airwaybillPrefix
            + '-' + this.irregularitiesDataModel.airwaybill.airwaybillSerial
            : title = 'HWB  ' + this.irregularitiesDataModel.houseAirwaybill.houseAirwaybillNumber;

        AppUIHelper.getInstance().i18nTextWithCallBack(title, (i18nTextValue: string) => {
            this.navigationBarViewModel.title = i18nTextValue;
        });
    }

    ngAfterViewInit() {
        this.splitPaneMasterDetailNavs.changes.subscribe((_comps: QueryList <Nav>) => {
            this.workOn_IsDeviceInLandScapeOrientationFlag();
          });
    }

    ionViewWillEnter() {
        super.ionViewWillEnter();
        this.onIrregularityCodeChanged();
    }

    ionViewDidEnter() {
        super.ionViewDidEnter();
        this.workOn_IsDeviceInLandScapeOrientationFlag();
    }

    onOrientationChangedTo_Portrait() {
        super.onOrientationChangedTo_Portrait();
        this.workOn_IsDeviceInLandScapeOrientationFlag();
    }

    onOrientationChangedTo_Landscape() {
        super.onOrientationChangedTo_Landscape();
        this.workOn_IsDeviceInLandScapeOrientationFlag();
    }

    onSelectIrregularityDetails() {
        this.navController.push(IrregularityDetailsPage,
            { 'irregularitiesDataModel': this.irregularitiesDataModel });
    }

    onSaveClicked() {
        this.prepareParameters();
        console.log(this.irregularityApiDataModel);
        let irregularityApiDataObject: IrregularityApiDataObject =
            ManualDIHelper.getInstance().createObject('IrregularityApiDataObject');
        irregularityApiDataObject.prepareRequestWithParameters(this.irregularityApiDataModel);
        AppMessagePopupHelper.showLoadingAppMessagePopup(AppUIHelper.getInstance().i18nText('saveinprogress'));
        irregularityApiDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
            AppMessagePopupHelper.getInstance().hideAppMessagePopup();
            if (_dataObject.serverErrorDataModel != null) {
                if (_dataObject.serverErrorDataModel.errorText === 'success') {
                    this.leaveComponent();
                    AppMessagePopupHelper.getInstance().showAppMessagePopup(
                        new AppMessagePopupViewModel(
                            AppUIHelper.getInstance().i18nText('savedsuccessfully'),
                            'assets/images/green-tick-small.svg', AppMessagePopupViewModelType.MessagePopup), 4);
                } else {
                    AppUIHelper.getInstance().showAlert(
                        AppUIHelper.getInstance().i18nText('error'),
                        _dataObject.serverErrorDataModel.errorText,
                        [AppUIHelper.getInstance().i18nText('ok')]);
                }
            }
        });
    }

    prepareParameters() {
        this.irregularityApiDataModel = new IrregularityApiDataModel();
        this.irregularityApiDataModel.pieces = this.irregularitiesDataModel.pieces;
        if (this.irregularitiesDataModel.airwaybill != null) {
            this.irregularityApiDataModel.airwaybillId = '' + this.irregularitiesDataModel.airwaybill.id;
        } else {
            this.irregularityApiDataModel.airwaybillId = '' + this.irregularitiesDataModel.houseAirwaybill.id;
        }
        this.irregularityApiDataModel.irregularityCode = this.irregularitiesDataModel.irregularityDetails.code;
        let locationsDataModel = new LocationsDataModel();
        locationsDataModel.code = this.irregularitiesDataModel.reportingLocation.code;
        locationsDataModel.type = this.irregularitiesDataModel.reportingLocation.type;
        this.irregularityApiDataModel.reportingLocation = locationsDataModel;
        let amountUnitDataModel = new AmountUnitDataModel();
        amountUnitDataModel.amount = this.irregularitiesDataModel.weight.amount;
        amountUnitDataModel.unit = this.irregularitiesDataModel.weight.unit;
        this.irregularityApiDataModel.weight = amountUnitDataModel;
    }

    onIrregularityCodeChanged() {
        console.log('onIrregularityCodeChanged');
        let awbPieces: number;
        this.irregularitiesDataModel.houseAirwaybill == null ?
          awbPieces = parseInt(this.irregularitiesDataModel.airwaybill.pieces, 10) :
          awbPieces = parseInt(this.irregularitiesDataModel.houseAirwaybill.masterAirwaybill.pieces, 10);
        if (this.irregularitiesDataModel.irregularityDetails.code != null &&
            this.irregularitiesDataModel.irregularityDetails.code !== '' &&
            this.irregularitiesDataModel.pieces <= awbPieces && this.irregularitiesDataModel.pieces > 0) {
            return true ;
        } else {
            return false;
        }

    }

    onIrregularityPiecesValueChanged() {
        let awbPieces: number;
        this.irregularitiesDataModel.houseAirwaybill == null ?
          awbPieces = parseInt(this.irregularitiesDataModel.airwaybill.pieces, 10) :
          awbPieces = parseInt(this.irregularitiesDataModel.houseAirwaybill.masterAirwaybill.pieces, 10);
        this.irregularitiesDataModel.pieces > awbPieces ?
          this.toShowAWBPiecesError = true : this.toShowAWBPiecesError = false;
        this.onIrregularityCodeChanged();
        }

    private prepareIrregularityRadioButtonsViewModel() {
        this.irregularityRadioButtonsViewModel = new RadioButtonsViewModel();

        for (let lookupDataModel of
            this.fetchIrregularityResponseObject.irregularityQualifierDataModel.irregularityDetailList) {
            this.irregularityRadioButtonsViewModel.addRadioButtonViewModel(
                lookupDataModel.code + ' - ' + lookupDataModel.name,
                false,
                lookupDataModel,
            );
        }
    }

    private workOn_IsDeviceInLandScapeOrientationFlag() {
        if (PlatformHelper.isDevice_A_Tablet()) {
            const isDeviceInLandScapeOrientation =
                ScreenOrientationHelper.currentScreenOrientation() ===
                ScreenOrientationType.LANDSCAPE;

            const calc = (() => {
                if (document.getElementById('irregularityLabel') != null) {
                    const top =
                        (document.getElementById('irregularityLabel').getBoundingClientRect().bottom +
                            document.getElementById('navigationBarHeader').getBoundingClientRect().height) +
                        40;

                    this.rectDataModel.height = PlatformHelper.screenHeight() - top;
                }
            });

            if (isDeviceInLandScapeOrientation) {
                setTimeout(() => {
                    if (this.splitPaneDetailComponentNav != null &&
                    this.splitPaneDetailComponentNav._views.length <= 0) {
                        this.splitPaneDetailComponentNav.push
                        (IrregularityDetailsPage,
                            { 'irregularitiesDataModel': this.irregularitiesDataModel,
                           'toRenderUnderScroll': true});
                        }
                    calc();
                    this.ngZone.run(() => {
                        this.toShowSplitPaneInTabletLandScapeOrientation = true;
                  });
                }, 200);
            } else this.toShowSplitPaneInTabletLandScapeOrientation = false;
        }
    }

    private leaveComponent() {
        setTimeout(() => {
            this.navController.pop();
        }, 4 * 1000);
    }
}
