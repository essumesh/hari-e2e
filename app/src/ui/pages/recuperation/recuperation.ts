import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams, ViewController } from 'ionic-angular';
import { RemarksPage } from '../remarks-and-boxes/remarks-and-boxes';
import { DamageDiscoveryPage } from '../damage-discovery/damage-discovery';
import { LookupDataModel } from '../../../datamodels/LookupDataModel';
import { LookupsDataModel } from '../../../datamodels/LookupsDataModel';
import { PreFetchDataObjectsHelper } from '../../../network/helpers/PreFetchDataObjectsHelper';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { FetchAWBDamageQualifiersResponseObject }
from '../../../network/responseobjects/FetchAWBDamageQualifiersResponseObject';
import { DamageReportsDataModel } from '../../../datamodels/DamageReportsDataModel';
import { RadioButtonsViewModel, RadioButtonViewModel } from '../../viewmodels/RadioButtonsViewModel';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { PlatformHelper } from '../../../helpers/PlatformHelper';
import { ScreenOrientationHelper, ScreenOrientationType } from '../../../helpers/ScreenOrientationHelper';

@Component({
  selector: 'champ-page-recuperation',
  templateUrl: 'recuperation.html',
})
export class RecuperationPage extends BaseUINavigationBarView {
  fetchAWBDamageQualifiersResponseObject: FetchAWBDamageQualifiersResponseObject =
  PreFetchDataObjectsHelper.getInstance().fetchAWBDamageQualifiersDataObject.responseObject  as
  FetchAWBDamageQualifiersResponseObject;
  private recuperageRadioButtonsViewModel: RadioButtonsViewModel;
  private damageReportsDataModel: DamageReportsDataModel;
  private isDeviceATablet: boolean = false;
  private isDeviceInLandScapeOrientation: boolean = false;

  constructor(private navParams: NavParams,
              private navController: NavController,
              private viewCtrl: ViewController) {
    super();

    this.damageReportsDataModel =
    this.navParams.get('damageReportsDataModel');
    this.navigationBarViewModel =
        new NavigationBarViewModel(true,
                               AppUIHelper.getInstance().i18nText('recuperation'),
                               true,
                               true,
                               true);
    if (PlatformHelper.isDevice_A_Tablet() &&
        ScreenOrientationHelper.currentScreenOrientation() === ScreenOrientationType.LANDSCAPE) {
        this.isDeviceATablet = true;
        this.isDeviceInLandScapeOrientation = true;
    }

    this.prepareRecuperageRadioButtonsViewModel();
  }

  onOrientationChangedTo_Landscape() {
    super.onOrientationChangedTo_Landscape();

    try {
        if (PlatformHelper.isDevice_A_Tablet() &&
            this.navController.canGoBack())
              this.navController.pop();
    } catch (e) {
    }
  }

  private prepareRecuperageRadioButtonsViewModel() {
    this.recuperageRadioButtonsViewModel = new RadioButtonsViewModel();

    for (let lookupDataModel of
        this.fetchAWBDamageQualifiersResponseObject.damageReferencesDataModel.recuperation.qualifierType) {
        this.recuperageRadioButtonsViewModel.addRadioButtonViewModel(
            lookupDataModel.name,
            this.damageReportsDataModel.recuperage != null &&
            this.damageReportsDataModel.recuperage === lookupDataModel.name,
            lookupDataModel,
        );
    }
  }

private tapEvent_On_RecuperageRadioButtonsViewModel(_radioButtonViewModel: RadioButtonViewModel) {
    this.damageReportsDataModel.recuperage = null;
    if (_radioButtonViewModel.isSelected) {
        this.damageReportsDataModel.recuperage =
        (_radioButtonViewModel.extras as LookupDataModel).name;
    }
    this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds')();
}

  private closePage() {
    this.navController.push(DamageDiscoveryPage,
      {'damageReportsDataModel': this.damageReportsDataModel,
       'onSaveClickedOnOneOfTheChilds': this.navParams.get('onSaveClickedOnOneOfTheChilds'),
       'someUIElementsHaveChangedItsStateOnOneOfTheChilds':
       this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds'),
       'doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds':
       this.navParams.get('doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds'),
       'doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds':
       this.navParams.get('doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds'),
       'onInputFocusedInAnyOfTheChilds': this.navParams.get('onInputFocusedInAnyOfTheChilds'),
       'onPrintClickedOnOneOfTheChilds': this.navParams.get('onPrintClickedOnOneOfTheChilds'),
       'onEmailClickedOnOneOfTheChilds': this.navParams.get('onEmailClickedOnOneOfTheChilds')}).then(() => {
      this.viewCtrl.dismiss();
    });
  }

  private nextPage() {
    this.navController.push(RemarksPage,
      {'damageReportsDataModel': this.damageReportsDataModel,
       'onSaveClickedOnOneOfTheChilds': this.navParams.get('onSaveClickedOnOneOfTheChilds'),
       'someUIElementsHaveChangedItsStateOnOneOfTheChilds':
       this.navParams.get('someUIElementsHaveChangedItsStateOnOneOfTheChilds'),
       'doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds':
       this.navParams.get('doACheckOnWhetherToShowPrintEmailButtonOnOneOfTheChilds'),
       'doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds':
       this.navParams.get('doACheckOnWhetherToShowSaveButtonOnOneOfTheChilds'),
       'onInputFocusedInAnyOfTheChilds': this.navParams.get('onInputFocusedInAnyOfTheChilds'),
       'onPrintClickedOnOneOfTheChilds': this.navParams.get('onPrintClickedOnOneOfTheChilds'),
       'onEmailClickedOnOneOfTheChilds': this.navParams.get('onEmailClickedOnOneOfTheChilds')}).then(() => {
      this.viewCtrl.dismiss();
    });
  }

  private swiped(event: any) {
    if (event.direction === 2) {
        this.nextPage();
    } else if (event.direction === 4) {
        this.closePage();
    }
  }

  private contentTitle(): string {
    if (this.damageReportsDataModel.airwaybill != null) {
      return AppUIHelper.getInstance().i18nText('damagereportfor')
             + ' '
             + this.damageReportsDataModel.airwaybill.airwaybillPrefix
             + '-'
             + this.damageReportsDataModel.airwaybill.airwaybillSerial;
    } else if (this.damageReportsDataModel.houseAirwaybill != null) {
            return AppUIHelper.getInstance().i18nText('damagereportfor')
                  + ' '
                  + this.damageReportsDataModel.houseAirwaybill.masterAirwaybill.airwaybillPrefix
                  + '-'
                  + this.damageReportsDataModel.houseAirwaybill.masterAirwaybill.airwaybillSerial;
    }
  }
}
