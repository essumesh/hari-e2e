import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ViewController } from 'ionic-angular';
import { FetchDamageReportsDataObject } from '../../../network/dataobjects/FetchDamageReportsDataObject';
import { FetchDamageReportsRequestObject } from '../../../network/requestobjects/FetchDamageReportsRequestObject';
import { FetchDamageReportsResponseObject } from '../../../network/responseobjects/FetchDamageReportsResponseObject';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { DamageReportsDataModel } from '../../../datamodels/DamageReportsDataModel';
import { LocationsDataModel } from '../../../datamodels/LocationsDataModel';
import { CreateDamageReportPage } from '../create-damage-report/create-damage-report';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { AirwaybillsDataModel } from '../../../datamodels/AirwaybillsDataModel';
import { HouseAirwaybillsDataModel } from '../../../datamodels/HouseAirwaybillsDataModel';
import { FetchAWBDataObject } from '../../../network/dataobjects/FetchAWBDataObject';
import { FetchAWBResponseObject } from '../../../network/responseobjects/FetchAWBResponseObject';
import { FetchHWBDataObject } from '../../../network/dataobjects/FetchHWBDataObject';
import { FetchHWBResponseObject } from '../../../network/responseobjects/FetchHWBResponseObject';
import { FilterRadioButtonPopover } from
'../filter-radiobutton-popover/filter-radiobutton-popover';
import { RadioButtonsViewModel, RadioButtonViewModel } from '../../viewmodels/RadioButtonsViewModel';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { AppMessagePopupHelper } from '../../../helpers/AppMessagePopupHelper';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';

@Component({
  selector: 'champ-page-damage-report-search-results',
  templateUrl: 'damage-report-search-results.html',
})
export class DamageReportSearchResultsPage extends BaseUINavigationBarView {
  fetchDamageReportsDataObject: FetchDamageReportsDataObject;
  radioButtonsViewModel: RadioButtonsViewModel = new RadioButtonsViewModel();
  private sortSelectedItem: string = AppUIHelper.getInstance().i18nText('date');
  private airwaybillsDataModel: AirwaybillsDataModel;
  private houseAirwaybillsDataModel: HouseAirwaybillsDataModel;
  private damageReportCount: string;
  private arrayOfSortItems: Array<string> = [];

  constructor(public navController: NavController,
    public navParams: NavParams,
    public popoverController: PopoverController) {
      super();

      this.airwaybillsDataModel =
      this.navParams.get('airwaybillsDataModel');
      this.houseAirwaybillsDataModel =
      this.navParams.get('houseAirwaybillsDataModel');

      this.navigationBarViewModel =
      new NavigationBarViewModel(false,
                          '',
                          true,
                          true,
                          true);
  }

  ionViewDidEnter() {
    super.ionViewDidEnter();

    this.refreshFetchDamageReportsDataObject();
  }

  prepare_ArrayOfSortItems() {
    this.arrayOfSortItems = [];

    this.arrayOfSortItems.push(AppUIHelper.getInstance().i18nText('date'));
    this.arrayOfSortItems.push(AppUIHelper.getInstance().i18nText('station'));

    this.radioButtonsViewModel = new RadioButtonsViewModel();
    for (let i = 0; i < this.arrayOfSortItems.length; i++) {
      this.radioButtonsViewModel.addRadioButtonViewModel(this.arrayOfSortItems[i], i === 0, null);
    }

    this.sortItemsBasedOn(this.radioButtonsViewModel.radioButtonViewModels[0].name);
  }

  presentPopover(event: any) {
    let popover = this.popoverController.create(FilterRadioButtonPopover, {
                                                'title': AppUIHelper.getInstance().i18nText('sortby'),
                                                'radioButtons': this.radioButtonsViewModel,
                                                'buttonText': null,
                                                'radioButtonViewModelTapEvent':
                                                  (_radioButtonViewModel: RadioButtonViewModel) => {
                                                    this.sortItemsBasedOn(_radioButtonViewModel.name);
                                                  }},
                                                );
    popover.present({
      ev: event,
    });

  }

  sortItemsBasedOn(sortBy: string) {
      let fetchDamageReportsResponseObject: FetchDamageReportsResponseObject =
      this.fetchDamageReportsDataObject.responseObject as FetchDamageReportsResponseObject;

      if (fetchDamageReportsResponseObject != null) {
        if (sortBy === AppUIHelper.getInstance().i18nText('date')) {
          this.sortSelectedItem = AppUIHelper.getInstance().i18nText('date');
          fetchDamageReportsResponseObject
          .pagedResourcesdamagereportsEmbeddedDataModel
          .damageReports.sort((damageReportsDataModel1: DamageReportsDataModel,
                               damageReportsDataModel2: DamageReportsDataModel) => {
            if (damageReportsDataModel1.reportedOn
              > damageReportsDataModel2.reportedOn) return -1;
            else if (damageReportsDataModel1.reportedOn
              < damageReportsDataModel2.reportedOn) return 1;
            else return 0;
          });
        } else if (sortBy === AppUIHelper.getInstance().i18nText('station')) {
          this.sortSelectedItem = AppUIHelper.getInstance().i18nText('station');
          fetchDamageReportsResponseObject
          .pagedResourcesdamagereportsEmbeddedDataModel
          .damageReports.sort((damageReportsDataModel1: DamageReportsDataModel,
                               damageReportsDataModel2: DamageReportsDataModel) => {
            if (damageReportsDataModel1.reportingLocation.name
              < damageReportsDataModel2.reportingLocation.name) return -1;
            else if (damageReportsDataModel1.reportingLocation.name
              > damageReportsDataModel2.reportingLocation.name) return 1;
            else return 0;
          });
        }
      }
  }

  damageReportsDataModels(): Array<DamageReportsDataModel> {
    if (this.fetchDamageReportsDataObject != null
        && this.fetchDamageReportsDataObject.responseObject != null) {
      let fetchDamageReportsResponseObject: FetchDamageReportsResponseObject =
      this.fetchDamageReportsDataObject.responseObject as FetchDamageReportsResponseObject;

      return fetchDamageReportsResponseObject
             .pagedResourcesdamagereportsEmbeddedDataModel
             .damageReports;
    }
    return [];
  }

  tapEvent_On_DamageReportsDataModel(_damageReportsDataModel: DamageReportsDataModel) {
    this.navController.push(CreateDamageReportPage,
                            {'wasDamageReportsDataModelNewlyCreated': false,
                            'damageReportsDataModel': _damageReportsDataModel});
  }

  createNewReportButtonClicked() {
    let fetchDamageReportsRequestObject: FetchDamageReportsRequestObject =
    this.fetchDamageReportsDataObject.requestObject as FetchDamageReportsRequestObject;

    let damageReportsDataModel: DamageReportsDataModel = new DamageReportsDataModel();
    if (this.airwaybillsDataModel != null) {
      damageReportsDataModel.airwaybill = this.airwaybillsDataModel;
      damageReportsDataModel.reportingShipmentType = 'airwaybill';

      damageReportsDataModel.reportingLocation = new LocationsDataModel();
      damageReportsDataModel.reportingLocation.code = 'FRA';
      damageReportsDataModel.reportingLocation.type = 'Airport';
      damageReportsDataModel.reportingLocation.name = 'Frankfurt am Main Flughafen';
    } else if (this.houseAirwaybillsDataModel != null) {
      damageReportsDataModel.houseAirwaybill = this.houseAirwaybillsDataModel;
      damageReportsDataModel.reportingShipmentType = 'houseairwaybill';

      damageReportsDataModel.reportingLocation = new LocationsDataModel();
      damageReportsDataModel.reportingLocation.code = 'FRA';
      damageReportsDataModel.reportingLocation.type = 'Airport';
      damageReportsDataModel.reportingLocation.name = 'Frankfurt am Main Flughafen';
    }

    this.navController.push(CreateDamageReportPage,
                            {'wasDamageReportsDataModelNewlyCreated': true,
                            'damageReportsDataModel': damageReportsDataModel});
  }

  private refreshFetchDamageReportsDataObject() {
    this.fetchDamageReportsDataObject = null;
    this.damageReportCount = '0';
    this.arrayOfSortItems = [];

    this.fetchDamageReportsDataObject =
    ManualDIHelper.getInstance().createObject('FetchDamageReportsDataObject');
    if (this.airwaybillsDataModel != null) {
      this.fetchDamageReportsDataObject.
      prepareRequestWithParameters(this.airwaybillsDataModel.airwaybillPrefix,
                                   this.airwaybillsDataModel.airwaybillSerial,
                                   null,
                                   null,
                                   25,
                                   null);
    } else {
      this.fetchDamageReportsDataObject.
      prepareRequestWithParameters(this.houseAirwaybillsDataModel.masterAirwaybill.airwaybillPrefix,
                                   this.houseAirwaybillsDataModel.masterAirwaybill.airwaybillSerial,
                                   this.houseAirwaybillsDataModel.houseAirwaybillNumber,
                                   null,
                                   25,
                                   null);
    }
    AppMessagePopupHelper.showLoadingAppMessagePopup(AppUIHelper.getInstance().i18nText('loading'));
    this.fetchDamageReportsDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
      AppMessagePopupHelper.getInstance().hideAppMessagePopup();
      if (_dataObject.serverErrorDataModel != null) {
        AppUIHelper.getInstance().showAlert(
          AppUIHelper.getInstance().i18nText('error'),
          _dataObject.serverErrorDataModel.errorText,
        [
          {
            text: AppUIHelper.getInstance().i18nText('retry'),
            handler: () => {
              this.refreshFetchDamageReportsDataObject();
            },
          },
          AppUIHelper.getInstance().i18nText('cancel'),
        ]);
      } else {
        this.damageReportCount = AppUIHelper.prependZeroToThisNumberIfRequired(this.damageReportsDataModels().length);
        this.navigationBarViewModel.title = this.navigationBarTitle();
        this.prepare_ArrayOfSortItems();
      }
    });
  }

  private navigationBarTitle() {
    let fetchDamageReportsRequestObject: FetchDamageReportsRequestObject =
    this.fetchDamageReportsDataObject.requestObject as FetchDamageReportsRequestObject;
    return fetchDamageReportsRequestObject.
            houseAirwaybillNumber == null
            ? AppUIHelper.getInstance().i18nText('awb')
              + ' '
              + fetchDamageReportsRequestObject.airwaybillPrefix
              + '-'
              + fetchDamageReportsRequestObject.airwaybillSerial
            : AppUIHelper.getInstance().i18nText('hwb')
              + ' - '
              + fetchDamageReportsRequestObject.houseAirwaybillNumber;
  }
}
