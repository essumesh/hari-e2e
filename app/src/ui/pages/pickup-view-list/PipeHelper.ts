import { Pipe, PipeTransform } from '@angular/core';
import { PickupViewListPage } from './pickup-view-list';

@Pipe({ name: 'champKeys' })
export class ChampKeys implements PipeTransform {
  transform(value: any): any {
    let keys = [];
    let temp = [];
    // tslint:disable-next-line:forin
    for (let key in value) {
      if (key === PickupViewListPage.itemssortkey)
        keys.push({ key: key, value: value[key] });
      else {
        temp.push({ key: key, value: value[key] });
      }
    }
    Array.prototype.push.apply(keys, temp);
    return keys;
  }
}
