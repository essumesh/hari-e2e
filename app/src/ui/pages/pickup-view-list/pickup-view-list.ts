import { Component, Pipe, PipeTransform, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, Content } from 'ionic-angular';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { PlatformHelper } from '../../../helpers/PlatformHelper';
import { FetchPickUpDeliveryDataObject } from '../../../network/dataobjects/FetchPickUpDeliveryDataObject';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { FetchPickUpDeliveryResponseObject } from '../../../network/responseobjects/FetchPickUpDeliveryResponseObject';
import { PreFetchDataObjectsHelper } from '../../../network/helpers/PreFetchDataObjectsHelper';
import { PickUpDeliveryDataModel } from '../../../datamodels/PickUpDeliveryDataModel';
import * as _ from 'lodash';
import { PickUpDeliveryViewDataModel, PickupViewModel, PickupViewArrayModel, PickUpViewDataModel }
  from '../../../datamodels/PickUpDeliveryViewDataModel';
import { BarcodeScannerHelper } from '../../../helpers/BarcodeScannerHelper';
import { DeliveryConfirmationPage } from '../delivery-confirmation/delivery-confirmation';
import { DeliveryConfirmationDataModel, DeliveryConfirmationItemDataModel }
  from '../../../datamodels/DeliveryConfirmationDataModel';
import { BarcodeParserHelper, BarcodeType } from '../../../helpers/BarcodeParserHelper';
import { ScreenOrientationHelper, ScreenOrientationType } from '../../../helpers/ScreenOrientationHelper';

@Component({
  selector: 'champ-page-pickup-view-list',
  templateUrl: 'pickup-view-list.html',
})
export class PickupViewListPage extends BaseUINavigationBarView {
  static itemssortkey: any = '';
  @ViewChild(Content) content: Content;
  pickup: string = 'location';

  awbViewModel: any;
  locationViewModel: any;
  private pickUpDeliveryDataModel: PickUpDeliveryDataModel;
  private _piecesCollected = 0;
  private _airwaybillCount: any;
  private _locationCount: any;
  private _awbTotalPieces = 0;
  private _locationTotalPieces = 0;
  private _selectAll: boolean;
  private _proceedDisble: boolean = false;
  private _proceedEnable: boolean = false;
  private _barCodeForInternalHardwareScannerForZebraDevicesSubscriber: any;

  constructor(private navParams: NavParams,
    private navCtrl: NavController,
    private viewCtrl: ViewController) {
    super();

    this.pickUpDeliveryDataModel = this.navParams.get('pickUpDeliveryDataModel');

    this.navigationBarViewModel =
      new NavigationBarViewModel(true,
        AppUIHelper.getInstance().i18nText(this.getTitle()),
        true,
        true,
        true);

    this.parsePickupDataModel();

  }

  ionViewDidEnter() {
    super.ionViewDidEnter();
    this.registerForBarCodeForInternalHardwareScannerForZebraDevices();
  }

  ionViewDidLeave() {
    super.ionViewDidLeave();
    this.unregisterForBarCodeForInternalHardwareScannerForZebraDevices();
  }

  parsePickupDataModel() {
    let pickupViewArrayModel = new PickupViewArrayModel();
    this.pickUpDeliveryDataModel.airwaybillArray.forEach(function (airwaybill) {
      if (airwaybill.airwaybillPrefix === null && airwaybill.airwaybillSerial === null) {
        // setting HouseWaybill Viewmodel
        let houseAirwaybillNumber = 'HWB ' + airwaybill.houseAirwaybill.houseAirwaybillNumber;
        airwaybill.houseAirwaybill.pieceLocations.forEach(function (location: any) {
          let pickupViewModel = new PickupViewModel();
          pickupViewModel.airwaybillno = houseAirwaybillNumber;
          pickupViewModel.deliverypieces = location.deliveryPieces;
          pickupViewModel.isChecked = false;
          pickupViewModel.location = location.warehouseLocation.location;
          pickupViewArrayModel.pickupViewArrayModel.push(pickupViewModel);
        });
      } else {
        // setting AirWaybill Viewmodel
        let airwaybillNumber = 'AWB ' + airwaybill.airwaybillPrefix + '-' + airwaybill.airwaybillSerial;
        airwaybill.pieceLocations.forEach(function (location: any) {
          let pickupViewModel = new PickupViewModel();
          pickupViewModel.airwaybillno = airwaybillNumber;
          pickupViewModel.deliverypieces = location.deliveryPieces;
          pickupViewModel.isChecked = false;
          pickupViewModel.location = location.warehouseLocation.location;
          pickupViewArrayModel.pickupViewArrayModel.push(pickupViewModel);
        });
      }
    });
    // creating AWBView UI Rendering
    this.awbViewModel = this.createAWBViewModel(pickupViewArrayModel);
    this._airwaybillCount = Object.keys(this.awbViewModel).length;
    this.awbViewModel = this.sortViewModel(this.awbViewModel, 'awb');
    console.log(this.awbViewModel);
    console.log(JSON.stringify(this.awbViewModel));
    // creating LocationView UI Rendering
    this.locationViewModel = this.createLocationViewModel(pickupViewArrayModel);
    this._locationCount = Object.keys(this.locationViewModel).length;
    this.locationViewModel = this.sortViewModel(this.locationViewModel, 'location');
    this.storedPiecesCount();
    this.onProceedDisable();
    this.onProceedEnable();
  }

  pickup_itemCheck(pickUpViewDataModel: PickUpViewDataModel, pickupViewModel: PickupViewModel) {
    pickUpViewDataModel.pickUpViewModel.every(pickup => pickup.isChecked) ?
      pickUpViewDataModel.isChecked = true : pickUpViewDataModel.isChecked = false;
    if (pickUpViewDataModel.isChecked) pickUpViewDataModel.storedCount = '1';
    // calculate collected pieces while clicking view
    if (pickUpViewDataModel.isChecked) {
      pickUpViewDataModel.collectedPieces = pickUpViewDataModel.storedPieces;
    } else {
      pickupViewModel.isChecked ? pickUpViewDataModel.collectedPieces = '' +
        (parseInt(pickUpViewDataModel.collectedPieces, 10) + parseInt(pickupViewModel.deliverypieces, 10)) :
        pickUpViewDataModel.collectedPieces = '' + (parseInt(pickUpViewDataModel.collectedPieces, 10) -
          parseInt(pickupViewModel.deliverypieces, 10));
    }
    this.isSelectedAll();
    this.onProceedDisable();
    this.onProceedEnable();
  }

  pickup_headerCheck(pickUpViewDataModel: PickUpViewDataModel, event: any) {
    console.log(pickUpViewDataModel);
    console.log(event.checked);
    let piecesCollected = 0;
    if (event.checked) {
      pickUpViewDataModel.pickUpViewModel.forEach(pickup => {
        pickup.isChecked = true;
        piecesCollected = piecesCollected + parseInt(pickup.deliverypieces, 10);
      });
    } else if (pickUpViewDataModel.pickUpViewModel.length ===
      pickUpViewDataModel.pickUpViewModel.filter(pickup => pickup.isChecked).length) {
      pickUpViewDataModel.pickUpViewModel.forEach(pickup => pickup.isChecked = false);
    }
    console.log(piecesCollected);
    // First header click
    // if (pickUpViewDataModel.storedCount === '0' && event.checked) {
    // if (pickUpViewDataModel.storedCount !== '' + piecesCollected && event.checked)
    if (event.checked)
      pickUpViewDataModel.collectedPieces = '' + pickUpViewDataModel.storedPieces;
    // pickUpViewDataModel.storedCount = '1';
    // }
    this.isSelectedAll();
    this.onProceedDisable();
    this.onProceedEnable();
  }

  onViewChanged() {
    if (this.pickup === 'awb') {
      // tslint:disable-next-line:forin
      for (let awbKey in this.awbViewModel) {
        this._piecesCollected = 0;
        let pickUpViewModel: Array<PickupViewModel> = [];
        pickUpViewModel = this.awbViewModel[awbKey].pickUpViewModel;
        pickUpViewModel.forEach(pickupViewModel => {
          this.pickup_onViewChanged(this.awbViewModel[awbKey], pickupViewModel);
        });
      }
    } else {
      // tslint:disable-next-line:forin
      for (let locatioKey in this.locationViewModel) {
        this._piecesCollected = 0;
        let pickUpViewModel: Array<PickupViewModel> = [];
        pickUpViewModel = this.locationViewModel[locatioKey].pickUpViewModel;
        pickUpViewModel.forEach(pickupViewModel => {
          this.pickup_onViewChanged(this.locationViewModel[locatioKey], pickupViewModel);
        });
      }
    }
  }
  storedPiecesCount() {
    // tslint:disable-next-line:forin
    for (let awbkKey in this.awbViewModel) {
      let pickUpViewModel: Array<PickupViewModel> = [];
      pickUpViewModel = this.awbViewModel[awbkKey].pickUpViewModel;
      let piecesStored = 0;
      pickUpViewModel.forEach(awbView => {
        piecesStored = piecesStored + parseInt(awbView.deliverypieces, 10);
      });
      this.awbViewModel[awbkKey].storedPieces = piecesStored;
      this._awbTotalPieces = this._awbTotalPieces + piecesStored;
    }

    // tslint:disable-next-line:forin
    for (let locationKey in this.locationViewModel) {
      let pickUpViewModel: Array<PickupViewModel> = [];
      pickUpViewModel = this.locationViewModel[locationKey].pickUpViewModel;
      let piecesStored = 0;
      pickUpViewModel.forEach(awbView => {
        piecesStored = piecesStored + parseInt(awbView.deliverypieces, 10);
      });
      this.locationViewModel[locationKey].storedPieces = piecesStored;
      this._locationTotalPieces = this._locationTotalPieces + piecesStored;
    }
  }
  onReset() {
    // tslint:disable-next-line:forin
    for (let key in this.awbViewModel) {
      let pickUpViewModel: Array<PickupViewModel> = [];
      pickUpViewModel = this.awbViewModel[key].pickUpViewModel;
      pickUpViewModel.forEach(pickup => { pickup.isChecked = false; });
    }
    // tslint:disable-next-line:forin
    for (let key in this.locationViewModel) {
      let pickUpViewModel: Array<PickupViewModel> = [];
      pickUpViewModel = this.locationViewModel[key].pickUpViewModel;
      pickUpViewModel.forEach(pickup => { pickup.isChecked = false; });
    }
  }
  onSortRequest(viewtype: any, sortdata: any) {
    if (viewtype === 'awbView')
      this.awbViewModel = this.sortViewModelbyScan(this.awbViewModel, sortdata);
    else
      this.locationViewModel = this.sortViewModelbyScan(this.locationViewModel, sortdata);
    this.content.scrollToTop(500);
  }
  onScanClicked() {
    BarcodeScannerHelper.getInstance().scan((_barcodeData: string) => {
      if (this.pickup === 'awb') {
        let barcodeType = BarcodeParserHelper.barcodeType(_barcodeData);
        if (barcodeType === BarcodeType.AWB) {
          const barcodeValues = BarcodeParserHelper.barcodeValues(_barcodeData, barcodeType);
          _barcodeData = 'AWB ' + barcodeValues[0] + '-' + barcodeValues[1];
        } else if (barcodeType === BarcodeType.HWB) {
          const barcodeValues = BarcodeParserHelper.barcodeValues(_barcodeData, barcodeType);
          _barcodeData = 'HWB ' + barcodeValues[0];
        }
        if (this.isBarCodeDataAvailable(this.awbViewModel, _barcodeData))
          this.awbViewModel = this.sortViewModelbyScan(this.awbViewModel, _barcodeData);
      } else {
        if (this.isBarCodeDataAvailable(this.locationViewModel, _barcodeData))
          this.locationViewModel = this.sortViewModelbyScan(this.locationViewModel, _barcodeData);
      }
    }, (errorText: string) => {
      alert(errorText);
    });
  }

  onProceed() {
    let deliveryConfirmationDataModel: Array<DeliveryConfirmationItemDataModel> = [];
    // tslint:disable-next-line:forin
    for (let awbKey in this.awbViewModel) {
      let deliveryConfirmationItemDataModel = new DeliveryConfirmationItemDataModel();
      deliveryConfirmationItemDataModel.airwaybillNumber = awbKey;
      deliveryConfirmationItemDataModel.collectedPieces = this.awbViewModel[awbKey].storedPieces;
      deliveryConfirmationDataModel.push(deliveryConfirmationItemDataModel);
    }
    this.navCtrl.push(DeliveryConfirmationPage,
      {
        'deliveryConfirmationDataModel': deliveryConfirmationDataModel,
        'receiptNumber': this.pickUpDeliveryDataModel.receiptOrTransferNumber,
        'totalPieces': this._awbTotalPieces,
        'headerText': this.getTitle(),
      });
  }

  pickup_onViewChanged(pickUpViewDataModel: PickUpViewDataModel, pickupViewModel: PickupViewModel) {
    pickUpViewDataModel.pickUpViewModel.every(pickup => pickup.isChecked) ?
      pickUpViewDataModel.isChecked = true : pickUpViewDataModel.isChecked = false;
    if (pickUpViewDataModel.isChecked) pickUpViewDataModel.storedCount = '1';
    //  Calculating  the collected pieces while view changed
    if (pickupViewModel.isChecked) {
      this._piecesCollected = this._piecesCollected + parseInt(pickupViewModel.deliverypieces, 10);
      pickUpViewDataModel.collectedPieces = '' + this._piecesCollected;
    }
    // Resetting the collected pieces while view changed
    if (!pickupViewModel.isChecked && this._piecesCollected === 0) {
      pickUpViewDataModel.collectedPieces = '0';
    }
  }
  isSelectedAll() {
    if (this.pickup === 'awb') {
      // tslint:disable-next-line:forin
      for (let awbKey in this.awbViewModel) {
        if (this.awbViewModel[awbKey].isChecked) this._selectAll = true;
        else {
          this._selectAll = false; break;
        }
      }
    } else {
      // tslint:disable-next-line:forin
      for (let locationKey in this.locationViewModel) {
        if (this.locationViewModel[locationKey].isChecked) this._selectAll = true;
        else {
          this._selectAll = false; break;
        }
      }
    }
  }

  Hi() {

  }
  onProceedEnable() {
    if (this.pickup === 'awb') {
      // tslint:disable-next-line:forin
      for (let awbKey in this.awbViewModel) {
        for (let i = 0; i < this.awbViewModel[awbKey].pickUpViewModel.length; i++) {
          if (this.awbViewModel[awbKey].pickUpViewModel[i].isChecked) {
            this._proceedEnable = true;
            break;
          }
        }
        if (this._proceedEnable)
          break;
      }
    } else {
      // tslint:disable-next-line:forin
      for (let locationKey in this.locationViewModel) {
        for (let i = 0; i < this.locationViewModel[locationKey].pickUpViewModel.length; i++) {
          if (this.locationViewModel[locationKey].pickUpViewModel[i].isChecked) {
            this._proceedEnable = true;
            break;
          }
        }
        if (this._proceedEnable) break;
      }
    }
    if (this._proceedEnable)
    this._proceedDisble = false;
    console.log(this._proceedEnable);
  }
  onProceedDisable() {
    if (this.pickup === 'awb') {
      // tslint:disable-next-line:forin
      for (let awbKey in this.awbViewModel) {
        if (!this.awbViewModel[awbKey].isChecked) this._proceedDisble = true;
      }
    } else {
      // tslint:disable-next-line:forin
      for (let locationKey in this.locationViewModel) {
        if (!this.locationViewModel[locationKey].isChecked) this._proceedDisble = true;
      }
    }
    if (this._proceedDisble)
    this._proceedEnable = false;
    console.log(this._proceedDisble);
  }
  sortViewModelbyScan(viewModelObject: any, sortBy: any) {
    console.log(viewModelObject);
    let returnViewModelObject: any = {};
    let key: any;
    let viewModelkey = Object.keys(viewModelObject);
    let index = viewModelkey.indexOf(sortBy);
    for (let i = index; i > 0; i--) {
      viewModelkey[i] = viewModelkey[i - 1];
    }
    viewModelkey[0] = sortBy;
    for (let i = 0; i < viewModelkey.length; i++) {
      key = '' + viewModelkey[i];
      returnViewModelObject[key] = viewModelObject[viewModelkey[i]];
    }
    console.log(returnViewModelObject);
    return returnViewModelObject;
  }
  isShowable() {
    if (PlatformHelper.isDevice_A_Tablet()
      && (ScreenOrientationHelper.currentScreenOrientation() ===
        ScreenOrientationType.LANDSCAPE)) {
      return true;
    } else {
      return false;
    }
  }
  isBarCodeDataAvailable(viewModelObject: any, sortBy: any) {
    let isBarCodeAvailable: boolean;
    let viewModelkey = Object.keys(viewModelObject);
    for (let k = 0; k < viewModelkey.length; k++) {
      if (viewModelkey[k] === sortBy) {
        isBarCodeAvailable = true;
        break;
      } else {
        isBarCodeAvailable = false;
      }
    }
    return isBarCodeAvailable;
  }

  contentTitle(): string {
      return this.getTitle()
        + ' ' + this.pickUpDeliveryDataModel.receiptOrTransferNumber;

  }

  getTitle(): string {
    console.log(this.pickUpDeliveryDataModel);
    if (this.pickUpDeliveryDataModel.pickupType === 'deliveryReceipt') {
    return 'Delivery Receipt';
     } else if (this.pickUpDeliveryDataModel.pickupType === 'bondTransfer') {
     return 'Bond Transfer';
    } else if (this.pickUpDeliveryDataModel.pickupType === 'airlineTransfer') {
      return 'Airline Transfer';
    } else {
      return this.pickUpDeliveryDataModel.pickupType;
    }
  }
  sortViewModel(pickUpViewDataModel: any, sortBy: any) {
    let pickUpViewDataModelkey = Object.keys(pickUpViewDataModel).sort();
    let returnViewModelObject: any = {};
    for (let i = 0; i < pickUpViewDataModelkey.length; i++) {
      let pickUpViewModel: Array<PickupViewModel> = [];
      pickUpViewModel = pickUpViewDataModel[pickUpViewDataModelkey[i]].pickUpViewModel;
      let key = '' + pickUpViewDataModelkey[i];
      pickUpViewDataModel[key].pickUpViewModel = this.sortbyAWB(pickUpViewModel, sortBy);
      returnViewModelObject[key] = pickUpViewDataModel[pickUpViewDataModelkey[i]];
    }
    return returnViewModelObject;
  }
  sortbyAWB(pickupViewModelArray: any, sortBy: any) {
    if (sortBy === 'location') {
      pickupViewModelArray.sort(function (pickupViewModel1: PickupViewModel, pickupViewModel2: PickupViewModel) {
        if (pickupViewModel1.airwaybillno
          < pickupViewModel2.airwaybillno) return -1;
        else if (pickupViewModel1.airwaybillno
          > pickupViewModel2.airwaybillno) return 1;
        else return 0;
      });
    } else {
      pickupViewModelArray.sort(function (pickupViewModel1: PickupViewModel, pickupViewModel2: PickupViewModel) {
        if (pickupViewModel1.location
          < pickupViewModel2.location) return -1;
        else if (pickupViewModel1.location
          > pickupViewModel2.location) return 1;
        else return 0;
      });
    }
    return pickupViewModelArray;
  }

  createAWBViewModel(pickupViewArrayModel: PickupViewArrayModel) {
    return pickupViewArrayModel.pickupViewArrayModel.
      reduce(function (previous: any, current: any) {
        if (!previous[current.airwaybillno]) {
          previous[current.airwaybillno] = new PickUpViewDataModel();
          previous[current.airwaybillno].collectedPieces = '0';
          previous[current.airwaybillno].storedCount = '0';
          previous[current.airwaybillno].storedPieces = '0';
          previous[current.airwaybillno].isChecked = false;
          previous[current.airwaybillno].pickUpViewModel.push(current);
        }
        // tslint:disable-next-line:one-line
        else {
          previous[current.airwaybillno].pickUpViewModel.push(current);
        }
        return previous;
      }, Object.create(null));
  }

  createLocationViewModel(pickupViewArrayModel: PickupViewArrayModel) {
    return pickupViewArrayModel.pickupViewArrayModel.
      reduce(function (previous: any, current: any) {
        if (!previous[current.location]) {
          previous[current.location] = new PickUpViewDataModel();
          previous[current.location].collectedPieces = '0';
          previous[current.location].storedCount = '0';
          previous[current.location].storedPieces = '0';
          previous[current.location].isChecked = false;
          previous[current.location].pickUpViewModel.push(current);
        }
        // tslint:disable-next-line:one-line
        else {
          previous[current.location].pickUpViewModel.push(current);
        }
        return previous;
      }, Object.create(null));
  }

  private registerForBarCodeForInternalHardwareScannerForZebraDevices() {
    this.unregisterForBarCodeForInternalHardwareScannerForZebraDevices();

    this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber =
    BarcodeScannerHelper.getInstance()
    .barCodeForInternalHardwareScannerForZebraDevices.subscribe
    ((_barcodeData: string) => {
      if (this.pickup === 'awb') {
        let barcodeType = BarcodeParserHelper.barcodeType(_barcodeData);
        if (barcodeType === BarcodeType.AWB) {
          const barcodeValues = BarcodeParserHelper.barcodeValues(_barcodeData, barcodeType);
          _barcodeData = 'AWB ' + barcodeValues[0] + '-' + barcodeValues[1];
        } else if (barcodeType === BarcodeType.HWB) {
          const barcodeValues = BarcodeParserHelper.barcodeValues(_barcodeData, barcodeType);
          _barcodeData = 'HWB ' + barcodeValues[0];
        }
        if (this.isBarCodeDataAvailable(this.awbViewModel, _barcodeData))
          this.awbViewModel = this.sortViewModelbyScan(this.awbViewModel, _barcodeData);
      } else {
        if (this.isBarCodeDataAvailable(this.locationViewModel, _barcodeData))
          this.locationViewModel = this.sortViewModelbyScan(this.locationViewModel, _barcodeData);
      }
    });
  }

  private unregisterForBarCodeForInternalHardwareScannerForZebraDevices() {
    if (this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber != null) {
      this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber.unsubscribe();
      this._barCodeForInternalHardwareScannerForZebraDevicesSubscriber = null;
    }
  }

}
