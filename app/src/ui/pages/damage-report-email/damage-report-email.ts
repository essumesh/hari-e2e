import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { BaseUINavigationBarView } from '../../baseuiviews/forpages/BaseUINavigationBarView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { DamageReportsDataModel } from '../../../datamodels/DamageReportsDataModel';
import { AWBEmailDataObject } from '../../../network/dataobjects/AWBEmailDataObject';
import { ManualDIHelper } from '../../../helpers/ManualDIHelper';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';
import { AppMessagePopupHelper } from '../../../helpers/AppMessagePopupHelper';
import { AppMessagePopupViewModel, AppMessagePopupViewModelType } from '../../viewmodels/AppMessagePopupViewModel';
import { AWBEmailDataModel, Thumbnail, ImageURL } from '../../../datamodels/AWBEmailDataModel';
import { EmailCheckBoxesViewModel } from '../../viewmodels/EmailCheckBoxesViewModel';

@Component({
  selector: 'champ-page-damage-report-email',
  templateUrl: 'damage-report-email.html',
})
export class DamageReportEmailPage extends BaseUINavigationBarView {
  damageReportsDataModel: DamageReportsDataModel;
  awbEmailDataModel: AWBEmailDataModel;
  isAllImagesSelected: boolean;
  imageCheckboxall: any;
  private emailCheckBoxesViewModel: EmailCheckBoxesViewModel;

  constructor(private navParams: NavParams,
    private navCtrl: NavController,
    private viewCtrl: ViewController) {
    super();

    this.navigationBarViewModel =
      new NavigationBarViewModel(true,
        AppUIHelper.getInstance().i18nText('selectmediafiles'),
        true,
        true,
        true);
    this.damageReportsDataModel =
      this.navParams.get('damageReportsDataModel');
    console.log(this.damageReportsDataModel);
    this.prepareEmailCheckBoxesViewModel();

  }
  contentTitle(): string {
    return this.damageReportsDataModel.images.linkMediaDataModels.length + ' Images';
  }

  onTriggerAWBEmailService() {
    let awbEmailREquestObject: AWBEmailDataObject =
      ManualDIHelper.getInstance().createObject('AWBEmailDataObject');

    awbEmailREquestObject.prepareRequestWithParameters(this.damageReportsDataModel.reportId, this.awbEmailDataModel);

    awbEmailREquestObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
      if (_dataObject.serverErrorDataModel != null) {
        if (_dataObject.serverErrorDataModel.errorText === 'success') {
          AppMessagePopupHelper.getInstance().showAppMessagePopup(
            new AppMessagePopupViewModel(
              AppUIHelper.getInstance().i18nText('Email Sent Successfully'),
              'assets/images/green-tick-small.svg', AppMessagePopupViewModelType.MessagePopup), 4);
        } else {
          AppUIHelper.getInstance().showAlert(
            AppUIHelper.getInstance().i18nText('error'),
            _dataObject.serverErrorDataModel.errorText,
            [AppUIHelper.getInstance().i18nText('ok')]);
        }
      }
    });
  }

  selectAllEmailImages() {
    if (this.imageCheckboxall) {
      this.isAllImagesSelected = true;
      this.emailCheckBoxesViewModel.checkBoxViewModels.forEach(imageUrl => imageUrl.isChecked = true);
    } else if (!this.imageCheckboxall && this.emailCheckBoxesViewModel.checkBoxViewModels.length ===
      this.emailCheckBoxesViewModel.checkBoxViewModels.filter(imageUrl => imageUrl.isChecked).length) {
      this.isAllImagesSelected = false;
      this.emailCheckBoxesViewModel.checkBoxViewModels.forEach(imageUrl => imageUrl.isChecked = false);
    }
  }

  updateEmailImageItem() {
    this.emailCheckBoxesViewModel.checkBoxViewModels.
      every(imageUrl => imageUrl.isChecked) ? this.isAllImagesSelected = true : this.isAllImagesSelected = false;
  }

  onsendEmailClicked() {
    this.awbEmailDataModel = new AWBEmailDataModel();
    this.emailCheckBoxesViewModel.checkBoxViewModels.
    filter(image => image.isChecked).forEach(image => {
      let thumbnail = new Thumbnail();
      thumbnail.base64Content = image.linkMediaDataModel.thumbnailBase64Data;
      let imageURL = new ImageURL();
      imageURL.href = image.linkMediaDataModel.href;
      imageURL.thumbnail = thumbnail;
      this.awbEmailDataModel.imageURLs.push(imageURL);
   });
    if (this.awbEmailDataModel.imageURLs != null && this.awbEmailDataModel.imageURLs.length !== 0) {
    this.onTriggerAWBEmailService();
    }else {
      AppUIHelper.getInstance().showAlert(AppUIHelper.getInstance().i18nText('error'),
      AppUIHelper.getInstance().i18nText('selectionerror'), [AppUIHelper.getInstance().i18nText('ok')]);
    }
  }

  prepareEmailCheckBoxesViewModel() {
    this.emailCheckBoxesViewModel = new EmailCheckBoxesViewModel();
    for (let emailDataModel of
      this.damageReportsDataModel.images.linkMediaDataModels) {
        console.log('EmailCheckBoxesViewModel: ' + emailDataModel);
        this.emailCheckBoxesViewModel.addCheckBoxViewModel(emailDataModel, false);
        }
    }
}
