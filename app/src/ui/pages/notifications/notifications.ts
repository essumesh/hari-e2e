import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ViewController, AlertController } from 'ionic-angular';
import { FilterCheckboxPopover } from '../filter-checkbox-popover/filter-checkbox-popover';
import { NotificationGenericListItemViewModel } from '../../viewmodels/NotificationGenericListItemViewModel';
import { FilterRadioButtonPopover } from
'../filter-radiobutton-popover/filter-radiobutton-popover';
import { AppUIHelper } from '../../../helpers/AppUIHelper';
import { CheckBoxesViewModel, CheckBoxViewModel } from '../../viewmodels/CheckBoxesViewModel';
import { RadioButtonsViewModel, RadioButtonViewModel } from '../../viewmodels/RadioButtonsViewModel';
import { BaseUIPageView } from '../../baseuiviews/forpages/BaseUIPageView';
import { NavigationBarViewModel } from '../../viewmodels/NavigationBarViewModel';
import { NavigationBarComponent } from '../../components/navigation-bar/navigation-bar';
import { LocalStorageHelper } from '../../../localstorage/helpers/LocalStorageHelper';
import { LocalStorageDataSyncHelper } from '../../../localstorage/helpers/LocalStorageDataSyncHelper';
import { LocalStorageModel, LocalStorageModelSyncStatus }
from '../../../localstorage/localstoragemodels/LocalStorageModel';
import { DateParserHelper } from '../../../helpers/DateParserHelper';
import { BaseDataObject } from '../../../network/dataobjects/BaseDataObject';

@Component({
  selector: 'champ-page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage extends BaseUIPageView {
  notificationGenericListItemViewModelsAll: Array<NotificationGenericListItemViewModel> = [];
  notificationGenericListItemViewModelsToShow: Array<NotificationGenericListItemViewModel> = [];
  checkBoxesViewModel: CheckBoxesViewModel = new CheckBoxesViewModel();
  radioButtonsViewModel: RadioButtonsViewModel = new RadioButtonsViewModel();
  protected navigationBarViewModel: NavigationBarViewModel;
  @ViewChild('navigationBarComponent') protected navigationBarComponent: NavigationBarComponent;
  private arrayOfFilterStatus: Array<string> = [];
  private arrayOfSections: Array<string> = [];
  private localStorageModelsAll: Array<LocalStorageModel> = [];
  private localStorageModelSyncStatusChangedSubscriber: any;
  private localStorageModelDeletedSubscriber: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public viewController: ViewController,
              public popoverController: PopoverController,
              public alertCtrl: AlertController,
              private ngZone: NgZone) {
    super();

    this.navigationBarViewModel =
        new NavigationBarViewModel(false,
                               AppUIHelper.getInstance().i18nText('notificationtitle'),
                               false,
                               true,
                               true);
    this.navigationBarViewModel.toShowBackButton = true;

    this.prepare_ArrayOfFilterStatus();
    this.prepareArrayOfSections();
    this.refreshUI();
  }

  ionViewWillEnter() {
      super.ionViewWillEnter();
      this.navigationBarComponent.ionViewWillEnter();

      if (this.localStorageModelSyncStatusChangedSubscriber == null) {
        this.localStorageModelSyncStatusChangedSubscriber =
        LocalStorageDataSyncHelper.getInstance().localStorageModelSyncStatusChanged.subscribe
        ((_localStorageModel: LocalStorageModel) => {
          this.ngZone.run(() => {
            let _notificationGenericListItemViewModel: NotificationGenericListItemViewModel =
            this.notificationGenericListItemViewModelFor(_localStorageModel);
            if (_notificationGenericListItemViewModel != null) {
              _notificationGenericListItemViewModel.localStorageModel = _localStorageModel;
            }
          });
        });
      }

      if (this.localStorageModelDeletedSubscriber == null) {
        this.localStorageModelDeletedSubscriber =
        LocalStorageDataSyncHelper.getInstance().localStorageModelDeleted.subscribe
        ((_localStorageModel: LocalStorageModel) => {
          this.ngZone.run(() => {
            this.refreshUI();
          });
        });
      }
  }

  ionViewWillLeave() {
      super.ionViewWillLeave();
      this.navigationBarComponent.ionViewWillLeave();

      if (this.localStorageModelSyncStatusChangedSubscriber != null) {
        this.localStorageModelSyncStatusChangedSubscriber.unsubscribe();
        this.localStorageModelSyncStatusChangedSubscriber = null;
      }

      if (this.localStorageModelDeletedSubscriber != null) {
        this.localStorageModelDeletedSubscriber.unsubscribe();
        this.localStorageModelDeletedSubscriber = null;
      }
  }

  presentPopover(_event: any) {
    let popover = this.popoverController.create(FilterCheckboxPopover, {
                                                'title': AppUIHelper.getInstance().i18nText('filterByStatus'),
                                                'checkboxes': this.checkBoxesViewModel,
                                                'buttonText': AppUIHelper.getInstance().i18nText('apply'),
                                                'checkBoxViewModelTapEvent':
                                                  (checkBoxViewModel: CheckBoxViewModel) => {
                                                  this.changeArrayOfCheckedItems(checkBoxViewModel);
                                                }});
    popover.present({
      ev: event,
    });

    popover.onDidDismiss(() => {
        this.prepareNotificationGenericListItemViewModelsToShow();
      },
    );
  }

  backButtonClicked(_navigationBarViewModel: NavigationBarViewModel) {
    this.viewController.dismiss();
  }

  tapEvent_On_GenericListItemComponent() {
  }

  tapEventOnNotificationUserAction(_i: number) {
  }

  prepare_ArrayOfFilterStatus() {
    this.arrayOfFilterStatus.push(AppUIHelper.getInstance().i18nText('all'));
    this.arrayOfFilterStatus.push(AppUIHelper.getInstance().i18nText('pendingSync'));
    this.arrayOfFilterStatus.push(AppUIHelper.getInstance().i18nText('syncFailed'));
    this.arrayOfFilterStatus.push(AppUIHelper.getInstance().i18nText('syncInProgress'));
    this.arrayOfFilterStatus.push(AppUIHelper.getInstance().i18nText('errorValidationError'));

    for (let _filterStatus of this.arrayOfFilterStatus)
      this.checkBoxesViewModel.addCheckBoxViewModel(_filterStatus, true, null);
  }

  prepareArrayOfSections() {
    this.arrayOfSections.push(AppUIHelper.getInstance().i18nText('damagereport'));
    this.arrayOfSections.push(AppUIHelper.getInstance().i18nText('pickup'));

    for (let section of this.arrayOfSections) {
      this.radioButtonsViewModel.addRadioButtonViewModel(section, false, null);
    }
  }

  presentSectionSelectionPopover() {
    let popover = this.popoverController.create(FilterRadioButtonPopover, {
                                                'title': AppUIHelper.getInstance().i18nText('filterByStatus'),
                                                'radioButtons': this.radioButtonsViewModel,
                                                'buttonText': AppUIHelper.getInstance().i18nText('done'),
                                                'radioButtonViewModelTapEvent':
                                                  (_radioButtonViewModel: RadioButtonViewModel) => {
                                                  }},
                                                );
    popover.present({
      ev: event,
    });

    popover.onDidDismiss(() => {
     },
    );
  }

  private prepare_NotificationGenericListItemViewModels() {
    this.notificationGenericListItemViewModelsAll = [];
    this.notificationGenericListItemViewModelsToShow = [];

    this.localStorageModelsAll.forEach((_localStorageModel, _index) => {
      let _notificationGenericListItemViewModel: NotificationGenericListItemViewModel =
      new NotificationGenericListItemViewModel(_index, '' + _index, _localStorageModel);

      _notificationGenericListItemViewModel.mainText =
      DateParserHelper.parseDateToString_InFormat_ShortDayOfWeek(_localStorageModel.dateWhenConstructed)
      + ' '
      + DateParserHelper.parseDateToString_InFormat_ddMMMyy(_localStorageModel.dateWhenConstructed)
      + ' at '
      + DateParserHelper.parseDateToString_InFormat_HHmm(_localStorageModel.dateWhenConstructed);

      if (_localStorageModel.objectToBeLocallyStored.requestObject.damageReportsDataModel.airwaybill)
        _notificationGenericListItemViewModel.subText =
        AppUIHelper.getInstance().i18nText('awbdetails')
        + _localStorageModel.objectToBeLocallyStored.requestObject.damageReportsDataModel.airwaybill.airwaybillPrefix
        + ' '
        + _localStorageModel.objectToBeLocallyStored.requestObject.damageReportsDataModel.airwaybill.airwaybillSerial;
      else if (_localStorageModel.objectToBeLocallyStored.requestObject.damageReportsDataModel.houseAirwaybill)
        _notificationGenericListItemViewModel.subText =
        AppUIHelper.getInstance().i18nText('hwbdetails') +
        _localStorageModel.objectToBeLocallyStored.requestObject.
        damageReportsDataModel.houseAirwaybill.houseAirwaybillNumber;

      this.notificationGenericListItemViewModelsAll.push(_notificationGenericListItemViewModel);
    });

    this.prepareNotificationGenericListItemViewModelsToShow();
  }

  private prepareNotificationGenericListItemViewModelsToShow() {
    this.notificationGenericListItemViewModelsToShow = [];

    const isItValidationError = ((_notificationGenericListItemViewModelTemp: NotificationGenericListItemViewModel) => {
      let dataObject: BaseDataObject =
      _notificationGenericListItemViewModelTemp.localStorageModel.objectToBeLocallyStored;
      if (dataObject.serverErrorDataModel != null
          && dataObject.serverErrorDataModel.errorText != null
          && AppUIHelper.isString(dataObject.serverErrorDataModel.errorText)
          && dataObject.serverErrorDataModel.errorText.length > 0)
          return true;
      else return false;
    });

    const addInNotificationGenericListItemViewModelsToShowIfNotAdded =
    ((_notificationGenericListItemViewModelTemp: NotificationGenericListItemViewModel) => {
      let isItAdded: boolean = false;
      for (let j = 0; j < this.notificationGenericListItemViewModelsToShow.length; j++) {
        if (_notificationGenericListItemViewModelTemp === this.notificationGenericListItemViewModelsToShow[j]) {
          isItAdded = true;
          break;
        }
      }
      if (!isItAdded)
        this.notificationGenericListItemViewModelsToShow.push(_notificationGenericListItemViewModelTemp);
    });

    if (this.checkBoxesViewModel.doesASelectedCheckBoxViewModelContainThisText
      (AppUIHelper.getInstance().i18nText('all'))) {
        for (let i = 0; i < this.notificationGenericListItemViewModelsAll.length ; i++) {
          let _notificationGenericListItemViewModel = this.notificationGenericListItemViewModelsAll[i];
          addInNotificationGenericListItemViewModelsToShowIfNotAdded(_notificationGenericListItemViewModel);
        }
    }
    if (this.checkBoxesViewModel.doesASelectedCheckBoxViewModelContainThisText
      (AppUIHelper.getInstance().i18nText('pendingSync'))) {
        for (let i = 0; i < this.notificationGenericListItemViewModelsAll.length ; i++) {
          let _notificationGenericListItemViewModel = this.notificationGenericListItemViewModelsAll[i];
          if (_notificationGenericListItemViewModel.localStorageModel.localStorageModelSyncStatus ===
              LocalStorageModelSyncStatus.SyncPending)
            addInNotificationGenericListItemViewModelsToShowIfNotAdded(_notificationGenericListItemViewModel);
        }
    }
    if (this.checkBoxesViewModel.doesASelectedCheckBoxViewModelContainThisText
      (AppUIHelper.getInstance().i18nText('syncFailed'))) {
        for (let i = 0; i < this.notificationGenericListItemViewModelsAll.length ; i++) {
          let _notificationGenericListItemViewModel = this.notificationGenericListItemViewModelsAll[i];
          if (_notificationGenericListItemViewModel.localStorageModel.localStorageModelSyncStatus ===
              LocalStorageModelSyncStatus.SyncFailed
              && !isItValidationError(_notificationGenericListItemViewModel))
            addInNotificationGenericListItemViewModelsToShowIfNotAdded(_notificationGenericListItemViewModel);
        }
    }
    if (this.checkBoxesViewModel.doesASelectedCheckBoxViewModelContainThisText
      (AppUIHelper.getInstance().i18nText('syncInProgress'))) {
        for (let i = 0; i < this.notificationGenericListItemViewModelsAll.length ; i++) {
          let _notificationGenericListItemViewModel = this.notificationGenericListItemViewModelsAll[i];
          if (_notificationGenericListItemViewModel.localStorageModel.localStorageModelSyncStatus ===
              LocalStorageModelSyncStatus.SyncInProgress)
            addInNotificationGenericListItemViewModelsToShowIfNotAdded(_notificationGenericListItemViewModel);
        }
    }
    if (this.checkBoxesViewModel.doesASelectedCheckBoxViewModelContainThisText
      (AppUIHelper.getInstance().i18nText('errorValidationError'))) {
        for (let i = 0; i < this.notificationGenericListItemViewModelsAll.length ; i++) {
          let _notificationGenericListItemViewModel = this.notificationGenericListItemViewModelsAll[i];
          if (_notificationGenericListItemViewModel.localStorageModel.localStorageModelSyncStatus ===
              LocalStorageModelSyncStatus.SyncFailed
              && isItValidationError(_notificationGenericListItemViewModel))
            addInNotificationGenericListItemViewModelsToShowIfNotAdded(_notificationGenericListItemViewModel);
        }
    }
  }

  private changeArrayOfCheckedItems(_checkBoxViewModel: CheckBoxViewModel) {
    if (_checkBoxViewModel.name === AppUIHelper.getInstance().i18nText('all')) {
      if (_checkBoxViewModel.isSelected) {
        for (let i = 1; i < this.checkBoxesViewModel.checkBoxViewModels.length ; i++) {
          this.checkBoxesViewModel.checkBoxViewModels[i].isSelected = true;
        }
      } else {
        for (let i = 1; i < this.checkBoxesViewModel.checkBoxViewModels.length ; i++) {
          this.checkBoxesViewModel.checkBoxViewModels[i].isSelected = false;
        }
      }
    } else {
      let checkBoxesArray: Array<CheckBoxViewModel> = [];
      for (let i = 1; i < this.checkBoxesViewModel.checkBoxViewModels.length ; i++) {
        checkBoxesArray.push(this.checkBoxesViewModel.checkBoxViewModels[i]);
      }

      const areAllValuesInCheckBoxesArrayTrue = (() => {
          for (let _checkBoxViewModelTemp of checkBoxesArray) {
            if (!_checkBoxViewModelTemp.isSelected) return false;
          }
          return true;
      });

      const isAnyOneValueInCheckBoxesArrayFalse = (() => {
          for (let _checkBoxViewModelTemp of checkBoxesArray) {
            if (!_checkBoxViewModelTemp.isSelected) return true;
          }
          return false;
      });

      if (areAllValuesInCheckBoxesArrayTrue()) {
          this.checkBoxesViewModel.checkBoxViewModels[0].isSelected = true;
      }
      if (isAnyOneValueInCheckBoxesArrayFalse()) {
          this.checkBoxesViewModel.checkBoxViewModels[0].isSelected = false;
      }
    }
  }

  private notificationGenericListItemViewModelFor(_localStorageModel: LocalStorageModel):
  NotificationGenericListItemViewModel {
    for (let _notificationGenericListItemViewModel of this.notificationGenericListItemViewModelsAll) {
      if (_notificationGenericListItemViewModel.localStorageModel.key() ===
          _localStorageModel.key())
          return _notificationGenericListItemViewModel;
    }
    return null;
  }

  private refreshUI() {
    LocalStorageHelper.getInstance().fetchLocalStorageModels
      ((localStorageModels: Array<LocalStorageModel>) => {
        this.localStorageModelsAll = localStorageModels;

        this.prepare_NotificationGenericListItemViewModels();
    }, LocalStorageModel.identifierFor_DataObjectType());
  }
}
