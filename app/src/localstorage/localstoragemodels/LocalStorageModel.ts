import { BaseDataObject } from '../../network/dataobjects/BaseDataObject';
import { ManualDIHelper } from '../../helpers/ManualDIHelper';
import { DateParserHelper } from '../../helpers/DateParserHelper';

export enum LocalStorageModelType {
    DataObject,
}

export enum LocalStorageModelSyncStatus {
    SyncInProgress,
    SyncFailed,
    SyncPending,
}

export class LocalStorageModel {
    private static readonly DATAOBJECTTYPE = 'DATAOBJECTTYPE';
    public objectToBeLocallyStored: any;
    private _dateWhenConstructed: Date;
    private _localStorageModelSyncStatus: LocalStorageModelSyncStatus =
    LocalStorageModelSyncStatus.SyncPending;

    public static identifierFor_DataObjectType(): string {
        return LocalStorageModel.DATAOBJECTTYPE;
    }

    public static isLocalStorageModelOfType_DataObject(_key: string): boolean {
        return _key.startsWith(LocalStorageModel.DATAOBJECTTYPE);
    }

    constructor(objectToBeLocallyStored: any) {
        this.objectToBeLocallyStored = objectToBeLocallyStored;
        this._dateWhenConstructed = new Date();
    }

    public key(): string {
        return this.localStorageModelTypeKey()
               + this.uniqueObjectId_Of_ObjectToBeLocallyStored();
    }

    public isLocalStorageModelOfType_DataObject(): boolean {
        return this.objectToBeLocallyStored instanceof BaseDataObject;
    }

    public get dateWhenConstructed(): Date {
        return this._dateWhenConstructed;
    }

    public set dateWhenConstructed(_dateWhenConstructed: Date) {
        this._dateWhenConstructed = _dateWhenConstructed;
    }

    public get localStorageModelSyncStatus(): LocalStorageModelSyncStatus {
        return this._localStorageModelSyncStatus;
    }

    public set localStorageModelSyncStatus(_localStorageModelSyncStatus: LocalStorageModelSyncStatus) {
        this._localStorageModelSyncStatus = _localStorageModelSyncStatus;
    }

    toJSON(): any {
        let jsonData = {
            key: this.key(),
            dateWhenConstructed: this.dateWhenConstructed,
            localStorageModelSyncStatus: this.localStorageModelSyncStatus,
            objectToBeLocallyStored: JSON.stringify(this.objectToBeLocallyStored),
        };
        return jsonData;
    }

    fromJSON(json: any) {
        const key = json.key;
        this.dateWhenConstructed = DateParserHelper.parseStringToDate(json.dateWhenConstructed);
        this._localStorageModelSyncStatus = json.localStorageModelSyncStatus;
        if (LocalStorageModel.isLocalStorageModelOfType_DataObject(key)) {
            let dataObject: BaseDataObject = ManualDIHelper.getInstance().createObject(
                key.substring(
                    LocalStorageModel.identifierFor_DataObjectType().length,
                    key.indexOf('_')));
            dataObject.fromJSON(JSON.parse(json.objectToBeLocallyStored));
            this.objectToBeLocallyStored = dataObject;
        }
    }

    private localStorageModelTypeKey(): string {
        if (this.isLocalStorageModelOfType_DataObject())
            return LocalStorageModel.DATAOBJECTTYPE;
        return null;
    }

    private uniqueObjectId_Of_ObjectToBeLocallyStored(): string {
        if (this.isLocalStorageModelOfType_DataObject())
            return (this.objectToBeLocallyStored as BaseDataObject).getUniqueObjectId();
        return null;
    }
}
