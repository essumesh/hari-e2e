import { NetworkConnectivityMonitorHelper } from '../../helpers/NetworkConnectivityMonitorHelper';
import { LocalStorageHelper } from './LocalStorageHelper';
import { BaseDataObject } from '../../network/dataobjects/BaseDataObject';
import { LocalStorageModel, LocalStorageModelSyncStatus } from '../localstoragemodels/LocalStorageModel';
import { Observable } from 'rxjs/Observable';

export class LocalStorageDataSyncHelper {
    private static instance: LocalStorageDataSyncHelper;
    private _localStorageModelSyncStatusChanged: any;
    private localStorageModelSyncStatusChangedObserver: any;
    private _localStorageModelDeleted: any;
    private localStorageModelDeletedObserver: any;
    private syncInProgress: boolean = false;

    static createInstance() {
        LocalStorageDataSyncHelper.instance = new LocalStorageDataSyncHelper();
    }

    static getInstance() {
        return this.instance;
    }

    get localStorageModelSyncStatusChanged(): any {
        return this._localStorageModelSyncStatusChanged;
    }

    get localStorageModelDeleted(): any {
        return this._localStorageModelDeleted;
    }

    private constructor() {
        this._localStorageModelSyncStatusChanged = Observable.create((observer: any) => {
            this.localStorageModelSyncStatusChangedObserver = observer;
        });
        this._localStorageModelSyncStatusChanged = this._localStorageModelSyncStatusChanged.share();

        this._localStorageModelDeleted = Observable.create((observer: any) => {
            this.localStorageModelDeletedObserver = observer;
        });
        this._localStorageModelDeleted = this._localStorageModelDeleted.share();

        NetworkConnectivityMonitorHelper.getInstance().onlineOffline.subscribe((_networkState: boolean) => {
            this.attemptToSync(_networkState);
        });

        if (!NetworkConnectivityMonitorHelper.isNoConnection()) {
            this.attemptToSync(true);
        }
    }

    private attemptToSync(_networkState: boolean) {
        if (_networkState && !this.syncInProgress) {
            this.syncInProgress = true;
            LocalStorageHelper.getInstance().
            fetchLocalStorageModels((_allLocalStorageModels: Array<LocalStorageModel>) => {
                let index = 0;

                const doTheCall = (() => {
                            if (_allLocalStorageModels.length === 0)
                                this.syncInProgress = false;
                            else {
                                const localStorageModel: LocalStorageModel = _allLocalStorageModels[index];
                                localStorageModel.localStorageModelSyncStatus =
                                LocalStorageModelSyncStatus.SyncInProgress;
                                this.postLocalStorageModelSyncStatusChangedObserver(localStorageModel);
                                const dataObject: BaseDataObject = localStorageModel.objectToBeLocallyStored;
                                dataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
                                    if (_dataObject.serverErrorDataModel != null) {
                                        localStorageModel.objectToBeLocallyStored = dataObject;
                                        localStorageModel.localStorageModelSyncStatus =
                                        LocalStorageModelSyncStatus.SyncFailed;
                                        LocalStorageHelper.getInstance().updateLocalStorageModel
                                        (localStorageModel, () => {
                                            this.postLocalStorageModelSyncStatusChangedObserver(localStorageModel);
                                            index ++;
                                            if (index >= _allLocalStorageModels.length)
                                                this.syncInProgress = false;
                                             else
                                                doTheCall();
                                        });
                                    } else {
                                        LocalStorageHelper.getInstance().deleteLocalStorageModel(
                                            localStorageModel,
                                            () => {
                                                this.postLocalStorageModelDeletedObserver(localStorageModel);

                                                if (_allLocalStorageModels.indexOf(localStorageModel) !== -1) {
                                                    _allLocalStorageModels.splice
                                                    (_allLocalStorageModels.indexOf(localStorageModel), 1);
                                                }

                                                if (index >= _allLocalStorageModels.length)
                                                    this.syncInProgress = false;
                                                else
                                                    doTheCall();
                                            },
                                        );
                                    }
                                });
                            }
                        });

                doTheCall();
            }, LocalStorageModel.identifierFor_DataObjectType());
        }
    }

    private postLocalStorageModelSyncStatusChangedObserver(_localStorageModel: LocalStorageModel) {
        if (this.localStorageModelSyncStatusChangedObserver != null)
            this.localStorageModelSyncStatusChangedObserver.next(_localStorageModel);
    }

    private postLocalStorageModelDeletedObserver(_localStorageModel: LocalStorageModel) {
        if (this.localStorageModelDeletedObserver != null)
            this.localStorageModelDeletedObserver.next(_localStorageModel);
    }
}
