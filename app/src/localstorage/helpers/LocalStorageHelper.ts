import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { BaseDataModel } from '../../datamodels/BaseDataModel';
import { BaseDataObject } from '../../network/dataobjects/BaseDataObject';
import { AppUIHelper } from '../../helpers/AppUIHelper';
import { ManualDIHelper } from '../../helpers/ManualDIHelper';
import { LocalStorageModel } from '../localstoragemodels/LocalStorageModel';
import { Observable } from 'rxjs/Observable';

export class LocalStorageHelper {
    private static instance: LocalStorageHelper;
    private storage: Storage;
    private _localStorageModelAddedOrDeleted: any;
    private localStorageModelAddedOrDeletedObserver: any;

    static createInstance(storage: Storage) {
        LocalStorageHelper.instance = new LocalStorageHelper(storage);
    }

    static getInstance() {
        return this.instance;
    }

    get localStorageModelAddedOrDeleted(): any {
        return this._localStorageModelAddedOrDeleted;
    }

    storeLocalStorageModel(_localStorageModel: LocalStorageModel, onLocalStorageModelSaved: () => void) {
        this.storage.set(_localStorageModel.key(),
            JSON.stringify(_localStorageModel)).then(() => {
                onLocalStorageModelSaved();
                this.postLocalStorageModelAddedOrDeletedObserver(_localStorageModel);
        });
    }

    updateLocalStorageModel(_localStorageModel: LocalStorageModel, onLocalStorageModelUpdated?: () => void) {
        this.storage.set(_localStorageModel.key(),
            JSON.stringify(_localStorageModel)).then(() => {
                if (onLocalStorageModelUpdated)
                    onLocalStorageModelUpdated();
        });
    }

    fetchLocalStorageModelHavingKey(key: string, onLocalStorageModelFetched: (value: any) => void) {
        this.storage.get(key).then((value) => {
            onLocalStorageModelFetched(value);
        });
    }

    async fetchObjectHavingKey_Async(key: string) {
        return await this.storage.get(key);
    }

    fetchLocalStorageModels
    (onLocalStorageModelsFetched:
        (localStorageModels: Array<LocalStorageModel>) => void, _ofType?: string) {
        const localStorageModels: Array<LocalStorageModel> = [];
        const allPromises: any[] = [];

        this.numberOfLocalStorageModelsStored((numberOfLocalStorageModelsStored) => {
            if (numberOfLocalStorageModelsStored === 0)
                onLocalStorageModelsFetched(localStorageModels);
            else {
                    this.storage.forEach((_value: any, key: any, index: any) => {
                    if (AppUIHelper.isString(key)) {
                        const keyInString = String(key);
                        if (_ofType != null) {
                            if (LocalStorageModel.isLocalStorageModelOfType_DataObject(key)) {
                                allPromises.push(this.fetchObjectHavingKey_Async(key));
                                localStorageModels.push(new LocalStorageModel(null));
                            }
                        }
                    }
                    if (index === numberOfLocalStorageModelsStored) {
                        Promise.all(allPromises).then(arrayOfJSONs => {
                            for (index in arrayOfJSONs) {
                                if (arrayOfJSONs.hasOwnProperty(index)) {
                                    localStorageModels[index].fromJSON(JSON.parse(arrayOfJSONs[index]));
                                }
                            }
                            onLocalStorageModelsFetched(localStorageModels);
                        });
                    }
                });
            }
        });
    }

    numberOfLocalStorageModelsStored(onNumberOfLocalStorageModelsStored:
        (numberOfLocalStorageModelsStored: number) => void) {
        this.storage.length().then((numberOfLocalStorageModelsStored) => {
            onNumberOfLocalStorageModelsStored(numberOfLocalStorageModelsStored);
        });
    }

    allKeys(onAllKeysRetrieved: (allKeys: any[]) => void) {
        this.storage.keys().then((allKeys) => {
            onAllKeysRetrieved(allKeys);
        });
    }

    deleteLocalStorageModel(_localStorageModel: LocalStorageModel, onLocalStorageModelDeleted: () => void) {
        this.storage.remove(_localStorageModel.key()).then(() => {
            onLocalStorageModelDeleted();
            this.postLocalStorageModelAddedOrDeletedObserver(_localStorageModel);
        });
    }

    deleteAllLocalStorageModels(onAllLocalStorageModelsDeleted?: () => void) {
        this.storage.clear().then(() => {
            if (onAllLocalStorageModelsDeleted)
                onAllLocalStorageModelsDeleted();
        });
    }

    private constructor(storage: Storage) {
        this.storage = storage;

        this._localStorageModelAddedOrDeleted = Observable.create((observer: any) => {
            this.localStorageModelAddedOrDeletedObserver = observer;
        });
        this._localStorageModelAddedOrDeleted = this._localStorageModelAddedOrDeleted.share();
    }

    private postLocalStorageModelAddedOrDeletedObserver(_localStorageModel: LocalStorageModel) {
        if (this.localStorageModelAddedOrDeletedObserver != null)
            this.localStorageModelAddedOrDeletedObserver.next(_localStorageModel);
    }
}
