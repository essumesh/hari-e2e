import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Keyboard } from '@ionic-native/keyboard';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpModule, Http } from '@angular/http';

import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from
'@ionic-native/media-capture';
import { VideoEditor } from '@ionic-native/video-editor';
import { AppUIHelper } from '../helpers/AppUIHelper';
import { CameraHelper } from '../helpers/CameraHelper';
import { DatePickerHelper } from '../helpers/DatePickerHelper';
import { DatePicker } from '@ionic-native/date-picker';
import { IonicStorageModule } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { SignaturePadModule } from 'angular2-signaturepad';

import { DamageReportSearchPage } from '../ui/pages/damage-report-search/damage-report-search';
import { DamageReportSearchResultsPage } from '../ui/pages/damage-report-search-results/damage-report-search-results';
import { DamageReportListItemComponent } from '../ui/components/damage-report-list-item/damage-report-list-item';
import { CreateDamageReportPage } from '../ui/pages/create-damage-report/create-damage-report';
import { GenericListItemComponent } from '../ui/components/generic-list-item/generic-list-item';
import { ShipmentDetailsPage } from '../ui/pages/shipment-details/shipment-details';
import { PackagingDetailPage } from '../ui/pages/packaging-details/packaging-details';
import { DamageReportPage } from '../ui/pages/damage-report/damage-report';
import { DamageDiscoveryPage } from '../ui/pages/damage-discovery/damage-discovery';
import { RecuperationPage } from '../ui/pages/recuperation/recuperation';
import { RemarksPage } from '../ui/pages/remarks-and-boxes/remarks-and-boxes';
import { NotificationsPage } from '../ui/pages/notifications/notifications';
import { ExportAcceptancePage } from '../ui/pages/export-acceptance/export-acceptance';
import { FilterCheckboxPopover } from '../ui/pages/filter-checkbox-popover/filter-checkbox-popover';
import { RadioButtonsItemComponent } from '../ui/components/radio-buttons-item/radio-buttons-item';
import { CheckBoxesItemComponent } from '../ui/components/check-boxes-item/check-boxes-item';
import { FilterRadioButtonPopover } from '../ui/pages/filter-radiobutton-popover/filter-radiobutton-popover';
import { ListPopoverPage } from '../ui/pages/list-popover/list-popover';
import { NavigationBarComponent } from '../ui/components/navigation-bar/navigation-bar';
import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';
import { DeliveryTransferPage } from '../ui/pages/deliveries-and-transfers/deliveries-and-transfers';
import { DeliveryConfirmationPage } from '../ui/pages/delivery-confirmation/delivery-confirmation';
import { ChampKeys } from '../ui/pages/pickup-view-list/PipeHelper';
import { PickupTypeListPage } from '../ui/pages/pickup-types-list/pickup-types-list';
import { PickupViewListPage } from '../ui/pages/pickup-view-list/pickup-view-list';
import { DamageReportEmailPage } from '../ui/pages/damage-report-email/damage-report-email';
import { FoundCargoSearchPage } from '../ui/pages/found-cargo-search/found-cargo-search';
import { CreateFoundCargoPage } from '../ui/pages/create-found-cargo/create-found-cargo';
import { SelectOriginPage } from '../ui/pages/found-cargo-select-origin/found-cargo-select-origin';
import { SelectSHCPage } from '../ui/pages/found-cargo-select-shc/found-cargo-select-shc';
import { ExportAcceptanceReportsPage } from '../ui/pages/export-acceptance-reports/export-acceptance-reports';
import { CreateExportAcceptancePage } from '../ui/pages/create-export-acceptance/create-export-acceptance';
import { CreateIrregularityPage } from '../ui/pages/create-irregularities/create-irregularities';
import { IrregularityDetailsPage } from '../ui/pages/irregularities-detail/irregularities-detail';
import { IrregularitySearchResultsPage } from '../ui/pages/irregularity-search-results/irregularity-search-results';
import { IrregularityReportListItemComponent } from
 '../ui/components/irregularity-report-list-item/irregularity-report-list-item';

export function createTranslateLoader(http: Http) {
    return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        MyApp,
        DamageReportSearchPage,
        DamageReportSearchResultsPage,
        DamageReportListItemComponent,
        CreateDamageReportPage,
        GenericListItemComponent,
        IrregularityReportListItemComponent,
        ShipmentDetailsPage,
        PackagingDetailPage,
        DamageReportPage,
        DamageDiscoveryPage,
        RecuperationPage,
        RemarksPage,
        NotificationsPage,
        FilterCheckboxPopover,
        RadioButtonsItemComponent,
        CheckBoxesItemComponent,
        FilterRadioButtonPopover,
        ListPopoverPage,
        NavigationBarComponent,
        DeliveryTransferPage,
        PickupTypeListPage,
        PickupViewListPage,
        ChampKeys,
        DeliveryConfirmationPage,
        DamageReportEmailPage,
        FoundCargoSearchPage,
        CreateFoundCargoPage,
        SelectOriginPage,
        SelectSHCPage,
        ExportAcceptancePage,
        ExportAcceptanceReportsPage,
        CreateExportAcceptancePage,
        CreateIrregularityPage,
        IrregularityDetailsPage,
        IrregularitySearchResultsPage,

    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp, {
            platforms: {
                ios: {
                 statusbarPadding: true,
                },
             },
            }),
        HttpModule,
        TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: (createTranslateLoader),
            deps: [Http],
        },
        }),
        IonicStorageModule.forRoot(),
        SignaturePadModule,
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        DamageReportSearchPage,
        DamageReportSearchResultsPage,
        CreateDamageReportPage,
        ShipmentDetailsPage,
        PackagingDetailPage,
        DamageReportPage,
        DamageDiscoveryPage,
        RecuperationPage,
        RemarksPage,
        NotificationsPage,
        FilterCheckboxPopover,
        FilterRadioButtonPopover,
        ListPopoverPage,
        GenericListItemComponent,
        DeliveryTransferPage,
        PickupTypeListPage,
        PickupViewListPage,
        DeliveryConfirmationPage,
        DamageReportEmailPage,
        FoundCargoSearchPage,
        CreateFoundCargoPage,
        SelectOriginPage,
        SelectSHCPage,
        ExportAcceptancePage,
        ExportAcceptanceReportsPage,
        CreateExportAcceptancePage,
        CreateIrregularityPage,
        IrregularityDetailsPage,
        IrregularitySearchResultsPage,
    ],
    providers: [SplashScreen,
                StatusBar,
                MediaCapture,
                Network,
                CameraHelper,
                DatePicker,
                DatePickerHelper,
                BarcodeScanner,
                VideoEditor,
                ScreenOrientation,
                ImageResizer,
                Keyboard,
    ],
})
export class AppModule {
}
