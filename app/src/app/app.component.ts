import { Component, Injector, ViewChild, Pipe, PipeTransform } from '@angular/core';
import { Platform, Nav, LoadingController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Keyboard } from '@ionic-native/keyboard';

import { DamageReportSearchPage } from '../ui/pages/damage-report-search/damage-report-search';

import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { MediaCapture } from '@ionic-native/media-capture';
import { VideoEditor } from '@ionic-native/video-editor';
import { NgClass } from '@angular/common';
import { ManualDIHelper } from '../helpers/ManualDIHelper';
import { LocalStorageHelper } from '../localstorage/helpers/LocalStorageHelper';
import { NetworkConnectivityMonitorHelper } from '../helpers/NetworkConnectivityMonitorHelper';
import { LocalStorageDataSyncHelper } from '../localstorage/helpers/LocalStorageDataSyncHelper';
import { BarcodeScannerHelper } from '../helpers/BarcodeScannerHelper';
import { CameraHelper } from '../helpers/CameraHelper';
import { DatePickerHelper } from '../helpers/DatePickerHelper';
import { AppUIHelper } from '../helpers/AppUIHelper';
import { KeyboardHelper } from '../helpers/KeyboardHelper';
import { PlatformHelper } from '../helpers/PlatformHelper';
import { AlertController } from 'ionic-angular';
import { PreFetchDataObjectsHelper } from '../network/helpers/PreFetchDataObjectsHelper';
import { ScreenOrientationHelper } from '../helpers/ScreenOrientationHelper';
import { AppMessagePopupHelper } from '../helpers/AppMessagePopupHelper';
import { DatePicker } from '@ionic-native/date-picker';
import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';
import { DeliveryTransferPage } from '../ui/pages/deliveries-and-transfers/deliveries-and-transfers';
import { ExportAcceptancePage } from '../ui/pages/export-acceptance/export-acceptance';
import { TreeBranchDataModel } from '../datamodels/TreeBranchDataModel';
import { GenericListItemViewModel, GenericListItemViewModelType } from '../ui/viewmodels/GenericListItemViewModel';
import { FoundCargoSearchPage } from '../ui/pages/found-cargo-search/found-cargo-search';
import { PageConstants } from '../ui/constants/PageConstants';

@Component({
    templateUrl: `app.html`,
})
export class MyApp {
    @ViewChild('content') nav: Nav;

    rootPage: any = DamageReportSearchPage;

    pages: Array<{title: string, component: any}>;

    private treeBranchTitle: string = '';
    private rootTreeBranchDataModel: TreeBranchDataModel;
    private genericListItemViewModelsToShow: Array<GenericListItemViewModel>;
    private treeBranchDataModelCurrentlyShown: TreeBranchDataModel;
    private toShowBackButton: boolean = false;
    private navigationStackOfMenu: Array<TreeBranchDataModel> = [];

    constructor(public platform: Platform,
                public splashScreen: SplashScreen,
                public statusBar: StatusBar,
                translateService: TranslateService,
                alertController: AlertController,
                storage: Storage,
                network: Network,
                injector: Injector,
                barcodeScanner: BarcodeScanner,
                mediaCapture: MediaCapture,
                videoEditor: VideoEditor,
                screenOrientation: ScreenOrientation,
                loadingController: LoadingController,
                datePicker: DatePicker,
                imageResizer: ImageResizer,
                keyboard: Keyboard) {

        translateService.addLangs(['en']);
        translateService.setDefaultLang('en');
        let browserLang = translateService.getBrowserLang();
        translateService.use(browserLang.match(/en|es/) ? browserLang : 'en');

        AppUIHelper.createInstance(translateService, alertController);
        ManualDIHelper.createInstance(injector);
        LocalStorageHelper.createInstance(storage);
        NetworkConnectivityMonitorHelper.createInstance(network);
        LocalStorageDataSyncHelper.createInstance();
        PreFetchDataObjectsHelper.createInstance();
        CameraHelper.createInstance(mediaCapture, videoEditor, imageResizer);
        ScreenOrientationHelper.createInstance(screenOrientation);
        AppMessagePopupHelper.createInstance(loadingController);
        DatePickerHelper.createInstance(datePicker);
        PlatformHelper.createInstance(platform);
        KeyboardHelper.createInstance(keyboard);
        BarcodeScannerHelper.createInstance(barcodeScanner);

        this.platform.ready().then(() => {
            this.splashScreen.hide();
            this.statusBar.overlaysWebView(false);

            this.pages = [
                { title: AppUIHelper.getInstance().i18nText('damagereport'), component: DamageReportSearchPage },
                { title: AppUIHelper.getInstance().i18nText('menu-delivery'), component: DeliveryTransferPage },
                { title: AppUIHelper.getInstance().i18nText('foundcargo'), component: FoundCargoSearchPage },
            ];

            this.prepareTreeBranches();

            PreFetchDataObjectsHelper.getInstance().startPreFetchingDataObjects();

            BarcodeScannerHelper.getInstance().registerBroadcastReceiver_For_InternalHardwareScanner_For_ZebraDevices();
        });
    }

    openPage(page: any) {
        this.nav.setRoot(page);
    }

    private prepareTreeBranches() {
        this.rootTreeBranchDataModel = new TreeBranchDataModel
        (AppUIHelper.getInstance().i18nText('menu'),
         [new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('menuitemexport'), null),
          new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('menuitemimport'), null),
          new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('menuitemwarehouse'), null),
          new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('menuitemsettings'), null),
          new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('menuitemprint'), null),
          new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('menuitemotherservices'), null)]);

        this.rootTreeBranchDataModel.data[0].data =
        [new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('acceptance'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('security'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('checklist'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('buildup'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('offloadandshortshipped'), null)];

        this.rootTreeBranchDataModel.data[1].data =
        [new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('checkin'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('foundcargo'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('deliveriesandtransfers'), null)];
        this.rootTreeBranchDataModel.data[1].data[2].data =
        [new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('receiptbondtransfer'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('airlinetransfer'), null)];

        this.rootTreeBranchDataModel.data[2].data =
        [new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('bondcheck'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('relocation'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('damage'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('irregularities'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('foundcargo'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('breakdownhouse'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('npr'), null)];

        this.rootTreeBranchDataModel.data[3].data =
        [new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('officelist'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('timezone'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('logout'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('changepassword'), null)];

        this.rootTreeBranchDataModel.data[4].data =
        [new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('labels'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('reports'), null)];

        this.rootTreeBranchDataModel.data[5].data =
        [new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('report'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('additionalservices'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('statusandhistory'), null),
        new TreeBranchDataModel(AppUIHelper.getInstance().i18nText('progressmonitor'), null)];

        this.navigationStackOfMenu.push(this.rootTreeBranchDataModel);
        this.prepareGenericListItemViewModelsToShow(this.rootTreeBranchDataModel);
    }

    private prepareGenericListItemViewModelsToShow(_treeBranchDataModelToShow: TreeBranchDataModel) {
        if (_treeBranchDataModelToShow.data != null) {
            this.treeBranchDataModelCurrentlyShown = _treeBranchDataModelToShow;
            this.toShowBackButton = this.navigationStackOfMenu.length > 1;
            this.treeBranchTitle = this.treeBranchDataModelCurrentlyShown.name;
            const _genericListItemViewModelsToShowTemp: Array<GenericListItemViewModel> =
            new Array<GenericListItemViewModel>();
            for (let _treeBranchDataModel of this.treeBranchDataModelCurrentlyShown.data) {
                const genericListItemViewModel: GenericListItemViewModel =
                new GenericListItemViewModel
                (_treeBranchDataModel.uniqueId,
                 '0',
                 GenericListItemViewModelType.MainText);
                genericListItemViewModel.mainText = _treeBranchDataModel.name;
                if (_treeBranchDataModel.data != null) {
                    genericListItemViewModel.shouldShowRightArrow = true;
                } else {
                    genericListItemViewModel.shouldTakeActionForHamBurgerMenuClose = true;
                }
                _genericListItemViewModelsToShowTemp.push(genericListItemViewModel);
            }
            this.genericListItemViewModelsToShow = _genericListItemViewModelsToShowTemp;
        }
    }

    private tapEvent_On_GenericListItemComponent(_genericListItemViewModel: GenericListItemViewModel) {
        for (let _treeBranchDataModel of this.treeBranchDataModelCurrentlyShown.data) {
            if (_treeBranchDataModel.data != null) {
                if (_treeBranchDataModel.name === _genericListItemViewModel.mainText) {
                    this.navigationStackOfMenu.push(_treeBranchDataModel);
                    this.prepareGenericListItemViewModelsToShow(_treeBranchDataModel);
                    return;
                }
            }
        }

        if (_genericListItemViewModel.mainText === AppUIHelper.getInstance().i18nText('damage') ||
        _genericListItemViewModel.mainText === AppUIHelper.getInstance().i18nText('irregularities')) {
            PageConstants.getScreenID(_genericListItemViewModel.mainText);
            this.openPage(DamageReportSearchPage);
        } else if (_genericListItemViewModel.mainText === AppUIHelper.getInstance().i18nText('receiptbondtransfer') ||
         _genericListItemViewModel.mainText === AppUIHelper.getInstance().i18nText('airlinetransfer')) {
            PageConstants.getScreenID(_genericListItemViewModel.mainText);
            this.openPage(DeliveryTransferPage);
        } else if (_genericListItemViewModel.mainText === AppUIHelper.getInstance().i18nText('foundcargo')) {
            this.openPage(FoundCargoSearchPage);
        } else if (_genericListItemViewModel.mainText === AppUIHelper.getInstance().i18nText('acceptance')) {
            this.openPage(ExportAcceptancePage);
        }
    }

    private onBackButtonTap() {
        this.navigationStackOfMenu.pop();
        this.prepareGenericListItemViewModelsToShow(this.navigationStackOfMenu[this.navigationStackOfMenu.length - 1]);
      }
}
