import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Observable } from 'rxjs/Observable';
import { PlatformHelper } from './PlatformHelper';

export enum ScreenOrientationType {
    PORTRAIT,
    LANDSCAPE,
}

export class ScreenOrientationHelper {
    private static instance: ScreenOrientationHelper;
    private screenOrientation: ScreenOrientation;
    private _screenOrientationChange: any;
    private screenOrientationChangeObserver: any;

    static createInstance(screenOrientation: ScreenOrientation) {
        ScreenOrientationHelper.instance = new ScreenOrientationHelper(screenOrientation);
    }

    static getInstance() {
        return this.instance;
    }

    static currentScreenOrientation(): ScreenOrientationType {
        return ScreenOrientationHelper.instance.screenOrientation.type.startsWith('p')
               ? ScreenOrientationType.PORTRAIT
               : ScreenOrientationType.LANDSCAPE;
    }

    static lock_ScreenOrientation_To_Portrait() {
        ScreenOrientationHelper.instance.screenOrientation.lock
        (ScreenOrientationHelper.instance.screenOrientation.ORIENTATIONS.PORTRAIT);
    }

    static lock_ScreenOrientation_To_Landscape() {
        ScreenOrientationHelper.instance.screenOrientation.lock
        (ScreenOrientationHelper.instance.screenOrientation.ORIENTATIONS.LANDSCAPE);
    }

    static unlock_ScreenOrientation() {
        ScreenOrientationHelper.instance.screenOrientation.unlock();
    }

    get screenOrientationChange(): any {
        return this._screenOrientationChange;
    }

    private constructor(screenOrientation: ScreenOrientation) {
        this.screenOrientation = screenOrientation;
        this.registerForOrientationChange();
    }

    private registerForOrientationChange() {
        this._screenOrientationChange = Observable.create((observer: any) => {
            this.screenOrientationChangeObserver = observer;
        });
        this._screenOrientationChange = this._screenOrientationChange.share();

        this.screenOrientation.onChange().subscribe(
            () => {
                if (this.screenOrientationChangeObserver != null) {
                    setTimeout(() => {
                        const screenOrientationType = this.screenOrientation.type;
                        if (screenOrientationType != null) {
                            this.screenOrientationChangeObserver.next
                            (screenOrientationType.indexOf('portrait') >= 0 ? ScreenOrientationType.PORTRAIT :
                             ScreenOrientationType.LANDSCAPE);
                        }
                      }, 200);
                }
            },
         );
    }
}
