import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PlatformHelper } from '../helpers/PlatformHelper';

@Injectable()
export class BarcodeScannerHelper {
    private static instance: BarcodeScannerHelper;
    private barcodeScanner: BarcodeScanner;
    private _barCodeForInternalHardwareScannerForZebraDevices: any;
    private _barCodeForInternalHardwareScannerForZebraDevicesObserver: any;

    static createInstance(barcodeScanner: BarcodeScanner) {
        BarcodeScannerHelper.instance = new BarcodeScannerHelper(barcodeScanner);
    }

    static getInstance() {
        return this.instance;
    }

    scan(successCallBack: (barcodeDataText: string) => void, errorCallBack: (errorText: string) => void) {
     this.barcodeScanner.scan().then((barcodeScanResult) => {
          if (!barcodeScanResult.cancelled) {
                    successCallBack(barcodeScanResult.text);
                }
        }, (err) => {
          errorCallBack(err);
        });
    }

    get barCodeForInternalHardwareScannerForZebraDevices(): any {
        return this._barCodeForInternalHardwareScannerForZebraDevices;
    }

    // To be called only Once
    registerBroadcastReceiver_For_InternalHardwareScanner_For_ZebraDevices() {
        if (!PlatformHelper.isDevice_A_Desktop() && PlatformHelper.isPlatform_Android()) {
            (<any>window).plugins.intentShim.registerBroadcastReceiver({
                filterActions: [
                    'com.zebra.datawedgecordova.ACTION',
                    ],
                }, (_intent: any) => {
                    if (_intent.extras['com.symbol.datawedge.data_string'] != null) {
                        if (this._barCodeForInternalHardwareScannerForZebraDevicesObserver != null)
                            this._barCodeForInternalHardwareScannerForZebraDevicesObserver.next
                            (_intent.extras['com.symbol.datawedge.data_string']);
                    }
                },
            );
        }
    }

    private constructor(barcodeScanner: BarcodeScanner) {
        this.barcodeScanner = barcodeScanner;
        this.registerBarCodeForInternalHardwareScannerForZebraDevices_For_InternalHardwareScanner_For_ZebraDevices();
    }

    private registerBarCodeForInternalHardwareScannerForZebraDevices_For_InternalHardwareScanner_For_ZebraDevices() {
        this._barCodeForInternalHardwareScannerForZebraDevices =
        Observable.create((observer: any) => {
            this._barCodeForInternalHardwareScannerForZebraDevicesObserver = observer;
        });
        this._barCodeForInternalHardwareScannerForZebraDevices =
        this._barCodeForInternalHardwareScannerForZebraDevices.share();
    }
}
