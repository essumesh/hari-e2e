import { Component } from '@angular/core';
import { DatePicker } from '@ionic-native/date-picker';
import { Injectable } from '@angular/core';

@Injectable()
export class DatePickerHelper {

    private static instance: DatePickerHelper;
    private datePicker: DatePicker;

    static createInstance(datePicker: DatePicker) {
        DatePickerHelper.instance = new DatePickerHelper(datePicker);
    }

    static getInstance() {
        return this.instance;
    }

    showDatePicker(successCallBack: (date: Date) => void, errorCallBack: (errorText: string) => void) {
        this.datePicker.show({
                    date: new Date(),
                    mode: 'date',
                }).then(
                    date => {
                        successCallBack(date);
                    },
                    err => {
                        errorCallBack(err);
                    },
                );
    }

    constructor(datePicker: DatePicker) {
        this.datePicker = datePicker;
    }
}
