import { Keyboard } from '@ionic-native/keyboard';
import { Observable } from 'rxjs/Observable';

export class KeyboardHelper {
    private static instance: KeyboardHelper;
    private keyboard: Keyboard;
    private _showHide: any;
    private showHideObserver: any;
    private _isKeyboardShown: boolean = false;

    static createInstance(keyboard: Keyboard) {
        KeyboardHelper.instance = new KeyboardHelper(keyboard);
    }

    static getInstance() {
        return this.instance;
    }

    get showHide(): any {
        return this._showHide;
    }

    get isKeyboardShown(): boolean {
        return this._isKeyboardShown;
    }

    showKeyboard() {
        this.keyboard.show();
    }

    hideKeyboard() {
        this.keyboard.close();
    }

    constructor(_keyboard: Keyboard) {
        this.keyboard = _keyboard;

        this._showHide = Observable.create((observer: any) => {
            this.showHideObserver = observer;
        });

        this._showHide = this._showHide.share();

        let context: any = this;
        window.addEventListener('native.keyboardshow', (_e: any) => {
            this._isKeyboardShown = true;
            if (context.showHideObserver != null) {
                context.showHideObserver.next({showing: true, height: _e.keyboardHeight});
            }
        });

        window.addEventListener('native.keyboardhide', (_e: any) => {
            this._isKeyboardShown = false;
            if (context.showHideObserver != null) {
                context.showHideObserver.next({
                    showing: false,
                });
            }
        });
    }
}
