import * as moment from 'moment';

export class DateParserHelper {
    private static instance: DateParserHelper;

    static createInstance() {
        DateParserHelper.instance = new DateParserHelper();
    }

    static getInstance() {
        return this.instance;
    }

    static parseStringToDate(_dateString: string): Date {
        if (_dateString != null) {
            let date = new Date(_dateString);
            return date;
        }
        return null;
    }

    static parseDateToString(_date: Date): string {
        if (_date != null) {
            return _date.toDateString();
        }
        return null;
    }

    // Monday
    static parseDateToString_InFormat_FullDayOfWeek(_date: Date): string {
        let _dateString = moment(_date).format('dddd');
        return _dateString;
    }

    // Mon
    static parseDateToString_InFormat_ShortDayOfWeek(_date: Date): string {
        let _dateString = DateParserHelper.parseDateToString_InFormat_FullDayOfWeek(_date);
        _dateString = _dateString.substring(0, 3);
        return _dateString;
    }

    // 12Nov17
    static parseDateToString_InFormat_ddMMMyy(_date: Date): string {
        let _dateString = moment(_date).format('DDMMMYY');
        return _dateString;
    }

    // 12Nov17
    static parseDateToString_InFormat_ddMMMyy_UTC(_date: Date): string {
        let _dateString = moment.utc(_date).format('DDMMMYY');
        return _dateString;
    }

    // 6:04
    static parseDateToString_InFormat_hmm(_date: Date): string {
        let _dateString = moment(_date).format('h:mm');
        return _dateString;
    }

    // 16:04
    static parseDateToString_InFormat_HHmm(_date: Date): string {
        let _dateString = moment(_date).format('HH:mm');
        return _dateString;
    }

    // 16:04
    static parseDateToString_InFormat_HHmm_UTC(_date: Date): string {
        let _dateString = moment.utc(_date).format('HH:mm');
        return _dateString;
    }

    static parseDateToString_InFormat_Network(_date: Date): string {
        // return moment(_date).format('YYYY-MM-DDTHH:mm:ss');
        return moment.utc(_date).format('YYYY-MM-DDTHH:mm:ss');
    }

    static difference_Between_TwoDates(_endDate: Date, _startDate: Date): number {
        const duration = moment.duration(moment(_endDate).diff(moment(_startDate)));
        return duration.asSeconds();
    }

    private constructor() {
    }
}
