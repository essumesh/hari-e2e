import { ReflectiveInjector, Injector } from '@angular/core';
import { FetchAWBDamageQualifiersDataObject } from '../network/dataobjects/FetchAWBDamageQualifiersDataObject';
import { FetchAWBDataObject } from '../network/dataobjects/FetchAWBDataObject';
import { FetchHWBDataObject } from '../network/dataobjects/FetchHWBDataObject';
import { FetchDamageReportsDataObject } from '../network/dataobjects/FetchDamageReportsDataObject';
import { AWBDamageAPIDataObject } from '../network/dataobjects/AWBDamageAPIDataObject';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { FileTransferDataObject } from '../network/dataobjects/FileTransferDataObject';
import { FetchPickUpDeliveryDataObject } from '../network/dataobjects/FetchPickUpDeliveryDataObject';
import { PickupDriverInfoDataObject } from '../network/dataobjects/PickupDriverInfoDataObject';
import { PickupDeliveryReceiptDataObject } from '../network/dataobjects/PickupDeliveryReceiptDataObject';
import { AWBEmailDataObject } from '../network/dataobjects/AWBEmailDataObject';
import { FoundCargoSHCDataObject } from '../network/dataobjects/FoundCargoSHCDataObject';
import { FoundCargoOrignDataObject } from '../network/dataobjects/FoundCargoOrignDataObject';
import { FoundCargoUnKnownAWBDataObject } from '../network/dataobjects/FoundCargoUnKnownAWBDataObject';
import { CreateFoundCargoDataObject } from '../network/dataobjects/CreateFoundCargoDataObject';
import { IrregularityQualifierDataObject } from '../network/dataobjects/IrregularityQualifierDataObject';
import { IrregularityApiDataObject } from '../network/dataobjects/IrregularityApiDataObject';
import { FetchIrregularityDataObject } from '../network/dataobjects/FetchIrregularityDataObject';
import { FetchAirlineTransferDataObject } from '../network/dataobjects/FetchAirlineTransferDataObject';

export class ManualDIHelper {
    private static instance: ManualDIHelper;
    private injector: Injector;

    static createInstance(injector: Injector) {
        ManualDIHelper.instance = new ManualDIHelper(injector);
    }

    static getInstance() {
        return this.instance;
    }

    createObject(_className: string): any {
        if (_className === 'FetchAWBDamageQualifiersDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([FetchAWBDamageQualifiersDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(FetchAWBDamageQualifiersDataObject);
        } else if (_className === 'FetchAWBDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([FetchAWBDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(FetchAWBDataObject);
        } else if (_className === 'FetchHWBDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([FetchHWBDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(FetchHWBDataObject);
        } else if (_className === 'FetchDamageReportsDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([FetchDamageReportsDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(FetchDamageReportsDataObject);
        } else if (_className === 'AWBDamageAPIDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([AWBDamageAPIDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(AWBDamageAPIDataObject);
        } else if (_className === 'FileTransfer') {
            let resolvedProviders = ReflectiveInjector.resolve([FileTransfer]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(FileTransfer);
        } else if (_className === 'File') {
            let resolvedProviders = ReflectiveInjector.resolve([File]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(File);
        } else if (_className === 'FileTransferDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([FileTransferDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(FileTransferDataObject);
        }else if (_className === 'FetchPickUpDeliveryDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([FetchPickUpDeliveryDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(FetchPickUpDeliveryDataObject);
        }else if (_className === 'PickupDriverInfoDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([PickupDriverInfoDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(PickupDriverInfoDataObject);
        }else if (_className === 'PickupDeliveryReceiptDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([PickupDeliveryReceiptDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(PickupDeliveryReceiptDataObject);
        }else if (_className === 'AWBEmailDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([AWBEmailDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(AWBEmailDataObject);
        }else if (_className === 'FoundCargoSHCDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([FoundCargoSHCDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(FoundCargoSHCDataObject);
        }else if (_className === 'FoundCargoOrignDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([FoundCargoOrignDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(FoundCargoOrignDataObject);
        }else if (_className === 'FoundCargoUnKnownAWBDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([FoundCargoUnKnownAWBDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(FoundCargoUnKnownAWBDataObject);
        }else if (_className === 'CreateFoundCargoDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([CreateFoundCargoDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(CreateFoundCargoDataObject);
        }else if (_className === 'IrregularityQualifierDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([IrregularityQualifierDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(IrregularityQualifierDataObject);
        }else if (_className === 'IrregularityApiDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([IrregularityApiDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(IrregularityApiDataObject);
        }else if (_className === 'FetchIrregularityDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([FetchIrregularityDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(FetchIrregularityDataObject);
        }else if (_className === 'FetchAirlineTransferDataObject') {
            let resolvedProviders = ReflectiveInjector.resolve([FetchAirlineTransferDataObject]);
            let childInjector = ReflectiveInjector.fromResolvedProviders(resolvedProviders, this.injector);
            return childInjector.get(FetchAirlineTransferDataObject);
        }

        return null;
    }

    private constructor(injector: Injector) {
        this.injector = injector;
    }
}
