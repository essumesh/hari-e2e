import { Component } from '@angular/core';
import { AppMessagePopupViewModel, AppMessagePopupViewModelType } from '../ui/viewmodels/AppMessagePopupViewModel';
import { LoadingController } from 'ionic-angular';
import { AppUIHelper } from './AppUIHelper';

export class AppMessagePopupHelper {
  private static instance: AppMessagePopupHelper;
  private loadingController: LoadingController;
  private loadingWindow: any;

  static createInstance(loadingContoller: LoadingController) {
      AppMessagePopupHelper.instance = new AppMessagePopupHelper(loadingContoller);
  }

  static getInstance() {
      return this.instance;
  }

  static showLoadingAppMessagePopup(text: string) {
    let appMessagePopupViewModel: AppMessagePopupViewModel =
    new AppMessagePopupViewModel(text, '', AppMessagePopupViewModelType.LoadingPopup);
    AppMessagePopupHelper.getInstance().showAppMessagePopup(appMessagePopupViewModel);
  }

  // _timeOut :: In Seconds
  showAppMessagePopup(appMessagePopupViewModel: AppMessagePopupViewModel, _timeOut?: number) {
    let content: string;
    let spinner: string;

    if (appMessagePopupViewModel.appMessagePopupViewModelType ===
        AppMessagePopupViewModelType.LoadingPopup) {
           content = `<div>` + appMessagePopupViewModel.text  + `</div>`;
           spinner = 'crescent';
    } else if (appMessagePopupViewModel.appMessagePopupViewModelType ===
              AppMessagePopupViewModelType.MessagePopup) {
        const imageContent = appMessagePopupViewModel.imagePath != null
                             ? `<div class="appimg"><img src=` + appMessagePopupViewModel.imagePath +
                              ` width=40px; height=25px;/></div>`
                             : null;
        const textContent = `<div class="appmessage">`
         + appMessagePopupViewModel.text  + `</div>`;

        // content = `<div><img src=` + appMessagePopupViewModel.imagePath + ` />`
        //             + `<div>` + appMessagePopupViewModel.text  + `</div></div>`;
        content = imageContent != null
                  ? imageContent + textContent
                  : textContent;
        spinner = 'hide';
    }

    this.loadingWindow = this.loadingContoller.create({
        spinner: spinner,
        content: content,
        cssClass: 'appmessage',
    });

    this.loadingWindow.onDidDismiss(() => {
    });

    this.loadingWindow.present();

    if (_timeOut != null) {
        setTimeout(() => {
            AppMessagePopupHelper.getInstance().hideAppMessagePopup();
        }, _timeOut * 1000);
    }
  }

  hideAppMessagePopup() {
     this.loadingWindow.dismiss();
  }

  constructor(public loadingContoller: LoadingController) {
    this.loadingContoller = loadingContoller;
  }
}
