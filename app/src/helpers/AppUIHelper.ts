import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AlertController } from 'ionic-angular';
import cloneDeep from 'lodash/cloneDeep';
import { MediaFile } from '@ionic-native/media-capture';

export class AppUIHelper {
    private static instance: AppUIHelper;
    private translateService: TranslateService;
    private alertController: AlertController;

    static createInstance(translateService: TranslateService,
                          alertController: AlertController) {
        AppUIHelper.instance = new AppUIHelper(translateService,
                                               alertController);
    }

    static getInstance() {
        return this.instance;
    }

    static guid() {
        /* tslint:disable:no-bitwise */
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            let r = Math.random() * 16 | 0, v = c === 'x' ? r : ( r & 0x3 | 0x8 );
            return v.toString(16);
        });
        /* tslint:enable:no-bitwise */
    }

    static isString(s: any) {
        return typeof(s) === 'string' || s instanceof String;
    }

    static isDigit(s: any): boolean {
        return isFinite(s);
    }

    static deepClone(_object: any): any {
        return cloneDeep(_object);
    }

    static prependZeroToThisNumberIfRequired(_value: number): string {
        return ((_value >= 1 && _value < 10) ? '0' + _value : '' + _value);
    }

    static isNaN(value: any): boolean {
        return Number.isNaN(value);
    }

    static isValidAWBNumber(awbPrefix: string, awbSerial: string): boolean {
        return (awbPrefix != null
            && awbPrefix.length === 3
            && awbSerial != null
            && awbSerial.length === 8
            && (Number(awbSerial.substring(0, 7)) % 7 ===
            Number(awbSerial.substring(7, 8))));
    }

    i18nText(i18nTextKey: string): string {
        return this.translateService.instant(i18nTextKey);
    }

    i18nTextWithCallBack(i18nTextKey: string, _i18nTextValueCallBack: (i18nTextValue: string) => void) {
        this.translateService.get(i18nTextKey).subscribe(data => _i18nTextValueCallBack(data));
    }

    showAlert(title: string, subTitle: string, buttons: Array<any>) {
        this.alertController.create({
            title: title,
            subTitle: subTitle,
            buttons: buttons}).present();
    }

    // 'limit' :: In Bytes
    isMediaFileSizeLessThanLimit(mediaFile: MediaFile, limit: number): boolean {
        return mediaFile.size < limit;
    }

    private constructor(translateService: TranslateService,
                        alertController: AlertController) {
        this.translateService = translateService;
        this.alertController = alertController;
    }
}
