import { AppUIHelper } from './AppUIHelper';

export enum BarcodeType {
    AWB,
    HWB,
    NOT_KNOWN,
}

export class BarcodeParserHelper {
    static barcodeType(_barcodeValue: string): BarcodeType {
        // To read "950-123456745"
        if (_barcodeValue.match(/^\d{3}\-\d{8}/)) {
            return BarcodeType.AWB;
        // To read "950123456745"
        } else if (_barcodeValue.match(/^\d{11}/)) {
            return BarcodeType.AWB;
        // To read "LHR 950-00100012"
        } else if (_barcodeValue.match(/^\w{3}\s\d{3}\-\d{8}/)) {
            return BarcodeType.AWB;
        } else if (_barcodeValue.length === 11 && AppUIHelper.isDigit(_barcodeValue)) {
            return BarcodeType.AWB;
        // To read "HWB001...... HWB alphanumeric"
        } else if (_barcodeValue.match(/^H\w{5,}/)) {
            return BarcodeType.HWB;
        } else if (_barcodeValue.length === 11) {
            return BarcodeType.HWB;
        }
        return BarcodeType.NOT_KNOWN;
    }

    static barcodeValues(_barcodeValue: string, _barcodeType: BarcodeType): (string)[] {
        if (_barcodeType === BarcodeType.AWB) {
            if (_barcodeValue.match(/^\d{3}\-\d{8}/)) {
                // To return 950,123456745 if it matches "950-123456745"
                return [_barcodeValue.substring(0, 3), _barcodeValue.substring(4, 12)];
            } else if (_barcodeValue.match(/^\d{11}/)) {
                // To return 950,123456745 if it matches "950123456745"
                return [_barcodeValue.substring(0, 3), _barcodeValue.substring(3, 11)];
            } else if (_barcodeValue.match(/^\w{3}\s\d{3}\-\d{8}/)) {
                // To return 950,123456745 if it matches "LHR 950-123456745"
                return [_barcodeValue.substring(4, 7), _barcodeValue.substring(8, 16)];
            }
            return [_barcodeValue.substring(0, 3), _barcodeValue.substring(3, 11)];
            } else if (_barcodeType === BarcodeType.HWB) {
            if (_barcodeValue.match(/^H\w{5,}/)) {
                // To return "HWB001........" if it matches "HHWB001........"
                return [_barcodeValue.substring(1)];
            }
    }
        return [];
    }
}
