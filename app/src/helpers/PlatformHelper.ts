import { Platform } from 'ionic-angular';

export class PlatformHelper {
    private static instance: PlatformHelper;
    private _platform: Platform;

    static createInstance(platform: Platform) {
        PlatformHelper.instance = new PlatformHelper(platform);
    }

    static getInstance() {
        return this.instance;
    }

    static isPlatform_iOS() {
        return PlatformHelper.instance._platform.is('ios');
    }

    static isPlatform_Android() {
        return PlatformHelper.instance._platform.is('android');
    }

    static isPlatform_Windows() {
        return PlatformHelper.instance._platform.is('windows');
    }

    static isDevice_A_Desktop() {
        return PlatformHelper.instance._platform.is('mobileweb');
    }

    static isDevice_A_Phone() {
        return PlatformHelper.instance._platform.is('mobile');
    }

    static isDevice_A_Tablet() {
        return PlatformHelper.instance._platform.is('tablet');
    }

    static isDevice_A_Phablet() {
        return PlatformHelper.instance._platform.is('phablet');
    }

    static screenHeight(): number {
        return PlatformHelper.instance._platform.height();
    }

    static screenWidth(): number {
        return PlatformHelper.instance._platform.width();
    }

    static isDevice_A_iPad() {
        return PlatformHelper.instance._platform.is('ipad');
    }

    static isOrientation_Portrait() {
        return PlatformHelper.instance._platform.isPortrait();
    }

    static isOrientation_Landscape() {
        return PlatformHelper.instance._platform.isLandscape();
    }

    get platform(): Platform {
        return this._platform;
    }

    private constructor(platform: Platform) {
        this._platform = platform;
    }
}
