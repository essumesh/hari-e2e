import { Component } from '@angular/core';
// import { Camera, CameraOptions } from '@ionic-native/camera';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from
'@ionic-native/media-capture';
import { Injectable } from '@angular/core';
import { VideoEditor, CreateThumbnailOptions } from '@ionic-native/video-editor';
import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';
import { PlatformHelper } from './PlatformHelper';

@Injectable()
export class CameraHelper {
    private static instance: CameraHelper;
    private mediaCapture: MediaCapture;
    private videoEditor: VideoEditor;
    private imageResizer: ImageResizer;

    static createInstance(mediaCapture: MediaCapture,
                          videoEditor: VideoEditor,
                          imageResizer: ImageResizer) {
        CameraHelper.instance = new CameraHelper(mediaCapture,
                                                 videoEditor,
                                                imageResizer);
    }

    static getInstance() {
        return this.instance;
    }

    captureImage(toResizeToOptimum: boolean,
                 successCallBack: (mediaFile: MediaFile, reSizedImageFullPath: string) => void,
                 errorCallBack: (errorText: string) => void) {
        // Resize is not working in iOS
        if (PlatformHelper.isPlatform_iOS()) {
            toResizeToOptimum = false;
        }

        let reSizedImageFullPath: string = null;
        const imageResizerSuccess = ((mediaFileArray: MediaFile[], _filePath: string) => {
            successCallBack(mediaFileArray[0], _filePath);
        });
        const imageResizerFailure = ((mediaFileArray: MediaFile[], _e: any) => {
            successCallBack(mediaFileArray[0], null);
        });

        let options: CaptureImageOptions = { limit: 1 };
        this.mediaCapture.captureImage(options)
        .then(
            (mediaFileArray: MediaFile[]) => {
                if (toResizeToOptimum) {
                    let imageResizerOptions = {
                        uri: mediaFileArray[0].fullPath,
                        quality: 20,
                        width: 500,
                        height: 500,
                    } as ImageResizerOptions;

                    this.imageResizer
                        .resize(imageResizerOptions)
                        .then((_filePath: string) => imageResizerSuccess(mediaFileArray, _filePath))
                        .catch((_e: any) => imageResizerFailure(mediaFileArray, _e));
                } else
                    successCallBack(mediaFileArray[0], null);
            },
            (captureError: CaptureError) => {
                errorCallBack(captureError.code);
            },
        );
    }

    captureVideo(successCallBack: (mediaFile: MediaFile) => void, errorCallBack: (errorText: string) => void) {
        let options: CaptureImageOptions = { limit: 1 };
        this.mediaCapture.captureVideo(options)
        .then(
            (mediaFileArray: MediaFile[]) => {
                successCallBack(mediaFileArray[0]);
            },
            (captureError: CaptureError) => {
                errorCallBack(captureError.code);
            },
        );
    }

    createThumbnailForAVideoFile(mediaFile: MediaFile,
                           width: number,
                           height: number,
                           successCallBack: (thumbNailURL: string) => void,
                           errorCallBack: (errorText: string) => void) {
        const thumbnailOptions: CreateThumbnailOptions = {
            fileUri: mediaFile.fullPath,
            outputFileName: mediaFile.name + (new Date).getTime(),
            width: width,
            height: height,
            quality: 100,
        };
        this.videoEditor.createThumbnail(thumbnailOptions)
            .then((thumbNailURL: string) => {
                successCallBack(thumbNailURL);
            }, (err) => {
                errorCallBack(err);
            });
    }

    constructor(mediaCapture: MediaCapture,
                videoEditor: VideoEditor,
                imageResizer: ImageResizer) {
        this.mediaCapture = mediaCapture;
        this.videoEditor = videoEditor;
        this.imageResizer = imageResizer;
    }
}
