import { Network } from '@ionic-native/network';
import { Observable } from 'rxjs/Observable';

export class NetworkConnectivityMonitorHelper {
    private static instance: NetworkConnectivityMonitorHelper;
    private _onlineOffline: any;
    private onlineOfflineObserver: any;
    private network: Network;

    static createInstance(network: Network) {
        NetworkConnectivityMonitorHelper.instance = new NetworkConnectivityMonitorHelper(network);
    }

    static getInstance() {
        return this.instance;
    }

    static isNoConnection(): boolean {
        return (this.instance.network.type === 'none');
    }

    get onlineOffline(): any {
        return this._onlineOffline;
    }

    private constructor(network: Network) {
        this.network = network;

        this._onlineOffline = Observable.create((observer: any) => {
            this.onlineOfflineObserver = observer;
        });
        this._onlineOffline = this._onlineOffline.share();

        this.network.onConnect().subscribe(() => {
            if (this.onlineOfflineObserver != null)
                this.onlineOfflineObserver.next(true);
        });

        this.network.onDisconnect().subscribe(() => {
            if (this.onlineOfflineObserver != null)
                this.onlineOfflineObserver.next(false);
        });
    }
}
