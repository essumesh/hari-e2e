import { BaseRequestObject } from './BaseRequestObject';
import { ContainersAcceptanceDataModel } from '../../datamodels/ContainersAcceptanceDataModel';

export class ContainersAcceptanceRequestObject extends BaseRequestObject {
    id: string;
    containersAcceptanceDataModel: ContainersAcceptanceDataModel;

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['containersAcceptanceDataModel'] = JSON.stringify(this.containersAcceptanceDataModel);
        return jsonData;
    }

    fromJSON(_json: any) {
        super.fromJSON(_json);
        this.containersAcceptanceDataModel = new ContainersAcceptanceDataModel();
        this.containersAcceptanceDataModel.fromJSON(JSON.parse(_json.containersAcceptanceDataModel));
    }
}
