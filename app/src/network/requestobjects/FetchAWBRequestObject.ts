import { BaseRequestObject } from './BaseRequestObject';

export class FetchAWBRequestObject extends BaseRequestObject {
    airwaybillPrefix: string;
    airwaybillSerial: string;
}
