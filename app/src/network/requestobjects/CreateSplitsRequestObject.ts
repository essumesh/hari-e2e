import { BaseRequestObject } from './BaseRequestObject';
import { SplitsDataModel } from '../../datamodels/SplitsDataModel';

export class CreateSplitsRequestObject extends BaseRequestObject {
    splits: SplitsDataModel;

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['splits'] = JSON.stringify(this.splits);
        return jsonData;
    }

    fromJSON(_json: any) {
        super.fromJSON(_json);
        this.splits = new SplitsDataModel();
        this.splits.fromJSON(JSON.parse(_json.parts));
    }
}
