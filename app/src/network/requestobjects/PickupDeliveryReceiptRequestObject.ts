import { BaseRequestObject } from './BaseRequestObject';
import { PickupSignatureDataModel } from '../../datamodels/PickupSignatureDataModel';

export class PickupDeliveryReceiptRequestObject extends BaseRequestObject {
    deliveryReceiptID: string;
    pickupSignatureDataModel: PickupSignatureDataModel;

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['pickupSignatureDataModel'] = JSON.stringify(this.pickupSignatureDataModel);
        return jsonData;
    }

    fromJSON(_json: any) {
        super.fromJSON(_json);
        this.pickupSignatureDataModel = new PickupSignatureDataModel();
        this.pickupSignatureDataModel.fromJSON(JSON.parse(_json.pickupSignatureDataModel));
    }
}
