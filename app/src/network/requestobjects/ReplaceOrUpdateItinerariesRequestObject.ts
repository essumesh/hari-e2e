import { BaseRequestObject } from './BaseRequestObject';
import { ItinerariesDataModel } from '../../datamodels/ItinerariesDataModel';

export class ReplaceOrUpdateItinerariesRequestObject extends BaseRequestObject {
    id: string;
    itineraries: ItinerariesDataModel;
}
