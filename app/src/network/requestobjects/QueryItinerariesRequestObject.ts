import { BaseRequestObject } from './BaseRequestObject';

export class QueryItinerariesRequestObject extends BaseRequestObject {
    accept: string;
    id: string;
    segment: number;
    originid: string;
    origincode: string;
    origintype: string;
    destinationid: string;
    destinationcode: string;
    destinationtype: string;
    page: number;
    sort: Array<string>;
}
