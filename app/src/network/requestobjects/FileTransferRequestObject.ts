import { BaseRequestObject } from './BaseRequestObject';

export enum FileTransferType {
    UPLOAD,
    DONWLOAD,
    DELETE,
}

export enum FileTransferCategory {
    IMAGE,
    VIDEO,
}

export class FileTransferRequestObject extends BaseRequestObject {
    fileTransferType: FileTransferType = FileTransferType.UPLOAD;
    deviceFileURL: string;
    serverURLToTalkTo: string;
    fileNameWithExtension: string;
    fileTransferCategory: FileTransferCategory.IMAGE;
}
