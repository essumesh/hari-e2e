import { BaseRequestObject } from './BaseRequestObject';
import { SplitsDataModel } from '../../datamodels/SplitsDataModel';

export class ReplaceOrUpdateSplitsRequestObject extends BaseRequestObject {
    id: string;
    splits: SplitsDataModel;
}
