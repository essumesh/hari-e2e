import { BaseRequestObject } from './BaseRequestObject';

export class QueryLocationsRequestObject extends BaseRequestObject {
    accept: string;
    id: string;
    code: string;
    type: string;
    page: number;
    sort: Array<string>;
}
