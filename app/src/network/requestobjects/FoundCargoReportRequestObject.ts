import { BaseRequestObject } from './BaseRequestObject';
import { FoundCargoDataModel } from '../../datamodels/FoundCargoDataModel';

export class FoundCargoReportRequestObject extends BaseRequestObject {
    awbNumber: string;
    foundCargoDataModel: FoundCargoDataModel;

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['foundCargoDataModel'] = JSON.stringify(this.foundCargoDataModel);
        return jsonData;
    }

    fromJSON(_json: any) {
        super.fromJSON(_json);
        this.foundCargoDataModel = new FoundCargoDataModel();
        this.foundCargoDataModel.fromJSON(JSON.parse(_json.foundCargoDataModel));
    }
}
