import { BaseRequestObject } from './BaseRequestObject';

export class QuerySplitsRequestObject extends BaseRequestObject {
    accept: string;
    id: string;
    airwaybillid: string;
    airwaybillPrefix: string;
    airwaybillSerial: string;
    houseAirwaybillid: string;
    houseAirwaybillNumber: string;
    partid: string;
    page: number;
    sort: Array<string>;
}
