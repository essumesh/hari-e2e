import { BaseRequestObject } from './BaseRequestObject';

export class FetchContainersRequestObject extends BaseRequestObject {
    accept: string;
    id: string;
}
