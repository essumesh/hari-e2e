import { BaseRequestObject } from './BaseRequestObject';

export class FetchDamageReportsRequestObject extends BaseRequestObject {
    accept: string = 'application/hal+json';
    airwaybillPrefix: string;
    airwaybillSerial: string;
    houseAirwaybillNumber: string;
    page: number;
    size: number;
    sort: Array<string> = [];
}
