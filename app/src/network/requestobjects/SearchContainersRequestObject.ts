import { BaseRequestObject } from './BaseRequestObject';

export class SearchContainersRequestObject extends BaseRequestObject {
    accept: string;
    query: string;
    page: number;
    sort: Array<string>;
}
