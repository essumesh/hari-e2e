import { BaseRequestObject } from './BaseRequestObject';

export class QueryPartsRequestObject extends BaseRequestObject {
    accept: string;
    id: string;
    airwaybillid: string;
    airwaybillPrefix: string;
    airwaybillSerial: string;
    houseAirwaybillid: string;
    houseAirwaybillNumber: string;
    transportMeansid: string;
    mode: string;
    transportNumber: string;
    date: Date;
    page: number;
    sort: Array<string>;
}
