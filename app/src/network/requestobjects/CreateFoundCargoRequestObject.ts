
import { BaseRequestObject } from './BaseRequestObject';
import { FoundCargoDataModel } from '../../datamodels/FoundCargoDataModel';

export class CreateFoundCargoRequestObject extends BaseRequestObject {
    foundCargoDataModel: FoundCargoDataModel;
}
