import { BaseRequestObject } from './BaseRequestObject';
import { ContainersDataModel } from '../../datamodels/ContainersDataModel';
import { ItinerariesDataModel } from '../../datamodels/ItinerariesDataModel';

export class ReplaceOrUpdateContainersRequestObject extends BaseRequestObject {
    id: string;
    containers: ContainersDataModel;
    containerItinerary: Array<ItinerariesDataModel>;
}
