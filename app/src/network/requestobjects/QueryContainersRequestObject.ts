import { BaseRequestObject } from './BaseRequestObject';

export class QueryContainersRequestObject extends BaseRequestObject {
    accept: string;
    id: string;
    type: string;
    containerNumber: string;
    containerDestinationid: string;
    containerDestinationcode: string;
    containerDestinationtype: string;
    status: string;
    page: number;
    sort: Array<string>;
}
