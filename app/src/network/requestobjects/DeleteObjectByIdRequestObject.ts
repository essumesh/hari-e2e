import { BaseRequestObject } from './BaseRequestObject';

export class DeleteObjectByIdRequestObject extends BaseRequestObject {
    id: string;
}
