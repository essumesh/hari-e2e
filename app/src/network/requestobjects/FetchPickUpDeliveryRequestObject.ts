import { BaseRequestObject } from './BaseRequestObject';

export class FetchPickUpDeliveryRequestObject extends BaseRequestObject {
    receiptNumber: string;
}
