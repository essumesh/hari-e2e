import { BaseRequestObject } from './BaseRequestObject';
import { PartsDataModel } from '../../datamodels/PartsDataModel';

export class ReplaceOrUpdatePartsRequestObject extends BaseRequestObject {
    id: string;
    parts: PartsDataModel;
}
