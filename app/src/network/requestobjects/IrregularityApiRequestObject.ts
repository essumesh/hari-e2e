import { BaseRequestObject } from './BaseRequestObject';
import { IrregularityApiDataModel } from '../../datamodels/IrregularityApiDataModel';

export class IrregularityApiRequestObject extends BaseRequestObject {
    irregularityApiDataModel: IrregularityApiDataModel;
}
