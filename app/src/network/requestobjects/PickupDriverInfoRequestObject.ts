
import { BaseRequestObject } from './BaseRequestObject';

export class PickupDriverInfoRequestObject extends BaseRequestObject {
    driverID: string;
}
