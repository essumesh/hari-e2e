import { BaseRequestObject } from './BaseRequestObject';
import { ItinerariesDataModel } from '../../datamodels/ItinerariesDataModel';

export class CreateItinerariesRequestObject extends BaseRequestObject {
    itineraries: ItinerariesDataModel;

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['itineraries'] = JSON.stringify(this.itineraries);
        return jsonData;
    }

    fromJSON(_json: any) {
        super.fromJSON(_json);
        this.itineraries = new ItinerariesDataModel();
        this.itineraries.fromJSON(JSON.parse(_json.itineraries));
    }
}
