import { BaseRequestObject } from './BaseRequestObject';
import { LogisticsPackagesDataModel } from '../../datamodels/LogisticsPackagesDataModel';

export class ReplaceOrUpdateLogisticsPackagesRequestObject extends BaseRequestObject {
    id: string;
    logisticsPackages: LogisticsPackagesDataModel;
}
