import { BaseRequestObject } from './BaseRequestObject';
import { PartsDataModel } from '../../datamodels/PartsDataModel';

export class CreatePartsRequestObject extends BaseRequestObject {
    parts: PartsDataModel;

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['parts'] = JSON.stringify(this.parts);
        return jsonData;
    }

    fromJSON(_json: any) {
        super.fromJSON(_json);
        this.parts = new PartsDataModel();
        this.parts.fromJSON(JSON.parse(_json.parts));
    }
}
