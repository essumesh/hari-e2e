import { BaseRequestObject } from './BaseRequestObject';

export class QueryHandlingInformationRequestObject extends BaseRequestObject {
    accept: string;
    id: string;
    source: string;
    page: number;
    sort: Array<string>;
}
