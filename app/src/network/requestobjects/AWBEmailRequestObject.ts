import { BaseRequestObject } from './BaseRequestObject';
import { AWBEmailDataModel } from '../../datamodels/AWBEmailDataModel';

export class AWBEmailRequestObject extends BaseRequestObject {
    awbReportID: string;
    awbEmailDataModel: AWBEmailDataModel;

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['awbEmailDataModel'] = JSON.stringify(this.awbEmailDataModel);
        return jsonData;
    }

    fromJSON(_json: any) {
        super.fromJSON(_json);
        this.awbEmailDataModel = new AWBEmailDataModel();
        this.awbEmailDataModel.fromJSON(JSON.parse(_json.awbEmailDataModel));
    }
}
