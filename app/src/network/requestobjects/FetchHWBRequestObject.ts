import { BaseRequestObject } from './BaseRequestObject';

export class FetchHWBRequestObject extends BaseRequestObject {
    houseAirwaybillNumber: string;
}
