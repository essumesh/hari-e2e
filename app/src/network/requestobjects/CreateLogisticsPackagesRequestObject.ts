import { BaseRequestObject } from './BaseRequestObject';
import { LogisticsPackagesDataModel } from '../../datamodels/LogisticsPackagesDataModel';

export class CreateLogisticsPackagesRequestObject extends BaseRequestObject {
    logisticsPackages: LogisticsPackagesDataModel;

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['logisticsPackages'] = JSON.stringify(this.logisticsPackages);
        return jsonData;
    }

    fromJSON(_json: any) {
        super.fromJSON(_json);
        this.logisticsPackages = new LogisticsPackagesDataModel();
        this.logisticsPackages.fromJSON(JSON.parse(_json.logisticsPackages));
    }
}
