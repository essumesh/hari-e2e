import { BaseRequestObject } from './BaseRequestObject';
import { DamageReportsDataModel } from '../../datamodels/DamageReportsDataModel';

export class AWBDamageAPIRequestObject extends BaseRequestObject {
    isThisAFreshlyCreatedDamageReport: boolean = false;
    damageReportsDataModel: DamageReportsDataModel;
    directlySyncMediaWithoutAnySyncingAnythingOnDamageReportData: boolean = false;

    toJSON(): any {
        let jsonData = super.toJSON();

        jsonData['isThisAFreshlyCreatedDamageReport'] = this.isThisAFreshlyCreatedDamageReport;
        jsonData['damageReportsDataModel'] = JSON.stringify(this.damageReportsDataModel);

        return jsonData;
    }

    fromJSON(_json: any) {
        super.fromJSON(_json);

        this.isThisAFreshlyCreatedDamageReport = _json.isThisAFreshlyCreatedDamageReport;

        this.damageReportsDataModel = new DamageReportsDataModel();
        this.damageReportsDataModel.fromJSON(JSON.parse(_json.damageReportsDataModel));
    }
}
