import { BaseRequestObject } from './BaseRequestObject';
import { PartsAcceptanceDataModel } from '../../datamodels/PartsAcceptanceDataModel';

export class PartsAcceptanceRequestObject extends BaseRequestObject {
    id: string;
    partsAcceptance: PartsAcceptanceDataModel;

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['partsAcceptance'] = JSON.stringify(this.partsAcceptance);
        return jsonData;
    }

    fromJSON(_json: any) {
        super.fromJSON(_json);
        this.partsAcceptance = new PartsAcceptanceDataModel();
        this.partsAcceptance.fromJSON(JSON.parse(_json.partsAcceptance));
    }
}
