import { Component } from '@angular/core';

export enum NetworkMethodType {
    GET,
    POST,
    PUT,
    DELETE,
    PATCH,
    HEAD,
}
