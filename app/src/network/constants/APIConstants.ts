import { Component } from '@angular/core';

export enum APIServerType {
    DEV,
    UAT,
}

export class APIConstants {
    public static TO_USE_MOCKS: boolean = false;

    private static readonly APISERVERTYPE: APIServerType = APIServerType.UAT;
    private static readonly BASEURL_FOR_DEV: string = 'http://cs-v-mobile-dev02.champ.aero';
    private static readonly BASEURL_FOR_UAT: string = 'http://cs-v-mobilitydev1.champ.aero';
    private static readonly BASEURL_FOR_MOCKS: string = 'assets/mocks';
    // tslint:disable-next-line:max-line-length
    private static readonly AUTHORIZATION_TOKEN_FOR_DEV: string =
    'Bearer eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiJpZCIsImlhdCI6MTUwNzIwMDA4NCwic3ViIjoiYWRtaW5AY2hhbXAuYWVybyIsInVzZXJfbmFtZSI6ImFkbWluQGNoYW1wLmFlcm8iLCJjaWQiOiJjaGFtcCIsImF1dGhvcml0aWVzIjoiW10iLCJzY29wZSI6WyJVU0VSIl0sInN0YXRpb24iOnsiY29kZSI6IlpSSCIsInR5cGUiOiJBSVJQT1JUIn19.i3FutIBqQl8A_YIyORJW9ET3ZkU9RF7ltSj7KxRzz28oWJuneMZ5W_c9JjTO1fGlcTnwGczqMqQCNeN40LaKQX1ZQ0mfqfZhSy5q24qvsmp4wP5E73WDNHnF2st-lmKeOOieiMwUcigOgk-aTDNGBkSjZLkA3Vt8OMT4VXV3Stq-NhhW9zitND-HL9jdD6LxO-_D8UuTV5MY66UlKH_VneTtBtcjY4g6rn7KOSU9lNedh_P0i7_VkEHi571wxERQCgirlK7LJS7QTyQL1It2IBwBig_p9JUbhh53pmDBPWv6RmESw6Nd9iFBCsC6BXZpRJ5LQeg-_vt4AdySRO_7SQ';
    // tslint:disable-next-line:max-line-length
    private static readonly AUTHORIZATION_TOKEN_FOR_UAT: string =
    'Bearer eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiJpZCIsImlhdCI6MTUwODc1NDA0Niwic3ViIjoiYWRtaW5AY2hhbXAuYWVybyIsInVzZXJfbmFtZSI6ImFkbWluQGNoYW1wLmFlcm8iLCJjaWQiOiJjaGFtcCIsImF1dGhvcml0aWVzIjoiW10iLCJzY29wZSI6WyJVU0VSIl0sInN0YXRpb24iOnsiY29kZSI6IlpSSCIsInR5cGUiOiJBSVJQT1JUIn19.YdACfKABlWUhlsTr_bx77IvDFP0PDtOJu8u-JiBJExHWsZGb7l3oNEP85k4Qg55B3SbHpFOVYsMWMW_4cBakXN5Ve-mDLI5nDBnNfjrHii2dG93S3IqwU3Cn297VvlfjGeWXr8mPSVtiCdg-xp0FNPFcwu_jSizvEvjAvtFLlxzm1kF0mmgwteSu-fYqSMIaxQAGzAe2zddJOO9ZvoW369ssDHPPZ9-ysOPPgZDjHWIfask3Y31PrBM5OaZidjJB_s6xngR38pKVp10JMkYudOtiJc0sEINDVYOInXg3sopcshRTEaVVFlGkJCApJ_oDJRMiZLhWFHf7CkbHsX4UbQ';

    public static baseURL(): string {
        return APIConstants.TO_USE_MOCKS
                ? APIConstants.BASEURL_FOR_MOCKS
                : (APIConstants.APISERVERTYPE === APIServerType.DEV
                   ? APIConstants.BASEURL_FOR_DEV
                   : APIConstants.BASEURL_FOR_UAT);
    }

    public static authorizationToken(): string {
        return APIConstants.TO_USE_MOCKS
                ? ''
                : (APIConstants.APISERVERTYPE === APIServerType.DEV
                   ? APIConstants.AUTHORIZATION_TOKEN_FOR_DEV
                   : APIConstants.AUTHORIZATION_TOKEN_FOR_UAT);
    }
}
