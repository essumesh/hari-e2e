
import { BaseResponseObject } from './BaseResponseObject';
import { FoundCargoStationDataModel } from '../../datamodels/FoundCargoStationDataModel';

export class FetchOriginResponseObject extends BaseResponseObject {
    foundCargoStationDataModel?: FoundCargoStationDataModel;
}
