import { BaseResponseObject } from './BaseResponseObject';
import { HouseAirwaybillsDataModel } from '../../datamodels/HouseAirwaybillsDataModel';

export class FetchHWBResponseObject extends BaseResponseObject {
    houseAirwaybillsDataModels: Array<HouseAirwaybillsDataModel> = [];
    hwbNotFound: boolean = false;
}
