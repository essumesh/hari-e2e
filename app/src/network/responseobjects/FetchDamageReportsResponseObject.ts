import { BaseResponseObject } from './BaseResponseObject';
import { PagedResourcesdamagereportsEmbeddedDataModel } from
'../../datamodels/PagedResourcesdamagereportsEmbeddedDataModel';

export class FetchDamageReportsResponseObject extends BaseResponseObject {
    pagedResourcesdamagereportsEmbeddedDataModel: PagedResourcesdamagereportsEmbeddedDataModel =
    new PagedResourcesdamagereportsEmbeddedDataModel();
}
