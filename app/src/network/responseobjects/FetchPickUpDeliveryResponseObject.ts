import { BaseResponseObject } from './BaseResponseObject';
import { PickUpDeliveryReceiptDataModel } from '../../datamodels/PickUpDeliveryReceiptDataModel';
import { PickUpDeliveryDataModel } from '../../datamodels/PickUpDeliveryDataModel';

export class FetchPickUpDeliveryResponseObject extends BaseResponseObject {
    pickupReferencesDataModel?: PickUpDeliveryReceiptDataModel;
    pickUpReferencesAirlineTransferDataModel?: PickUpDeliveryDataModel;
}
