import { BaseResponseObject } from './BaseResponseObject';

export class AWBDamageAPIResponseObject extends BaseResponseObject {
    damageReportCreatedAtLocation: string;
    reportId: string;
}
