import { BaseResponseObject } from './BaseResponseObject';
import { IrregularityReportListDataModel } from '../../datamodels/IrregularityReportListDataModel';

export class FetchIrregularityReportResponseObject extends BaseResponseObject {
    irregularityReportListDataModel: IrregularityReportListDataModel =
    new IrregularityReportListDataModel();
}
