import { BaseResponseObject } from './BaseResponseObject';
import { DamageReferencesDataModel } from '../../datamodels/DamageReferencesDataModel';

export class FetchAWBDamageQualifiersResponseObject extends BaseResponseObject {
    damageReferencesDataModel?: DamageReferencesDataModel;
}
