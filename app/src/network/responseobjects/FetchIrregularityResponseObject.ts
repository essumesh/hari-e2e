import { BaseResponseObject } from './BaseResponseObject';
import { IrregularityQualifierDataModel } from '../../datamodels/IrregularityQualifierDataModel';

export class FetchIrregularityResponseObject extends BaseResponseObject {
    irregularityQualifierDataModel?: IrregularityQualifierDataModel;
}
