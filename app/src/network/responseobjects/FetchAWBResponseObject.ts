import { BaseResponseObject } from './BaseResponseObject';
import { AirwaybillsDataModel } from '../../datamodels/AirwaybillsDataModel';

export class FetchAWBResponseObject extends BaseResponseObject {
    airwaybillsDataModels: Array<AirwaybillsDataModel> = [];
    awbNotFound: boolean = false;
}
