
import { BaseResponseObject } from './BaseResponseObject';
import { FoundCargoSHCDataModel } from '../../datamodels/FoundCargoSHCDataModel';

export class FetchSHCResponseObject extends BaseResponseObject {
    foundCargoSHCDataModel?: FoundCargoSHCDataModel;
}
