import { BaseResponseObject } from './BaseResponseObject';
import { AirwaybillsDataModel } from '../../datamodels/AirwaybillsDataModel';

export class FetchUnKnownAWBResponseObject extends BaseResponseObject {
    airwaybillsDataModel?: AirwaybillsDataModel;
}
