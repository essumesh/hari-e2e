import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { QueryLocationsRequestObject } from '../requestobjects/QueryLocationsRequestObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class QueryLocationsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/locations';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let queryLocationsRequestObject: QueryLocationsRequestObject =
        new QueryLocationsRequestObject();
        if (_parameters.length > 0) {
            queryLocationsRequestObject.accept = _parameters[0];
            queryLocationsRequestObject.id = _parameters[1];
            queryLocationsRequestObject.code = _parameters[2];
            queryLocationsRequestObject.type = _parameters[3];
            queryLocationsRequestObject.page = _parameters[4];
            queryLocationsRequestObject.sort = _parameters[5];
          }
        this.requestObject = queryLocationsRequestObject;
    }

    requestParameters(): URLSearchParams {
        let queryLocationsRequestObject: QueryLocationsRequestObject =
        this.requestObject as QueryLocationsRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', queryLocationsRequestObject.accept);
        requestParameters.set('id', queryLocationsRequestObject.id);
        requestParameters.set('code', queryLocationsRequestObject.code);
        requestParameters.set('type', queryLocationsRequestObject.type);
        requestParameters.set('page',
        queryLocationsRequestObject.page != null
        ? queryLocationsRequestObject.page.toString()
        : null);
        requestParameters.set('sort',
        queryLocationsRequestObject.sort != null
        ? queryLocationsRequestObject.sort.toString()
        : null);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new QueryLocationsRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
