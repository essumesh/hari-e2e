import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { QuerySplitsRequestObject } from '../requestobjects/QuerySplitsRequestObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class QuerySplitsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/splits';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let querySplitsRequestObject: QuerySplitsRequestObject =
        new QuerySplitsRequestObject();
        if (_parameters.length > 0) {
            querySplitsRequestObject.accept = _parameters[0];
            querySplitsRequestObject.id = _parameters[1];
            querySplitsRequestObject.airwaybillid = _parameters[2];
            querySplitsRequestObject.airwaybillPrefix = _parameters[3];
            querySplitsRequestObject.airwaybillSerial = _parameters[4];
            querySplitsRequestObject.houseAirwaybillid = _parameters[5];
            querySplitsRequestObject.houseAirwaybillNumber = _parameters[6];
            querySplitsRequestObject.partid = _parameters[7];
            querySplitsRequestObject.page = _parameters[8];
            querySplitsRequestObject.sort = _parameters[9];
          }
        this.requestObject = querySplitsRequestObject;
    }

    requestParameters(): URLSearchParams {
        let querySplitsRequestObject: QuerySplitsRequestObject =
        this.requestObject as QuerySplitsRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();
        requestParameters.set('accept', querySplitsRequestObject.accept);
        requestParameters.set('id', querySplitsRequestObject.id);
        requestParameters.set('airwaybillid', querySplitsRequestObject.airwaybillid);
        requestParameters.set('airwaybillPrefix', querySplitsRequestObject.airwaybillPrefix);
        requestParameters.set('airwaybillSerial', querySplitsRequestObject.airwaybillSerial);
        requestParameters.set('houseAirwaybillid', querySplitsRequestObject.houseAirwaybillid);
        requestParameters.set('houseAirwaybillNumber', querySplitsRequestObject.houseAirwaybillNumber);
        requestParameters.set('partid', querySplitsRequestObject.partid);
        requestParameters.set('page',
        querySplitsRequestObject.page != null
        ? querySplitsRequestObject.page.toString()
        : null);
        requestParameters.set('sort',
        querySplitsRequestObject.sort != null
        ? querySplitsRequestObject.sort.toString()
        : null);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new QuerySplitsRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
