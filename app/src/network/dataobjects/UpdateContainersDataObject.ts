import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { ReplaceOrUpdateContainersRequestObject } from '../requestobjects/ReplaceOrUpdateContainersRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class UpdateContainersDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.PATCH;
    }

    methodName(): string | undefined {
        let updateContainersRequestObject: ReplaceOrUpdateContainersRequestObject =
            this.requestObject as ReplaceOrUpdateContainersRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
            : '/acceptance/v1/containers/' + updateContainersRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let updateContainersRequestObject: ReplaceOrUpdateContainersRequestObject =
            new ReplaceOrUpdateContainersRequestObject();
        if (_parameters.length > 0) {
            updateContainersRequestObject.id = _parameters[0];
            updateContainersRequestObject.containers = _parameters[1];
        }
        this.requestObject = updateContainersRequestObject;
    }

    requestBody(): any {
        let updateContainersRequestObject: ReplaceOrUpdateContainersRequestObject =
            this.requestObject as ReplaceOrUpdateContainersRequestObject;

        let jsonData = updateContainersRequestObject.containers.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new ReplaceOrUpdateContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
