import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { ContainersAcceptanceRequestObject } from '../requestobjects/ContainersAcceptanceRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class ContainersAcceptanceDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.POST;
    }

    methodName(): string | undefined {
        let containersAcceptanceRequestObject: ContainersAcceptanceRequestObject =
            this.requestObject as ContainersAcceptanceRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
            : ('/acceptance/v1/containers-reports/'
                + containersAcceptanceRequestObject.id
            + '/acceptance-requests');
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let containersAcceptanceRequestObject: ContainersAcceptanceRequestObject =
            new ContainersAcceptanceRequestObject();
        if (_parameters.length > 0) {
            containersAcceptanceRequestObject.id = _parameters[0];
            containersAcceptanceRequestObject.containersAcceptanceDataModel = _parameters[1];
        }
        this.requestObject = containersAcceptanceRequestObject;
    }

    requestBody(): any {
        let containersAcceptanceRequestObject: ContainersAcceptanceRequestObject =
            this.requestObject as ContainersAcceptanceRequestObject;

        let jsonData = containersAcceptanceRequestObject.containersAcceptanceDataModel.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new ContainersAcceptanceRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
