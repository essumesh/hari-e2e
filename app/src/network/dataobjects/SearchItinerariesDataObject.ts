import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { SearchContainersRequestObject } from '../requestobjects/SearchContainersRequestObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class SearchItinerariesDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/itineraries/search';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let searchItinerariesRequestObject: SearchContainersRequestObject =
        new SearchContainersRequestObject();
        if (_parameters.length > 0) {
            searchItinerariesRequestObject.accept = _parameters[0];
            searchItinerariesRequestObject.query = _parameters[1];
            searchItinerariesRequestObject.page = _parameters[2];
            searchItinerariesRequestObject.sort = _parameters[3];
          }
        this.requestObject = searchItinerariesRequestObject;
    }

    requestParameters(): URLSearchParams {
        let searchItinerariesRequestObject: SearchContainersRequestObject =
        this.requestObject as SearchContainersRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', searchItinerariesRequestObject.accept);
        requestParameters.set('query', searchItinerariesRequestObject.query);
        requestParameters.set('page',
        searchItinerariesRequestObject.page != null
        ? searchItinerariesRequestObject.page.toString()
        : null);
        requestParameters.set('sort',
        searchItinerariesRequestObject.sort != null
        ? searchItinerariesRequestObject.sort.toString()
        : null);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new SearchContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
