import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { CreateSplitsRequestObject } from '../requestobjects/CreateSplitsRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class CreateSplitsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.POST;
    }

    methodName(): string | undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
            : '/acceptance/v1/splits';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let createSplitsRequestObject: CreateSplitsRequestObject =
            new CreateSplitsRequestObject();
        if (_parameters.length > 0) {
            createSplitsRequestObject.splits = _parameters[0];
        }
        this.requestObject = createSplitsRequestObject;
    }

    requestBody(): any {
        let createSplitsRequestObject: CreateSplitsRequestObject =
            this.requestObject as CreateSplitsRequestObject;

        let jsonData = createSplitsRequestObject.splits.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new CreateSplitsRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
