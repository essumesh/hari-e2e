import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { SearchContainersRequestObject } from '../requestobjects/SearchContainersRequestObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class SearchSplitsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/splits/search';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let searchSplitsRequestObject: SearchContainersRequestObject =
        new SearchContainersRequestObject();
        if (_parameters.length > 0) {
            searchSplitsRequestObject.accept = _parameters[0];
            searchSplitsRequestObject.query = _parameters[1];
            searchSplitsRequestObject.page = _parameters[2];
            searchSplitsRequestObject.sort = _parameters[3];
          }
        this.requestObject = searchSplitsRequestObject;
    }

    requestParameters(): URLSearchParams {
        let searchSplitsRequestObject: SearchContainersRequestObject =
        this.requestObject as SearchContainersRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', searchSplitsRequestObject.accept);
        requestParameters.set('query', searchSplitsRequestObject.query);
        requestParameters.set('page',
        searchSplitsRequestObject.page != null
        ? searchSplitsRequestObject.page.toString()
        : null);
        requestParameters.set('sort',
        searchSplitsRequestObject.sort != null
        ? searchSplitsRequestObject.sort.toString()
        : null);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new SearchContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
