import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { QueryHandlingInformationRequestObject } from '../requestobjects/QueryHandlingInformationRequestObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class QueryHandlingInformationDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/handling-information';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let queryHandlingInformationRequestObject: QueryHandlingInformationRequestObject =
        new QueryHandlingInformationRequestObject();
        if (_parameters.length > 0) {
            queryHandlingInformationRequestObject.accept = _parameters[0];
            queryHandlingInformationRequestObject.id = _parameters[1];
            queryHandlingInformationRequestObject.source = _parameters[2];
            queryHandlingInformationRequestObject.page = _parameters[3];
            queryHandlingInformationRequestObject.sort = _parameters[4];
          }
        this.requestObject = queryHandlingInformationRequestObject;
    }

    requestParameters(): URLSearchParams {
        let queryHandlingInformationRequestObject: QueryHandlingInformationRequestObject =
        this.requestObject as QueryHandlingInformationRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', queryHandlingInformationRequestObject.accept);
        requestParameters.set('id', queryHandlingInformationRequestObject.id);
        requestParameters.set('source', queryHandlingInformationRequestObject.source);
        requestParameters.set('page',
        queryHandlingInformationRequestObject.page != null
        ? queryHandlingInformationRequestObject.page.toString()
        : null);
        requestParameters.set('sort',
        queryHandlingInformationRequestObject.sort != null
        ? queryHandlingInformationRequestObject.sort.toString()
        : null);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new QueryHandlingInformationRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
