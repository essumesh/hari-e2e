import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { ReplaceOrUpdateLogisticsPackagesRequestObject }
from '../requestobjects/ReplaceOrUpdateLogisticsPackagesRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class ReplaceLogisticsPackagesDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.PUT;
    }

    methodName(): string | undefined {
        let replaceLogisticsPackagesRequestObject: ReplaceOrUpdateLogisticsPackagesRequestObject =
            this.requestObject as ReplaceOrUpdateLogisticsPackagesRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
            : '/acceptance/v1/logistics-packages/' + replaceLogisticsPackagesRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let replaceLogisticsPackagesRequestObject: ReplaceOrUpdateLogisticsPackagesRequestObject =
            new ReplaceOrUpdateLogisticsPackagesRequestObject();
        if (_parameters.length > 0) {
            replaceLogisticsPackagesRequestObject.id = _parameters[0];
            replaceLogisticsPackagesRequestObject.logisticsPackages = _parameters[1];
        }
        this.requestObject = replaceLogisticsPackagesRequestObject;
    }

    requestBody(): any {
        let replaceLogisticsPackagesRequestObject: ReplaceOrUpdateLogisticsPackagesRequestObject =
            this.requestObject as ReplaceOrUpdateLogisticsPackagesRequestObject;

        let jsonData = replaceLogisticsPackagesRequestObject.logisticsPackages.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new ReplaceOrUpdateLogisticsPackagesRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
