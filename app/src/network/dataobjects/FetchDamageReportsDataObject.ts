import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { FetchDamageReportsRequestObject } from '../requestobjects/FetchDamageReportsRequestObject';
import { FetchDamageReportsResponseObject } from '../responseobjects/FetchDamageReportsResponseObject';
import { PagedResourcesdamagereportsEmbeddedDataModel } from
'../../datamodels/PagedResourcesdamagereportsEmbeddedDataModel';
import { URLSearchParams } from '@angular/http';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';
import { AppUIHelper } from '../../helpers/AppUIHelper';

@Injectable()
export class FetchDamageReportsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/fetchdamagereportsdataobject.json'
                                         : '/discrepancy/v1/damage-reports/';
    }

    requestHeaders(): Headers {
        let headers = super.requestHeaders();
        headers.set('accept', 'application/hal+json');
        return headers;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fetchDamageReportsRequestObject: FetchDamageReportsRequestObject =
        new FetchDamageReportsRequestObject();
        if (_parameters.length > 0) {
            fetchDamageReportsRequestObject.airwaybillPrefix = _parameters[0];
            fetchDamageReportsRequestObject.airwaybillSerial = _parameters[1];
            fetchDamageReportsRequestObject.houseAirwaybillNumber = _parameters[2];
            fetchDamageReportsRequestObject.page = _parameters[3];
            fetchDamageReportsRequestObject.size = _parameters[4];
            fetchDamageReportsRequestObject.sort = _parameters[5];
          }
        this.requestObject = fetchDamageReportsRequestObject;
    }

    requestParameters(): URLSearchParams {
        let fetchDamageReportsRequestObject: FetchDamageReportsRequestObject =
        this.requestObject as FetchDamageReportsRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('airwaybillPrefix', fetchDamageReportsRequestObject.airwaybillPrefix);
        requestParameters.set('airwaybillSerial', fetchDamageReportsRequestObject.airwaybillSerial);
        requestParameters.set('houseAirwaybillNumber', fetchDamageReportsRequestObject.houseAirwaybillNumber);
        requestParameters.set('page',
        fetchDamageReportsRequestObject.page != null
        ? fetchDamageReportsRequestObject.page.toString()
        : null);
        requestParameters.set('size',
        fetchDamageReportsRequestObject.size != null
        ? fetchDamageReportsRequestObject.size.toString()
        : null);
        requestParameters.set('sort',
        fetchDamageReportsRequestObject.sort != null
        ? fetchDamageReportsRequestObject.sort.toString()
        : null);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200) {
            let fetchDamageReportsResponseObject = new FetchDamageReportsResponseObject();
            this.responseObject = fetchDamageReportsResponseObject;

            let dataRecievedFromServer = this.responseRecievedFromServer.json();
            if (dataRecievedFromServer != null) {
                if (dataRecievedFromServer.constructor === {}.constructor) {
                    fetchDamageReportsResponseObject.pagedResourcesdamagereportsEmbeddedDataModel
                    .parse(dataRecievedFromServer);
                }
            }
        } else if (this.responseRecievedFromServer.status === 404) {
            let fetchDamageReportsResponseObject = new FetchDamageReportsResponseObject();
            this.responseObject = fetchDamageReportsResponseObject;
        }
    }

    protected isStatusCodeRecievedToBeConsideredAsError(_statusCode: number): boolean {
        if (_statusCode === 404) return false;
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
    super.fromJSON(json);

    this.requestObject = new FetchDamageReportsRequestObject();
    this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }
}
