import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { FetchAWBRequestObject } from '../requestobjects/FetchAWBRequestObject';
import { FetchAWBResponseObject } from '../responseobjects/FetchAWBResponseObject';
import { AirwaybillsDataModel } from '../../datamodels/AirwaybillsDataModel';
import { URLSearchParams } from '@angular/http';
import { AppUIHelper } from '../../helpers/AppUIHelper';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

@Injectable()
export class FetchAWBDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/fetchawbdataobject.json'
                                         : '/discrepancy/v1/airwaybills/';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fetchAWBRequestObject: FetchAWBRequestObject =
        new FetchAWBRequestObject();
        if (_parameters.length > 0) {
            fetchAWBRequestObject.airwaybillPrefix = _parameters[0];
            fetchAWBRequestObject.airwaybillSerial = _parameters[1];
        }
        this.requestObject = fetchAWBRequestObject;
    }

    requestParameters(): URLSearchParams {
        let fetchAWBRequestObject: FetchAWBRequestObject = this.requestObject as FetchAWBRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('airwaybillPrefix', fetchAWBRequestObject.airwaybillPrefix);
        requestParameters.set('airwaybillSerial', fetchAWBRequestObject.airwaybillSerial);

        return requestParameters;
    }

    parse() {
        let fetchAWBResponseObject = new FetchAWBResponseObject();
        this.responseObject = fetchAWBResponseObject;

        if (this.responseRecievedFromServer.status === 200) {
            let dataRecievedFromServer = this.responseRecievedFromServer.json();
            if (dataRecievedFromServer != null) {
                if (dataRecievedFromServer.constructor === {}.constructor) {
                    let _embeddedJSON = dataRecievedFromServer['_embedded'];
                    if (_embeddedJSON != null && _embeddedJSON.constructor === {}.constructor) {
                        let airwaybillsArray = _embeddedJSON['awbs'];
                        if (airwaybillsArray != null && airwaybillsArray instanceof Array) {
                            for (let arrayItem of airwaybillsArray) {
                                let airwaybillsDataModel = new AirwaybillsDataModel();
                                airwaybillsDataModel.parse(arrayItem);
                                fetchAWBResponseObject.airwaybillsDataModels.push(airwaybillsDataModel);
                              }
                        }
                    }
                }
            }
        } else if (this.responseRecievedFromServer.status === 404) {
            let fetchAWBRequestObject: FetchAWBRequestObject = this.requestObject as FetchAWBRequestObject;
            if (AppUIHelper.isValidAWBNumber
                (fetchAWBRequestObject.airwaybillPrefix, fetchAWBRequestObject.airwaybillSerial))
                fetchAWBResponseObject.awbNotFound = true;
            else this.serverErrorDataModel = new ServerErrorDataModel
            (AppUIHelper.getInstance().i18nText('invalidawbnumber'));
        }
    }

    protected isStatusCodeRecievedToBeConsideredAsError(_statusCode: number): boolean {
        if (_statusCode === 404) return false;
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new FetchAWBRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
