import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { PartsAcceptanceRequestObject } from '../requestobjects/PartsAcceptanceRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class PartsAcceptanceDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.POST;
    }

    methodName(): string | undefined {
        let partsAcceptanceRequestObject: PartsAcceptanceRequestObject =
            this.requestObject as PartsAcceptanceRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
            : ('/acceptance/v1/parts/'
                + partsAcceptanceRequestObject.id
            + '/acceptance-requests');
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let partsAcceptanceRequestObject: PartsAcceptanceRequestObject =
            new PartsAcceptanceRequestObject();
        if (_parameters.length > 0) {
            partsAcceptanceRequestObject.id = _parameters[0];
            partsAcceptanceRequestObject.partsAcceptance = _parameters[1];
        }
        this.requestObject = partsAcceptanceRequestObject;
    }

    requestBody(): any {
        let partsAcceptanceRequestObject: PartsAcceptanceRequestObject =
            this.requestObject as PartsAcceptanceRequestObject;

        let jsonData = partsAcceptanceRequestObject.partsAcceptance.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new PartsAcceptanceRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
