
import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { FetchPickUpDeliveryRequestObject } from '../requestobjects/FetchPickUpDeliveryRequestObject';
import { FetchPickUpDeliveryResponseObject } from '../responseobjects/FetchPickUpDeliveryResponseObject';
import { PickUpDeliveryDataModel } from '../../datamodels/PickUpDeliveryDataModel';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';
import { URLSearchParams } from '@angular/http';
import { PickupDeliveryReceiptRequestObject } from '../requestobjects/PickupDeliveryReceiptRequestObject';

@Injectable()
export class PickupDeliveryReceiptDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.PATCH;
    }

    methodName(): string | undefined {
        let pickupDeliveryReceiptRequestObject: PickupDeliveryReceiptRequestObject =
            this.requestObject as PickupDeliveryReceiptRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/pickupdelivery.json'
            : ('/pickup/v1/delivery-receipts/'
                + pickupDeliveryReceiptRequestObject.deliveryReceiptID);
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let pickupDeliveryReceiptRequestObject: PickupDeliveryReceiptRequestObject =
            new PickupDeliveryReceiptRequestObject();
        if (_parameters.length > 0) {
            pickupDeliveryReceiptRequestObject.deliveryReceiptID = _parameters[0];
            pickupDeliveryReceiptRequestObject.pickupSignatureDataModel = _parameters[1];
            // console.log(pickupDeliveryReceiptRequestObject.pickupSignatureDataModel);
        }
        this.requestObject = pickupDeliveryReceiptRequestObject;
    }

    requestBody(): any {
        let pickupDeliveryReceiptRequestObject: PickupDeliveryReceiptRequestObject =
            this.requestObject as PickupDeliveryReceiptRequestObject;

        let jsonData = pickupDeliveryReceiptRequestObject.pickupSignatureDataModel.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new PickupDeliveryReceiptRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
