import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { FetchContainersRequestObject } from '../requestobjects/FetchContainersRequestObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class FetchSplitsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        let fetchSplitsRequestObject: FetchContainersRequestObject =
        this.requestObject as FetchContainersRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/splits/'
                                         + fetchSplitsRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fetchSplitsRequestObject: FetchContainersRequestObject =
        new FetchContainersRequestObject();
        if (_parameters.length > 0) {
            fetchSplitsRequestObject.accept = _parameters[0];
            fetchSplitsRequestObject.id = _parameters[1];
          }
        this.requestObject = fetchSplitsRequestObject;
    }

    requestParameters(): URLSearchParams {
        let fetchSplitsRequestObject: FetchContainersRequestObject =
        this.requestObject as FetchContainersRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', fetchSplitsRequestObject.accept);
        requestParameters.set('id', fetchSplitsRequestObject.id);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new FetchContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
