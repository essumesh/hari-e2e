import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { ReplaceOrUpdateContainersRequestObject } from '../requestobjects/ReplaceOrUpdateContainersRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class ReplaceContainersDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.PUT;
    }

    methodName(): string | undefined {
        let replaceContainersRequestObject: ReplaceOrUpdateContainersRequestObject =
            this.requestObject as ReplaceOrUpdateContainersRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
            : '/acceptance/v1/containers/' + replaceContainersRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let replaceContainersRequestObject: ReplaceOrUpdateContainersRequestObject =
            new ReplaceOrUpdateContainersRequestObject();
        if (_parameters.length > 0) {
            replaceContainersRequestObject.id = _parameters[0];
            replaceContainersRequestObject.containers = _parameters[1];
        }
        this.requestObject = replaceContainersRequestObject;
    }

    requestBody(): any {
        let replaceContainersRequestObject: ReplaceOrUpdateContainersRequestObject =
            this.requestObject as ReplaceOrUpdateContainersRequestObject;

        let jsonData = replaceContainersRequestObject.containers.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new ReplaceOrUpdateContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
