import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { ReplaceOrUpdatePartsRequestObject } from '../requestobjects/ReplaceOrUpdatePartsRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class UpdatePartsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.PATCH;
    }

    methodName(): string | undefined {
        let updatePartsRequestObject: ReplaceOrUpdatePartsRequestObject =
            this.requestObject as ReplaceOrUpdatePartsRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
            : '/acceptance/v1/parts/' + updatePartsRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let updatePartsRequestObject: ReplaceOrUpdatePartsRequestObject =
            new ReplaceOrUpdatePartsRequestObject();
        if (_parameters.length > 0) {
            updatePartsRequestObject.id = _parameters[0];
            updatePartsRequestObject.parts = _parameters[1];
        }
        this.requestObject = updatePartsRequestObject;
    }

    requestBody(): any {
        let updateContainersRequestObject: ReplaceOrUpdatePartsRequestObject =
            this.requestObject as ReplaceOrUpdatePartsRequestObject;

        let jsonData = updateContainersRequestObject.parts.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new ReplaceOrUpdatePartsRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
