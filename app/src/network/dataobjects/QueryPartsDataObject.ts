import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { QueryPartsRequestObject } from '../requestobjects/QueryPartsRequestObject';
import { URLSearchParams } from '@angular/http';
import { DateParserHelper } from '../../helpers/DateParserHelper';

@Injectable()
export class QueryPartsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/parts';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let queryPartsRequestObject: QueryPartsRequestObject =
        new QueryPartsRequestObject();
        if (_parameters.length > 0) {
            queryPartsRequestObject.accept = _parameters[0];
            queryPartsRequestObject.id = _parameters[1];
            queryPartsRequestObject.airwaybillid = _parameters[2];
            queryPartsRequestObject.airwaybillPrefix = _parameters[3];
            queryPartsRequestObject.airwaybillSerial = _parameters[4];
            queryPartsRequestObject.houseAirwaybillid = _parameters[5];
            queryPartsRequestObject.houseAirwaybillNumber = _parameters[6];
            queryPartsRequestObject.transportMeansid = _parameters[7];
            queryPartsRequestObject.mode = _parameters[8];
            queryPartsRequestObject.transportNumber = _parameters[9];
            queryPartsRequestObject.date = _parameters[10];
            queryPartsRequestObject.page = _parameters[11];
            queryPartsRequestObject.sort = _parameters[12];
          }
        this.requestObject = queryPartsRequestObject;
    }

    requestParameters(): URLSearchParams {
        let queryPartsRequestObject: QueryPartsRequestObject =
        this.requestObject as QueryPartsRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();
        requestParameters.set('accept', queryPartsRequestObject.accept);
        requestParameters.set('id', queryPartsRequestObject.id);
        requestParameters.set('airwaybillid', queryPartsRequestObject.airwaybillid);
        requestParameters.set('airwaybillPrefix', queryPartsRequestObject.airwaybillPrefix);
        requestParameters.set('airwaybillSerial', queryPartsRequestObject.airwaybillSerial);
        requestParameters.set('houseAirwaybillid', queryPartsRequestObject.houseAirwaybillid);
        requestParameters.set('houseAirwaybillNumber', queryPartsRequestObject.houseAirwaybillNumber);
        requestParameters.set('transportMeansid', queryPartsRequestObject.transportMeansid);
        requestParameters.set('mode', queryPartsRequestObject.mode);
        requestParameters.set('transportNumber', queryPartsRequestObject.transportNumber);
        requestParameters.set('date',
        DateParserHelper.parseDateToString_InFormat_Network(queryPartsRequestObject.date));
        requestParameters.set('page',
        queryPartsRequestObject.page != null
        ? queryPartsRequestObject.page.toString()
        : null);
        requestParameters.set('sort',
        queryPartsRequestObject.sort != null
        ? queryPartsRequestObject.sort.toString()
        : null);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new QueryPartsRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
