import { BaseDataObject } from './BaseDataObject';
import { Injectable } from '@angular/core';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { FoundCargoReportRequestObject } from '../requestobjects/FoundCargoReportRequestObject';
import { APIConstants } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

@Injectable()
export class FoundCargoReportDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.POST;
    }

    methodName(): string | undefined {
        let foundCargoReportRequestObject: FoundCargoReportRequestObject =
            this.requestObject as FoundCargoReportRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/pickupdelivery.json'
            : ('/pickup/v1/delivery-receipts/'
                + foundCargoReportRequestObject.awbNumber);
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let foundCargoReportRequestObject: FoundCargoReportRequestObject =
            new FoundCargoReportRequestObject();
        if (_parameters.length > 0) {
            foundCargoReportRequestObject.awbNumber = _parameters[0];
            foundCargoReportRequestObject.foundCargoDataModel = _parameters[1];
            // console.log(pickupDeliveryReceiptRequestObject.pickupSignatureDataModel);
        }
        this.requestObject = foundCargoReportRequestObject;
    }

    requestBody(): any {
        let foundCargoReportRequestObject: FoundCargoReportRequestObject =
            this.requestObject as FoundCargoReportRequestObject;

        let jsonData = foundCargoReportRequestObject.foundCargoDataModel.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new FoundCargoReportRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
