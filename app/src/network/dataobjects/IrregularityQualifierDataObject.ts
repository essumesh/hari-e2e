import { BaseDataObject } from './BaseDataObject';
import { Injectable } from '@angular/core';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { FetchIrregularityRequestObject } from '../requestobjects/FetchIrregularityRequestObject';
import { FetchIrregularityResponseObject } from '../responseobjects/FetchIrregularityResponseObject';
import { IrregularityQualifierDataModel } from '../../datamodels/IrregularityQualifierDataModel';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';
@Injectable()
export class IrregularityQualifierDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/IrregularityQualifier.json'
                                         : '/discrepancy/v1/irregularities-qualifiers';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fetchIrregularityRequestObject: FetchIrregularityRequestObject =
        new FetchIrregularityRequestObject();
        this.requestObject = fetchIrregularityRequestObject;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200) {
        let fetchIrregularityResponseObject = new FetchIrregularityResponseObject();
        this.responseObject = fetchIrregularityResponseObject;

        let dataRecievedFromServer = this.responseRecievedFromServer.json();
        if (dataRecievedFromServer != null) {
            fetchIrregularityResponseObject.irregularityQualifierDataModel =
            new IrregularityQualifierDataModel();
            fetchIrregularityResponseObject.
            irregularityQualifierDataModel.parse(dataRecievedFromServer);

        }
    }else if (this.responseRecievedFromServer.status === 404) {
        this.serverErrorDataModel = new ServerErrorDataModel('Record not found');
    }
}

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new FetchIrregularityRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }
}
