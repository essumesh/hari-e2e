import { Injectable } from '@angular/core';
import { Http, URLSearchParams, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/timeout';
import { Observable } from 'rxjs/Observable';
import { BaseRequestObject } from '../requestobjects/BaseRequestObject';
import { BaseResponseObject } from '../responseobjects/BaseResponseObject';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { AppUIHelper } from '../../helpers/AppUIHelper';
import { LocalStorageHelper } from '../../localstorage/helpers/LocalStorageHelper';
import { LocalStorageModel } from '../../localstorage/localstoragemodels/LocalStorageModel';

@Injectable()
export class BaseDataObject {
  protected static readonly TIMEOUT_IN_SECONDS = 60;
  requestObject: BaseRequestObject;
  responseObject: BaseResponseObject;
  serverErrorDataModel: ServerErrorDataModel;
  protected callBack: (dataObject: BaseDataObject) => void;
  protected _localStorageModel: LocalStorageModel = null;
  protected responseRecievedFromServer: any;
  private _toSaveLocallyAndThenSync: boolean = false;
  private _hasNetworkProcessFinished: boolean = false;
  private networkCallSubscription: any;
  private uniqueObjectId: string = this.getUniqueObjectId();

  constructor(public http: Http) {
  }

  uniqueObjectIdClassName(): string {
    return this.constructor.name;
  }

  getUniqueObjectId(): string {
    if (this.uniqueObjectId == null) {
        this.uniqueObjectId =  this.uniqueObjectIdClassName() + '_' + AppUIHelper.guid();
    }
    return this.uniqueObjectId;
  }

  methodType(): NetworkMethodType {
    return NetworkMethodType.POST;
  }

  baseURL(): string|undefined {
    return APIConstants.baseURL();
  }

  methodName(): string|undefined {
    return null;
  }

  prepareRequestWithParameters(..._parameters: any[]) {
  }

  requestHeaders(): Headers {
    let headers = new Headers();
    headers.append('accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', APIConstants.authorizationToken());
    return headers;
  }

  requestOptions(): RequestOptions {
    return new RequestOptions({ headers: this.requestHeaders() });
  }

  requestParameters(): URLSearchParams {
    let requestParameters: URLSearchParams = new URLSearchParams();
    return requestParameters;
  }

  requestBody(): any {
    return null;
  }

  queryTheServer(_toSaveLocallyAndThenSync: boolean,
                 callBack: (dataObject: BaseDataObject) => void) {
    this.saveThese(_toSaveLocallyAndThenSync, callBack);

    const _startSync = (() => {
      let requestOptions = new RequestOptions();
      requestOptions.search = this.requestParameters();
      requestOptions.headers = this.requestHeaders();

      const _map = ((_res: any) => {
        this.responseRecievedFromServer = _res;
        return _res._body === '' ? _res.body : _res.json();
      });

      const _cleanUp = (() => {
        this._hasNetworkProcessFinished = true;
      });

      const _success = ((_data: any) => {
        this.parse();
        this.doCleanUpActivitiesOnSuccess();
        this.baseProcessingDone();
      });

      const _failure = ((_err: any) => {
        if (_err != null && _err.constructor.name != null && _err.constructor.name === 'TimeoutError')
          _err = { status: 408 };
        if (this.isStatusCodeRecievedToBeConsideredAsError(_err.status)) {
          this.serverErrorDataModel =
          new ServerErrorDataModel(this.errorTextForStatusCode(_err.status));
          this.doCleanUpActivitiesOnFailure();
          this.baseProcessingDone();
        } else {
          this.responseRecievedFromServer = _err;
          _success(null);
        }
      });

      let realRequestObject: Observable<Response> = null;
      if (this.methodType() === NetworkMethodType.GET) {
        realRequestObject = this.http.get(this.baseURL() + this.methodName(), requestOptions).map(_res => _map(_res));
      } else if (this.methodType() === NetworkMethodType.POST) {
        realRequestObject = this.http.post(this.baseURL() + this.methodName(),
        this.requestBody(), requestOptions).map(_res => _map(_res));
      } else if (this.methodType() === NetworkMethodType.PUT) {
        realRequestObject = this.http.put(this.baseURL() + this.methodName(),
        this.requestBody(), requestOptions).map(_res => _map(_res));
      } else if (this.methodType() === NetworkMethodType.DELETE) {
        realRequestObject = this.http.delete(this.baseURL(), requestOptions).map(_res => _map(_res));
      }else if (this.methodType() === NetworkMethodType.HEAD) {
        realRequestObject = this.http.head(this.baseURL() + this.methodName(),
        requestOptions).map(_res => _map(_res));
      }else if (this.methodType() === NetworkMethodType.PATCH) {
        realRequestObject = this.http.patch(this.baseURL() + this.methodName(),
        this.requestBody(), requestOptions).map(_res => _map(_res));
      }

      this.networkCallSubscription = realRequestObject
      .timeout(BaseDataObject.TIMEOUT_IN_SECONDS * 1000)
      .finally(() => _cleanUp())
      .subscribe((_data: any) => _success(_data), (_err: any) => _failure(_err));
    });

    if (this.methodType() === NetworkMethodType.GET) {
      this._toSaveLocallyAndThenSync = false;
    }

    if (this._toSaveLocallyAndThenSync) {
      this._localStorageModel = new LocalStorageModel(this);
      LocalStorageHelper.getInstance().storeLocalStorageModel(this._localStorageModel, () => {
        _startSync();
      });
    } else
      _startSync();
  }

  parse() {
  }

  protected saveThese(_toSaveLocallyAndThenSync: boolean,
                      callBack: (dataObject: BaseDataObject) => void) {
    this._toSaveLocallyAndThenSync = _toSaveLocallyAndThenSync;
    this.callBack = callBack;
  }

  protected responseHeaders(): any {
    return (this.responseRecievedFromServer.headers)['_headers'];
  }

  protected errorTextForStatusCode(_statusCode: number): string {
    if (_statusCode === 400) return AppUIHelper.getInstance().i18nText('badrequest');
    else if (_statusCode === 0) return AppUIHelper.getInstance().i18nText('errorinternetdisconnected');
    else if (_statusCode === 500) return AppUIHelper.getInstance().i18nText('errorinternalserver');
    else if (_statusCode === 408) return AppUIHelper.getInstance().i18nText('errorrequesttimeout');
    return '';
  }

  protected isStatusCodeRecievedToBeConsideredAsError(_statusCode: number): boolean {
    return true;
  }

  protected baseProcessingDone_ShouldIGive_DataObjectCallBack(): boolean {
    return true;
  }

  protected baseProcessingDone() {
  }

  protected doCleanUpActivitiesOnSuccess() {
    if (this.baseProcessingDone_ShouldIGive_DataObjectCallBack()) {
      this.callBack(this);
      this.deleteFromLocalStorageIfRequired();
    }
  }

  protected doCleanUpActivitiesOnFailure() {
    if (this.baseProcessingDone_ShouldIGive_DataObjectCallBack()) {
      this.callBack(this);
    }
  }

  cancelRequest() {
    if (this.networkCallSubscription != null) {
      this.networkCallSubscription.unsubscribe();
    }
  }

  public get hasNetworkProcessFinished(): boolean {
    return this._hasNetworkProcessFinished;
  }

  toJSON(): any {
    let jsonData: any = {
      uniqueObjectId: this.uniqueObjectId,
    };
    if (this.serverErrorDataModel != null)
      jsonData['serverErrorDataModel'] = JSON.stringify(this.serverErrorDataModel);

    // At present we will only store statuscode that we recieve from 'responseRecievedFromServer'
    if (this.responseRecievedFromServer != null) {
      let responseRecievedFromServerTemp: any = {
        status: this.responseRecievedFromServer.status,
      };
      jsonData['responseRecievedFromServer'] = JSON.stringify(responseRecievedFromServerTemp);
    }
    return jsonData;
  }

  fromJSON(json: any) {
    this.uniqueObjectId = json.uniqueObjectId;
    if (json.serverErrorDataModel != null) {
      this.serverErrorDataModel = new ServerErrorDataModel('');
      this.serverErrorDataModel.fromJSON(JSON.parse(json.serverErrorDataModel));
    }
    if (json.responseRecievedFromServer != null) {
      this.responseRecievedFromServer = JSON.parse(json.responseRecievedFromServer);
      if (this.isStatusCodeRecievedToBeConsideredAsError(this.responseRecievedFromServer.status)) {
        this.serverErrorDataModel =
        new ServerErrorDataModel(this.errorTextForStatusCode(this.responseRecievedFromServer.status));
      }
    }
  }

  private deleteFromLocalStorageIfRequired() {
    if (this._toSaveLocallyAndThenSync) {
      LocalStorageHelper.getInstance().deleteLocalStorageModel(this._localStorageModel, () => {
      });
    }
  }
}
