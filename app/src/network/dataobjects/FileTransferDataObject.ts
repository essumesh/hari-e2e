import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { FileTransferRequestObject, FileTransferType, FileTransferCategory }
from '../requestobjects/FileTransferRequestObject';
import { FileTransferResponseObject } from '../responseobjects/FileTransferResponseObject';
import { URLSearchParams } from '@angular/http';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { ManualDIHelper } from '../../helpers/ManualDIHelper';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

@Injectable()
export class FileTransferDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.DELETE;
    }

    baseURL(): string|undefined {
        let fileTransferRequestObject: FileTransferRequestObject =
        this.requestObject as FileTransferRequestObject;
        return fileTransferRequestObject.serverURLToTalkTo;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fileTransferRequestObject: FileTransferRequestObject =
        new FileTransferRequestObject();
        if (_parameters.length > 0) {
            fileTransferRequestObject.fileTransferType = _parameters[0];
            if (fileTransferRequestObject.fileTransferType === FileTransferType.UPLOAD) {
                fileTransferRequestObject.deviceFileURL = _parameters[1];
                fileTransferRequestObject.serverURLToTalkTo = _parameters[2];
                fileTransferRequestObject.fileTransferCategory = _parameters[3];
            } else if (fileTransferRequestObject.fileTransferType === FileTransferType.DELETE) {
                fileTransferRequestObject.serverURLToTalkTo = _parameters[1];
            }
        }
        this.requestObject = fileTransferRequestObject;
    }

    queryTheServer(_toSaveLocallyAndThenSync: boolean, _callBack: (dataObject: BaseDataObject) => void) {
        let fileTransferRequestObject: FileTransferRequestObject =
        this.requestObject as FileTransferRequestObject;

        if (fileTransferRequestObject.fileTransferType === FileTransferType.UPLOAD) {
            let fileUploadOptions: FileUploadOptions = {
                httpMethod: 'POST',
                headers: {'Authorization': APIConstants.authorizationToken(),
                            'accept': 'application/json'},
                mimeType: fileTransferRequestObject.fileTransferCategory === FileTransferCategory.IMAGE ?
                            'image/jpeg' : 'video/mp4',
            };

            let fileTransferResponseRecieved: boolean = false;
            const _failure = ((_err: any) => {
                fileTransferResponseRecieved = true;
                this.serverErrorDataModel =
                new ServerErrorDataModel(this.errorTextForStatusCode(_err.status));
                _callBack(this);
            });

            let fileTransfer: FileTransfer =
            ManualDIHelper.getInstance().createObject('FileTransfer');
            const fileTransferObject: FileTransferObject = fileTransfer.create();
            fileTransferObject.upload
            (fileTransferRequestObject.deviceFileURL, fileTransferRequestObject.serverURLToTalkTo, fileUploadOptions)
                .then((_data: any) => {
                    fileTransferResponseRecieved = true;
                    this.parse();
                    _callBack(this);
                }, (_err: any) => {
                    _failure(_err);
            });
            setTimeout(() => {
                if (!fileTransferResponseRecieved) {
                    fileTransferObject.abort();
                    _failure({status: 0});
                }
            }, (BaseDataObject.TIMEOUT_IN_SECONDS * 10) * 1000);
        } else if (fileTransferRequestObject.fileTransferType === FileTransferType.DELETE) {
            super.queryTheServer(_toSaveLocallyAndThenSync, _callBack);
        }
    }

    parse() {
        let fileTransferResponseObject = new FileTransferResponseObject();
        this.responseObject = fileTransferResponseObject;
    }
}
