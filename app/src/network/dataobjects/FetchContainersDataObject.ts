import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { FetchContainersRequestObject } from '../requestobjects/FetchContainersRequestObject';
import { FetchContainersResponseObject } from '../responseobjects/FetchContainersResponseObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class FetchContainersDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        let fetchContainersRequestObject: FetchContainersRequestObject =
        this.requestObject as FetchContainersRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/containers/'
                                         + fetchContainersRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fetchContainersRequestObject: FetchContainersRequestObject =
        new FetchContainersRequestObject();
        if (_parameters.length > 0) {
            fetchContainersRequestObject.accept = _parameters[0];
            fetchContainersRequestObject.id = _parameters[1];
          }
        this.requestObject = fetchContainersRequestObject;
    }

    requestParameters(): URLSearchParams {
        let fetchContainersRequestObject: FetchContainersRequestObject =
        this.requestObject as FetchContainersRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', fetchContainersRequestObject.accept);
        requestParameters.set('id', fetchContainersRequestObject.id);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new FetchContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
