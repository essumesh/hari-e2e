
import { BaseDataObject } from './BaseDataObject';
import { Injectable } from '@angular/core';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';
import { FetchOriginRequestObject } from '../requestobjects/FetchOriginRequestObject';
import { FoundCargoStationDataModel } from '../../datamodels/FoundCargoStationDataModel';
import { FetchOriginResponseObject } from '../responseobjects/FetchOriginResponseObject';

@Injectable()
export class FoundCargoOrignDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }
    baseURL(): string|undefined {
        return 'assets/mocks';
      }

    methodName(): string | undefined {
        return APIConstants.TO_USE_MOCKS ? '/foundcargoOrigin.json'
            : '/foundcargoOrigin.json';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fetchOriginDataObject: FetchOriginRequestObject =
            new FetchOriginRequestObject();
        this.requestObject = fetchOriginDataObject;
    }
    parse() {
        if (this.responseRecievedFromServer.status === 200) {
            let fetchOriginResponseObject = new FetchOriginResponseObject();
            this.responseObject = fetchOriginResponseObject;

            let dataRecievedFromServer = this.responseRecievedFromServer.json();
            if (dataRecievedFromServer != null) {
                fetchOriginResponseObject.foundCargoStationDataModel = new FoundCargoStationDataModel();
                fetchOriginResponseObject.foundCargoStationDataModel.parse(dataRecievedFromServer);
            } else if (this.responseRecievedFromServer.status === 404) {
                this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
            }
        }
    }
    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new FetchOriginRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }
}
