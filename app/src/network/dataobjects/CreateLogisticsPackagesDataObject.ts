import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { CreateLogisticsPackagesRequestObject } from '../requestobjects/CreateLogisticsPackagesRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class CreateLogisticsPackagesDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.POST;
    }

    methodName(): string | undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
            : '/acceptance/v1/logistics-packages';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let createLogisticsPackagesRequestObject: CreateLogisticsPackagesRequestObject =
            new CreateLogisticsPackagesRequestObject();
        if (_parameters.length > 0) {
            createLogisticsPackagesRequestObject.logisticsPackages = _parameters[0];
        }
        this.requestObject = createLogisticsPackagesRequestObject;
    }

    requestBody(): any {
        let createLogisticsPackagesRequestObject: CreateLogisticsPackagesRequestObject =
            this.requestObject as CreateLogisticsPackagesRequestObject;

        let jsonData = createLogisticsPackagesRequestObject.logisticsPackages.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new CreateLogisticsPackagesRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
