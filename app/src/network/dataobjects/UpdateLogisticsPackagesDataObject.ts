import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { ReplaceOrUpdateLogisticsPackagesRequestObject }
from '../requestobjects/ReplaceOrUpdateLogisticsPackagesRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class UpdateLogisticsPackagesDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.PATCH;
    }

    methodName(): string | undefined {
        let updateLogisticsPackagesRequestObject: ReplaceOrUpdateLogisticsPackagesRequestObject =
            this.requestObject as ReplaceOrUpdateLogisticsPackagesRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
            : '/acceptance/v1/logistics-packages/' + updateLogisticsPackagesRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let updateLogisticsPackagesRequestObject: ReplaceOrUpdateLogisticsPackagesRequestObject =
            new ReplaceOrUpdateLogisticsPackagesRequestObject();
        if (_parameters.length > 0) {
            updateLogisticsPackagesRequestObject.id = _parameters[0];
            updateLogisticsPackagesRequestObject.logisticsPackages = _parameters[1];
        }
        this.requestObject = updateLogisticsPackagesRequestObject;
    }

    requestBody(): any {
        let updateLogisticsPackagesRequestObject: ReplaceOrUpdateLogisticsPackagesRequestObject =
            this.requestObject as ReplaceOrUpdateLogisticsPackagesRequestObject;

        let jsonData = updateLogisticsPackagesRequestObject.logisticsPackages.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new ReplaceOrUpdateLogisticsPackagesRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
