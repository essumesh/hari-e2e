import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { FetchContainersRequestObject } from '../requestobjects/FetchContainersRequestObject';
import { FetchHandlingInformationResponseObject } from '../responseobjects/FetchHandlingInformationResponseObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class FetchHandlingInformationDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        let fetchHandlingInformationRequestObject: FetchContainersRequestObject =
        this.requestObject as FetchContainersRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/handling-information/'
                                         + fetchHandlingInformationRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fetchHandlingInformationRequestObject: FetchContainersRequestObject =
        new FetchContainersRequestObject();
        if (_parameters.length > 0) {
            fetchHandlingInformationRequestObject.accept = _parameters[0];
            fetchHandlingInformationRequestObject.id = _parameters[1];
          }
        this.requestObject = fetchHandlingInformationRequestObject;
    }

    requestParameters(): URLSearchParams {
        let fetchHandlingInformationRequestObject: FetchContainersRequestObject =
        this.requestObject as FetchContainersRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', fetchHandlingInformationRequestObject.accept);
        requestParameters.set('id', fetchHandlingInformationRequestObject.id);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new FetchContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
