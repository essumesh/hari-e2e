import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { ReplaceOrUpdateSplitsRequestObject } from '../requestobjects/ReplaceOrUpdateSplitsRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class ReplaceSplitsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.PUT;
    }

    methodName(): string | undefined {
        let replaceSplitsRequestObject: ReplaceOrUpdateSplitsRequestObject =
            this.requestObject as ReplaceOrUpdateSplitsRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
            : '/acceptance/v1/splits/' + replaceSplitsRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let replaceSplitsRequestObject: ReplaceOrUpdateSplitsRequestObject =
            new ReplaceOrUpdateSplitsRequestObject();
        if (_parameters.length > 0) {
            replaceSplitsRequestObject.id = _parameters[0];
            replaceSplitsRequestObject.splits = _parameters[1];
        }
        this.requestObject = replaceSplitsRequestObject;
    }

    requestBody(): any {
        let replaceSplitsRequestObject: ReplaceOrUpdateSplitsRequestObject =
            this.requestObject as ReplaceOrUpdateSplitsRequestObject;

        let jsonData = replaceSplitsRequestObject.splits.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new ReplaceOrUpdateSplitsRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
