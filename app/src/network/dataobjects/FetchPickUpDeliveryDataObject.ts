import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { FetchPickUpDeliveryRequestObject } from '../requestobjects/FetchPickUpDeliveryRequestObject';
import { FetchPickUpDeliveryResponseObject } from '../responseobjects/FetchPickUpDeliveryResponseObject';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';
import { URLSearchParams } from '@angular/http';
import { AppUIHelper } from '../../helpers/AppUIHelper';
import { PickUpDeliveryReceiptDataModel } from '../../datamodels/PickUpDeliveryReceiptDataModel';
@Injectable()
export class FetchPickUpDeliveryDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/pickupdelivery.json'
                                         : '/pickup/v1/deliveries/';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fetchPickUpDeliveryRequestObject: FetchPickUpDeliveryRequestObject =
        new FetchPickUpDeliveryRequestObject();
        if (_parameters.length > 0) {
            fetchPickUpDeliveryRequestObject.receiptNumber = _parameters[0];
          }
        this.requestObject = fetchPickUpDeliveryRequestObject;
    }

    requestParameters(): URLSearchParams {
        let fetchPickUpDeliveryRequestObject: FetchPickUpDeliveryRequestObject =
        this.requestObject as FetchPickUpDeliveryRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('deliveryNumber', fetchPickUpDeliveryRequestObject.receiptNumber);

        return requestParameters;
    }

    parse() {
            if (this.responseRecievedFromServer.status === 200) {
            let fetchPickUpDeliveryRequestObject = new FetchPickUpDeliveryResponseObject();
            this.responseObject = fetchPickUpDeliveryRequestObject;

            let dataRecievedFromServer = this.responseRecievedFromServer.json();
            if (dataRecievedFromServer != null) {
                fetchPickUpDeliveryRequestObject.pickupReferencesDataModel =
                new PickUpDeliveryReceiptDataModel();
                fetchPickUpDeliveryRequestObject.
                pickupReferencesDataModel.parse(dataRecievedFromServer);

            }
        }else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Record not found');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new FetchPickUpDeliveryRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }
    protected errorTextForStatusCode(_statusCode: number): string {
        if (_statusCode === 404)
            return AppUIHelper.getInstance().i18nText('Record not found');
        else
            return super.errorTextForStatusCode(_statusCode);
    }
}
