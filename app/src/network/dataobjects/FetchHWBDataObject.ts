import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { FetchHWBRequestObject } from '../requestobjects/FetchHWBRequestObject';
import { FetchHWBResponseObject } from '../responseobjects/FetchHWBResponseObject';
import { HouseAirwaybillsDataModel } from '../../datamodels/HouseAirwaybillsDataModel';
import { URLSearchParams } from '@angular/http';
import { BarcodeParserHelper } from '../../helpers/BarcodeParserHelper';

@Injectable()
export class FetchHWBDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/fetchhwbdataobject.json'
                                         : '/discrepancy/v1/house-airwaybills/';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fetchHWBRequestObject: FetchHWBRequestObject =
        new FetchHWBRequestObject();
        if (_parameters.length > 0) {
            fetchHWBRequestObject.houseAirwaybillNumber = _parameters[0];
          }
        this.requestObject = fetchHWBRequestObject;
    }

    requestParameters(): URLSearchParams {
        let fetchHWBRequestObject: FetchHWBRequestObject = this.requestObject as FetchHWBRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('houseAirwaybillNumber', fetchHWBRequestObject.houseAirwaybillNumber);

        return requestParameters;
    }

    parse() {
        let fetchHWBResponseObject = new FetchHWBResponseObject();
        this.responseObject = fetchHWBResponseObject;

        if (this.responseRecievedFromServer.status === 200) {
            let dataRecievedFromServer = this.responseRecievedFromServer.json();
            if (dataRecievedFromServer.constructor === {}.constructor) {
                let _embeddedJSON = dataRecievedFromServer['_embedded'];
                if (_embeddedJSON != null && _embeddedJSON.constructor === {}.constructor) {
                    let houseAirwaybillsArray = _embeddedJSON['houseAwbs'];
                    if (houseAirwaybillsArray != null && houseAirwaybillsArray instanceof Array) {
                        for (let arrayItem of houseAirwaybillsArray) {
                            let houseAirwaybillsDataModel = new HouseAirwaybillsDataModel();
                            houseAirwaybillsDataModel.parse(arrayItem);
                            fetchHWBResponseObject.houseAirwaybillsDataModels.push(houseAirwaybillsDataModel);
                          }
                    }
                }
            }
        }  else if (this.responseRecievedFromServer.status === 404) {
            let fetchHWBRequestObject: FetchHWBRequestObject = this.requestObject as FetchHWBRequestObject;
            fetchHWBResponseObject.hwbNotFound = true;
        }
    }

    protected isStatusCodeRecievedToBeConsideredAsError(_statusCode: number): boolean {
        if (_statusCode === 404) return false;
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new FetchHWBRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }
}
