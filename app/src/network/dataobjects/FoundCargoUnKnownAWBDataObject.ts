import { BaseDataObject } from './BaseDataObject';
import { Injectable } from '@angular/core';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { FetchUnKnownAWBResponseObject } from '../responseobjects/FetchUnKnownAWBResponseObject';
import { AirwaybillsDataModel } from '../../datamodels/AirwaybillsDataModel';
import { FetchUnKnownAWBRequestObject } from '../requestobjects/FetchUnKnownAWBRequestObject';

@Injectable()
export class FoundCargoUnKnownAWBDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/fetchunknownAWB.json'
                                         : '/discrepancy/v1/unknown-airwaybills/';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fetchUnKnownAWBRequestObject: FetchUnKnownAWBRequestObject =
        new FetchUnKnownAWBRequestObject();
        this.requestObject = fetchUnKnownAWBRequestObject;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200) {
            let fetchUnKnownAWBResponseObject = new FetchUnKnownAWBResponseObject();
            this.responseObject = fetchUnKnownAWBResponseObject;

            let dataRecievedFromServer = this.responseRecievedFromServer.json();
            if (dataRecievedFromServer != null) {
                fetchUnKnownAWBResponseObject.airwaybillsDataModel =
                new AirwaybillsDataModel();
                fetchUnKnownAWBResponseObject.
                airwaybillsDataModel.parse(dataRecievedFromServer);

            }
        }else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new FetchUnKnownAWBRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }
}
