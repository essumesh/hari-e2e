import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { QueryItinerariesRequestObject } from '../requestobjects/QueryItinerariesRequestObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class QueryItinerariesDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/itineraries';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let queryItinerariesRequestObject: QueryItinerariesRequestObject =
        new QueryItinerariesRequestObject();
        if (_parameters.length > 0) {
            queryItinerariesRequestObject.accept = _parameters[0];
            queryItinerariesRequestObject.id = _parameters[1];
            queryItinerariesRequestObject.segment = _parameters[2];
            queryItinerariesRequestObject.originid = _parameters[3];
            queryItinerariesRequestObject.origincode = _parameters[4];
            queryItinerariesRequestObject.origintype = _parameters[5];
            queryItinerariesRequestObject.destinationid = _parameters[6];
            queryItinerariesRequestObject.destinationtype = _parameters[7];
            queryItinerariesRequestObject.page = _parameters[8];
            queryItinerariesRequestObject.sort = _parameters[9];
          }
        this.requestObject = queryItinerariesRequestObject;
    }

    requestParameters(): URLSearchParams {
        let queryItinerariesRequestObject: QueryItinerariesRequestObject =
        this.requestObject as QueryItinerariesRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();
        requestParameters.set('accept', queryItinerariesRequestObject.accept);
        requestParameters.set('id', queryItinerariesRequestObject.id);
        requestParameters.set('segment', queryItinerariesRequestObject.segment.toString());
        requestParameters.set('originid', queryItinerariesRequestObject.originid);
        requestParameters.set('origincode', queryItinerariesRequestObject.origincode);
        requestParameters.set('origintype', queryItinerariesRequestObject.origintype);
        requestParameters.set('destinationid', queryItinerariesRequestObject.destinationid);
        requestParameters.set('destinationcode',
        queryItinerariesRequestObject.destinationcode);
        requestParameters.set('destinationtype', queryItinerariesRequestObject.destinationtype);
        requestParameters.set('page',
        queryItinerariesRequestObject.page != null
        ? queryItinerariesRequestObject.page.toString()
        : null);
        requestParameters.set('sort',
        queryItinerariesRequestObject.sort != null
        ? queryItinerariesRequestObject.sort.toString()
        : null);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new QueryItinerariesRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
