import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { CreatePartsRequestObject } from '../requestobjects/CreatePartsRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class CreatePartsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.POST;
    }

    methodName(): string | undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
            : '/acceptance/v1/parts';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let createPartsRequestObject: CreatePartsRequestObject =
            new CreatePartsRequestObject();
        if (_parameters.length > 0) {
            createPartsRequestObject.parts = _parameters[0];
        }
        this.requestObject = createPartsRequestObject;
    }

    requestBody(): any {
        let createPartsRequestObject: CreatePartsRequestObject =
            this.requestObject as CreatePartsRequestObject;

        let jsonData = createPartsRequestObject.parts.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new CreatePartsRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
