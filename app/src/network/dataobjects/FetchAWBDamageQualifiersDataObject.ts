import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { FetchAWBDamageQualifiersRequestObject } from '../requestobjects/FetchAWBDamageQualifiersRequestObject';
import { FetchAWBDamageQualifiersResponseObject } from '../responseobjects/FetchAWBDamageQualifiersResponseObject';
import { DamageReferencesDataModel } from '../../datamodels/DamageReferencesDataModel';

@Injectable()
export class FetchAWBDamageQualifiersDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/qualifiers.json'
                                         : '/discrepancy/v1/qualifiers/';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fetchAWBDamageQualifiersRequestObject: FetchAWBDamageQualifiersRequestObject =
        new FetchAWBDamageQualifiersRequestObject();
        this.requestObject = fetchAWBDamageQualifiersRequestObject;
    }

    parse() {
        let fetchAWBDamageQualifiersResponseObject = new FetchAWBDamageQualifiersResponseObject();
        this.responseObject = fetchAWBDamageQualifiersResponseObject;

        let dataRecievedFromServer = this.responseRecievedFromServer.json();
        if (dataRecievedFromServer != null) {
            if (dataRecievedFromServer.constructor === {}.constructor) {
                let embeddedJSON = dataRecievedFromServer['_embedded'];
                if (embeddedJSON != null && embeddedJSON.constructor === {}.constructor) {
                    let damageQualifiersArray = embeddedJSON['damageQualifiers'];
                    if (damageQualifiersArray != null && damageQualifiersArray instanceof Array) {
                        if (damageQualifiersArray.length > 0) {
                            fetchAWBDamageQualifiersResponseObject.damageReferencesDataModel =
                            new DamageReferencesDataModel();
                            fetchAWBDamageQualifiersResponseObject.
                            damageReferencesDataModel.parse(damageQualifiersArray[0]);
                        }
                    }
                }
            }
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new FetchAWBDamageQualifiersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }
}
