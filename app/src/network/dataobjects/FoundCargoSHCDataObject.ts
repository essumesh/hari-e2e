import { BaseDataObject } from './BaseDataObject';
import { Injectable } from '@angular/core';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { FetchSHCRequestObject } from '../requestobjects/FetchSHCRequestObject';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';
import { FetchSHCResponseObject } from '../responseobjects/FetchSHCResponseObject';
import { FoundCargoSHCDataModel } from '../../datamodels/FoundCargoSHCDataModel';

@Injectable()
export class FoundCargoSHCDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }
    baseURL(): string|undefined {
        return 'assets/mocks';
      }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/FoundCargoSHC.json'
                                         : '/FoundCargoSHC.json';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fetchSHCRequestObject: FetchSHCRequestObject =
        new FetchSHCRequestObject();
        this.requestObject = fetchSHCRequestObject;
    }
    parse() {
        if (this.responseRecievedFromServer.status === 200) {
           let fetchSHCResponseObject = new FetchSHCResponseObject();
           this.responseObject = fetchSHCResponseObject;

           let dataRecievedFromServer = this.responseRecievedFromServer.json();
           if (dataRecievedFromServer != null) {
            fetchSHCResponseObject.foundCargoSHCDataModel = new FoundCargoSHCDataModel();
            fetchSHCResponseObject.foundCargoSHCDataModel.parse(dataRecievedFromServer);
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }
}

        toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

        fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new FetchSHCRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }
}
