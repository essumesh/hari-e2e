import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { FetchPickUpDeliveryRequestObject } from '../requestobjects/FetchPickUpDeliveryRequestObject';
import { FetchPickUpDeliveryResponseObject } from '../responseobjects/FetchPickUpDeliveryResponseObject';
import { PickUpDeliveryDataModel } from '../../datamodels/PickUpDeliveryDataModel';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';
import { URLSearchParams } from '@angular/http';
import { PickupDriverInfoRequestObject } from '../requestobjects/PickupDriverInfoRequestObject';

@Injectable()
export class PickupDriverInfoDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.HEAD;
    }

    methodName(): string|undefined {
        let pickupDriverInfoRequestObject: PickupDriverInfoRequestObject =
        this.requestObject as PickupDriverInfoRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/pickup/v1/pickup-drivers/'
                                         : '/pickup/v1/pickup-drivers/'
                                         + pickupDriverInfoRequestObject.driverID;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let pickupDriverInfoRequestObject: PickupDriverInfoRequestObject =
        new PickupDriverInfoRequestObject();
        if (_parameters.length > 0) {
            pickupDriverInfoRequestObject.driverID = _parameters[0];
          }
        this.requestObject = pickupDriverInfoRequestObject;
    }

    parse() {
         if (this.responseRecievedFromServer.status === 200) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        }else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Record not found');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new PickupDriverInfoRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }
}
