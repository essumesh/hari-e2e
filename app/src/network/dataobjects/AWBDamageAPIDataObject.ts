import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { URLSearchParams } from '@angular/http';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { AWBDamageAPIRequestObject } from '../requestobjects/AWBDamageAPIRequestObject';
import { AWBDamageAPIResponseObject } from '../responseobjects/AWBDamageAPIResponseObject';
import { DamageReferencesDataModel } from '../../datamodels/DamageReferencesDataModel';
import { ImagesDataModel } from '../../datamodels/ImagesDataModel';
import { LinkMediaDataModel } from '../../datamodels/LinkMediaDataModel';
import { AppUIHelper } from '../../helpers/AppUIHelper';
import { FileTransferRequestObject, FileTransferType, FileTransferCategory }
from '../requestobjects/FileTransferRequestObject';
import { FileTransferDataObject } from './FileTransferDataObject';
import { ManualDIHelper } from '../../helpers/ManualDIHelper';
import { AirwaybillsDataModel } from '../../datamodels/AirwaybillsDataModel';

@Injectable()
export class AWBDamageAPIDataObject extends BaseDataObject {
    private isSyncMediaDone: boolean = false;

    uniqueObjectIdClassName(): string {
        return 'AWBDamageAPIDataObject';
    }

    methodType(): NetworkMethodType {
        let awbDamageAPIRequestObject: AWBDamageAPIRequestObject = this.requestObject as AWBDamageAPIRequestObject;
        return awbDamageAPIRequestObject.isThisAFreshlyCreatedDamageReport
               ? NetworkMethodType.POST
               : NetworkMethodType.PUT;
    }

    methodName(): string|undefined {
        let awbDamageAPIRequestObject: AWBDamageAPIRequestObject = this.requestObject as AWBDamageAPIRequestObject;

        return APIConstants.TO_USE_MOCKS ? '/qualifiers.json'
                                         : (awbDamageAPIRequestObject.isThisAFreshlyCreatedDamageReport
                                            ? (awbDamageAPIRequestObject.
                                                damageReportsDataModel.
                                                doWeHaveOnlyThePresenceOfMediaOnDamageReportsDataModel()
                                                ? '/discrepancy/v1/damage-reports-drafts'
                                                : '/discrepancy/v1/damage-reports')
                                            : '/discrepancy/v1/damage-reports/'
                                              + awbDamageAPIRequestObject.damageReportsDataModel.reportId);
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let awbDamageAPIRequestObject: AWBDamageAPIRequestObject =
        new AWBDamageAPIRequestObject();
        if (_parameters.length > 0) {
            awbDamageAPIRequestObject.isThisAFreshlyCreatedDamageReport = _parameters[0];
            awbDamageAPIRequestObject.damageReportsDataModel = _parameters[1];
            awbDamageAPIRequestObject.directlySyncMediaWithoutAnySyncingAnythingOnDamageReportData =
            _parameters[2];
        }
        this.requestObject = awbDamageAPIRequestObject;
    }

    requestBody(): any {
        let awbDamageAPIRequestObject: AWBDamageAPIRequestObject =
        this.requestObject as AWBDamageAPIRequestObject;

        let airwaybillsDataModel: AirwaybillsDataModel =
        awbDamageAPIRequestObject.damageReportsDataModel.airwaybill;
        let wasAirwaybillsDataModelManipulated: boolean = false;
        if (airwaybillsDataModel == null
            && awbDamageAPIRequestObject.damageReportsDataModel.houseAirwaybill != null
            && awbDamageAPIRequestObject.damageReportsDataModel.houseAirwaybill.masterAirwaybill != null) {
            wasAirwaybillsDataModelManipulated = true;
            awbDamageAPIRequestObject.damageReportsDataModel.airwaybill =
            awbDamageAPIRequestObject.damageReportsDataModel.houseAirwaybill.masterAirwaybill;
        }

        let linkMediaDataModelsBeforeManipulation: Array<LinkMediaDataModel> = null;
        const linkMediaDataModelsManipulated: Array<LinkMediaDataModel> = [];
        if (awbDamageAPIRequestObject.damageReportsDataModel.images != null) {
            linkMediaDataModelsBeforeManipulation =
            awbDamageAPIRequestObject.damageReportsDataModel.images.linkMediaDataModels;
            for (let linkMediaDataModel of
                linkMediaDataModelsBeforeManipulation) {
                if (linkMediaDataModel.haveWeGotMediaDataFromServer()) {
                   linkMediaDataModelsManipulated.push(linkMediaDataModel);
               }
           }
            awbDamageAPIRequestObject.damageReportsDataModel.images.linkMediaDataModels =
            linkMediaDataModelsManipulated;
        }

        let jsonData: any = awbDamageAPIRequestObject.damageReportsDataModel.toJSON();

        if (wasAirwaybillsDataModelManipulated)
            awbDamageAPIRequestObject.damageReportsDataModel.airwaybill =
            null;
        awbDamageAPIRequestObject.damageReportsDataModel.images.linkMediaDataModels =
        linkMediaDataModelsBeforeManipulation;

        return jsonData;
    }

    queryTheServer(_toSaveLocallyAndThenSync: boolean,
        callBack: (dataObject: BaseDataObject) => void) {
        let awbDamageAPIRequestObject: AWBDamageAPIRequestObject = this.requestObject as AWBDamageAPIRequestObject;
        if (awbDamageAPIRequestObject.directlySyncMediaWithoutAnySyncingAnythingOnDamageReportData) {
            this.saveThese(_toSaveLocallyAndThenSync, callBack);

            let awbDamageAPIResponseObject = new AWBDamageAPIResponseObject();
            this.responseObject = awbDamageAPIResponseObject;

            this.baseProcessingDone();
        } else {
            super.queryTheServer(_toSaveLocallyAndThenSync, callBack);
        }
    }

    parse() {
        let awbDamageAPIRequestObject: AWBDamageAPIRequestObject =
        this.requestObject as AWBDamageAPIRequestObject;

        let awbDamageAPIResponseObject = new AWBDamageAPIResponseObject();
        this.responseObject = awbDamageAPIResponseObject;

        const assignStuff = ((_reportLocation: string) => {
            awbDamageAPIResponseObject.damageReportCreatedAtLocation =
            _reportLocation;
            awbDamageAPIResponseObject.reportId =
            awbDamageAPIResponseObject.damageReportCreatedAtLocation.substring
            (awbDamageAPIResponseObject.damageReportCreatedAtLocation.lastIndexOf('/') + 1);
        });

        if (awbDamageAPIRequestObject.isThisAFreshlyCreatedDamageReport) {
            let responseHeaders: Map<string, Array<string>> = this.responseHeaders();
            let locationArray = responseHeaders.get('location');
            if (locationArray != null) assignStuff(locationArray[0]);
        } else {
            const _urlOfReportLocation: string = this.responseRecievedFromServer['url'];
            if (_urlOfReportLocation != null) assignStuff(_urlOfReportLocation);
        }
    }

    urlToPOSTMediaTo(_linkMediaDataModel: LinkMediaDataModel): string {
        let awbDamageAPIRequestObject: AWBDamageAPIRequestObject =
        this.requestObject as AWBDamageAPIRequestObject;

        let awbDamageAPIResponseObject: AWBDamageAPIResponseObject =
        this.responseObject as AWBDamageAPIResponseObject;

        if (awbDamageAPIRequestObject.isThisAFreshlyCreatedDamageReport)
            return awbDamageAPIResponseObject.damageReportCreatedAtLocation
                   + '/files';
        else
            return this.baseURL()
                   + this.methodName()
                   + '/files';
    }

    urlToDELETEMediafrom(_linkMediaDataModel: LinkMediaDataModel): string {
        return this.baseURL()
                   + this.methodName()
                   + '/files/'
                   + _linkMediaDataModel.fileNameWithExtension();
    }

    protected errorTextForStatusCode(_statusCode: number): string {
        if (_statusCode === 422)
            return AppUIHelper.getInstance().i18nText('errorValidationError');
        else
            return super.errorTextForStatusCode(_statusCode);
    }

    protected baseProcessingDone_ShouldIGive_DataObjectCallBack(): boolean {
        if (this.serverErrorDataModel != null) return true;
        else return this.isSyncMediaDone;
    }

    protected baseProcessingDone() {
        if (!this.isSyncMediaDone && this.serverErrorDataModel == null) {
            this.syncMedia(0);
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new AWBDamageAPIRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

    private syncMedia(_startingFromIndex: number) {
        let awbDamageAPIRequestObject: AWBDamageAPIRequestObject = this.requestObject as AWBDamageAPIRequestObject;
        let awbDamageAPIResponseObject: AWBDamageAPIResponseObject = this.responseObject as AWBDamageAPIResponseObject;

        const syncNextOneInPipeLine = (() => {
            this.syncMedia(++_startingFromIndex);
        });

        let images: ImagesDataModel = awbDamageAPIRequestObject.damageReportsDataModel.images;
        if (images != null && _startingFromIndex < images.linkMediaDataModels.length) {
            let linkMediaDataModel: LinkMediaDataModel = images.linkMediaDataModels[_startingFromIndex];
            if (!linkMediaDataModel.haveWeGotMediaDataFromServer()) {
                if (linkMediaDataModel.href != null) {
                    let fileTransferDataObject: FileTransferDataObject =
                    ManualDIHelper.getInstance().createObject('FileTransferDataObject');
                    fileTransferDataObject.prepareRequestWithParameters
                    (FileTransferType.UPLOAD,
                     linkMediaDataModel.href,
                     this.urlToPOSTMediaTo(linkMediaDataModel),
                     linkMediaDataModel.fileTransferCategory);
                    fileTransferDataObject.queryTheServer(true, (_dataObject: BaseDataObject) => {
                        syncNextOneInPipeLine();
                    });
                }
            } else syncNextOneInPipeLine();
        } else {
            this.isSyncMediaDone = true;
            if (this.serverErrorDataModel != null) this.doCleanUpActivitiesOnFailure();
            else this.doCleanUpActivitiesOnSuccess();
        }
    }
}
