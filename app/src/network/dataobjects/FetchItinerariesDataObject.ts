import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { FetchContainersRequestObject } from '../requestobjects/FetchContainersRequestObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class FetchItinerariesDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        let fetchItinerariesRequestObject: FetchContainersRequestObject =
        this.requestObject as FetchContainersRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/itineraries/'
                                         + fetchItinerariesRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fetchItinerariesRequestObject: FetchContainersRequestObject =
        new FetchContainersRequestObject();
        if (_parameters.length > 0) {
            fetchItinerariesRequestObject.accept = _parameters[0];
            fetchItinerariesRequestObject.id = _parameters[1];
          }
        this.requestObject = fetchItinerariesRequestObject;
    }

    requestParameters(): URLSearchParams {
        let fetchItinerariesRequestObject: FetchContainersRequestObject =
        this.requestObject as FetchContainersRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', fetchItinerariesRequestObject.accept);
        requestParameters.set('id', fetchItinerariesRequestObject.id);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new FetchContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
