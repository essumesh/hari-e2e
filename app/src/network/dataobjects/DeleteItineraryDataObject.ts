import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { DeleteObjectByIdRequestObject } from '../requestobjects/DeleteObjectByIdRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class DeleteItineraryDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.DELETE;
    }

    methodName(): string | undefined {
        let deleteObjectByIdRequestObject: DeleteObjectByIdRequestObject =
        this.requestObject as DeleteObjectByIdRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
            : '/acceptance/v1/itineraries'
            + deleteObjectByIdRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let deleteObjectByIdRequestObject: DeleteObjectByIdRequestObject =
            new DeleteObjectByIdRequestObject();
        if (_parameters.length > 0) {
            deleteObjectByIdRequestObject.id = _parameters[0];
        }
        this.requestObject = deleteObjectByIdRequestObject;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new DeleteObjectByIdRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
