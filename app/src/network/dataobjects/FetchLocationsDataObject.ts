import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { FetchContainersRequestObject } from '../requestobjects/FetchContainersRequestObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class FetchLocationsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        let fetchLocationsRequestObject: FetchContainersRequestObject =
        this.requestObject as FetchContainersRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/locations/'
                                         + fetchLocationsRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let fetchLocationsRequestObject: FetchContainersRequestObject =
        new FetchContainersRequestObject();
        if (_parameters.length > 0) {
            fetchLocationsRequestObject.accept = _parameters[0];
            fetchLocationsRequestObject.id = _parameters[1];
          }
        this.requestObject = fetchLocationsRequestObject;
    }

    requestParameters(): URLSearchParams {
        let fetchLocationsRequestObject: FetchContainersRequestObject =
        this.requestObject as FetchContainersRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', fetchLocationsRequestObject.accept);
        requestParameters.set('id', fetchLocationsRequestObject.id);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new FetchContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
