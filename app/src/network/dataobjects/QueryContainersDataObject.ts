import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { QueryContainersRequestObject } from '../requestobjects/QueryContainersRequestObject';
import { QueryContainersResponseObject } from '../responseobjects/QueryContainersResponseObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class QueryContainersDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/containers/';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let queryContainersRequestObject: QueryContainersRequestObject =
        new QueryContainersRequestObject();
        if (_parameters.length > 0) {
            queryContainersRequestObject.accept = _parameters[0];
            queryContainersRequestObject.id = _parameters[1];
            queryContainersRequestObject.type = _parameters[2];
            queryContainersRequestObject.containerNumber = _parameters[3];
            queryContainersRequestObject.containerDestinationid = _parameters[4];
            queryContainersRequestObject.containerDestinationcode = _parameters[5];
            queryContainersRequestObject.containerDestinationtype = _parameters[6];
            queryContainersRequestObject.status = _parameters[7];
            queryContainersRequestObject.page = _parameters[8];
            queryContainersRequestObject.sort = _parameters[9];
          }
        this.requestObject = queryContainersRequestObject;
    }

    requestParameters(): URLSearchParams {
        let queryContainersRequestObject: QueryContainersRequestObject =
        this.requestObject as QueryContainersRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', queryContainersRequestObject.accept);
        requestParameters.set('id', queryContainersRequestObject.id);
        requestParameters.set('type', queryContainersRequestObject.type);
        requestParameters.set('containerNumber', queryContainersRequestObject.containerNumber);
        requestParameters.set('containerDestinationid', queryContainersRequestObject.containerDestinationid);
        requestParameters.set('containerDestinationcode', queryContainersRequestObject.containerDestinationcode);
        requestParameters.set('containerDestinationtype', queryContainersRequestObject.containerDestinationtype);
        requestParameters.set('status', queryContainersRequestObject.status);
        requestParameters.set('page',
        queryContainersRequestObject.page != null
        ? queryContainersRequestObject.page.toString()
        : null);
        requestParameters.set('sort',
        queryContainersRequestObject.sort != null
        ? queryContainersRequestObject.sort.toString()
        : null);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new QueryContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
