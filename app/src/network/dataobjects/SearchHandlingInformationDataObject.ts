import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { SearchContainersRequestObject } from '../requestobjects/SearchContainersRequestObject';
import { SearchHandlingInformationResponseObject } from '../responseobjects/SearchHandlingInformationResponseObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class SearchHandlingInformationDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/handling-information/search';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let searchHandlingInformationRequestObject: SearchContainersRequestObject =
        new SearchContainersRequestObject();
        if (_parameters.length > 0) {
            searchHandlingInformationRequestObject.accept = _parameters[0];
            searchHandlingInformationRequestObject.query = _parameters[1];
            searchHandlingInformationRequestObject.page = _parameters[2];
            searchHandlingInformationRequestObject.sort = _parameters[3];
          }
        this.requestObject = searchHandlingInformationRequestObject;
    }

    requestParameters(): URLSearchParams {
        let searchHandlingInformationRequestObject: SearchContainersRequestObject =
        this.requestObject as SearchContainersRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', searchHandlingInformationRequestObject.accept);
        requestParameters.set('query', searchHandlingInformationRequestObject.query);
        requestParameters.set('page',
        searchHandlingInformationRequestObject.page != null
        ? searchHandlingInformationRequestObject.page.toString()
        : null);
        requestParameters.set('sort',
        searchHandlingInformationRequestObject.sort != null
        ? searchHandlingInformationRequestObject.sort.toString()
        : null);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new SearchContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
