import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { CreateFoundCargoRequestObject } from '../requestobjects/CreateFoundCargoRequestObject';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';
import { AppUIHelper } from '../../helpers/AppUIHelper';
@Injectable()
export class CreateFoundCargoDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.POST;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/discrepancy/v1/found-cargo'
        : '/discrepancy/v1/found-cargo';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let createFoundCargoRequestObject: CreateFoundCargoRequestObject =
            new CreateFoundCargoRequestObject();
        if (_parameters.length > 0) {
            createFoundCargoRequestObject.foundCargoDataModel = _parameters[0];
            }
        this.requestObject = createFoundCargoRequestObject;
    }

    requestBody(): any {
        let createFoundCargoRequestObject: CreateFoundCargoRequestObject =
            this.requestObject as CreateFoundCargoRequestObject;

        let jsonData = createFoundCargoRequestObject.foundCargoDataModel.toJSON();
        console.log(jsonData);
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        } else if (this.responseRecievedFromServer.status === 422) {
            this.serverErrorDataModel = new ServerErrorDataModel('Validation Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new CreateFoundCargoRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

    protected errorTextForStatusCode(_statusCode: number): string {
        if (_statusCode === 422)
            return AppUIHelper.getInstance().i18nText('Validation Error');
        else
            return super.errorTextForStatusCode(_statusCode);
    }
}
