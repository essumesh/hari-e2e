import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { CreateItinerariesRequestObject } from '../requestobjects/CreateItinerariesRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class CreateItinerariesDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.POST;
    }

    methodName(): string | undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
            : '/acceptance/v1/itineraries';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let createItinerariesRequestObject: CreateItinerariesRequestObject =
            new CreateItinerariesRequestObject();
        if (_parameters.length > 0) {
            createItinerariesRequestObject.itineraries = _parameters[0];
        }
        this.requestObject = createItinerariesRequestObject;
    }

    requestBody(): any {
        let createItinerariesRequestObject: CreateItinerariesRequestObject =
            this.requestObject as CreateItinerariesRequestObject;

        let jsonData = createItinerariesRequestObject.itineraries.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new CreateItinerariesRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
