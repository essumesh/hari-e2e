import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { SearchContainersRequestObject } from '../requestobjects/SearchContainersRequestObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class SearchLogisticsPackagesDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/logistics-packages/search';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let searchLogisticsPackagesRequestObject: SearchContainersRequestObject =
        new SearchContainersRequestObject();
        if (_parameters.length > 0) {
            searchLogisticsPackagesRequestObject.accept = _parameters[0];
            searchLogisticsPackagesRequestObject.query = _parameters[1];
            searchLogisticsPackagesRequestObject.page = _parameters[2];
            searchLogisticsPackagesRequestObject.sort = _parameters[3];
          }
        this.requestObject = searchLogisticsPackagesRequestObject;
    }

    requestParameters(): URLSearchParams {
        let searchLogisticsPackagesRequestObject: SearchContainersRequestObject =
        this.requestObject as SearchContainersRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', searchLogisticsPackagesRequestObject.accept);
        requestParameters.set('query', searchLogisticsPackagesRequestObject.query);
        requestParameters.set('page',
        searchLogisticsPackagesRequestObject.page != null
        ? searchLogisticsPackagesRequestObject.page.toString()
        : null);
        requestParameters.set('sort',
        searchLogisticsPackagesRequestObject.sort != null
        ? searchLogisticsPackagesRequestObject.sort.toString()
        : null);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new SearchContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
