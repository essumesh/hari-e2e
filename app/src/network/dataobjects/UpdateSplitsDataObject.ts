import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { ReplaceOrUpdateSplitsRequestObject } from '../requestobjects/ReplaceOrUpdateSplitsRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class UpdateSplitsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.PATCH;
    }

    methodName(): string | undefined {
        let updateSplitsRequestObject: ReplaceOrUpdateSplitsRequestObject =
            this.requestObject as ReplaceOrUpdateSplitsRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
            : '/acceptance/v1/splits/' + updateSplitsRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let updateSplitsRequestObject: ReplaceOrUpdateSplitsRequestObject =
            new ReplaceOrUpdateSplitsRequestObject();
        if (_parameters.length > 0) {
            updateSplitsRequestObject.id = _parameters[0];
            updateSplitsRequestObject.splits = _parameters[1];
        }
        this.requestObject = updateSplitsRequestObject;
    }

    requestBody(): any {
        let updateSplitsRequestObject: ReplaceOrUpdateSplitsRequestObject =
            this.requestObject as ReplaceOrUpdateSplitsRequestObject;

        let jsonData = updateSplitsRequestObject.splits.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new ReplaceOrUpdateSplitsRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
