import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { SearchContainersRequestObject } from '../requestobjects/SearchContainersRequestObject';
import { SearchContainersResponseObject } from '../responseobjects/SearchContainersResponseObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class SearchContainersDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/containers/search';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let searchContainersRequestObject: SearchContainersRequestObject =
        new SearchContainersRequestObject();
        if (_parameters.length > 0) {
            searchContainersRequestObject.accept = _parameters[0];
            searchContainersRequestObject.query = _parameters[1];
            searchContainersRequestObject.page = _parameters[2];
            searchContainersRequestObject.sort = _parameters[3];
          }
        this.requestObject = searchContainersRequestObject;
    }

    requestParameters(): URLSearchParams {
        let searchContainersRequestObject: SearchContainersRequestObject =
        this.requestObject as SearchContainersRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', searchContainersRequestObject.accept);
        requestParameters.set('query', searchContainersRequestObject.query);
        requestParameters.set('page',
        searchContainersRequestObject.page != null
        ? searchContainersRequestObject.page.toString()
        : null);
        requestParameters.set('sort',
        searchContainersRequestObject.sort != null
        ? searchContainersRequestObject.sort.toString()
        : null);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new SearchContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
