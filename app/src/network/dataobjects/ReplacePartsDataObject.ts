import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { ReplaceOrUpdatePartsRequestObject } from '../requestobjects/ReplaceOrUpdatePartsRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class ReplacePartsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.PUT;
    }

    methodName(): string | undefined {
        let replacePartsRequestObject: ReplaceOrUpdatePartsRequestObject =
            this.requestObject as ReplaceOrUpdatePartsRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
            : '/acceptance/v1/parts/' + replacePartsRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let replacePartsRequestObject: ReplaceOrUpdatePartsRequestObject =
            new ReplaceOrUpdatePartsRequestObject();
        if (_parameters.length > 0) {
            replacePartsRequestObject.id = _parameters[0];
            replacePartsRequestObject.parts = _parameters[1];
        }
        this.requestObject = replacePartsRequestObject;
    }

    requestBody(): any {
        let replacePartsRequestObject: ReplaceOrUpdatePartsRequestObject =
            this.requestObject as ReplaceOrUpdatePartsRequestObject;

        let jsonData = replacePartsRequestObject.parts.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new ReplaceOrUpdatePartsRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
