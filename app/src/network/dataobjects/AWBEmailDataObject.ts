import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';
import { URLSearchParams } from '@angular/http';
import { AWBEmailRequestObject } from '../requestobjects/AWBEmailRequestObject';

@Injectable()
export class AWBEmailDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.POST;
    }

    methodName(): string | undefined {
        let awbEmailREquestObject: AWBEmailRequestObject =
            this.requestObject as AWBEmailRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/pickupdelivery.json'
            : ('/discrepancy/v1/damage-reports/'
                + awbEmailREquestObject.awbReportID
            + '/email-requests');
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let awbEmailREquestObject: AWBEmailRequestObject =
            new AWBEmailRequestObject();
        if (_parameters.length > 0) {
            awbEmailREquestObject.awbReportID = _parameters[0];
            awbEmailREquestObject.awbEmailDataModel = _parameters[1];
            // console.log(pickupDeliveryReceiptRequestObject.pickupSignatureDataModel);
        }
        this.requestObject = awbEmailREquestObject;
    }

    requestBody(): any {
        let awbEmailREquestObject: AWBEmailRequestObject =
            this.requestObject as AWBEmailRequestObject;

        let jsonData = awbEmailREquestObject.awbEmailDataModel.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new AWBEmailRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
