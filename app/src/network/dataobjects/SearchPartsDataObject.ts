import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { SearchContainersRequestObject } from '../requestobjects/SearchContainersRequestObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class SearchPartsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/parts/search';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let searchPartsRequestObject: SearchContainersRequestObject =
        new SearchContainersRequestObject();
        if (_parameters.length > 0) {
            searchPartsRequestObject.accept = _parameters[0];
            searchPartsRequestObject.query = _parameters[1];
            searchPartsRequestObject.page = _parameters[2];
            searchPartsRequestObject.sort = _parameters[3];
          }
        this.requestObject = searchPartsRequestObject;
    }

    requestParameters(): URLSearchParams {
        let searchPartsRequestObject: SearchContainersRequestObject =
        this.requestObject as SearchContainersRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', searchPartsRequestObject.accept);
        requestParameters.set('query', searchPartsRequestObject.query);
        requestParameters.set('page',
        searchPartsRequestObject.page != null
        ? searchPartsRequestObject.page.toString()
        : null);
        requestParameters.set('sort',
        searchPartsRequestObject.sort != null
        ? searchPartsRequestObject.sort.toString()
        : null);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new SearchContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
