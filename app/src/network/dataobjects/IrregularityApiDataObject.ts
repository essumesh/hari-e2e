import { BaseDataObject } from './BaseDataObject';
import { Injectable } from '@angular/core';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { IrregularityApiRequestObject } from '../requestobjects/IrregularityApiRequestObject';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

@Injectable()
export class IrregularityApiDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.POST;
    }

    methodName(): string | undefined {
        return APIConstants.TO_USE_MOCKS ? '/IrregularityQualifier.json'
            : '/discrepancy/v1/irregularities';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let irregularityApiRequestObject: IrregularityApiRequestObject =
            new IrregularityApiRequestObject();
        if (_parameters.length > 0) {
            irregularityApiRequestObject.irregularityApiDataModel = _parameters[0];
        }
        this.requestObject = irregularityApiRequestObject;
    }

    requestBody(): any {
        let irregularityApiRequestObject: IrregularityApiRequestObject =
            this.requestObject as IrregularityApiRequestObject;

        let jsonData = irregularityApiRequestObject.irregularityApiDataModel.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new IrregularityApiRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }
}
