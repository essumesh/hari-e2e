import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { APIConstants } from '../constants/APIConstants';
import { SearchContainersRequestObject } from '../requestobjects/SearchContainersRequestObject';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class SearchLocationsDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.GET;
    }

    methodName(): string|undefined {
        return APIConstants.TO_USE_MOCKS ? '/'
                                         : '/acceptance/v1/locations/search';
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let searchLocationsRequestObject: SearchContainersRequestObject =
        new SearchContainersRequestObject();
        if (_parameters.length > 0) {
            searchLocationsRequestObject.accept = _parameters[0];
            searchLocationsRequestObject.query = _parameters[1];
            searchLocationsRequestObject.page = _parameters[2];
            searchLocationsRequestObject.sort = _parameters[3];
          }
        this.requestObject = searchLocationsRequestObject;
    }

    requestParameters(): URLSearchParams {
        let searchLocationsRequestObject: SearchContainersRequestObject =
        this.requestObject as SearchContainersRequestObject;

        let requestParameters: URLSearchParams = super.requestParameters();

        requestParameters.set('accept', searchLocationsRequestObject.accept);
        requestParameters.set('query', searchLocationsRequestObject.query);
        requestParameters.set('page',
        searchLocationsRequestObject.page != null
        ? searchLocationsRequestObject.page.toString()
        : null);
        requestParameters.set('sort',
        searchLocationsRequestObject.sort != null
        ? searchLocationsRequestObject.sort.toString()
        : null);

        return requestParameters;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
        } else if (this.responseRecievedFromServer.status === 404) {
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
      }

      fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new SearchContainersRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
      }
}
