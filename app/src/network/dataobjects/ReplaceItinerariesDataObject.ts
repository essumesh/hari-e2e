import { Injectable } from '@angular/core';
import { BaseDataObject } from './BaseDataObject';
import { NetworkMethodType } from '../constants/NetworkMethodType';
import { ReplaceOrUpdateItinerariesRequestObject } from '../requestobjects/ReplaceOrUpdateItinerariesRequestObject';
import { APIConstants, APIServerType } from '../constants/APIConstants';
import { ServerErrorDataModel } from '../../datamodels/ServerErrorDataModel';

export class ReplaceItinerariesDataObject extends BaseDataObject {
    methodType(): NetworkMethodType {
        return NetworkMethodType.PUT;
    }

    methodName(): string | undefined {
        let replaceItinerariesRequestObject: ReplaceOrUpdateItinerariesRequestObject =
            this.requestObject as ReplaceOrUpdateItinerariesRequestObject;
        return APIConstants.TO_USE_MOCKS ? '/'
            : '/acceptance/v1/itineraries/' + replaceItinerariesRequestObject.id;
    }

    prepareRequestWithParameters(..._parameters: any[]) {
        let replaceItinerariesRequestObject: ReplaceOrUpdateItinerariesRequestObject =
            new ReplaceOrUpdateItinerariesRequestObject();
        if (_parameters.length > 0) {
            replaceItinerariesRequestObject.id = _parameters[0];
            replaceItinerariesRequestObject.itineraries = _parameters[1];
        }
        this.requestObject = replaceItinerariesRequestObject;
    }

    requestBody(): any {
        let replaceItinerariesRequestObject: ReplaceOrUpdateItinerariesRequestObject =
            this.requestObject as ReplaceOrUpdateItinerariesRequestObject;

        let jsonData = replaceItinerariesRequestObject.itineraries.toJSON();
        return jsonData;
    }

    parse() {
        if (this.responseRecievedFromServer.status === 200 ||
            this.responseRecievedFromServer.status === 201 ) {
            this.serverErrorDataModel = new ServerErrorDataModel('success');
        } else if (this.responseRecievedFromServer.status === 404) {
            this.serverErrorDataModel = new ServerErrorDataModel('Server Error');
        }
    }

    toJSON(): any {
        let jsonData = super.toJSON();
        jsonData['requestObject'] = JSON.stringify(this.requestObject);
        return jsonData;
    }

    fromJSON(json: any) {
        super.fromJSON(json);

        this.requestObject = new ReplaceOrUpdateItinerariesRequestObject();
        this.requestObject.fromJSON(JSON.parse(json.requestObject));
    }

}
