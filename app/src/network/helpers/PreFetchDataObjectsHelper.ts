import { BaseDataObject } from '../dataobjects/BaseDataObject';
import { FetchAWBDamageQualifiersDataObject } from '../dataobjects/FetchAWBDamageQualifiersDataObject';
import { ManualDIHelper } from '../../helpers/ManualDIHelper';
import { FetchAWBDamageQualifiersResponseObject } from
'../responseobjects/FetchAWBDamageQualifiersResponseObject';
import { FoundCargoSHCDataObject } from '../dataobjects/FoundCargoSHCDataObject';
import { FoundCargoOrignDataObject } from '../dataobjects/FoundCargoOrignDataObject';

export class PreFetchDataObjectsHelper {
    private static instance: PreFetchDataObjectsHelper;
    fetchAWBDamageQualifiersDataObject: FetchAWBDamageQualifiersDataObject;
    fetchFoundCargoSHCDataObject: FoundCargoSHCDataObject;
    fetchFoundCargoOrignDataObject: FoundCargoOrignDataObject;

    static createInstance() {
        PreFetchDataObjectsHelper.instance = new PreFetchDataObjectsHelper();
    }

    static getInstance() {
        return this.instance;
    }

    startPreFetchingDataObjects() {
        this.preFetch_FetchAWBDamageQualifiersDataObject((_dataObject: BaseDataObject) => {
        });

        this.preFetch_FetchSHCDataObject((_dataObject: BaseDataObject) => {
        });

        this.preFetch_FetchOriginDataObject((_dataObject: BaseDataObject) => {
        });
    }

    preFetch_FetchAWBDamageQualifiersDataObject(_callBack: (dataObject: BaseDataObject) => void) {
        if (this.fetchAWBDamageQualifiersDataObject != null) {
            this.fetchAWBDamageQualifiersDataObject.cancelRequest();
            this.fetchAWBDamageQualifiersDataObject = null;
        }

        this.fetchAWBDamageQualifiersDataObject =
        ManualDIHelper.getInstance().createObject('FetchAWBDamageQualifiersDataObject');
        this.fetchAWBDamageQualifiersDataObject.prepareRequestWithParameters();
        this.fetchAWBDamageQualifiersDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
            _callBack(_dataObject);
        });
    }

    doWeHave_Valid_Data_For_FetchAWBDamageQualifiersDataObject() {
        return this.fetchAWBDamageQualifiersDataObject != null
               && this.fetchAWBDamageQualifiersDataObject.hasNetworkProcessFinished
               && this.fetchAWBDamageQualifiersDataObject.serverErrorDataModel == null;
    }

    preFetch_FetchSHCDataObject(_callBack: (dataObject: BaseDataObject) => void) {
        if (this.fetchFoundCargoSHCDataObject != null) {
            this.fetchFoundCargoSHCDataObject.cancelRequest();
            this.fetchFoundCargoSHCDataObject = null;
        }

        this.fetchFoundCargoSHCDataObject =
        ManualDIHelper.getInstance().createObject('FoundCargoSHCDataObject');
        this.fetchFoundCargoSHCDataObject.prepareRequestWithParameters();
        this.fetchFoundCargoSHCDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
            _callBack(_dataObject);
        });
    }

    doWeHave_Valid_Data_For_FetchSHCDataObject() {
        return this.fetchFoundCargoSHCDataObject != null
               && this.fetchFoundCargoSHCDataObject.hasNetworkProcessFinished
               && this.fetchFoundCargoSHCDataObject.serverErrorDataModel == null;
    }

    preFetch_FetchOriginDataObject(_callBack: (dataObject: BaseDataObject) => void) {
        if (this.fetchFoundCargoOrignDataObject != null) {
            this.fetchFoundCargoOrignDataObject.cancelRequest();
            this.fetchFoundCargoOrignDataObject = null;
        }

        this.fetchFoundCargoOrignDataObject =
        ManualDIHelper.getInstance().createObject('FoundCargoOrignDataObject');
        this.fetchFoundCargoOrignDataObject.prepareRequestWithParameters();
        this.fetchFoundCargoOrignDataObject.queryTheServer(false, (_dataObject: BaseDataObject) => {
            _callBack(_dataObject);
        });
    }

    doWeHave_Valid_Data_For_FetchOriginDataObject() {
        return this.fetchFoundCargoOrignDataObject != null
               && this.fetchFoundCargoOrignDataObject.hasNetworkProcessFinished
               && this.fetchFoundCargoOrignDataObject.serverErrorDataModel == null;
    }

    private constructor() {
    }
}
