#!/bin/bash
cd ./platforms/android/build/outputs/apk
HOCKEY_NOTES="$(git log -1 --pretty=%B)"
curl -F "keep=10" -H "X-HockeyAppToken: $HOCKEY_APP_TOKEN" https://rink.hockeyapp.net/api/2/apps/$HOCKEY_APP_ID/app_versions/delete
curl -F "status=2" -F "notify=1" -F "notes=Pipeline $CI_PIPELINE_ID, build $CI_BUILD_ID, msg $HOCKEY_NOTES" -F "notes_type=0" -F "ipa=@champcargosystems.apk" -H "X-HockeyAppToken: $HOCKEY_APP_TOKEN" https://rink.hockeyapp.net/api/2/apps/$HOCKEY_APP_ID/app_versions/upload
cd ../../../../..
