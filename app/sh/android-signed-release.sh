#!/bin/bash

#to generate a signed apk file

ionic cordova build android --prod --release
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore src/assets/androidreleaseassets/champcargosystems-release-key.keystore -storepass $ANDROID_KEY_SIGN_PASS_PHRASE platforms/android/build/outputs/apk/android-release-unsigned.apk champcargosystems
$ANDROID_HOME/build-tools/$ANDROID_BUILD_TOOLS_VERSION/zipalign -v 4 platforms/android/build/outputs/apk/android-release-unsigned.apk platforms/android/build/outputs/apk/champcargosystems.apk
