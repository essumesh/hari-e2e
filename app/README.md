# CHAMP Cargo Mobile App

Mobile app development project for Cargo Terminal users.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Install Git, Node.js 6.x, NPM, Ionic

```
install instructions goes here
```

### Installing

Clone the repo and build

```
Installation commands 
```


## Running the tests

Command to run unit and end to end test goes here

### Break down into end to end tests

Unit testing

```
Commands to trigger unit test
```

E2E testing

```
Commands to trigger E2E test
```

### And coding style tests

Coding style verification

```
lint commands
```

## Deployment

Deployment steps

## Built With

* [Ionic 3](https://ionicframework.com/) 
